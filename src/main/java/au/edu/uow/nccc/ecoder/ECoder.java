
package au.edu.uow.nccc.ecoder;

import java.io.File;

/**
 * <code>ECoder</code> is the place-holder for application level definitions and functionality.
 * 
 * @author jturnbul
 */
public class ECoder {

	/**
	 * The actual name of the application.
	 */
	private static final String APP_NAME = "Codexpert"; //$NON-NLS-1$

	/**
	 * The product version number, such as 7 for 7th Edition.
	 * Updated to 8 for 8th Edition
	 */
	private static final int VERSION_NUMBER = 8;

	/**
	 * The user directory.
	 */
	private static final String USER_DIR = System.getProperty("user.home") + System.getProperty("file.separator") //$NON-NLS-1$ //$NON-NLS-2$
	    + getApplicationName();

	/**
	 * Creates the E-Coder user directory if not already present.
	 * 
	 * @return True if the user directory already exists or was successfully created.
	 */
	public static boolean createUserDirIfRequired() {
		final File userDir = new File(USER_DIR);

		return userDir.exists() || userDir.mkdir();
	}

	/**
	 * Returns the application name.
	 * 
	 * @return The application name.
	 */
	public static String getApplicationName() {
		return APP_NAME;
	}

	/**
	 * Returns the user directory.
	 * 
	 * @return The user directory.
	 */
	public static String getUserDirectory() {
		return USER_DIR;
	}

	/**
	 * Returns the user ID of the current user.
	 * 
	 * @return The current user's ID.
	 */
	public static String getUserID() {
		return System.getProperty("user.name"); //$NON-NLS-1$
	}

	/**
	 * Returns the product version number.
	 * 
	 * @return The product version number.
	 */
	public static int getVersionNumber() {
		return VERSION_NUMBER;
	}
}