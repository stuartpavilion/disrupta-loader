
package au.edu.uow.nccc.ecoder;

import au.edu.uow.nccc.ecoder.model.util.DataFiles;
import au.edu.uow.nccc.ecoder.utility.FileLoader;
import au.edu.uow.nccc.ecoder.utility.HTMLProcessor;

/**
 * <code>Resources</code> is responsible for managing CodeXpert resources and configuration information.
 * 
 * @author jturnbul
 */
public class Resources {

	/**
	 * The system dependent file separator.
	 */
	public static final String SEPARATOR_FILE = System.getProperty("file.separator"); //$NON-NLS-1$

	/**
	 * The application's working directory.
	 */
	//public static final String DIRECTORY_WORKING = System.getProperty("user.dir"); //$NON-NLS-1$
	public static final String DIRECTORY_WORKING = System.getProperty("user.home"); //$NON-NLS-1$

	/**
	 * The directory containing static HTML files, scripts and images.
	 */
	public static final String DIRECTORY_HTML = ".." + SEPARATOR_FILE + "html"; //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The directory containing HTML files for the resources section.
	 */
	public static final String DIRECTORY_RESOURCES = DIRECTORY_HTML + SEPARATOR_FILE + "resources"; //$NON-NLS-1$

	/**
	 * The directory containing the data files.
	 */
	public static final String DIRECTORY_DATA = Resources.DIRECTORY_WORKING + Resources.SEPARATOR_FILE + ".." //$NON-NLS-1$
	    + SEPARATOR_FILE + "data"; //$NON-NLS-1$

	/**
	 * The configuration directory.
	 */
	private static final String DIRECTORY_CONFIG = DIRECTORY_WORKING + SEPARATOR_FILE + "configuration"; //$NON-NLS-1$

	/**
	 * The name of the file containing the license text.
	 */
	private static final String FILE_NAME_HTML_HEADER = "/au/edu/uow/nccc/ecoder/header.html"; //$NON-NLS-1$

	/**
	 * The name of the file containing header without CSS style
	 */
	private static final String FILE_NAME_HTML_HEADER_NOSTYLE = "/au/edu/uow/nccc/ecoder/headerNoStyle.html"; //$NON-NLS-1$

	/**
	 * The name of the file containing the license text.
	 */
	private static final String FILE_NAME_LICENSE_TEXT = "/au/edu/uow/nccc/ecoder/license/license.txt"; //$NON-NLS-1$

	/**
	 * The standard header for HTML content.
	 */
	private static String htmlHeader;

	/**
	 * The license text as loaded from the license file.
	 */
	private static String licenseText;

	/**
	 * Returns the configuration directory.
	 * 
	 * @return The configuration directory.
	 */
	public static String getConfigDirectory() {
		return DIRECTORY_CONFIG;
	}

	/**
	 * Returns the standard HTML footer.
	 * 
	 * @return The HTML footer.
	 */
	public static String getHTMLFooter() {
		return "</body></html>"; //$NON-NLS-1$
	}

	/**
	 * Returns the standard HTML header, loading it from a file if necessary.
	 * 
	 * @return The standard HTML header.
	 */
	public static String getHTMLHeader() {

		// Load the HTML header from a file if it is not already cached.
		if (htmlHeader == null) {
			htmlHeader = FileLoader.loadFile(Resources.class.getResourceAsStream(FILE_NAME_HTML_HEADER));
		}

		return htmlHeader;
	}

	/**
	 * Returns the standard HTML header, loading it from a file if necessary.
	 * 
	 * @return The standard HTML header.
	 */
	public static String getHTMLHeaderNoStyle() {

		// Load the HTML header from a file if it is not already cached.
		if (htmlHeader == null) {
			htmlHeader = FileLoader.loadFile(Resources.class.getResourceAsStream(FILE_NAME_HTML_HEADER_NOSTYLE));
		}

		return htmlHeader;
	}

	/**
	 * Returns the license text.
	 * 
	 * @return The license text.
	 */
	public static String getLicenseText() {

		// Load the license text from the license file if it is not already cached.
		if (licenseText == null) {
			licenseText = FileLoader.loadFile(Resources.class.getResourceAsStream(FILE_NAME_LICENSE_TEXT));
		}

		return licenseText;
	}

	/**
	 * Returns a path to the data file with the specified name.
	 * 
	 * @param fileName The data file name.
	 * @return The full path to the specified file.
	 */
	public static String getPathToDataFile(final String fileName) {
		return DIRECTORY_DATA + SEPARATOR_FILE + fileName + "." + DataFiles.EXTENSION; //$NON-NLS-1$
	}

	/**
	 * Returns the working directory.
	 * 
	 * @return The working directory.
	 */
	public static String getWorkingDirectory() {
		return DIRECTORY_WORKING;
	}

	/**
	 * Localises the content for an HTML file by inserting a &lt;base&gt; tag which specifies the base location of
	 * content determined at run time.
	 * 
	 * @param html The HTML to localise.
	 * @return The HTML content localised.
	 */
	public static String localiseHTML(final String html) {
		return HTMLProcessor.addBaseTag(html, DIRECTORY_WORKING + SEPARATOR_FILE + DIRECTORY_HTML + SEPARATOR_FILE);
	}
}