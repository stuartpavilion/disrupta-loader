
package au.edu.uow.nccc.ecoder.italiciser;

import java.util.HashSet;
import java.util.Set;

/**
 * An <code>HTMLItaliciser</code> is the class which handles the responsibility of italicising blocks of HTML based on a
 * set of Latin words and phrases. The phrases are for situations where individual words should only be italicised when
 * they occur along with other words such as "E. coli" where it would make no sense to italicise "E." on its own.
 * 
 * @author jturnbul
 */
public enum HTMLItaliciser {

	/**
	 * The singleton instance.
	 */
	INSTANCE;

	/**
	 * The set of Latin words.
	 */
	private Set<String> latinWords;

	/**
	 * The set of Latin phrases.
	 */
	private Set<String> latinPhrases;

	/**
	 * Returns true if the provided text is a Latin word.
	 * 
	 * @param text The text of the term to be tested.
	 * @return True if the provided text is a Latin word.
	 */
	private boolean isLatinWord(final String text) {
		return latinWords.contains(text);
	}

	/**
	 * Returns the original block of HTML with all occurrences of Latin words and phrases italicised.
	 * 
	 * @param html The block of HTML to italicise.
	 * @return The original block of HTML with all occurrences of Latin words and phrases italicised.
	 */
	public String italicise(final String html) {
		String result = html;
		result = italicisePhrases(result);
		result = italiciseWords(result);

		return result;
	}

	/**
	 * Returns the original block of HTML with all occurrences of Latin phrases italicised.
	 * 
	 * @param html The block of HTML to italicise.
	 * @return The original block of HTML with all occurrences of Latin phrases italicised.
	 */
	private String italicisePhrases(final String html) {
		String result = html;
		for (final String phrase : latinPhrases) {
			result = italiciseWordOrPhrase(result, phrase);
		}

		return result;
	}

	/**
	 * Italicises all occurrences of a word or phrase in a block of text.
	 * 
	 * @param html The block of HTML which may contain the word to be italicised.
	 * @param wordOrPhrase The word to italicise.
	 * @return The original block of HTML with all occurrences of the specified word italicised.
	 */
	private String italiciseWordOrPhrase(final String html, final String wordOrPhrase) {
		final StringBuilder sb = new StringBuilder();
		int index = 0;
		int lastindex = 0;
		do {
			// Find the word in the block of text.
			index = html.indexOf(wordOrPhrase, index);
			if (index > -1) {

				// Make sure we ignore words which are part of other words.
				if ((index == 0 || !Character.isLetter(html.charAt(index - 1)))
				    && (index + wordOrPhrase.length() == html.length() || !Character.isLetter(html.charAt(index
				        + wordOrPhrase.length())))) {
					sb.append(html.substring(lastindex, index));

					// Wrap the word in italics.
					sb.append("<i>"); //$NON-NLS-1$
					sb.append(html.substring(index, index + wordOrPhrase.length()));
					sb.append("</i>"); //$NON-NLS-1$
				} else {
					sb.append(html.substring(lastindex, index + wordOrPhrase.length()));
				}
				index = lastindex = index + wordOrPhrase.length();
			}
		} while (index > -1);

		sb.append(html.substring(lastindex, html.length()));

		return sb.toString();
	}

	/**
	 * Returns the original block of HTML with all occurrences of Latin words italicised.
	 * 
	 * @param html The block of HTML to italicise.
	 * @return The original block of HTML with all occurrences of Latin words italicised.
	 */
	private String italiciseWords(final String html) {

		// Split the text on non-alphabetic boundaries.
		final String[] words = html.split("[^\\p{Alpha}]"); //$NON-NLS-1$

		String result = html;

		// Add each word to a set to remove duplicates.
		final Set<String> wordSet = new HashSet<>();
		for (final String word : words) {
			wordSet.add(word);
		}

		// Loop through all unique words.
		for (final String word : wordSet) {

			// Italicise each word if appropriate.
			if (isLatinWord(word)) {
				result = italiciseWordOrPhrase(result, word);
			}
		}

		return result;
	}

	/**
	 * Set the model of Latin phrases to be used for italicisation.
	 * 
	 * @param model The set of Latin phrases to use.
	 */
	public void setPhrases(final Set<String> model) {
		latinPhrases = model;
	}

	/**
	 * Set the model of Latin words to be used for italicisation.
	 * 
	 * @param model The set of Latin words to use.
	 */
	public void setWords(final Set<String> model) {
		latinWords = model;
	}
}