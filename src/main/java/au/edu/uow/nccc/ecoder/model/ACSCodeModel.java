
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.bean.ACSCode;
import au.edu.uow.nccc.ecoder.model.util.DataFiles;

/**
 * An <code>ACSCodeModel</code> is the model which manages the application's ACS codes.
 * 
 * @author jturnbul
 */
public class ACSCodeModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(ACSCodeModel.class);

	/**
	 * A map of codes by ID.
	 */
	private final Map<Integer, ACSCode> codesByID = new HashMap<>();

	/**
	 * A map of codes by code text.
	 */
	private final Map<String, ACSCode> codesByText = new HashMap<>();

	/**
	 * The list of ACS codes.
	 */
	private final List<ACSCode> codeList = new ArrayList<>();

	/**
	 * The root code in the ACS code hierarchy.
	 */
	private final ACSCode root;

	/**
	 * Creates a new <code>ACSCodeModel</code> with a default root.
	 */
	ACSCodeModel() {
		root = new ACSCode(0);
		root.setText("ACS"); //$NON-NLS-1$
		root.setDescription("ACS"); //$NON-NLS-1$
	}

	/**
	 * Returns the ACS code with the given ID.
	 * 
	 * @param id The code's ID.
	 * @return The corresponding ACS code.
	 */
	ACSCode getCodeForID(final int id) {
		return codesByID.get(id);
	}

	/**
	 * Returns the ACS code with the given code text.
	 * 
	 * @param text The code's text.
	 * @return The corresponding ACS code.
	 */
	ACSCode getCodeForText(final String text) {
		return codesByText.get(text);
	}

	/**
	 * Returns the list of ACS codes.
	 * 
	 * @return The list of ACS codes.
	 */
	List<ACSCode> getCodes() {

		// Return a defensive copy.
		return new ArrayList<>(codeList);
	}

	/**
	 * Returns the root ACS code in the hierarchy.
	 * 
	 * @return The root ACS code.
	 */
	ACSCode getRoot() {
		return root;
	}

	/**
	 * Loads all ACS codes from the data files into memory.
	 */
	void load() {

		logger.info("Loading ACS codes..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_ACS));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String s = new String(input, "utf8"); //$NON-NLS-1$
				if (s.length() < 2) {
					break;
				}

				// Split the string into tokens which contain each field.
				final String[] tokens = s.split("\\" + DataFiles.SEPARATOR_FIELD, 7); //$NON-NLS-1$

				// Extract the fields.
				final Integer id = ModelUtils.tokenAsInteger(tokens, 0);
				final Integer parentId = ModelUtils.tokenAsInteger(tokens, 1);
				final String text = ModelUtils.tokenAsString(tokens, 2);
				final String description = ModelUtils.tokenAsString(tokens, 3);
				final String notes = ModelUtils.tokenAsString(tokens, 4);
				final String head = ModelUtils.tokenAsString(tokens, 5);

				// Create and initialise a new ACS code.
				final ACSCode code = new ACSCode(id);
				final ACSCode parent = parentId == null ? root : codesByID.get(parentId);
				code.setParent(parent);
				parent.addChild(code);
				code.setText(text);
				code.setDescription(description);
				code.setNotes(notes);
				code.setHead(head);

				// Add the code to the memory structures.
				codesByID.put(id, code);
				codesByText.put(text, code);
				codeList.add(code);

				if (++i % 10000 == 0) {
					logger.info("" + i + " entries processed."); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}

			// Clean-up.
			dis.close();

			// All codes loaded successfully.
			logger.info("" + i + " ACS codes successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}

		// Ensure the code list is in ID order.
		Collections.sort(codeList, new Comparator<ACSCode>() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
			 */
			@Override
			public int compare(final ACSCode a, final ACSCode b) {
				return a.getID().compareTo(b.getID());
			}
		});

		// Prune the list so that only the leaf nodes remain.
		final Iterator<ACSCode> it = codeList.iterator();
		while (it.hasNext()) {
			final ACSCode code = it.next();
			if (!code.getChildren().isEmpty()) {
				it.remove();
			}
		}
	}
}