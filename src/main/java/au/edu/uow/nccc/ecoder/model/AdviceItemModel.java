
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.bean.AdviceItem;
import au.edu.uow.nccc.ecoder.model.bean.YearMonth;
import au.edu.uow.nccc.ecoder.model.util.DataFiles;

/**
 * An <code>AdviceItemModel</code> is the model which manages the application's advice items.
 * 
 * @author jturnbul
 */
public class AdviceItemModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(AdviceItemModel.class);

	/**
	 * A map of advice items by ID.
	 */
	private final Map<Integer, AdviceItem> itemsByID = new HashMap<>();

	/**
	 * A map of lists of advice items by year/month.
	 */
	private final Map<YearMonth, List<AdviceItem>> itemsByYearMonth = new HashMap<>();

	/**
	 * Returns the advice item with the given ID.
	 * 
	 * @param id The item's ID.
	 * @return The corresponding advice item.
	 */
	AdviceItem getItemForID(final int id) {
		return itemsByID.get(id);
	}

	/**
	 * Returns a list of advice items that occur within the given year and month.
	 * 
	 * @param year The year of publication.
	 * @param month The month of publication.
	 * @return A list of items published in the given year and month.
	 */
	List<AdviceItem> getItemsForYearMonth(final int year, final int month) {

		// Return a sorted defensive copy.
		final List<AdviceItem> entries = new ArrayList<>(itemsByYearMonth.get(new YearMonth(year, month)));
		Collections.sort(entries);

		return entries;
	}

	/**
	 * Returns a list of all year/month combinations in which advice was published.
	 * 
	 * @return The list of all year/month combinations.
	 */
	List<YearMonth> getYearMonths() {

		// Return a sorted defensive copy.
		final List<YearMonth> list = new ArrayList<>(itemsByYearMonth.keySet());
		Collections.sort(list);

		return list;
	}

	/**
	 * Loads all advice items into memory.
	 */
	void load() {

		logger.info("Loading advice items..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_ADVICE));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String s = new String(input, "utf8"); //$NON-NLS-1$
				if (s.length() < 2) {
					break;
				}

				// Split the string into tokens which contain each field.
				final String[] tokens = s.split("\\" + DataFiles.SEPARATOR_FIELD, 7); //$NON-NLS-1$

				// Extract the fields.
				final Integer id = ModelUtils.tokenAsInteger(tokens, 0);
				final String type = ModelUtils.tokenAsString(tokens, 1);
				final String subject = ModelUtils.tokenAsString(tokens, 2);
				final String message = ModelUtils.tokenAsString(tokens, 3);
				final Date published = ModelUtils.tokenAsDate(tokens, 4);
				final Date expired = ModelUtils.tokenAsDate(tokens, 5);

				// Create and initialise a new advice item.
				final AdviceItem item = new AdviceItem(id);
				item.setType(AdviceItem.Type.valueOf(type));
				item.setSubject(subject);
				item.setMessage(message);
				item.setPublished(published);
				item.setExpired(expired);

				// Add the item to the memory structures.
				itemsByID.put(id, item);
				final YearMonth ym = new YearMonth(published);
				List<AdviceItem> list = itemsByYearMonth.get(ym);
				if (list == null) {
					list = new ArrayList<>();
					itemsByYearMonth.put(ym, list);
				}
				list.add(item);

				if (++i % 1000 == 0) {
					logger.info("" + i + " items processed."); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}

			// Clean-up.
			dis.close();

			// All codes loaded successfully.
			logger.info("" + i + " advice items successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}
	}
}