
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.ECoder;
import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.bean.CodingCase;
import au.edu.uow.nccc.ecoder.model.bean.SelectedCode;
import au.edu.uow.nccc.ecoder.model.util.DiseasesMasterCodeUtils;
import au.edu.uow.nccc.ecoder.model.util.InterventionsMasterCodeUtils;
import au.edu.uow.nccc.ecoder.model.util.MorphologyCodeUtils;

/**
 * A <code>CaseModel</code> is the model which manages persistence and retrieval of coding cases/episodes.
 * 
 * @author jturnbul
 */
public class CaseModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(CaseModel.class);

	/**
	 * The directory where coding cases are stored.
	 */
	private static final String DIRECTORY_CASES = ECoder.getUserDirectory() + Resources.SEPARATOR_FILE + "cases"; //$NON-NLS-1$

	/**
	 * Writes a case and its codes to a file.
	 * 
	 * @param id The ID of the case.
	 * @param codes The list of codes.
	 * @return A message if there is an error or null if the add is successful.
	 */
	String addCase(final String id, final List<SelectedCode> codes) {

		// Check to see if the file already exists.
		final File file = new File(DIRECTORY_CASES + Resources.SEPARATOR_FILE + sanitiseID(id));
		if (file.exists()) {
			return "A case using that ID already exists.  Please choose a different case ID."; //$NON-NLS-1$
		}

		try {

			// Create the file.
			file.createNewFile();

			// Write the ID and codes to the file.
			final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(id);
			writer.newLine();
			for (final SelectedCode code : codes) {
				writer.write(code.getCodeText());
				writer.newLine();
				writer.write(code.getNotes() == null ? "" : code.getNotes()); //$NON-NLS-1$
				writer.newLine();
			}
			writer.flush();
			writer.close();
		} catch (final IOException ioe) {
			logger.error("There was an error saving the case", ioe); //$NON-NLS-1$

			return "There was an error saving the case: " + ioe.getMessage(); //$NON-NLS-1$
		}

		// Return null to indicate success.
		return null;
	}

	/**
	 * Returns true if there are cases yet to be exported.
	 * 
	 * @return True if there are cases database yet to be exported.
	 */
	boolean canExport() {

		// Check to see if any coding case files exist.
		final File dir = new File(DIRECTORY_CASES);
		final String[] files = dir.list();

		return files != null && files.length > 0;
	}

	/**
	 * Returns true if a case with the given ID already exists.
	 * 
	 * @param id The ID of the case.
	 * @return True if the case exists.
	 */
	boolean caseExists(final String id) {
		final File file = new File(DIRECTORY_CASES + Resources.SEPARATOR_FILE + sanitiseID(id));

		return file.length() > 0;
	}

	/**
	 * Deletes all existing cases. This is used after an export.
	 */
	void deleteAllCases() {
		final File dir = new File(DIRECTORY_CASES);
		for (final String fileName : dir.list()) {
			final File file = new File(DIRECTORY_CASES + Resources.SEPARATOR_FILE + fileName);
			file.delete();
		}
	}

	/**
	 * Returns a list of all saved codes. This is used for exporting.
	 * 
	 * @return
	 */
	List<CodingCase> getSavedCases() {

		final List<CodingCase> result = new ArrayList<>();

		// List all file names of existing cases.
		final File dir = new File(DIRECTORY_CASES);
		final String[] files = dir.list();

		try {
			// Loop through all cases.
			for (final String fileName : files) {
				final File file = new File(DIRECTORY_CASES + Resources.SEPARATOR_FILE + fileName);
				final BufferedReader reader = new BufferedReader(new FileReader(file));

				// Extract the ID and create a new case.
				final String id = reader.readLine();
				final CodingCase cCase = new CodingCase(id);

				// Loop through all codes.
				while (true) {

					// Extract the code's text.
					String codeText = reader.readLine();
					if (codeText == null) {
						break;
					}

					// Handle the block number in interventions codes.
					final int bracketPos = codeText.indexOf('[');
					if (bracketPos > -1) {
						codeText = codeText.substring(0, bracketPos - 1);
					}

					// Extract the attached notes.
					final String notes = reader.readLine();

					// Determine what kind of code it is, create a selected code wrapper and add it
					// to the case.,
					if (DiseasesMasterCodeUtils.isCode(codeText)) {
						final SelectedCode code = new SelectedCode(Model.INSTANCE.getDiseasesCodeForText(codeText));
						code.setNotes(notes);
						cCase.addCode(code);
					} else if (InterventionsMasterCodeUtils.isCode(codeText)) {
						final SelectedCode code =
						    new SelectedCode(Model.INSTANCE.getInterventionsCodeForText(codeText));
						code.setNotes(notes);
						cCase.addCode(code);
					} else if (MorphologyCodeUtils.isCode(codeText)) {
						final SelectedCode code = new SelectedCode(Model.INSTANCE.getMorphologyCodeForText(codeText));
						code.setNotes(notes);
						cCase.addCode(code);
					}
				}

				// Add the case to the result list.
				result.add(cCase);

				// Clean up.
				reader.close();
			}
		} catch (final IOException ioe) {
			logger.error("There was an error loading saved cases", ioe); //$NON-NLS-1$

			return Collections.emptyList();
		}

		return result;
	}

	/**
	 * Initialises the model by ensuring the case directory exists.
	 */
	public void init() {
		final File dir = new File(DIRECTORY_CASES);
		if (!dir.exists()) {
			dir.mkdir();
		}
	}

	/**
	 * Sanitises a coding case ID to remove any characters not suitable for being used in a file name.
	 * 
	 * @param id The coding case ID.
	 * @return A sanitised version of the ID.
	 */
	private String sanitiseID(final String id) {
		final StringBuilder sb = new StringBuilder();
		for (int index = 0; index < id.length(); index++) {
			final char c = id.charAt(index);
			if (Character.isLetterOrDigit(c)) {
				sb.append(c);
			}
		}

		return sb.toString();
	}
}