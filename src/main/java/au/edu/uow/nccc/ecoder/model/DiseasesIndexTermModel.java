
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesIndexTerm;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesIndexTerm.Category;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesIndexTermNote;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesMasterCode;
import au.edu.uow.nccc.ecoder.model.util.DataFiles;

/**
 * A <code>DiseasesIndexTermModel</code> is the model which manages diseases index terms for the application.
 * 
 * @author jturnbul
 */
public class DiseasesIndexTermModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(DiseasesIndexTermModel.class);

	/**
	 * A map of terms by ID.
	 */
	private final Map<Integer, DiseasesIndexTerm> termsByID = new HashMap<>();

	/**
	 * A map of root index terms by category.
	 */
	private final Map<Category, DiseasesIndexTerm> roots = new HashMap<>();

	/**
	 * A map of sets of diseases master codes by index term which specifies the codes which are linked to a particular
	 * term.
	 */
	private final Map<DiseasesIndexTerm, Set<DiseasesMasterCode>> linkedCodesByTerm = new HashMap<>();

	/**
	 * Creates a new <code>DiseasesIndexTermModel</code> with a default root term for each category.
	 */
	DiseasesIndexTermModel() {
		for (final Category cat : Category.values()) {
			final DiseasesIndexTerm root = new DiseasesIndexTerm(-1);
			root.setText("Index"); //$NON-NLS-1$
			root.setLevel(-1);
			roots.put(cat, root);
		}
	}

	/**
	 * Returns the set of linked master codes for the given term.
	 * 
	 * @param term The index term.
	 * @return The set of linked master codes for the given term.
	 */
	Set<DiseasesMasterCode> getLinkedCodes(final DiseasesIndexTerm term) {

		// Return a defensive copy.
		final Set<DiseasesMasterCode> codes = linkedCodesByTerm.get(term);

		return codes == null ? Collections.<DiseasesMasterCode> emptySet() : codes;
	}

	/**
	 * Returns the root term for the given category.
	 * 
	 * @param cat The category.
	 * @return The root term for the category.
	 */
	DiseasesIndexTerm getRoot(final Category cat) {
		return roots.get(cat);
	}

	/**
	 * Returns the index term with the given ID.
	 * 
	 * @param id The term's ID.
	 * @return The corresponding term.
	 */
	DiseasesIndexTerm getTermForID(final int id) {
		return termsByID.get(id);
	}

	/**
	 * Loads all diseases index terms into memory.
	 */
	void load() {

		logger.info("Loading diseases index terms and notes..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_DISEASES_INDEX));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String s = new String(input, "utf8"); //$NON-NLS-1$
				if (s.length() < 2) {
					break;
				}

				// Split the string into tokens which contain each field.
				final String[] tokens = s.split("\\" + DataFiles.SEPARATOR_FIELD, 17); //$NON-NLS-1$

				// Extract the fields.
				final String category = tokens[0];
				final Integer id = ModelUtils.tokenAsInteger(tokens, 1);
				final Integer parentId = ModelUtils.tokenAsInteger(tokens, 2);
				final String text = tokens[3];
				final String fullText = tokens[4];
				final String secondPart = ModelUtils.tokenAsString(tokens, 5);
				final int level = ModelUtils.tokenAsInteger(tokens, 6);
				final String linkedCodes = ModelUtils.tokenAsString(tokens, 7);
				DiseasesIndexTermNote.Type noteType = null;
				Integer noteId = null;
				String noteText = null;
				String linkedViewId = null;
				String linkedCategory = null;
				String linkedIds = null;
				String linkedExpression = null;
				String heading = null;
				noteId = ModelUtils.tokenAsInteger(tokens, 8);
				if (noteId != null) {
					noteType = DiseasesIndexTermNote.Type.valueOf(tokens[9]);
					noteText = tokens[10];
					linkedViewId = ModelUtils.tokenAsString(tokens, 11);
					linkedCategory = ModelUtils.tokenAsString(tokens, 12);
					linkedIds = ModelUtils.tokenAsString(tokens, 13);
					linkedExpression = ModelUtils.tokenAsString(tokens, 14);
				}
				heading = ModelUtils.tokenAsString(tokens, 15);

				final Category cat = Category.valueOf(category);
				DiseasesIndexTerm term = termsByID.get(id);
				if (term == null) {

					// Create and initialise a new index term.
					term = new DiseasesIndexTerm(id);
					term.setCategory(cat);
					final DiseasesIndexTerm parent = parentId == null ? roots.get(cat) : termsByID.get(parentId);
					term.setParent(parent);
					parent.addChild(term);
					term.setText(text);
					term.setFullText(fullText);
					term.setSecondPart(secondPart);
					term.setLevel(level);

					// Add the term to the map.
					termsByID.put(id, term);

					// Create a set of linked master codes from the comma-separated list of code
					// texts.
					final Set<DiseasesMasterCode> codeSet = new HashSet<>();
					linkedCodesByTerm.put(term, codeSet);
					if (linkedCodes != null && !linkedCodes.isEmpty()) {
						final String[] codes = linkedCodes.split(","); //$NON-NLS-1$
						for (int j = 0; j < codes.length; j++) {
							final DiseasesMasterCode code = Model.INSTANCE.getDiseasesCodeForText(codes[j]);
							codeSet.add(code);
						}
					}
				}

				if (noteId != null) {

					// Create and initialise a new note.
					final DiseasesIndexTermNote note = new DiseasesIndexTermNote();
					note.setId(noteId);
					note.setType(noteType);
					note.setText(noteText);
					note.setHeading(heading);

					// Create the hyper links for the note.
//					if (linkedViewId != null) {
//						final List<HyperLink> hyperLinks = new ArrayList<>();
//						if (linkedExpression == null) {
//							if (linkedIds == null) {
//
//								// Create a hyper link to the root term in the linked category.
//								hyperLinks.add(new HyperLink(View.fromId(linkedViewId), linkedCategory, (Integer)null,
//								    null));
//							} else {
//
//								// Create hyper links for each of the IDs in the comma-separated
//								// list of linked IDs.
//								for (final String linkedId : linkedIds.split(",")) { //$NON-NLS-1$
//									hyperLinks.add(new HyperLink(View.fromId(linkedViewId), linkedCategory, Integer
//									    .valueOf(linkedId), null));
//								}
//							}
//						} else {
//
//							// Create an expression-based hyper link (to a code of some kind).
//							hyperLinks.add(new HyperLink(View.fromId(linkedViewId), linkedCategory, linkedExpression,
//							    null));
//						}
//						note.setHyperLinks(hyperLinks);
//					}

					// Add the note to the term.
					term.addNote(note);
				}

				++i;
			}

			// Clean-up.
			dis.close();

			// All terms and notes loaded successfully.
			logger.info("" + i + " diseases index terms and notes successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}
	}
}