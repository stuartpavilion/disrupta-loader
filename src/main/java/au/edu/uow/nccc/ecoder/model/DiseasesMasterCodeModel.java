
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesMasterCode;
import au.edu.uow.nccc.ecoder.model.bean.Reference;
import au.edu.uow.nccc.ecoder.model.util.DataFiles;
import au.edu.uow.nccc.ecoder.model.util.DiseasesMasterCodeUtils;
import au.edu.uow.nccc.ecoder.special.SpecialCharacters;

/**
 * A <code>DiseasesMasterCodeModel</code> is the model which manages the diseases master codes for the application.
 * 
 * @author jturnbul
 */
public class DiseasesMasterCodeModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(DiseasesMasterCodeModel.class);

	/**
	 * A map of codes by ID.
	 */
	private final Map<Integer, DiseasesMasterCode> codesByID = new HashMap<>();

	/**
	 * A map of codes by code text.
	 */
	private final Map<String, DiseasesMasterCode> codesByText = new HashMap<>();

	/**
	 * A list of all master codes.
	 */
	private final List<DiseasesMasterCode> codeList = new ArrayList<>();

	/**
	 * A map of lists of linked ACS codes (text) by master code ID.
	 */
	private final Map<Integer, List<String>> linkedACSCodesByID = new HashMap<>();

	/**
	 * A map of lists of linked references (ID) by master code ID.
	 */
	private final Map<Integer, List<String>> linkedReferencesByID = new HashMap<>();

	/**
	 * A map of lists of linked advice item IDs (text) by master code ID.
	 */
	private final Map<Integer, List<String>> linkedAdviceItemsByID = new HashMap<>();

	/**
	 * A map of lists of edits by master code ID.
	 */
	private final Map<Integer, List<String>> editsByID = new HashMap<>();

	/**
	 * The root master code in the hierarchy.
	 */
	private final DiseasesMasterCode root;

	/**
	 * Creates a new <code>DiseasesMasterCodeModel</code> with a default root.
	 */
	DiseasesMasterCodeModel() {
		root = new DiseasesMasterCode(0);
		root.setText("Tabular"); //$NON-NLS-1$
		root.setDescription("Tabular"); //$NON-NLS-1$
	}

	/**
	 * Returns the master code with the given ID.
	 * 
	 * @param id The code's ID.
	 * @return The corresponding master code.
	 */
	DiseasesMasterCode getCodeForID(final int id) {
		return codesByID.get(id);
	}

	/**
	 * Returns the master code with the given text.
	 * 
	 * @param text The code's text.
	 * @return The corresponding master code.
	 */
	DiseasesMasterCode getCodeForText(final String text) {
		return codesByText.get(DiseasesMasterCodeUtils.rawCode(text));
	}

	/**
	 * Returns the list of master codes.
	 * 
	 * @return The list of master codes.
	 */
	List<DiseasesMasterCode> getCodeList() {

		// Return a defensive copy.
		return new ArrayList<>(codeList);
	}

	/**
	 * Returns a list of codes that match the given expression. The expression may be as simple as a specific code's
	 * text or may be a code range or partial code with dashes.
	 * 
	 * @param expression The expression to expand.
	 * @param collapse True if the resulting list is to be collapsed to the most succinct possible representation.
	 * @return The list of codes that match the expression.
	 */
	List<DiseasesMasterCode> getCodesForExpression(final String expression, final boolean collapse) {
		return DiseasesMasterCodeUtils.getCodesForExpression(Collections.unmodifiableList(codeList),
		    Collections.unmodifiableMap(codesByText), DiseasesMasterCodeUtils.rawCode(expression), collapse);
	}

	/**
	 * Returns a list of edits for the given code.
	 * 
	 * @param code The master code.
	 * @return The corresponding list of edits.
	 */
	List<String> getEdits(final DiseasesMasterCode code) {

		// Return a defensive copy.
		final List<String> edits = editsByID.get(code.getID());

		return edits == null ? Collections.<String> emptyList() : new ArrayList<>(editsByID.get(code.getID()));
	}

	/**
	 * Returns a list of ACS codes which are linked to the given code.
	 * 
	 * @param code The master code.
	 * @return The list of linked ACS codes.
	 */
	List<LinkedACSCode> getLinkedACSCodes(final DiseasesMasterCode code) {
		final List<LinkedACSCode> acsCodes = new ArrayList<>();
		final List<String> acsCodeTexts = linkedACSCodesByID.get(code.getID());
		if (acsCodeTexts != null && !acsCodeTexts.isEmpty()) {
			for (final String codeText : acsCodeTexts) {
				acsCodes.add(new LinkedACSCode(codeText));
			}
		}

		return Collections.unmodifiableList(acsCodes);
	}

	/**
	 * Returns a list of advice items which are linked to the given code.
	 * 
	 * @param code The master code.
	 * @return The list of linked advice items.
	 */
	List<LinkedAdviceItem> getLinkedAdviceItems(final DiseasesMasterCode code) {
		final List<LinkedAdviceItem> items = new ArrayList<>();
		final List<String> idTexts = linkedAdviceItemsByID.get(code.getID());
		if (idTexts != null && !idTexts.isEmpty()) {
			for (final String idText : idTexts) {
				items.add(new LinkedAdviceItem(idText));
			}
		}

		return Collections.unmodifiableList(items);
	}

	/**
	 * Returns a list of references which are linked to the given code.
	 * 
	 * @param code The master code.
	 * @return The list of linked references.
	 */
	List<Reference> getLinkedReferences(final DiseasesMasterCode code) {
		final List<Reference> references = new ArrayList<>();
		final List<String> referenceIDs = linkedReferencesByID.get(code.getID());
		if (referenceIDs != null && !referenceIDs.isEmpty()) {
			for (final String referenceID : referenceIDs) {
				references.add(Model.INSTANCE.getReferenceForID(Integer.valueOf(referenceID)));
			}
		}

		return Collections.unmodifiableList(references);
	}

	/**
	 * Returns the root master code in the hierarchy.
	 * 
	 * @return The root master code.
	 */
	DiseasesMasterCode getRoot() {
		return root;
	}

	/**
	 * Loads all diseases master codes into memory.
	 */
	void load() {

		logger.info("Loading diseases master codes..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_DISEASES_TABULAR));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String s = new String(input, "utf8"); //$NON-NLS-1$
				if (s.length() < 2) {
					break;
				}

				// Split the string into tokens which contain each field.
				final String[] tokens = s.split("\\" + DataFiles.SEPARATOR_FIELD, 16); //$NON-NLS-1$

				// Extract the fields.
				final DiseasesMasterCode.Type type = DiseasesMasterCode.Type.valueOf(tokens[0]);
				final Integer id = ModelUtils.tokenAsInteger(tokens, 1);
				final Integer parentId = ModelUtils.tokenAsInteger(tokens, 2);
				final String text = ModelUtils.tokenAsString(tokens, 3);
				final String description = ModelUtils.tokenAsString(tokens, 4);
				final int level = ModelUtils.tokenAsInteger(tokens, 5);
				final boolean dagger = ModelUtils.tokenAsBoolean(tokens, 6);
				final boolean asterisk = ModelUtils.tokenAsBoolean(tokens, 7);
				final boolean australian = ModelUtils.tokenAsBoolean(tokens, 8);
				final boolean valid = ModelUtils.tokenAsBoolean(tokens, 9);
				final String notes = ModelUtils.tokenAsString(tokens, 10);
				final String head = ModelUtils.tokenAsString(tokens, 11);
				final String linkedACSCodes = ModelUtils.tokenAsString(tokens, 12);
				final String linkedAdviceIDs = ModelUtils.tokenAsString(tokens, 13);
				final String linkedReferenceIDs = ModelUtils.tokenAsString(tokens, 14);
				final String edits = ModelUtils.tokenAsString(tokens, 15);

				// Create and initialise a new master code.
				final DiseasesMasterCode code = new DiseasesMasterCode(id);
				final DiseasesMasterCode parent = parentId == null ? root : codesByID.get(parentId);
				code.setParent(parent);
				parent.addChild(code);
				code.setType(type);
				final String sanitisedText = text.replaceAll(SpecialCharacters.EN_DASH, "-"); //$NON-NLS-1$
				code.setText(sanitisedText);
				code.setDescription(description);
				code.setLevel(level);
				code.setDagger(dagger);
				code.setAsterisk(asterisk);
				code.setAustralian(australian);
				code.setValid(valid);
				code.setNotes(notes);
				code.setHead(head);

				// Build a list of linked ACS codes (text) from the comma separated list and place
				// it in the map.
				final List<String> linkedACSCodeList = new ArrayList<>();
				linkedACSCodesByID.put(id, linkedACSCodeList);
				if (linkedACSCodes != null && !linkedACSCodes.isEmpty()) {
					final String[] acsCodes = linkedACSCodes.split(","); //$NON-NLS-1$
					for (final String acsCodeText : acsCodes) {
						linkedACSCodeList.add(acsCodeText);
					}
				}

				// Build a list of linked advice item IDs (text) from the comma separated list and
				// place it in the map.
				final List<String> linkedAdviceIDsList = new ArrayList<>();
				linkedAdviceItemsByID.put(id, linkedAdviceIDsList);
				if (linkedAdviceIDs != null && !linkedAdviceIDs.isEmpty()) {
					final String[] idTexts = linkedAdviceIDs.split(","); //$NON-NLS-1$
					for (final String idText : idTexts) {
						linkedAdviceIDsList.add(idText);
					}
				}

				// Build a list of linked references (ID) from the comma separated list and place
				// it in the map.
				final List<String> linkedReferenceList = new ArrayList<>();
				linkedReferencesByID.put(id, linkedReferenceList);
				if (linkedReferenceIDs != null && !linkedReferenceIDs.isEmpty()) {
					final String[] references = linkedReferenceIDs.split(","); //$NON-NLS-1$
					for (final String referenceID : references) {
						linkedReferenceList.add(referenceID);
					}
				}

				// Build a list of edits from the hash separated list and place it in the map.
				final List<String> editsList = new ArrayList<>();
				editsByID.put(id, editsList);
				if (edits != null && !edits.isEmpty()) {
					final String[] editsArray = edits.split("#"); //$NON-NLS-1$
					for (final String editValue : editsArray) {
						editsList.add(editValue);
					}
				}

				// Add the code to the memory structures.
				codesByID.put(id, code);
				codesByText.put(DiseasesMasterCodeUtils.rawCode(sanitisedText), code);
				codeList.add(code);

				++i;
			}

			// Clean-up.
			dis.close();

			// All codes loaded successfully.
			logger.info("" + i + " diseases master codes successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}
	}
}