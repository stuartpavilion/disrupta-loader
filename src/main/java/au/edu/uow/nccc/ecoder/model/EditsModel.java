
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.util.DataFiles;

/**
 * An <code>EditsModel</code> is the model which manages the edit types for the application.
 * 
 * @author jturnbul
 */
public class EditsModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(EditsModel.class);

	/**
	 * A list of diseases edit types. The order is important as it must match the order in which the edits themselves
	 * are loaded for each master code.
	 */
	private final List<String> diseasesEditTypes = new ArrayList<>();

	/**
	 * A list of interventions edit types. The order is important as it must match the order in which the edits
	 * themselves are loaded for each master code.
	 */
	private final List<String> interventionsEditTypes = new ArrayList<>();

	/**
	 * Returns the list of diseases edit types.
	 * 
	 * @return The list of diseases edit types.
	 */
	List<String> getDiseasesEditTypes() {

		// Return a defensive copy.
		return new ArrayList<>(diseasesEditTypes);
	}

	/**
	 * Returns the list of interventions edit types.
	 * 
	 * @return The list of interventions edit types.
	 */
	List<String> getInterventionsEditTypes() {

		// Return a defensive copy.
		return new ArrayList<>(interventionsEditTypes);
	}

	/**
	 * Loads all edit types into memory.
	 */
	void load() {
		// this is tempoprary and quick solution, and direct invoke the applicationworkbenchwindowadvisor is not encouraged.
		
//		if(!ApplicationWorkbenchWindowAdvisor.editoff)
//		{
			logger.info("Edit off");
			loadDiseasesEditTypes();
		    loadInterventionsEditTypes();
		}
		
//	}

	/**
	 * Loads diseases edit types into memory.
	 */
	private void loadDiseasesEditTypes() {

		logger.info("Loading diseases edit types..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_DISEASES_EDITS));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String s = new String(input, "utf8"); //$NON-NLS-1$
				if (s.length() < 2) {
					break;
				}

				diseasesEditTypes.add(s);
				++i;
			}

			// Clean-up.
			dis.close();

			// All data loaded successfully.
			logger.info("" + i + " diseases edit types successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}
	}

	/**
	 * Loads the interventions edit types into memory.
	 */
	private void loadInterventionsEditTypes() {

		logger.info("Loading interventions edit types..."); //$NON-NLS-1$

		try {

			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_INTERVENTIONS_EDITS));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;
			while (true) {
				final Integer size = dis.readInt();
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String text = new String(input, "utf8"); //$NON-NLS-1$
				if (text.length() < 2) {
					break;
				}

				interventionsEditTypes.add(text);
				++i;
			}

			// Clean-up.
			dis.close();

			// All data loaded successfully.
			logger.info("" + i + " interventions edit types successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}
	}
}