
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.bean.InterventionsIndexTerm;
import au.edu.uow.nccc.ecoder.model.bean.InterventionsIndexTermNote;
import au.edu.uow.nccc.ecoder.model.bean.InterventionsMasterCode;
import au.edu.uow.nccc.ecoder.model.util.DataFiles;

/**
 * A <code>InterventionsIndexTermModel</code> is the model which manages interventions index terms for the application.
 * 
 * @author jturnbul
 */
public class InterventionsIndexTermModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(InterventionsIndexTermModel.class);

	/**
	 * A map of index terms by ID.
	 */
	private final Map<Integer, InterventionsIndexTerm> terms = new HashMap<>();

	/**
	 * A map of sets of interventions master codes by index term which specifies the codes which are linked to a
	 * particular term.
	 */
	private final Map<InterventionsIndexTerm, Set<InterventionsMasterCode>> linkedCodesByTerm = new HashMap<>();

	/**
	 * The root index term.
	 */
	private final InterventionsIndexTerm root;

	/**
	 * Creates a new <code>InterventionsIndexTermModel</code> with a default root.
	 */
	InterventionsIndexTermModel() {
		root = new InterventionsIndexTerm(-1);
		root.setText("Index"); //$NON-NLS-1$
		root.setLevel(-1);
	}

	/**
	 * Returns a set of master codes which are linked to the given term.
	 * 
	 * @param term The index term.
	 * @return The set of linked master codes.
	 */
	Set<InterventionsMasterCode> getLinkedCodes(final InterventionsIndexTerm term) {

		// Return a defensive copy.
		final Set<InterventionsMasterCode> codes = linkedCodesByTerm.get(term);

		return codes == null ? Collections.<InterventionsMasterCode> emptySet() : codes;
	}

	/**
	 * Returns the root index term in the hierarchy.
	 * 
	 * @return The root index term.
	 */
	InterventionsIndexTerm getRoot() {
		return root;
	}

	/**
	 * Returns the index term for the given ID.
	 * 
	 * @param id The term's ID.
	 * @return The corresponding index term.
	 */
	InterventionsIndexTerm getTermForID(final int id) {
		return terms.get(id);
	}

	/**
	 * Loads all interventions index terms into memory.
	 */
	void load() {

		logger.info("Loading interventions index terms and notes..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_INTERVENTIONS_INDEX));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String s = new String(input, "utf8"); //$NON-NLS-1$
				if (s.length() < 2) {
					break;
				}

				// Split the string into tokens which contain each field.
				final String[] tokens = s.split("\\" + DataFiles.SEPARATOR_FIELD, 15); //$NON-NLS-1$

				// Extract the fields.
				final Integer id = ModelUtils.tokenAsInteger(tokens, 0);
				final Integer parentId = ModelUtils.tokenAsInteger(tokens, 1);
				final String text = tokens[2];
				final String fullText = tokens[3];
				final String secondPart = ModelUtils.tokenAsString(tokens, 4);
				final int level = ModelUtils.tokenAsInteger(tokens, 5);
				final String linkedCodes = ModelUtils.tokenAsString(tokens, 6);
				InterventionsIndexTermNote.Type noteType = null;
				Integer noteId = null;
				String noteText = null;
				String linkedViewId = null;
				String linkedIds = null;
				String linkedExpression = null;
				noteId = ModelUtils.tokenAsInteger(tokens, 7);
				if (noteId != null) {
					noteType = InterventionsIndexTermNote.Type.valueOf(tokens[8]);
					noteText = tokens[9];
					linkedViewId = ModelUtils.tokenAsString(tokens, 10);
					linkedIds = ModelUtils.tokenAsString(tokens, 11);
					linkedExpression = ModelUtils.tokenAsString(tokens, 12);
				}

				InterventionsIndexTerm term = terms.get(id);
				if (term == null) {

					// Create and initialise a new index term.
					term = new InterventionsIndexTerm(id);
					final InterventionsIndexTerm parent = parentId == null ? root : terms.get(parentId);
					term.setParent(parent);
					parent.addChild(term);
					term.setText(text);
					term.setFullText(fullText);
					term.setSecondPart(secondPart);
					term.setLevel(level);

					// Add it to the map.
					terms.put(id, term);

					// Build a set of linked master codes from the comma separated list of code
					// texts.
					final Set<InterventionsMasterCode> codeSet = new HashSet<>();
					linkedCodesByTerm.put(term, codeSet);
					if (linkedCodes != null && !linkedCodes.isEmpty()) {
						final String[] codes = linkedCodes.split(","); //$NON-NLS-1$
						for (int j = 0; j < codes.length; j++) {
							final InterventionsMasterCode code = Model.INSTANCE.getInterventionsCodeForText(codes[j]);
							codeSet.add(code);
						}
					}
				}

				if (noteId != null) {

					// Create and initialise a new note.
					final InterventionsIndexTermNote note = new InterventionsIndexTermNote();
					note.setId(noteId);
					note.setType(noteType);
					note.setText(noteText);

//					if (linkedViewId != null) {
//						final List<HyperLink> hyperLinks = new ArrayList<>();
//						if (linkedExpression == null) {
//							if (linkedIds == null) {
//
//								// Create a hyper link to the root term.
//								hyperLinks.add(new HyperLink(View.fromId(linkedViewId), null, (Integer)null, null));
//							} else {
//
//								// Create hyper links for each of the IDs in the comma-separated
//								// list of linked IDs.
//								for (final String linkedId : linkedIds.split(",")) { //$NON-NLS-1$
//									hyperLinks.add(new HyperLink(View.fromId(linkedViewId), null, Integer
//									    .valueOf(linkedId), null));
//								}
//							}
//						} else {
//
//							// Create an expression-based hyper link (to a code of some kind).
//							hyperLinks.add(new HyperLink(View.fromId(linkedViewId), null, linkedExpression, null));
//						}
//						note.setHyperLinks(hyperLinks);
//					}

					// Add the note to the term.
					term.addNote(note);
				}

				++i;
			}

			// Clean-up.
			dis.close();

			// All terms and notes loaded successfully.
			logger.info("" + i + " interventions index terms and notes successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the SQL exception.
			logger.error(ioe.getMessage());
		}
	}
}