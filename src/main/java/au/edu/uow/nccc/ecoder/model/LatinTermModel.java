
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.italiciser.HTMLItaliciser;
import au.edu.uow.nccc.ecoder.model.util.DataFiles;

/**
 * A <code>LatinTermModel</code> is the model which manages the Latin terms and phrases used by the
 * <code>Italiciser</code>.
 * 
 * @author jturnbul
 */
public class LatinTermModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(LatinTermModel.class);

	/**
	 * The set of all Latin words.
	 */
	private final Set<String> latinWords = new HashSet<>();

	/**
	 * The set of all Latin phrases.
	 */
	private final Set<String> latinPhrases = new HashSet<>();

	/**
	 * Loads all Latin words and phrases into memory.
	 */
	void load() {
		loadLatinWords();
		loadLatinPhrases();
	}

	/**
	 * Loads all Latin phrases into memory.
	 */
	private void loadLatinPhrases() {

		logger.info("Loading latin phrases..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_LATIN_PHRASES));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String phrase = new String(input, "utf8"); //$NON-NLS-1$
				if (phrase.length() < 2) {
					break;
				}

				latinPhrases.add(phrase);
				++i;
			}

			// Clean-up.
			dis.close();

			// Assign the model to the italiciser.
			HTMLItaliciser.INSTANCE.setPhrases(latinPhrases);

			// All codes loaded successfully.
			logger.info("" + i + " latin phrases successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}
	}

	/**
	 * Loads all Latin words into memory.
	 */
	private void loadLatinWords() {

		logger.info("Loading latin words..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_LATIN_WORDS));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String word = new String(input, "utf8"); //$NON-NLS-1$
				if (word.length() < 2) {
					break;
				}

				latinWords.add(word);
				++i;
			}

			// Clean-up.
			dis.close();

			// Assign the model to the italiciser.
			HTMLItaliciser.INSTANCE.setWords(latinWords);

			// All codes loaded successfully.
			logger.info("" + i + " latin words successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}
	}
}