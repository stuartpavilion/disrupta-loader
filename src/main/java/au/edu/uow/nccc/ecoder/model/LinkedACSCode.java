
package au.edu.uow.nccc.ecoder.model;

import au.edu.uow.nccc.ecoder.model.bean.ACSCode;

/**
 * A <code>LinkedACSCode</code> encapsulates an ACS code that is linked to a master code and indicates whether the link
 * is direct or indirect.
 * 
 * @author jturnbul
 */
public class LinkedACSCode {

	/**
	 * The linked ACS code.
	 */
	private final ACSCode code;

	/**
	 * A flag which is true if the link is an indirect or reverse link.
	 */
	private final boolean indirect;

	/**
	 * Creates a new <code>LinkedACSCode</code> from an existing ACS code and flags it as a direct link.
	 * 
	 * @param direct The directly linked ACS code.
	 */
	public LinkedACSCode(final ACSCode direct) {
		indirect = false;
		code = direct;
	}

	/**
	 * Creates a new <code>LinkedACSCode</code> from an ACS code text which may include an asterisk to indicate an
	 * indirect link.
	 * 
	 * @param text The text of the ACS code which may include an asterisk to indicate an indirect link.
	 */
	public LinkedACSCode(final String text) {
		indirect = text.endsWith("*"); //$NON-NLS-1$
		final String codeText = indirect ? text.substring(0, text.length() - 1) : text;
		code = Model.INSTANCE.getACSCodeForText(codeText);
	}

	/**
	 * Returns the linked ACS code.
	 * 
	 * @return The linked ACS code.
	 */
	public ACSCode getCode() {
		return code;
	}

	/**
	 * Returns true if the link is an indirect link.
	 * 
	 * @return True if the link is an indirect link.
	 */
	public boolean isIndirect() {
		return indirect;
	}
}