
package au.edu.uow.nccc.ecoder.model;

import au.edu.uow.nccc.ecoder.model.bean.AdviceItem;

/**
 * A <code>LinkedAdviceItem</code> encapsulates an advice item that is linked to a master code and indicates whether the
 * link is direct or indirect.
 * 
 * @author jturnbul
 */
public class LinkedAdviceItem {

	/**
	 * The linked advice item.
	 */
	private final AdviceItem item;

	/**
	 * A flag which is true if the link is an indirect or reverse link.
	 */
	private final boolean indirect;

	/**
	 * Creates a new <code>LinkedAdviceItem</code> from an existing advice item and flags it as a direct link.
	 * 
	 * @param direct The directly linked advice item.
	 */
	public LinkedAdviceItem(final AdviceItem direct) {
		indirect = false;
		item = direct;
	}

	/**
	 * Creates a new <code>LinkedAdviceItem</code> from an advice item ID (text) which may include an asterisk to
	 * indicate an indirect link.
	 * 
	 * @param text The ID (text) of the advice item which may include an asterisk to indicate an indirect link.
	 */
	public LinkedAdviceItem(final String text) {
		indirect = text.endsWith("*"); //$NON-NLS-1$
		final String raw = indirect ? text.substring(0, text.length() - 1) : text;
		item = Model.INSTANCE.getAdviceItemForID(Integer.valueOf(raw));
	}

	/**
	 * Returns the linked advice item.
	 * 
	 * @return The linked item.
	 */
	public AdviceItem getItem() {
		return item;
	}

	/**
	 * Returns true if the link is an indirect or reverse link.
	 * 
	 * @return True if the link is indirect.
	 */
	public boolean isIndirect() {
		return indirect;
	}
}