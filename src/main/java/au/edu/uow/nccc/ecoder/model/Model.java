
package au.edu.uow.nccc.ecoder.model;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import au.edu.uow.nccc.ecoder.model.bean.ACSCode;
import au.edu.uow.nccc.ecoder.model.bean.AdviceItem;
import au.edu.uow.nccc.ecoder.model.bean.CodingCase;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesIndexTerm;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesMasterCode;
import au.edu.uow.nccc.ecoder.model.bean.InterventionsIndexTerm;
import au.edu.uow.nccc.ecoder.model.bean.InterventionsMasterCode;
import au.edu.uow.nccc.ecoder.model.bean.MasterCode;
import au.edu.uow.nccc.ecoder.model.bean.MorphologyCode;
import au.edu.uow.nccc.ecoder.model.bean.Reference;
import au.edu.uow.nccc.ecoder.model.bean.SelectedCode;
import au.edu.uow.nccc.ecoder.model.bean.YearMonth;

/**
 * <code>Model</code> is the composite model which manages access to all the beans and other data in the application.
 * The various sub-models should not be accessed directly from client code. <code>Model</code> is a singleton.
 * 
 * @author jturnbul
 */
public enum Model {

	/**
	 * The singleton instance.
	 */
	INSTANCE;

	// The various vertical sub-models.
	private final DiseasesIndexTermModel diseasesIndexTermModel;

	private final DiseasesMasterCodeModel diseasesMasterCodeModel;

	private final InterventionsIndexTermModel interventionsIndexTermModel;

	private final InterventionsMasterCodeModel interventionsMasterCodeModel;

	private final ACSCodeModel acsCodeModel;

	private final AdviceItemModel adviceItemModel;

	private final ReferenceModel referenceModel;

	private final MorphologyCodeModel morphologyCodeModel;

	private final EditsModel editsModel;

	private final CaseModel caseModel;

	private final NotesModel notesModel;

	/**
	 * Instantiates the <code>Model</code> singleton and creates the sub-models.
	 */
	private Model() {
		diseasesIndexTermModel = new DiseasesIndexTermModel();
		diseasesMasterCodeModel = new DiseasesMasterCodeModel();
		interventionsIndexTermModel = new InterventionsIndexTermModel();
		interventionsMasterCodeModel = new InterventionsMasterCodeModel();
		acsCodeModel = new ACSCodeModel();
		adviceItemModel = new AdviceItemModel();
		referenceModel = new ReferenceModel();
		morphologyCodeModel = new MorphologyCodeModel();
		editsModel = new EditsModel();
		caseModel = new CaseModel();
		notesModel = new NotesModel();
	}

	/**
	 * Adds a coding case to the database.
	 * 
	 * @param id The ID of the case.
	 * @param codes A list of associated codes.
	 * @return An error message if there was an error, null if successful.
	 */
	public String addCase(final String id, final List<SelectedCode> codes) {
		return caseModel.addCase(id, codes);
	}

	/**
	 * Returns true if there are saved coding cases awaiting export.
	 * 
	 * @return True if there are saved coding cases awaiting export.
	 */
	public boolean canExportCases() {
		return caseModel.canExport();
	}

	/**
	 * Returns true if a case with the specified ID already exists in the database.
	 * 
	 * @param id The case's ID.
	 * @return True if the case already exists.
	 */
	public boolean caseExists(final String id) {
		return caseModel.caseExists(id);
	}

	/**
	 * Returns true if there are notes associated with the specified master code.
	 * 
	 * @param code The master code.
	 * @return True if the code has notes associated with it.
	 */
	public boolean codeHasNotes(final MasterCode code) {
		return notesModel.codeHasNotes(code);
	}

	/**
	 * Returns true if there are notes associated with the specified master code of a particular type.
	 * 
	 * @param type The type of note.
	 * @param code The master code.
	 * @return True if the code has notes associated with it.
	 */
	public boolean codeHasNotes(final NotesModel.NoteType type, final MasterCode code) {
		return notesModel.codeHasNotes(type, code);
	}

	/**
	 * Removes all existing coding cases from the database.
	 */
	public void deleteAllCases() {
		caseModel.deleteAllCases();
	}

	/**
	 * Returns the ACS code with the given ID.
	 * 
	 * @param id The code's ID.
	 * @return The corresponding ACS code.
	 */
	public ACSCode getACSCodeForID(final int id) {
		return acsCodeModel.getCodeForID(id);
	}

	/**
	 * Returns the ACS code with the given code text.
	 * 
	 * @param text The text of the code.
	 * @return The corresponding ACS code.
	 */
	public ACSCode getACSCodeForText(final String text) {
		return acsCodeModel.getCodeForText(text);
	}

	/**
	 * Returns a list of all ACS codes in the model.
	 * 
	 * @return A list of all ACS codes.
	 */
	public List<ACSCode> getACSCodes() {
		return acsCodeModel.getCodes();
	}

	/**
	 * Returns the advice item with the given ID.
	 * 
	 * @param id The item's ID.
	 * @return The corresponding advice item.
	 */
	public AdviceItem getAdviceItemForID(final int id) {
		return adviceItemModel.getItemForID(id);
	}

	/**
	 * Returns a list of advice items published in the given year and month.
	 * 
	 * @param year The year of publication.
	 * @param month The month of publication.
	 * @return The list of all advice items published in the given year and month.
	 */
	public List<AdviceItem> getAdviceItemsForYearMonth(final int year, final int month) {
		return adviceItemModel.getItemsForYearMonth(year, month);
	}

	/**
	 * Returns a list of all year/month combinations in which advice items have been published.
	 * 
	 * @return A list of all year/month combinations in which advice items have been published.
	 */
	public List<YearMonth> getAdviceYearMonths() {
		return adviceItemModel.getYearMonths();
	}

	/**
	 * Returns the diseases master code with the specified ID.
	 * 
	 * @param id The code's ID.
	 * @return The corresponding master code.
	 */
	public DiseasesMasterCode getDiseasesCodeForID(final int id) {
		return diseasesMasterCodeModel.getCodeForID(id);
	}

	/**
	 * Returns the diseases master code with the specified code text.
	 * 
	 * @param text The code's text.
	 * @return The corresponding master code.
	 */
	public DiseasesMasterCode getDiseasesCodeForText(final String text) {
		return diseasesMasterCodeModel.getCodeForText(text);
	}

	/**
	 * Returns a list of all diseases master codes in the model.
	 * 
	 * @return A list of all diseases master codes.
	 */
	public List<DiseasesMasterCode> getDiseasesCodeList() {
		return diseasesMasterCodeModel.getCodeList();
	}

	/**
	 * Returns a list of codes that match the given expression. The expression may be as simple as a specific code's
	 * text or may be a code range or partial code with dashes.
	 * 
	 * @param expression The expression to expand.
	 * @param collapse True if the resulting list is to be collapsed to the most succinct possible representation.
	 * @return The list of codes that match the expression.
	 */
	public List<DiseasesMasterCode> getDiseasesCodesForExpression(final String expression, final boolean collapse) {
		return diseasesMasterCodeModel.getCodesForExpression(expression, collapse);
	}

	/**
	 * Returns the diseases index term with the specified ID.
	 * 
	 * @param id The term's ID.
	 * @return The corresponding index term.
	 */
	public DiseasesIndexTerm getDiseasesIndexTermForID(final int id) {
		return diseasesIndexTermModel.getTermForID(id);
	}

	/**
	 * Returns a list of edits for the given master code.
	 * 
	 * @param code The master code.
	 * @return The corresponding list of edits.
	 */
	public List<String> getEdits(final MasterCode code) {
		if (code instanceof DiseasesMasterCode) {
			return diseasesMasterCodeModel.getEdits((DiseasesMasterCode)code);
		}

		if (code instanceof InterventionsMasterCode) {
			return interventionsMasterCodeModel.getEdits((InterventionsMasterCode)code);
		}

		return Collections.emptyList();
	}

	/**
	 * Returns a list of edit types for the given master code.
	 * 
	 * @param code The master code.
	 * @return The corresponding list of edit types.
	 */
	public List<String> getEditTypes(final MasterCode code) {
		if (code instanceof DiseasesMasterCode) {
			return editsModel.getDiseasesEditTypes();
		}

		if (code instanceof InterventionsMasterCode) {
			return editsModel.getInterventionsEditTypes();
		}

		return Collections.emptyList();
	}

	/**
	 * Returns the interventions master code with the given ID.
	 * 
	 * @param id The code's ID.
	 * @return The corresponding master code.
	 */
	public InterventionsMasterCode getInterventionsCodeForID(final int id) {
		return interventionsMasterCodeModel.getCodeForID(id);
	}

	/**
	 * Returns the interventions master code with the given code text.
	 * 
	 * @param text The code's text.
	 * @return The corresponding master code.
	 */
	public InterventionsMasterCode getInterventionsCodeForText(final String text) {
		return interventionsMasterCodeModel.getCodeForText(text);
	}

	/**
	 * Returns a list of all interventions master codes in the model.
	 * 
	 * @return A list of all interventions master codes in the model.
	 */
	public List<InterventionsMasterCode> getInterventionsCodeList() {
		return interventionsMasterCodeModel.getCodeList();
	}

	/**
	 * Returns a list of codes that match the given expression. The expression may be as simple as a specific code's
	 * text or may be a partial code or block number.
	 * 
	 * @param expression The expression to expand.
	 * @param collapse True if the resulting list is to be collapsed to the most succinct possible representation.
	 * @return The list of codes that match the expression.
	 */
	public List<InterventionsMasterCode> getInterventionsCodesForExpression(final String expression,
	    final boolean collapse) {
		return interventionsMasterCodeModel.getCodesForExpression(expression, collapse);
	}

	/**
	 * Returns the interventions index term with the given ID.
	 * 
	 * @param id The term's ID.
	 * @return The corresponding index term.
	 */
	public InterventionsIndexTerm getInterventionsIndexTermForID(final int id) {
		return interventionsIndexTermModel.getTermForID(id);
	}

	/**
	 * Returns a list of linked ACS codes for a master code.
	 * 
	 * @param code The master code.
	 * @return The list of linked ACS codes.
	 */
	public List<LinkedACSCode> getLinkedACSCodes(final MasterCode code) {
		if (code instanceof DiseasesMasterCode) {
			return diseasesMasterCodeModel.getLinkedACSCodes((DiseasesMasterCode)code);
		}

		if (code instanceof InterventionsMasterCode) {
			return interventionsMasterCodeModel.getLinkedACSCodes((InterventionsMasterCode)code);
		}

		return Collections.emptyList();
	}

	/**
	 * Returns a list of linked advice items for a master code.
	 * 
	 * @param code The master code.
	 * @return A list of linked advice items.
	 */
	public List<LinkedAdviceItem> getLinkedAdviceItems(final MasterCode code) {
		if (code instanceof DiseasesMasterCode) {
			return diseasesMasterCodeModel.getLinkedAdviceItems((DiseasesMasterCode)code);
		}

		if (code instanceof InterventionsMasterCode) {
			return interventionsMasterCodeModel.getLinkedAdviceItems((InterventionsMasterCode)code);
		}

		return Collections.emptyList();
	}

	/**
	 * Returns a list of diseases master codes that are linked to the given index term.
	 * 
	 * @param term The index term.
	 * @return A list of linked master codes.
	 */
	public Set<DiseasesMasterCode> getLinkedDiseasesCodes(final DiseasesIndexTerm term) {
		return diseasesIndexTermModel.getLinkedCodes(term);
	}

	/**
	 * Returns a list of interventions master codes that are linked to the given index term.
	 * 
	 * @param term The index term.
	 * @return A list of linked master codes.
	 */
	public Set<InterventionsMasterCode> getLinkedInterventionsCodes(final InterventionsIndexTerm term) {
		return interventionsIndexTermModel.getLinkedCodes(term);
	}

	/**
	 * Returns a list of linked references for a master code.
	 * 
	 * @param code The master code.
	 * @return The list of linked references.
	 */
	public List<Reference> getLinkedReferences(final MasterCode code) {
		if (code instanceof DiseasesMasterCode) {
			return diseasesMasterCodeModel.getLinkedReferences((DiseasesMasterCode)code);
		}

		if (code instanceof InterventionsMasterCode) {
			return interventionsMasterCodeModel.getLinkedReferences((InterventionsMasterCode)code);
		}

		return Collections.emptyList();
	}

	/**
	 * Returns the morphology code with the given code text.
	 * 
	 * @param text The code's text.
	 * @return The corresponding morphology code.
	 */
	public MorphologyCode getMorphologyCodeForText(final String text) {
		return morphologyCodeModel.getCodeForText(text);
	}

	/**
	 * Returns a note of the specified type for the given master code.
	 * 
	 * @param type The note's type.
	 * @param code The master code.
	 * @return The corresponding note.
	 */
	public String getNoteForCode(final NotesModel.NoteType type, final MasterCode code) {
		return notesModel.getNoteForCode(type, code);
	}

	/**
	 * Returns the reference with the given ID.
	 * 
	 * @param id The reference's ID.
	 * @return The corresponding reference.
	 */
	public Reference getReferenceForID(final int id) {
		return referenceModel.getReferenceForID(id);
	}

	/**
	 * Returns the reference with the given code name.
	 * 
	 * @param text The name of the reference.
	 * @return The corresponding reference.
	 */
	public Reference getReferenceForText(final String name) {
		return referenceModel.getReferenceForName(name);
	}

	/**
	 * Returns a list of all references in the model.
	 * 
	 * @return A list of all references.
	 */
	public List<Reference> getReferences() {
		return referenceModel.getReferences();
	}

	/**
	 * Returns the root ACS code in the hierarchy.
	 * 
	 * @return The root ACS code.
	 */
	public ACSCode getRootACSCode() {
		return acsCodeModel.getRoot();
	}

	/**
	 * Returns the root diseases master code in the hierarchy.
	 * 
	 * @return The root diseases master code.
	 */
	public DiseasesMasterCode getRootDiseasesCode() {
		return diseasesMasterCodeModel.getRoot();
	}

	/**
	 * Returns the root diseases index term in the hierarchy for the given category.
	 * 
	 * @param cat The category.
	 * @return The root diseases index term within the given category.
	 */
	public DiseasesIndexTerm getRootDiseasesIndexTerm(final DiseasesIndexTerm.Category cat) {
		return diseasesIndexTermModel.getRoot(cat);
	}

	/**
	 * Returns the root interventions master code in the hierarchy.
	 * 
	 * @return The root interventions master code.
	 */
	public InterventionsMasterCode getRootInterventionsCode() {
		return interventionsMasterCodeModel.getRoot();
	}

	/**
	 * Returns the root interventions index term in the hierarchy.
	 * 
	 * @return The root interventions index term.
	 */
	public InterventionsIndexTerm getRootInterventionsIndexTerm() {
		return interventionsIndexTermModel.getRoot();
	}

	/**
	 * Returns the root reference in the hierarchy.
	 * 
	 * @return The root reference.
	 */
	public Reference getRootReference() {
		return referenceModel.getRoot();
	}

	/**
	 * Returns a list of coding cases that have been saved in the database..
	 * 
	 * @return The list of saved coding cases.
	 */
	public List<CodingCase> getSavedCases() {
		return caseModel.getSavedCases();
	}

	/**
	 * Returns the directory containing shared notes.
	 * 
	 * @return The shared notes directory.
	 */
	public String getSharedNotesDir() {
		return notesModel.getSharedNotesDir();
	}

	/**
	 * Loads the entire model by loading the individual vertical sub-models.
	 */
	public void load() {
		new LatinTermModel().load();
		diseasesMasterCodeModel.load();
		diseasesIndexTermModel.load();
		interventionsMasterCodeModel.load();
		interventionsIndexTermModel.load();
		acsCodeModel.load();
		adviceItemModel.load();
		referenceModel.load();
		morphologyCodeModel.load();
		editsModel.load();
		caseModel.init();
	}

	/**
	 * Saves a note of the given type for the given code to the file system.
	 * 
	 * @param type The type of note.
	 * @param code The master code.
	 * @param note The note itself.
	 * @return An error message if there was an error saving, null if successful.
	 */
	public String saveNoteForCode(final NotesModel.NoteType type, final MasterCode code, final String note) {
		return notesModel.saveNoteForCode(type, code, note);
	}

	/**
	 * Sets the directory in which notes shared between users are to be stored.
	 * 
	 * @param dir The new shared notes directory.
	 * @return An error message if there was an error setting the directory, null if successful.
	 */
	public String setSharedNotesDir(final String dir) {
		return notesModel.setSharedNotesDir(dir);
	}
}