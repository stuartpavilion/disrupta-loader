
package au.edu.uow.nccc.ecoder.model;

import java.util.Date;

/**
 * <code>ModelUtils</code> is a utility class that provides methods to assist with the loading of data models.
 * 
 */
public class ModelUtils {

	/**
	 * Extracts a token from the given array as a Boolean.
	 * 
	 * @param tokens The array of tokens.
	 * @param index The index of the token required.
	 * @return The specified token as a Boolean.
	 */
	public static Boolean tokenAsBoolean(final String[] tokens, final int index) {
		return Integer.valueOf(tokens[index]) == 1;
	}

	/**
	 * Extracts a token from the given array as a Date handling nulls appropriately.
	 * 
	 * @param tokens The array of tokens.
	 * @param index The index of the token required.
	 * @return The specified token as a Date.
	 */
	public static Date tokenAsDate(final String[] tokens, final int index) {
		Date d = null;
		if (tokens[index] != null && !tokens[index].isEmpty()) {
			d = new Date(Long.valueOf(tokens[index]));
		}

		return d;
	}

	/**
	 * Extracts a token from the given array as an Integer handling nulls appropriately.
	 * 
	 * @param tokens The array of tokens.
	 * @param index The index of the token required.
	 * @return The specified token as an Integer.
	 */
	public static Integer tokenAsInteger(final String[] tokens, final int index) {
		Integer i = null;
		if (tokens[index] != null && !tokens[index].isEmpty()) {
			i = Integer.valueOf(tokens[index]);
		}

		return i;
	}

	/**
	 * Extracts a token from the given array as a String handling nulls appropriately.
	 * 
	 * @param tokens The array of tokens.
	 * @param index The index of the token required.
	 * @return The specified token as a String.
	 */
	public static String tokenAsString(final String[] tokens, final int index) {
		String s = null;
		if (tokens[index] != null && !tokens[index].isEmpty()) {
			s = tokens[index];
		}

		return s;
	}
}