
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.bean.MorphologyCode;
import au.edu.uow.nccc.ecoder.model.util.DataFiles;

/**
 * A <code>MorphologyCodeModel</code> is the model responsible for managing all morphology codes in the application.
 * 
 * @author jturnbul
 */
public class MorphologyCodeModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(MorphologyCodeModel.class);

	/**
	 * A map of morphology codes by code text.
	 */
	private final Map<String, MorphologyCode> codesByText = new HashMap<>();

	/**
	 * Returns the morphology code with the given code text.
	 * 
	 * @param text The code's text.
	 * @return The corresponding morphology code.
	 */
	MorphologyCode getCodeForText(final String text) {
		return codesByText.get(text);
	}

	/**
	 * Loads all morphology codes into memory.
	 */
	void load() {

		logger.info("Loading morphology codes..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_MORPHOLOGY));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String s = new String(input, "utf8"); //$NON-NLS-1$
				if (s.length() < 2) {
					break;
				}

				// Split the string into tokens which contain each field.
				final String[] tokens = s.split("\\" + DataFiles.SEPARATOR_FIELD, 4); //$NON-NLS-1$

				// Extract the fields.
				final String text = ModelUtils.tokenAsString(tokens, 0);
				final String description = ModelUtils.tokenAsString(tokens, 1);
				final String notes = ModelUtils.tokenAsString(tokens, 2);

				// Create and initialise a new morphology code.
				final MorphologyCode code = new MorphologyCode();
				code.setText(text);
				code.setDescription(description);
				code.setNotes(notes);
				codesByText.put(text, code);

				++i;
			}

			// Clean-up.
			dis.close();

			// All codes loaded successfully.
			logger.info("" + i + " morphology codes successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}
	}
}