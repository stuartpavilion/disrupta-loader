
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;
//import org.eclipse.core.runtime.preferences.ConfigurationScope;
//import org.osgi.service.prefs.BackingStoreException;
//import org.osgi.service.prefs.Preferences;

import au.edu.uow.nccc.ecoder.ECoder;
import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesMasterCode;
import au.edu.uow.nccc.ecoder.model.bean.MasterCode;

/**
 * A <code>NotesModel</code> is the model responsible for managing access to notes associated with master codes in the
 * application.
 * 
 * @author jturnbul
 */
public class NotesModel {

	/**
	 * <code>NoteType</code> enumerates the various types of notes.
	 * 
	 * @author jturnbul
	 */
	public enum NoteType {

		// Note are private to the current user.
		PRIVATE,

		// Notes are shared between all users.
		SHARED
	}

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(NotesModel.class);

	/**
	 * The name of the directory in which notes are stored.
	 */
	private static final String DIRECTORY_NOTES = "notes"; //$NON-NLS-1$

	/**
	 * The name of the directory in which private notes are stored.
	 */
	private static final String DIRECTORY_PRIVATE_NOTES = ECoder.getUserDirectory() + Resources.SEPARATOR_FILE
	    + DIRECTORY_NOTES + Resources.SEPARATOR_FILE + "private"; //$NON-NLS-1$

	/**
	 * The name of the default directory in which shared notes are stored.
	 */
	private static final String DIRECTORY_SHARED_NOTES_DEFAULT = ECoder.getUserDirectory() + Resources.SEPARATOR_FILE
	    + DIRECTORY_NOTES + Resources.SEPARATOR_FILE + "shared"; //$NON-NLS-1$

	/**
	 * The file extension for note files.
	 */
	private static final String NOTE_FILE_EXTENSION = ".note"; //$NON-NLS-1$

	/**
	 * The node ID of the preference for storing the shared notes directory.
	 */
	private static final String PREFS_NODE_SHARED_NOTES_DIR = "au.edu.uow.ncc.codexpert.prefs.notes.shared"; //$NON-NLS-1$

	/**
	 * The key of the preference for storing the shared notes directory.
	 */
	private static final String PREFS_KEY_SHARED_NOTES_DIR = "dir"; //$NON-NLS-1$

	/**
	 * The error message to report when an error is encountered moving notes to a new location.
	 */
	private static final String ERROR_MOVING_NOTES = "Error moving existing notes to new location"; //$NON-NLS-1$

	/**
	 * The prefix part of a file name for note associated with a diseases master code.
	 */
	private static final String PREFIX_DISEASES_NOTE_FILE_NAME = "DIS-"; //$NON-NLS-1$

	/**
	 * The prefix part of a file name for note associated with an interventions master code.
	 */
	private static final String PREFIX_INTERVENTIONS_NOTE_FILE_NAME = "INT-"; //$NON-NLS-1$

	/**
	 * The name of the directory in which shared notes are stored.
	 */
	private String sharedNotesDir;

	/**
	 * Creates and initialises a new <code>NotesModel</code>.
	 */
	NotesModel() {
		init();
	}

	/**
	 * Returns true if the given master code has any notes associated with it.
	 * 
	 * @param code The master code.
	 * @return True if code has associated notes.
	 */
	boolean codeHasNotes(final MasterCode code) {
		final boolean hasPrivate = codeHasNotes(NoteType.PRIVATE, code);
		final boolean hasShared = codeHasNotes(NoteType.SHARED, code);

		return hasPrivate || hasShared;
	}

	/**
	 * Returns true if the given master code has any notes of the specified type associated with it.
	 * 
	 * @param type The type of note.
	 * @param code The master code.
	 * @return True if code has associated notes.
	 */
	boolean codeHasNotes(final NoteType type, final MasterCode code) {
		final String note = getNoteForCode(type, code);

		return note != null && !note.trim().isEmpty();
	}

	/**
	 * Returns the name of the file containing the note of the specified type for the specified master code.
	 * 
	 * @param type The type of note.
	 * @param code The master code.
	 * @return The note's file name.
	 */
	private String getFileName(final NoteType type, final MasterCode code) {
		final String fileSeparator = System.getProperty("file.separator"); //$NON-NLS-1$
		final String root = type == NoteType.PRIVATE ? DIRECTORY_PRIVATE_NOTES : sharedNotesDir;

		return root
		    + fileSeparator
		    + (code instanceof DiseasesMasterCode ? PREFIX_DISEASES_NOTE_FILE_NAME
		        : PREFIX_INTERVENTIONS_NOTE_FILE_NAME) + code.getText() + NOTE_FILE_EXTENSION;
	}

	/**
	 * Returns the note of the given type contained in the specified file.
	 * 
	 * @param type The type of note.
	 * @param file The note's file.
	 * @return The note contained in the specified file.
	 */
	String getNote(final NoteType type, final File file) {
		final StringBuilder sb = new StringBuilder();
		try {
			final BufferedReader br = new BufferedReader(new FileReader(file));
			String s;
			while ((s = br.readLine()) != null) {
				sb.append(s);
				sb.append("\n"); //$NON-NLS-1$
			}
			br.close();
		} catch (final IOException ioe) {
			logger.error("Error reading from notes file", ioe); //$NON-NLS-1$

			return null;
		}

		return sb.toString();
	}

	/**
	 * Returns the note of the given type for the specified master code.
	 * 
	 * @param type The type of note.
	 * @param code The master code.
	 * @return The associated note.
	 */
	String getNoteForCode(final NoteType type, final MasterCode code) {
		final File file = new File(getFileName(type, code));
		if (!file.exists()) {
			return null;
		}

		return getNote(type, file);
	}

	/**
	 * Returns the name of the directory in which shared notes are stored.
	 * 
	 * @return The shared notes directory name.
	 */
	String getSharedNotesDir() {
		return sharedNotesDir;
	}

	/**
	 * Initialises the model by ensuring the directories for storing the notes exist.
	 */
	private void init() {

		// Create the directory for storing private notes if required.
		File dir = new File(DIRECTORY_PRIVATE_NOTES);
		if (!dir.exists() && !dir.mkdirs()) {
			logger.error("The " + ECoder.getApplicationName() + " private notes directory could not be created."); //$NON-NLS-1$ //$NON-NLS-2$
		}

//		// Create the directory for storing shared notes if required.
//		@SuppressWarnings("deprecation") final Preferences prefs =
//		    new ConfigurationScope().getNode(PREFS_NODE_SHARED_NOTES_DIR);
//		sharedNotesDir = prefs.get(PREFS_KEY_SHARED_NOTES_DIR, DIRECTORY_SHARED_NOTES_DEFAULT);
//		dir = new File(sharedNotesDir);
//		if (!dir.exists() && !dir.mkdir()) {
//			logger
//			    .error("The " + ECoder.getApplicationName() + " default shared notes directory could not be created."); //$NON-NLS-1$ //$NON-NLS-2$
//		}
	}

	/**
	 * Saves the specified note to a file.
	 * 
	 * @param type The type of note.
	 * @param fileName The note's file name.
	 * @param note The note itself.
	 * @return An error message if there was an error saving the note and null otherwise.
	 */
	String saveNote(final NoteType type, final String fileName, final String note) {
		final File file = new File(fileName);
		try {

			// Delete any existing note file.
			if (file.exists()) {
				file.delete();
			}

			// Create a new note file.
			file.createNewFile();

			// Write the note's contents.
			final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(note);
			writer.flush();
			writer.close();

			return null;
		} catch (final IOException ioe) {
			logger.error("Error writing note file", ioe); //$NON-NLS-1$

			return ioe.getMessage();
		}
	}

	/**
	 * Saves the given note to a file.
	 * 
	 * @param type The type of note.
	 * @param code The master code.
	 * @param note The note itself.
	 * @return An error message if there was an error saving the note and null otherwise.
	 */
	String saveNoteForCode(final NoteType type, final MasterCode code, final String note) {
		return saveNote(type, getFileName(type, code), note);
	}

	/**
	 * Sets the directory in which shared notes are stored.
	 * 
	 * @param newDir The new shared notes directory.
	 * @return An error message if there was an error setting the directory and null otherwise.
	 */
	String setSharedNotesDir(final String newDir) {
		final File dir = new File(sharedNotesDir);
		final File[] files = dir.listFiles();
		if (files != null) {

			// Copy any existing shared notes to the new directory.
			for (final File file : files) {
				if (file.getName().endsWith(NOTE_FILE_EXTENSION)) {
					final String note = getNote(NoteType.SHARED, file);
					if (note != null) {
						final String fileName = newDir + Resources.SEPARATOR_FILE + file.getName();
						final String error = saveNote(NoteType.SHARED, fileName, note);
						if (error != null && !error.isEmpty()) {
							return ERROR_MOVING_NOTES + ": " + error; //$NON-NLS-1$
						}
					}
				}
			}

			// Delete any existing notes now that they have been saved in the new location.
			for (final File file : files) {
				if (file.exists() && file.getName().endsWith(NOTE_FILE_EXTENSION)) {
					file.delete();
				}
			}
		}

//		// Store the new directory location in the system preferences.
//		@SuppressWarnings("deprecation") final Preferences prefs =
//		    new ConfigurationScope().getNode(PREFS_NODE_SHARED_NOTES_DIR);
//		prefs.put(PREFS_KEY_SHARED_NOTES_DIR, newDir);
//		try {
//			// Force the application to save the preferences.
//			prefs.flush();
//		} catch (final BackingStoreException bse) {
//			logger.error("Error saving shared notes directory preferences", bse); //$NON-NLS-1$
//
//			return bse.getMessage();
//		}

		sharedNotesDir = newDir;

		// No errors.
		return null;
	}
}