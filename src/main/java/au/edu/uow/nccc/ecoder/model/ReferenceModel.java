
package au.edu.uow.nccc.ecoder.model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.bean.Reference;
import au.edu.uow.nccc.ecoder.model.util.DataFiles;

/**
 * An <code>ReferenceModel</code> is the model which manages the application's references.
 * 
 * @author jturnbul
 */
public class ReferenceModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(ReferenceModel.class);

	/**
	 * A map of references by ID.
	 */
	private final Map<Integer, Reference> referencesByID = new HashMap<>();

	/**
	 * A map of references by code text.
	 */
	private final Map<String, Reference> referencesByText = new HashMap<>();

	/**
	 * The list of references.
	 */
	private final List<Reference> referenceList = new ArrayList<>();

	/**
	 * The root code in the reference hierarchy.
	 */
	private final Reference root;

	/**
	 * Creates a new <code>ReferenceModel</code> with a default root.
	 */
	ReferenceModel() {
		root = new Reference(-1);
		root.setName("References"); //$NON-NLS-1$
	}

	/**
	 * Returns the reference with the given ID.
	 * 
	 * @param id The reference's ID.
	 * @return The corresponding reference.
	 */
	Reference getReferenceForID(final int id) {
		return referencesByID.get(id);
	}

	/**
	 * Returns the reference with the given reference text.
	 * 
	 * @param text The reference's text.
	 * @return The corresponding reference.
	 */
	Reference getReferenceForName(final String text) {
		return referencesByText.get(text);
	}

	/**
	 * Returns the list of references.
	 * 
	 * @return The list of references.
	 */
	List<Reference> getReferences() {

		// Return a defensive copy.
		return new ArrayList<>(referenceList);
	}

	/**
	 * Returns the root reference in the hierarchy.
	 * 
	 * @return The root reference.
	 */
	Reference getRoot() {
		return root;
	}

	/**
	 * Loads all references from the data files into memory.
	 */
	void load() {

		logger.info("Loading references..."); //$NON-NLS-1$

		try {

			// Open the data file and read its entire contents uncompressing as we go.
			final File fin = new File(Resources.getPathToDataFile(DataFiles.FILE_NAME_REFERENCES));
			final BufferedInputStream gis = new BufferedInputStream(new GZIPInputStream(new FileInputStream(fin)));
			final byte[] bytes = new byte[DataFiles.BUFFER_SIZE];
			gis.read(bytes, 0, DataFiles.BUFFER_SIZE);

			// Open a stream to read through the uncompressed bytes one chunk at a time.
			final ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			final DataInputStream dis = new DataInputStream(bis);
			final int i = 0;

			while (true) {

				// Read the size of the next chunk of bytes.
				final Integer size = dis.readInt();

				// Read the next chunk of bytes and convert it to a string.
				final byte[] input = new byte[size];
				dis.readFully(input, 0, size);
				final String s = new String(input, "utf8"); //$NON-NLS-1$
				if (s.length() < 2) {
					break;
				}

				// Split the string into tokens which contain each field.
				final String[] tokens = s.split("\\" + DataFiles.SEPARATOR_FIELD, 7); //$NON-NLS-1$

				// Extract the fields.
				final Integer id = ModelUtils.tokenAsInteger(tokens, 0);
				final Integer parentId = ModelUtils.tokenAsInteger(tokens, 1);
				final String name = ModelUtils.tokenAsString(tokens, 2);
				final String notes = ModelUtils.tokenAsString(tokens, 3);
				final String head = ModelUtils.tokenAsString(tokens, 4);

				// Create and initialise a new reference.
				final Reference reference = new Reference(id);
				final Reference parent = parentId == null ? root : referencesByID.get(parentId);
				reference.setParent(parent);
				parent.addChild(reference);
				reference.setName(name);
				reference.setNotes(notes);
				reference.setHead(head);

				// Add the reference to the memory structures.
				referencesByID.put(id, reference);
				referencesByText.put(name, reference);
				referenceList.add(reference);
			}

			// Clean-up.
			dis.close();

			// All references loaded successfully.
			logger.info("" + i + " references successfully loaded."); //$NON-NLS-1$ //$NON-NLS-2$

		} catch (final IOException ioe) {

			// Report the exception.
			logger.error(ioe.getMessage());
		}

		// Ensure the reference list is in ID order.
		Collections.sort(referenceList, new Comparator<Reference>() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
			 */
			@Override
			public int compare(final Reference a, final Reference b) {
				return a.getID().compareTo(b.getID());
			}
		});

		// Prune the list so that only the leaf nodes remain.
		final Iterator<Reference> it = referenceList.iterator();
		while (it.hasNext()) {
			final Reference reference = it.next();
			if (!reference.getChildren().isEmpty()) {
				it.remove();
			}
		}
	}
}