
package au.edu.uow.nccc.ecoder.model.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * An <code>ACSCode</code> corresponds to a code or level within the Australian Coding Standards.
 * 
 * @author jturnbul
 */
public class ACSCode extends CodeTermParentBase<ACSCode> implements Comparable<ACSCode>{

	/**
	 * A regular expression which matches text in the form of a valid ACS code.
	 */
	public static final String REGEX_CODE = "\\d{4}"; //$NON-NLS-1$

	/**
	 * The name of the CSS class which is used to indicate that no actual notes exist for the code.
	 */
	public static final String NO_CODE_NOTES_CSS_CLASS_NAME = "NoCodeNotes"; //$NON-NLS-1$

	/**
	 * A unique identifier for the code.
	 */
	private int id;

	/**
	 * The parent code for the code (may be null).
	 */
	private ACSCode parent;

	/**
	 * The text of the code.
	 */
	private String text;

	/**
	 * The description of the code.
	 */
	private String description;

	/**
	 * The notes for this code as an HTML snippet.
	 */
	private String notes;

	/**
	 * The <head> region of the code's notes as an HTML snippet. This usually contains style definitions.
	 */
	private String head;

	/**
	 * Creates a new <code>ACSCode</code>.
	 * 
	 * @param id The identifier for the new code.
	 */
	public ACSCode(final int id) {
		this.id = id;
	}

	/**
	 * Adds a child code to this code.
	 * 
	 * @param child The child code.
	 */
	public void addChild(final ACSCode child) {
		children.add(child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final ACSCode code) {
		return text.compareTo(code.text);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof ACSCode)) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		final ACSCode rhs = (ACSCode)obj;

		// Codes are considered equal if and only if they have the same identifier.
		return id == rhs.id;
	}

	/**
	 * Returns the description of this code.
	 * 
	 * @return The code's description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the text of this code formatted for display. This method is provided for compatibility with
	 * <code>MasterCode</code> implementations which also define a similar method.
	 * 
	 * @return The text of the code formatted for display.
	 */
	public String getDisplay() {
		return getText();
	}

	/**
	 * Returns a string representation for this code suitable for display and includes the text and description of the
	 * code.
	 * 
	 * @return A string representation of this code suitable for display.
	 */
	public String getFullDisplay() {
		return getText() + " - " + getDescription(); //$NON-NLS-1$
	}
	
	@Override
	public String getPreferredText(){
		return getFullDisplay();
	}

	/**
	 * Returns the <head> HTML snippet for this code.
	 * 
	 * @return The head HTML snippet.
	 */
	public String getHead() {
		return head;
	}

	/**
	 * Returns the identifier for this code.
	 * 
	 * @return The code's identifier.
	 */
	public Integer getID() {
		return id;
	}

	/**
	 * Returns the notes for this code.
	 * 
	 * @return The code's notes.
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Returns the parent code for this code.
	 * 
	 * @return The code's parent.
	 */
	public ACSCode getParent() {
		return parent;
	}

	/**
	 * Returns the specific text of this code.
	 * 
	 * @return The code's specific text.
	 */
	public String getText() {
		return text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		// Simply use the identifier as the hash code to match the equals() method.
		return id;
	}

	/**
	 * Returns true if notes exist for this code.
	 * 
	 * @return True if notes exist for this code.
	 */
	public boolean hasNotes() {
		return notes != null;
	}

	/**
	 * Sets the description of the code.
	 * 
	 * @param description The description.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Sets the head for this code.
	 * 
	 * @param head The head.
	 */
	public void setHead(final String head) {
		this.head = head;
	}

	/**
	 * Sets the ID of the code.
	 * 
	 * @param id The code's ID.
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Set the notes for this code.
	 * 
	 * @param The notes (as an HTML snippet).
	 */
	public void setNotes(final String notes) {
		this.notes = notes;
	}

	/**
	 * Sets the parent of the code.
	 * 
	 * @param parent The code's parent.
	 */
	public void setParent(final ACSCode parent) {
		this.parent = parent;
	}

	/**
	 * Sets the text of the code.
	 * 
	 * @param text The code's text.
	 */
	public void setText(final String text) {
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ACSCode["); //$NON-NLS-1$
		sb.append("@id:"); //$NON-NLS-1$
		sb.append(id);
		sb.append(",@text:"); //$NON-NLS-1$
		sb.append(text);
		sb.append(",@description:"); //$NON-NLS-1$
		sb.append(description);
		sb.append("]"); //$NON-NLS-1$

		return sb.toString();
	}
}