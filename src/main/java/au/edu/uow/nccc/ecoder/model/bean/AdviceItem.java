
package au.edu.uow.nccc.ecoder.model.bean;

import java.util.Date;

import com.i_mmerce.disruptalookup.mvc.json.IndexTerm;
import com.i_mmerce.disruptalookup.mvc.json.IndexTree;
import com.i_mmerce.disruptalookup.mvc.json.MessageComposite;

/**
 * An <code>AdviceItem</code> corresponds to an item from the Coding Matters, FAQ or Q and A.
 * 
 * @author jturnbul
 */
public class AdviceItem implements Comparable<AdviceItem>, CodeTerm {

	/**
	 * A <code>Type</code> is an enumeration of the various types of advice item.
	 * 
	 * @author jturnbul
	 */
	public enum Type {
		CMC("CMC"), FAQ("FAQ"), Q_AND_A("Q & A"), Clinical_Update("Clinical Update"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

		private String display;

		private Type(final String display) {
			this.display = display;
		}

		public String display() {
			return display;
		}
	}

	/**
	 * A unique numeric identifier.
	 */
	private final int id;

	/**
	 * The type of the advice item.
	 */
	private Type type;

	/**
	 * The subject of the advice item.
	 */
	private String subject;

	/**
	 * The message of the advice item.
	 */
	private String message;

	/**
	 * The date the advice item was published.
	 */
	private Date published;

	/**
	 * The date of expiration for the advice item. May be null.
	 */
	private Date expired;

	/**
	 * Creates a new <code>AdviceItem</code> with a specified id.
	 * 
	 * @param id The identifier to use for this advice item.
	 */
	public AdviceItem(final int id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final AdviceItem o) {
		return subject.compareTo(o.subject);
	}

	/**
	 * Returns a string representation of this item suitable for display.
	 * 
	 * @return The item's display string.
	 */
	public String getDisplay() {
		return type.display() + " - " + subject; //$NON-NLS-1$
	}

	/**
	 * Returns the date of expiration.
	 * 
	 * @return the expired The item's expiry date.
	 */
	public Date getExpired() {
		return expired;
	}

	/**
	 * Returns the item's identifier.
	 * 
	 * @return The item's identifier.
	 */
	public int getID() {
		return id;
	}

	/**
	 * Returns the item's message.
	 * 
	 * @return The item's message.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Returns the item's publish date.
	 * 
	 * @return The date the item was published.
	 */
	public Date getPublished() {
		return published;
	}

	/**
	 * Returns the item's subject.
	 * 
	 * @return The subject of the item.
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Returns the type of the item.
	 * 
	 * @return The type of the item.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Sets the item's date of expiration.
	 * 
	 * @param expired The expired date to set.
	 */
	public void setExpired(final Date expired) {
		this.expired = expired;
	}

	/**
	 * Sets the item's message.
	 * 
	 * @param message The message to set.
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * Sets the item's publish date.
	 * 
	 * @param published The published date to set.
	 */
	public void setPublished(final Date published) {
		this.published = published;
	}

	/**
	 * Sets the item's subject.
	 * 
	 * @param subject The subject to set.
	 */
	public void setSubject(final String subject) {
		this.subject = subject;
	}

	/**
	 * Sets the item's type.
	 * 
	 * @param type The type to set.
	 */
	public void setType(final Type type) {
		this.type = type;
	}

	/**
	 * Sets the type of the item based on a string representation of the type.
	 * 
	 * @param typeString A string representation of the item's type.
	 */
	public void setTypeFromString(final String typeString) {
		if (typeString.equals("Q & A")) { //$NON-NLS-1$
			setType(Type.Q_AND_A);
		} 
		else if (typeString.equals("Clinical Update")) { //$NON-NLS-1$
			setType(Type.Clinical_Update);
		} else {
			setType(Type.valueOf(typeString));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("AdviceItem["); //$NON-NLS-1$
		sb.append("@id:"); //$NON-NLS-1$
		sb.append(id);
		sb.append(",@subject:"); //$NON-NLS-1$
		sb.append(subject);
		sb.append(",@type:"); //$NON-NLS-1$
		sb.append(type);
		sb.append("]"); //$NON-NLS-1$

		return sb.toString();
	}

	@Override
	public String getPreferredText() {
		return getDisplay();
	}

	@Override
	public MessageComposite getHeirarchyMessage() {
		return new MessageComposite(getPreferredText());
	}

	@Override
	public MessageComposite getHeirarchyMessage(Integer maxChildDepth) {
		return new MessageComposite(getPreferredText());
	}

	@Override
	public IndexTerm getIndexTerm(IndexTree tree) {
		return new IndexTerm(getPreferredText());
	}

	@Override
	public IndexTerm getIndexTerm(IndexTree tree, Integer maxChildDepth) {
		return new IndexTerm(getPreferredText());
	}
	
}