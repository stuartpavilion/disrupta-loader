package au.edu.uow.nccc.ecoder.model.bean;

import com.i_mmerce.disruptalookup.mvc.json.IndexTerm;
import com.i_mmerce.disruptalookup.mvc.json.IndexTree;
import com.i_mmerce.disruptalookup.mvc.json.MessageComposite;

public interface CodeTerm {
	public String getPreferredText();
	public MessageComposite getHeirarchyMessage();
	public MessageComposite getHeirarchyMessage(Integer maxChildDepth);
	public IndexTerm getIndexTerm(IndexTree tree);
	public IndexTerm getIndexTerm(IndexTree tree, Integer maxChildDepth);
}
