package au.edu.uow.nccc.ecoder.model.bean;

import java.util.List;

import com.i_mmerce.disruptalookup.mvc.json.IndexTermData;

public interface CodeTermParent<T> extends CodeTerm{
	public List<T> getChildren();
	public IndexTermData getIndexTermData();
	public Integer getID();
	public T getParent();
	public int getLevel();
	public String getText();
	public String getSecondPart();
}
