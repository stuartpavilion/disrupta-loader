package au.edu.uow.nccc.ecoder.model.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.i_mmerce.disruptalookup.mvc.json.*;
import com.i_mmerce.disruptalookup.util.IndexTreeMapper;

public abstract class CodeTermParentBase<T> implements CodeTermParent<T> {
	
	/**
	 * A list of all child codes.
	 */
	protected final List<T> children = new ArrayList<>();
	
	/**
	 * Returns the children for this term.
	 * 
	 * @return The list of child terms.
	 */
	@Override
	public List<T> getChildren() {
		return children;
	}
	
	@Override
	public String getPreferredText() {
		return "";
	}

	@Override
	public MessageComposite getHeirarchyMessage() {
		MessageComposite msgComp = new MessageComposite(getPreferredText());
		for (T child: children){
			msgComp.add(((CodeTerm) child).getHeirarchyMessage());
		}
		return msgComp;
	}
	
	@Override
	public MessageComposite getHeirarchyMessage(Integer maxChildDepth) {
		if (maxChildDepth==null){
			return getHeirarchyMessage();
		}
		
		MessageComposite msgComp = new MessageComposite(getPreferredText());
		int childDepth = 0;
		for (T child: children){
			if (++childDepth>maxChildDepth){
				break;
			};
			msgComp.add(((CodeTerm) child).getHeirarchyMessage(maxChildDepth));
		}
		return msgComp;
	}
	
	@Override
	public IndexTerm getIndexTerm(IndexTree tree) {
		IndexTerm indexTerm = new IndexTerm(getPreferredText());
		indexTerm.setData(getIndexTermData());
		indexTerm.getData().setId(getID());
		attachToParent(tree, indexTerm, null);
		for (T child: children){
			indexTerm.add(((CodeTerm) child).getIndexTerm(tree));
		}
		return indexTerm;
	}
	
	@Override
	public IndexTerm getIndexTerm(IndexTree tree,Integer maxChildDepth) {
		if (maxChildDepth==null){
			return getIndexTerm(tree);
		}
		IndexTerm indexTerm = new IndexTerm(getPreferredText());
		indexTerm.setData(getIndexTermData());
		indexTerm.getData().setId(getID());
		
		attachToParent(tree, indexTerm, maxChildDepth);
		int childDepth = 0;
		for (T child: children){
			if (++childDepth>maxChildDepth){
				break;
			};
			indexTerm.add(((CodeTerm) child).getIndexTerm(tree, maxChildDepth));
		}
		return indexTerm;
	}
	
	protected void attachToParent(IndexTree tree, IndexTerm term,Integer maxChildDepth){
		if (getID()<0){
			//root.add(term);
			return;
		}
		IndexTerm parent = IndexTreeMapper.findIndexTermById(tree.getResults(), ((CodeTermParent<T>) getParent()).getID());
        if (parent == null) {
        	//parent = ((CodeTerm) getParent()).getIndexTerm(root, maxChildDepth);
            //attachToParent(root, this.getParent());
        }
        if (parent != null){
        	parent.add(term);
        }
	}
	
//	public IndexTermData getIndexTermData(){
//		String code ="";
//		String note ="";
//		if (this instanceof DiseasesIndexTerm){
//			Integer id = ((DiseasesIndexTerm) this).getID();
//			//logger.debug("DiseasesIndexTerm id="+id);
//			DiseasesMasterCode dmc = Model.INSTANCE.getDiseasesCodeForID(id);
//			if (dmc!=null){
//				code = dmc.getText();
//			}
//			
//		}
//		IndexTermData data = new IndexTermData(note, code);
//		return data;
//	}
	@Override
	public IndexTermData getIndexTermData(){
		return new IndexTermData(new ArrayList<IndexTermDataNote>(), "", "", -1);
	}

	@Override
	public int getLevel() {
		return 0;
	}

	@Override
	public String getSecondPart() {
		return null;
	}

	@Override
	public String getText() {
		return null;
	}
	
		

}
