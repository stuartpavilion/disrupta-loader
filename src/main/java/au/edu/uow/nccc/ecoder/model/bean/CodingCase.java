
package au.edu.uow.nccc.ecoder.model.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * A <code>CodingCase</code> is a case or episode used in coding and encapsulates the codes identified for the case.
 * 
 * @author jturnbul
 */
public class CodingCase {

	/**
	 * A unique numeric identifier for the case.
	 */
	private final String id;

	/**
	 * A list of the codes identified for this case.
	 */
	private final List<SelectedCode> codes = new ArrayList<>();

	/**
	 * Creates a new <code>CodingCase</code>.
	 * 
	 * @param id The identifier for the case.
	 */
	public CodingCase(final String id) {
		this.id = id;
	}

	/**
	 * Adds a code to the list of codes for this case.
	 * 
	 * @param code The code to be added.
	 */
	public void addCode(final SelectedCode code) {
		codes.add(code);
	}

	/**
	 * Returns the list of selected codes for this case.
	 * 
	 * @return The list of selected codes.
	 */
	public List<SelectedCode> getCodes() {
		return codes;
	}

	/**
	 * Returns the case's identifier.
	 * 
	 * @return The case identifier.
	 */
	public String getID() {
		return id;
	}
}