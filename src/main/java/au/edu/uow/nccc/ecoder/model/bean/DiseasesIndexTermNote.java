package au.edu.uow.nccc.ecoder.model.bean;

import java.util.List;

import au.edu.uow.nccc.hypertree.HyperLink;

/**
 * A <code>DiseasesIndexTermNote</code> refers to a database note associated with a diseases index
 * term.
 * 
 * @author jturnbul
 */
public class DiseasesIndexTermNote {

	/**
	 * A <code>Type</code> enumerates the various types of note.
	 * 
	 * @author jturnbul
	 */
	public enum Type {
		CODE, CODEALSO, CODEAS, CODEBY, CODETO, DAGGER, DIAMOND, G_DR, HASH, MORPHO, NEC, NEM, NOTE, OEM, SEE, SEEALSO;
	}

	/**
	 * A unique identifier for the note.
	 */
	private Integer id;

	/**
	 * The type of the note.
	 */
	private Type type;

	/**
	 * The text of the note.
	 */
	private String text;

	/**
	 * A list of hyper links associated with this note.
	 */
	private List<HyperLink> hyperLinks;

	/**
	 * A heading for the note (may be null).
	 */
	private String heading;

	/**
	 * Returns the note's heading.
	 * 
	 * @return The note's heading.
	 */
	public String getHeading() {
		return heading;
	}

	/**
	 * Returns a list of the note's hyper links.
	 * 
	 * @return The hyper links for this note as a list.
	 */
	public List<HyperLink> getHyperLinks() {
		return hyperLinks;
	}

	/**
	 * Returns the identifier for this note.
	 * 
	 * @return The note's id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Returns the text for this note.
	 * 
	 * @return The note's text.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns the type of this note.
	 * 
	 * @return The note's type.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Sets the notes heading.
	 * 
	 * @param The heading to set.
	 */
	public void setHeading(final String heading) {
		this.heading = heading;
	}

	/**
	 * Sets the note's hyper link list.
	 * 
	 * @param hyperLinks The list of hyper links to be set.
	 */
	public void setHyperLinks(final List<HyperLink> hyperLinks) {
		this.hyperLinks = hyperLinks;
	}

	/**
	 * Sets the note's id.
	 * 
	 * @param id The id to set.
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Sets the note's text.
	 * 
	 * @param text The text to set.
	 */
	public void setText(final String text) {
		this.text = text;
	}

	/**
	 * Sets the note's type.
	 * 
	 * @param type The type to set.
	 */
	public void setType(final Type type) {
		this.type = type;
	}
}