
package au.edu.uow.nccc.ecoder.model.bean;

import au.edu.uow.nccc.ecoder.special.SpecialCharacters;

/**
 * An <code>DiseasesMasterCode</code> corresponds to a code in the ICD-10-AM Tabular List.
 * 
 * @author jturnbul
 */
public class DiseasesMasterCode extends MasterCode {

	/**
	 * A <code>Type</code> enumerates the various types of diseases master code.
	 * 
	 * @author jturnbul
	 */
	public enum Type {
		BLOCK,
		CATEGORY,
		CHAPTER,
		CODE,
		SUBCHAPTER,
		SUBCODE,
		SUPERBLOCK
	}

	/**
	 * A regular expression that matches a chapter code text.
	 */
	public static final String REGEX_CHAPTER_CODE = "(CHAPTER (\\d{1,2}))|(SUBCHAPTER (\\d{1,2}))"; //$NON-NLS-1$

	/**
	 * A regular expression that matches text in the form of a diseases master code.
	 */
	public static final String REGEX_CODE = "([A-Z]((\\d-)|(\\d{2})))((\\.(((\\d-)|(-)|(\\d{1,2}))))?((" //$NON-NLS-1$
	    + SpecialCharacters.DAGGER + "?)|(\\+?)|(\\*?)))?"; //$NON-NLS-1$

	/**
	 * A regular expression that matches text in the form of a code range.
	 */
	public static final String REGEX_CODE_RANGE = "(" + REGEX_CODE + ")(\\s?)(" + SpecialCharacters.EN_DASH + "|" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	    + SpecialCharacters.EM_DASH + "|-)(\\s?)(" + REGEX_CODE + ")"; //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * A regular expression that matches text in the form of an exact code.
	 */
	public static final String REGEX_EXACT_CODE = "(([A-Z]\\d{2})(\\.((\\d{1,2})))?((" + SpecialCharacters.DAGGER //$NON-NLS-1$
	    + "?)|(\\+?)|(\\*?)))|(" + REGEX_CHAPTER_CODE + ")"; //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * A regular expression that matches text in the form of a special code range.
	 */
	public static final String REGEX_SPECIAL_CODE_RANGE = "[A-Z]\\d{1,2}(" + SpecialCharacters.EN_DASH //$NON-NLS-1$
	    + "|-)[A-Z]\\d{1,2} with.*((fourth)|(fifth)) character \\.\\d"; //$NON-NLS-1$

	/**
	 * The type of the code.
	 */
	private Type type;

	/**
	 * True if the code is a dagger code.
	 */
	private boolean dagger;

	/**
	 * True if the code is an asterisk code.
	 */
	private boolean asterisk;

	/**
	 * True if the code is an Australian code.
	 */
	private boolean australian;

	/**
	 * Creates a new <code>DiseasesMasterCode</code>.
	 * 
	 * @param id The identifier for the new code.
	 */
	public DiseasesMasterCode(final int id) {
		super(Category.DISEASES, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see au.edu.uow.nccc.ecoder.model.bean.MasterCode#getDisplay()
	 */
	@Override
	public String getDisplay() {
		final StringBuilder sb = new StringBuilder(getText());
		if (isDagger()) {
			sb.append(SpecialCharacters.DAGGER);
		} else if (isAsterisk()) {
			sb.append("*"); //$NON-NLS-1$
		}

		return sb.toString();
	}

	/**
	 * Returns the type of this code.
	 * 
	 * @return The code's type.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Returns true if this code is an asterisk code.
	 * 
	 * @return True if the code requires an asterisk.
	 */
	public boolean isAsterisk() {
		return asterisk;
	}

	/**
	 * Returns true if this code is an Australian code.
	 * 
	 * @return True if the code is Australian.
	 */
	public boolean isAustralian() {
		return australian;
	}

	/**
	 * Returns true if the code is a dagger code.
	 * 
	 * @return True if the code uses a dagger.
	 */
	public boolean isDagger() {
		return dagger;
	}

	/**
	 * Sets the asterisk status of this code.
	 * 
	 * @param asterisk The asterisk status to set.
	 */
	public void setAsterisk(final boolean asterisk) {
		this.asterisk = asterisk;
	}

	/**
	 * Sets the Australian status of this code.
	 * 
	 * @param australian The Australian status to set.
	 */
	public void setAustralian(final boolean australian) {
		this.australian = australian;
	}

	/**
	 * Sets the dagger status of the code.
	 * 
	 * @param dagger The dagger status to set.
	 */
	public void setDagger(final boolean dagger) {
		this.dagger = dagger;
	}

	/**
	 * Sets the text of the code.
	 * 
	 * @param text The code's text.
	 */
	@Override
	public void setText(final String text) {
		String sanitisedText = text;
		if (sanitisedText.contains("*")) { //$NON-NLS-1$

			// Strip the asterisk and set the asterisk status.
			sanitisedText = sanitisedText.replace("*", ""); //$NON-NLS-1$ //$NON-NLS-2$
			asterisk = true;
		} else if (sanitisedText.contains("+") || sanitisedText.contains(SpecialCharacters.DAGGER)) { //$NON-NLS-1$

			// Strip the dagger and set the dagger status.
			sanitisedText = sanitisedText.replace("+", ""); //$NON-NLS-1$ //$NON-NLS-2$
			sanitisedText = sanitisedText.replace(SpecialCharacters.DAGGER, ""); //$NON-NLS-1$
			dagger = true;
		}
		this.text = sanitisedText;
	}

	/**
	 * Sets the code's type.
	 * 
	 * @param type The type of the code.
	 */
	public void setType(final Type type) {
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("DiseasesMasterCode["); //$NON-NLS-1$
		sb.append("@id:"); //$NON-NLS-1$
		sb.append(id);
		sb.append(",@type:"); //$NON-NLS-1$
		sb.append(type.name());
		sb.append(",@text:"); //$NON-NLS-1$
		sb.append(text);
		sb.append("]"); //$NON-NLS-1$

		return sb.toString();
	}
	
	@Override
	public String getPreferredText(){
		return getFullDisplay();
	}
}