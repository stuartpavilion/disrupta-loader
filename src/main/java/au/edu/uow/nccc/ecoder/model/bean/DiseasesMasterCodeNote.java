/**
 * 
 */
package au.edu.uow.nccc.ecoder.model.bean;

/**
 * A <code>DiseasesMasterCodeNote</code> is a note associated with a diseases master code and is
 * used to build the HTML content for each code.
 * 
 * @author jturnbul
 */
public class DiseasesMasterCodeNote {

	/**
	 * <code>Type</code> enumerates the various types of diseases master code notes.
	 * 
	 * @author jturnbul
	 */
	public enum Type {
		CODEALSO, CODEFIRST, DEF, EXC, INC, IT, NOTE, RANGE, SEE, STANDARD, SUBDIVISION, USEADD
	}

	/**
	 * The numeric identifier for the master code.
	 */
	private final int codeId;

	/**
	 * The type of note.
	 */
	private final Type type;

	/**
	 * The text of the note.
	 */
	private final String text;

	/**
	 * Creates a new <code>DiseasesMasterCodeNote</code>
	 * 
	 * @param codeId The numeric identifier for the master code.
	 * @param type The type of the note.
	 * @param text The text of the note.
	 */
	public DiseasesMasterCodeNote(final int codeId, final Type type, final String text) {
		this.codeId = codeId;
		this.type = type;
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}

		if (!(other instanceof DiseasesMasterCodeNote)) {
			return false;
		}

		final DiseasesMasterCodeNote rhs = (DiseasesMasterCodeNote)other;
		final boolean idEquals = codeId == rhs.codeId;
		final boolean typeEquals = type == rhs.type;
		final boolean textEquals = text.equals(rhs.text);

		return idEquals && typeEquals && textEquals;
	}

	/**
	 * Returns the code id.
	 * 
	 * @return The code id.
	 */
	public int getCodeId() {
		return codeId;
	}

	/**
	 * Returns the text of the note.
	 * 
	 * @return The text of the note.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns the type of the note.
	 * 
	 * @return The type of the note.
	 */
	public Type getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int multiplier = 31;
		int hash = 17;

		hash = hash * multiplier + codeId;
		hash = hash * multiplier + type.hashCode();
		hash = hash * multiplier + text.hashCode();

		return hash;
	}
}