
package au.edu.uow.nccc.ecoder.model.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import au.edu.uow.nccc.ecoder.model.Model;

import com.i_mmerce.disruptalookup.mvc.json.IndexTermData;
import com.i_mmerce.disruptalookup.mvc.json.IndexTermDataNote;

/**
 * An <code>InterventionsIndexTerm</code> corresponds to an individual term in the ACHI Alphabetic Index.
 * 
 * @author jturnbul
 */
public class InterventionsIndexTerm extends CodeTermParentBase<InterventionsIndexTerm>{

	/**
	 * A unique identifier for the term.
	 */
	private int id;

	/**
	 * The parent term for the term (may be null).
	 */
	private InterventionsIndexTerm parent;

	/**
	 * The text of the term. This is not the full, expanded text but the specific text for this particular term.
	 */
	private String text;

	/**
	 * The full text of the term which comprises a concatenation of the text of all ancestral terms and this term.
	 */
	private String fullText;

	/**
	 * The second part of the term which may contain a SEE or SEEALSO reference for example.
	 */
	private String secondPart;

	/**
	 * The level of the term.
	 */
	private int level;

	/**
	 * A list of all the notes associated with this term.
	 */
	private final List<InterventionsIndexTermNote> notes = new ArrayList<>();

	/**
	 * Creates a new <code>InterventionsIndexTerm</code>.
	 * 
	 * @param id The identifier for the new term.
	 */
	public InterventionsIndexTerm(final int id) {
		this.id = id;
	}

	/**
	 * Adds a child term to this term.
	 * 
	 * @param child The child term.
	 */
	public void addChild(final InterventionsIndexTerm child) {
		children.add(child);
	}

	/**
	 * Associates a note with this term.
	 * 
	 * @param note The note to be added.
	 */
	public void addNote(final InterventionsIndexTermNote note) {
		notes.add(note);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof InterventionsIndexTerm)) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		final InterventionsIndexTerm rhs = (InterventionsIndexTerm)obj;

		// Terms are considered equal if and only if they have the same identifier.
		return id == rhs.id;
	}

	/**
	 * Returns the full text of this term.
	 * 
	 * @return The term's full text.
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Returns the identifier for this term.
	 * 
	 * @return The term's identifier.
	 */
	public Integer getID() {
		return id;
	}

	/**
	 * Returns the level for this term.
	 * 
	 * @return The term's level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Returns the notes associated with this term.
	 * 
	 * @return The list of linked notes.
	 */
	public List<InterventionsIndexTermNote> getNotes() {
		return notes;
	}

	/**
	 * Returns the parent term for this term.
	 * 
	 * @return The term's parent.
	 */
	public InterventionsIndexTerm getParent() {
		return parent;
	}

	/**
	 * Returns the second part of this term.
	 * 
	 * @return The term's second part.
	 */
	public String getSecondPart() {
		return secondPart;
	}

	/**
	 * Returns the specific text of this term.
	 * 
	 * @return The term's specific text.
	 */
	public String getText() {
		return text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		// Simply use the identifier as the hash code to match the equals() method.
		return id;
	}

	/**
	 * Returns true if the term is a lead term.
	 * 
	 * @return true if the term is a lead term.
	 */
	public boolean isLeadTerm() {
		return level == 1;
	}

	/**
	 * Sets the full text of the term.
	 * 
	 * @param fullText The full text.
	 */
	public void setFullText(final String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Set's the ID of the term.
	 * 
	 * @param id The term's ID.
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Set's the level of the term.
	 * 
	 * @param level The term's level.
	 */
	public void setLevel(final int level) {
		this.level = level;
	}

	/**
	 * Sets the parent of the term.
	 * 
	 * @param parent The term's parent.
	 */
	public void setParent(final InterventionsIndexTerm parent) {
		this.parent = parent;
	}

	/**
	 * Sets the second part of the term.
	 * 
	 * @param secondPart The term's second part.
	 */
	public void setSecondPart(final String secondPart) {
		this.secondPart = secondPart;
	}

	/**
	 * Sets the text of the term.
	 * 
	 * @param text The term's text.
	 */
	public void setText(final String text) {
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("InterventionsIndexTerm["); //$NON-NLS-1$
		sb.append("@id:"); //$NON-NLS-1$
		sb.append(id);
		sb.append(",@text:"); //$NON-NLS-1$
		sb.append(text);
		sb.append(",@fullText:"); //$NON-NLS-1$
		sb.append(fullText);
		sb.append(",@isLeadTerm():"); //$NON-NLS-1$
		sb.append(isLeadTerm());
		sb.append("]"); //$NON-NLS-1$

		return sb.toString();
	}
	
	@Override
	public String getPreferredText(){
		return getFullText();
	}
	
	@Override
	public IndexTermData getIndexTermData(){
		String code = "";
        ArrayList<IndexTermDataNote> otherNotes = new ArrayList<>();

		for (InterventionsIndexTermNote note: notes){
          if (note.getType() == InterventionsIndexTermNote.Type.CODE) {
              code = note.getText();
              if (code.contains("[")) {
                  code = code.split("\\[")[0];
              }
          } else {
			otherNotes.add(new IndexTermDataNote(note.getType().toString(), note.getText()));
          }
		}
		IndexTermData data = new IndexTermData(otherNotes, code, "achi", id);
		return data;
	}
}