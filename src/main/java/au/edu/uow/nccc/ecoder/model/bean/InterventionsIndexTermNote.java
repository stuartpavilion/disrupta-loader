package au.edu.uow.nccc.ecoder.model.bean;

import java.util.List;

import au.edu.uow.nccc.hypertree.HyperLink;

/**
 * A <code>true if the term is a lead term</code> refers to a note associated with an ACHI
 * Alphabetic Index term.
 * 
 * @author jturnbul
 */
public class InterventionsIndexTermNote {

	/**
	 * <code>Type</code> enumerates the various types of note.
	 * 
	 * @author jturnbul
	 */
	public enum Type {
		BLOCK, CODE, CODETO, NEC, NEM, OEM, OMIT, SEE, SEEALSO, SEEBLOCK;
	}

	/**
	 * A numeric identifier for the note.
	 */
	private Integer id;

	/**
	 * The type of the note.
	 */
	private Type type;

	/**
	 * The text of the note.
	 */
	private String text;

	/**
	 * A list of hyper links associated with this note.
	 */
	private List<HyperLink> hyperLinks;

	/**
	 * The heading associated with this note (may be null).
	 */
	private String heading;

	/**
	 * Returns the heading for the note.
	 * 
	 * @return The note's heading.
	 */
	public String getHeading() {
		return heading;
	}

	/**
	 * Returns the hyper links for this note.
	 * 
	 * @return A list of the note's hyper links.
	 */
	public List<HyperLink> getHyperLinks() {
		return hyperLinks;
	}

	/**
	 * Returns the id for the note.
	 * 
	 * @return The note's id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Returns the text for the note.
	 * 
	 * @return The note's text.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns the type for the note.
	 * 
	 * @return The note's type.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Sets the heading for the note.
	 * 
	 * @param heading The note's heading.
	 */
	public void setHeading(final String heading) {
		this.heading = heading;
	}

	/**
	 * Sets the hyper links for the note.
	 * 
	 * @param hyperLinks A list of the note's hyper links.
	 */
	public void setHyperLinks(final List<HyperLink> hyperLinks) {
		this.hyperLinks = hyperLinks;
	}

	/**
	 * Sets the notes id.
	 * 
	 * @param id The id to be set.
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * Sets the note's text.
	 * 
	 * @param text The text to be set.
	 */
	public void setText(final String text) {
		this.text = text;
	}

	/**
	 * Sets the note's type.
	 * 
	 * @param type The type to set.
	 */
	public void setType(final Type type) {
		this.type = type;
	}
}