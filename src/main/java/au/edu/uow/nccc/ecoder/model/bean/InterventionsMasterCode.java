/**
 * 
 */

package au.edu.uow.nccc.ecoder.model.bean;

/**
 * An <code>InterventionsMasterCode</code> corresponds to a code in the ACHI Tabular List.
 * 
 * @author jturnbul
 */
public class InterventionsMasterCode extends MasterCode {

	/**
	 * <code>Type</code> enumerates the various types of interventions master code.
	 * 
	 * @author jturnbul
	 */
	public enum Type {
		BLOCK,
		CHAPTER,
		CODE,
		MAJOR,
		MINOR
	}

	/**
	 * A regular expression that matches a code group.
	 */
	public static final String REGEX_CODE_GROUP = "\\d{5}"; //$NON-NLS-1$

	/**
	 * A regular expression that matches a block specification.
	 */
	public static final String REGEX_BLOCK = "\\[\\d+\\]"; //$NON-NLS-1$

	/**
	 * A regular expression that matches a special block specifier.
	 */
	public static final String REGEX_BLOCK_SPECIAL = REGEX_BLOCK + " with extension \\-\\d{2}"; //$NON-NLS-1$

	/**
	 * A regular expression that matches a block range specification.
	 */
	public static final String REGEX_BLOCK_RANGE = "(" + REGEX_BLOCK + ")( ((to)|(and)|(or)) )(" + REGEX_BLOCK + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

	/**
	 * A regular expression that matches a fully specified code.
	 */
	public static final String REGEX_CODE_SPECIFIC = REGEX_CODE_GROUP + "\\-\\d{2}"; //$NON-NLS-1$

	/**
	 * A regular expression that matches a code with a generic extension.
	 */
	public static final String REGEX_CODE_GENERIC = REGEX_CODE_GROUP + "\\-XX"; //$NON-NLS-1$

	/**
	 * A regular expression that matches an interventions master code (except a block specifier).
	 */
	public static final String REGEX_CODE = "(" + REGEX_CODE_GROUP + ")|(" + REGEX_CODE_SPECIFIC + ")|(" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	    + REGEX_CODE_GENERIC + ")"; //$NON-NLS-1$

	/**
	 * The type of the code.
	 */
	private Type type;

	/**
	 * The fully qualified text of the master code. Includes all ancestral codes separated by a slash '/'.
	 */
	private String fullyQualifiedText;

	/**
	 * Creates a new <code>InterventionsMasterCode</code>.
	 * 
	 * @param id The identifier for the new code.
	 */
	public InterventionsMasterCode(final int id) {
		super(Category.INTERVENTIONS, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see au.edu.uow.nccc.ecoder.model.bean.MasterCode#getDisplay()
	 */
	@Override
	public String getDisplay() {
		return type == Type.BLOCK ? "Block [" + text + "]" : text; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Returns the fully qualified text for the code.
	 * 
	 * @return the The fully qualified text.
	 */
	public String getFullyQualifiedText() {
		return fullyQualifiedText;
	}

	/**
	 * Returns the type of this code.
	 * 
	 * @return The code's type.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Sets the code's fully qualified text.
	 * 
	 * @param fullyQualifiedText The fully qualified text to set.
	 */
	public void setFullyQualifiedText(final String fullyQualifiedText) {
		this.fullyQualifiedText = fullyQualifiedText;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see au.edu.uow.nccc.ecoder.model.bean.MasterCode#setText(java.lang.String)
	 */
	@Override
	public void setText(final String text) {
		this.text = text;
	}

	/**
	 * Sets the code's type.
	 * 
	 * @param type The type of the code.
	 */
	public void setType(final Type type) {
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("InterventionsMasterCode["); //$NON-NLS-1$
		sb.append("@id:"); //$NON-NLS-1$
		sb.append(id);
		sb.append(",@type:"); //$NON-NLS-1$
		sb.append(type.name());
		sb.append(",@text:"); //$NON-NLS-1$
		sb.append(text);
		sb.append("]"); //$NON-NLS-1$

		return sb.toString();
	}
	
	@Override
	public String getPreferredText(){
		return getFullDisplay();
	}	
}