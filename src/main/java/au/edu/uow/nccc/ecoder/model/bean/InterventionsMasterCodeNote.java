package au.edu.uow.nccc.ecoder.model.bean;

/**
 * A <code>InterventionsMasterCodeNote</code> refers to a note associated with an interventions
 * master code.
 * 
 * @author jturnbul
 */
public class InterventionsMasterCodeNote {

	/**
	 * <code>Type</code> enumerates the various types of note.
	 * 
	 * @author jturnbul
	 */
	public enum Type {
		CODEALSO, CODEFIRST, CODEPERFORM, DEF, EXC, INC, IT, NOTE, SEE, STANDARD
	}

	/**
	 * The numeric identifier for the code.
	 */
	private final int codeId;

	/**
	 * The type of the note.
	 */
	private final Type type;

	/**
	 * The text of the note.
	 */
	private final String text;

	/**
	 * Creates a new <code>InterventionsMasterCodeNote</code>.
	 * 
	 * @param codeId The numeric identifier for the code.
	 * @param type The type of the note.
	 * @param text The text of the note.
	 */
	public InterventionsMasterCodeNote(final int codeId, final Type type, final String text) {
		this.codeId = codeId;
		this.type = type;
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}

		if (!(other instanceof InterventionsMasterCodeNote)) {
			return false;
		}

		final InterventionsMasterCodeNote rhs = (InterventionsMasterCodeNote)other;
		final boolean idEquals = codeId == rhs.codeId;
		final boolean typeEquals = type == rhs.type;
		final boolean textEquals = text.equals(rhs.text);

		return idEquals && typeEquals && textEquals;
	}

	/**
	 * Returns the code's numeric identifier.
	 * 
	 * @return The numeric id for the code.
	 */
	public int getCodeId() {
		return codeId;
	}

	/**
	 * Returns the text of the note.
	 * 
	 * @return The note's text.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns the type of the note.
	 * 
	 * @return The note's type.
	 */
	public Type getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int multiplier = 31;
		int hash = 17;

		hash = hash * multiplier + codeId;
		hash = hash * multiplier + type.hashCode();
		hash = hash * multiplier + text.hashCode();

		return hash;
	}
}