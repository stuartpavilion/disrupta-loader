
package au.edu.uow.nccc.ecoder.model.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * An <code>MasterCode</code> corresponds to a code in the ICD-10-AM or ACHI Tabular List.
 * 
 * @author jturnbul
 */
public abstract class MasterCode extends CodeTermParentBase<MasterCode> implements Comparable<MasterCode>{

	/**
	 * <code>Category</code> encapsulates the categories of master code.
	 * 
	 * @author jturnbul
	 */
	public enum Category {
		DISEASES,
		INTERVENTIONS
	}

	/**
	 * The name of the CSS class that is used to indicate that no notes exist for this code in the HTML notes content.
	 */
	public static final String NO_CODE_NOTES_CSS_CLASS_NAME = "NoCodeNotes"; //$NON-NLS-1$

	/**
	 * The code's category.
	 */
	private final Category category;

	/**
	 * A unique identifier for the code.
	 */
	protected int id;

	/**
	 * The unique key as used in the source database to identify the code.
	 */
	protected String key;

	/**
	 * The unique key of the parent code as used in the source database to identify a code.
	 */
	protected String parentKey;

	/**
	 * The parent code for the code (may be null).
	 */
	protected MasterCode parent;

	/**
	 * The text of the code.
	 */
	protected String text;

	/**
	 * The description of the code.
	 */
	protected String description;

	/**
	 * The level of the code in the hierarchy.
	 */
	protected int level;

	/**
	 * The notes for this code as an HTML snippet.
	 */
	protected String notes;

	/**
	 * An HTML snippet used to store any tags that belong in the <head> section of the HTML content. This is usually the
	 * style section(s).
	 */
	protected String head;

	/**
	 * A flag indicating whether the code is valid for coding.
	 */
	protected boolean valid;

	/**
	 * Creates a new <code>MasterCode</code>.
	 * 
	 * @param category The category for the new code.
	 * @param id The identifier for the new code.
	 */
	public MasterCode(final Category category, final int id) {
		this.category = category;
		this.id = id;
	}

	/**
	 * Adds a child code to this code.
	 * 
	 * @param child The child code.
	 */
	public void addChild(final MasterCode child) {
		children.add(child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final MasterCode code) {
		return text.compareTo(code.text);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof MasterCode)) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		final MasterCode rhs = (MasterCode)obj;

		// Codes are considered equal if and only if they have the same category and identifier.
		final boolean catEqual = category == rhs.category;
		final boolean idEqual = id == rhs.id;

		return catEqual && idEqual;
	}

	/**
	 * Returns the code's category.
	 * 
	 * @return The code's category.
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * Returns the description of this code.
	 * 
	 * @return The code's description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns a string representation of this code's text suitable for display.
	 * 
	 * @return The text of this code as a string for display..
	 */
	public abstract String getDisplay();

	/**
	 * Returns a string representation of this code suitable for display and includes the code's description.
	 * 
	 * @return A string representation of this code suitable for display .
	 */
	public String getFullDisplay() {
		return getText().equals(getDescription()) ? getDisplay() : getDisplay() + " - " + getDescription(); //$NON-NLS-1$
	}

	/**
	 * Returns the head section of the code's HTML notes content.
	 * 
	 * @return The code's head section.
	 */
	public String getHead() {
		return head;
	}

	/**
	 * Returns the identifier for this code.
	 * 
	 * @return The code's identifier.
	 */
	public Integer getID() {
		return id;
	}

	/**
	 * Returns the code's key string identifier from the source database.
	 * 
	 * @return The code's key string identifier from the source database.
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Returns the code's level in the hierarchy.
	 * 
	 * @return The code's level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Returns the notes for this code.
	 * 
	 * @return The code's notes.
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Returns the parent code for this code.
	 * 
	 * @return The code's parent.
	 */
	public MasterCode getParent() {
		return parent;
	}

	/**
	 * Returns the parent key for this code.
	 * 
	 * @return The code's parent key.
	 */
	public String getParentKey() {
		return parentKey;
	}

	/**
	 * Returns the specific text of this code.
	 * 
	 * @return The code's specific text.
	 */
	public String getText() {
		return text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int multiplier = 31;
		int hash = 17;

		hash = multiplier * hash + category.hashCode();
		hash = multiplier * hash + id;

		return hash;
	}

	/**
	 * Returns true if the HTML notes content for this code actually contains something useful.
	 * 
	 * @return True if notes exist for this code.
	 */
	public boolean hasNotes() {
		return notes != null && !notes.contains("class=" + NO_CODE_NOTES_CSS_CLASS_NAME); //$NON-NLS-1$
	}

	/**
	 * Returns true if this code is valid.
	 * 
	 * @return True if this code is valid.
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * Sets the description of the code.
	 * 
	 * @param description The description.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Sets the head section of the code.
	 * 
	 * @param head The head section to be set.
	 */
	public void setHead(final String head) {
		this.head = head;
	}

	/**
	 * Sets the ID of the code.
	 * 
	 * @param id The code's ID.
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Sets the key identifier of the code.
	 * 
	 * @param key The key to set.
	 */
	public void setKey(final String key) {
		this.key = key;
	}

	/**
	 * Sets the level of the code.
	 * 
	 * @param level The level to set.
	 */
	public void setLevel(final int level) {
		this.level = level;
	}

	/**
	 * Sets the notes for this code.
	 * 
	 * @param The notes (as an HTML snippet).
	 */
	public void setNotes(final String notes) {
		this.notes = notes;
	}

	/**
	 * Sets the parent of the code.
	 * 
	 * @param parent The code's parent.
	 */
	public void setParent(final MasterCode parent) {
		this.parent = parent;
	}

	/**
	 * Sets the parent key of the code.
	 * 
	 * @param parentKey The parent key to set.
	 */
	public void setParentKey(final String parentKey) {
		this.parentKey = parentKey;
	}

	/**
	 * Sets the text of the code.
	 * 
	 * @param text The code's text.
	 */
	public abstract void setText(final String text);

	/**
	 * Sets the valid status of the code.
	 * 
	 * @param valid The valid status to set.
	 */
	public void setValid(final boolean valid) {
		this.valid = valid;
	}
}