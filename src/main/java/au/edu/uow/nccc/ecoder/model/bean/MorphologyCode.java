
package au.edu.uow.nccc.ecoder.model.bean;

/**
 * An <code>MorphologyCode</code> corresponds to a morphology code in the NCCC code classification system.
 * 
 * @author jturnbul
 */
public class MorphologyCode {

	/**
	 * A regular expression that matches a non-specific morphology code.
	 */
	public static final String REGEX_CODE = "(M\\d{4})(\\/\\d)?"; //$NON-NLS-1$

	/**
	 * A regular expression that matches a specific morphology code.
	 */
	public static final String REGEX_SPECIFIC_CODE = "M\\d{4}\\/\\d"; //$NON-NLS-1$

	/**
	 * The text of the code.
	 */
	private String text;

	/**
	 * The description of the code.
	 */
	private String description;

	/**
	 * The HTML notes of the code.
	 */
	private String notes;

	/**
	 * Returns the code's description.
	 * 
	 * @return The code's description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the code's notes.
	 * 
	 * @return The code's notes.
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Returns the code's text.
	 * 
	 * @return The code's text.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the code's description.
	 * 
	 * @param description The description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Sets the code's notes.
	 * 
	 * @param notes The notes to set.
	 */
	public void setNotes(final String notes) {
		this.notes = notes;
	}

	/**
	 * Sets the code's text.
	 * 
	 * @param text The text to set.
	 */
	public void setText(final String text) {
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("DiseasesMasterCode["); //$NON-NLS-1$
		sb.append("@text:"); //$NON-NLS-1$
		sb.append(text);
		sb.append(",@description:"); //$NON-NLS-1$
		sb.append(description);
		sb.append("]"); //$NON-NLS-1$

		return sb.toString();
	}
}