
package au.edu.uow.nccc.ecoder.model.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * An <code>Reference</code> corresponds to
 * 
 * @author jturnbul
 */
public class Reference extends CodeTermParentBase<Reference> implements Comparable<Reference>{

	/**
	 * The name of the CSS class which is used to indicate that no actual notes exist for the reference.
	 */
	public static final String NO_reference_NOTES_CSS_CLASS_NAME = "NoreferenceNotes"; //$NON-NLS-1$

	/**
	 * A unique identifier for the reference.
	 */
	private int id;

	/**
	 * The parent reference for the reference (may be null).
	 */
	private Reference parent;

	/**
	 * The name of the reference name.
	 */
	private String name;

	/**
	 * The notes for this reference as an HTML snippet.
	 */
	private String notes;

	/**
	 * The <head> region of the reference's notes as an HTML snippet. This usually contains style definitions.
	 */
	private String head;

	/**
	 * Creates a new <code>Reference</code>.
	 * 
	 * @param id The identifier for the new reference.
	 */
	public Reference(final int id) {
		this.id = id;
	}

	/**
	 * Adds a child reference to this reference.
	 * 
	 * @param child The child reference.
	 */
	public void addChild(final Reference child) {
		children.add(child);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final Reference reference) {
		return name.compareTo(reference.name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Reference)) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		final Reference rhs = (Reference)obj;

		// References are considered equal if and only if they have the same identifier.
		return id == rhs.id;
	}

	/**
	 * Returns the name of this reference formatted for display. This method is provided for compatibility with
	 * <code>MasterCode</code> implementations which also define a similar method.
	 * 
	 * @return The name of the reference formatted for display.
	 */
	public String getDisplay() {
		//return name.startsWith("Volume") ? name : "Volume " + name.substring(3); //$NON-NLS-1$ //$NON-NLS-2$
		return name;
	}
	
	/**
	 * 
	 * @return The name of the reference formatted for display the text.
	 */
	public String getDisplayForTitle() {
		return name.substring(8); //$NON-NLS-1$ //$NON-NLS-2$
		
	}

	/**
	 * Returns the <head> HTML snippet for this reference.
	 * 
	 * @return The head HTML snippet.
	 */
	public String getHead() {
		return head;
	}

	/**
	 * Returns the identifier for this reference.
	 * 
	 * @return The reference's identifier.
	 */
	public Integer getID() {
		return id;
	}

	/**
	 * Returns the specific name of this reference.
	 * 
	 * @return The reference's specific name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the notes for this reference.
	 * 
	 * @return The reference's notes.
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Returns the parent reference for this reference.
	 * 
	 * @return The reference's parent.
	 */
	public Reference getParent() {
		return parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		// Simply use the identifier as the hash code to match the equals() method.
		return id;
	}

	/**
	 * Returns true if notes exist for this reference.
	 * 
	 * @return True if notes exist for this reference.
	 */
	public boolean hasNotes() {
		return notes != null;
	}

	/**
	 * Sets the head for this reference.
	 * 
	 * @param head The head.
	 */
	public void setHead(final String head) {
		this.head = head;
	}

	/**
	 * Sets the ID of the reference.
	 * 
	 * @param id The reference's ID.
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Sets the name of the reference.
	 * 
	 * @param name The reference's name.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Set the notes for this reference.
	 * 
	 * @param The notes (as an HTML snippet).
	 */
	public void setNotes(final String notes) {
		this.notes = notes;
	}

	/**
	 * Sets the parent of the reference.
	 * 
	 * @param parent The reference's parent.
	 */
	public void setParent(final Reference parent) {
		this.parent = parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Reference["); //$NON-NLS-1$
		sb.append("@id:"); //$NON-NLS-1$
		sb.append(id);
		sb.append(",@name:"); //$NON-NLS-1$
		sb.append(name);
		sb.append("]"); //$NON-NLS-1$

		return sb.toString();
	}
	
	@Override
	public String getPreferredText(){
		return getDisplay();
	}	
}