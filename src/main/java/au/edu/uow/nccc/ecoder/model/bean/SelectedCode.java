
package au.edu.uow.nccc.ecoder.model.bean;

/**
 * A <code>SelectedCode</code> represents any type of code that is selected by the coder during the usage of CodeXpert.
 * 
 * @author jturnbul
 */
public class SelectedCode {

	/**
	 * A unique numeric identifier for the selected code.
	 * 
	 * NB. This is NOT the same as the id of the encapsulated code.
	 */
	private int id;

	/**
	 * The corresponding code.
	 */
	private final Object code;

	/**
	 * Any notes associated with selection of this code.
	 */
	private String notes;

	/**
	 * Creates a new <code>SelectedCode</code>.
	 * 
	 * @param code The corresponding code that has been selected.
	 */
	public SelectedCode(final Object code) {
		this.code = code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object o) {
		if (!(o instanceof SelectedCode)) {
			return false;
		}

		if (this == o) {
			return true;
		}

		// Equals if and only if the ids are the same.
		return id == ((SelectedCode)o).id;
	}

	/**
	 * Returns the code associated with this selected code.
	 * 
	 * @return The corresponding code.
	 */
	public Object getCode() {
		return code;
	}

	/**
	 * Returns the description field of the corresponding code.
	 * 
	 * @return The corresponding code's description.
	 */
	public String getCodeDescription() {
		String text = null;
		if (code instanceof InterventionsMasterCode) {
			final InterventionsMasterCode imc = (InterventionsMasterCode)code;
			text = imc.getDescription();
		} else if (code instanceof DiseasesMasterCode) {
			final DiseasesMasterCode dmc = (DiseasesMasterCode)code;
			text = dmc.getDescription();
		} else if (code instanceof MorphologyCode) {
			final MorphologyCode mc = (MorphologyCode)code;
			text = mc.getDescription();
		}

		return text;
	}

	/**
	 * Returns the text field of the corresponding code.
	 * 
	 * @return The corresponding code's text.
	 */
	public String getCodeText() {
		String text = null;
		if (code instanceof InterventionsMasterCode) {
			final InterventionsMasterCode imc = (InterventionsMasterCode)code;
			text = imc.getDisplay() + " [" + imc.getParent().getText() + "]"; //$NON-NLS-1$ //$NON-NLS-2$
		} else if (code instanceof DiseasesMasterCode) {
			final DiseasesMasterCode dmc = (DiseasesMasterCode)code;
			text = dmc.getDisplay();
		} else if (code instanceof MorphologyCode) {
			final MorphologyCode mc = (MorphologyCode)code;
			text = mc.getText();
		}

		return text;
	}

	/**
	 * Returns the selected code's identifier.
	 * 
	 * @return The selected code's id.
	 */
	public int getID() {
		return id;
	}

	/**
	 * Returns the selected code's notes.
	 * 
	 * @return The selected code's notes.
	 */
	public String getNotes() {
		return notes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		// Simply use the identifier.
		return id;
	}

	/**
	 * Sets the numeric identifier of the selected code.
	 * 
	 * @param id The id to set.
	 */
	public void setID(final int id) {
		this.id = id;
	}

	/**
	 * Sets the notes of the selected code.
	 * 
	 * @param notes The notes to set.
	 */
	public void setNotes(final String notes) {
		this.notes = notes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("InterventionsMasterCode["); //$NON-NLS-1$
		sb.append("@id:"); //$NON-NLS-1$
		sb.append(id);
		sb.append(",@code:"); //$NON-NLS-1$
		sb.append(code.toString());
		sb.append("]"); //$NON-NLS-1$

		return sb.toString();
	}
}