
package au.edu.uow.nccc.ecoder.model.bean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A <code>YearMonth</code> represents a combination of a year and month and is used to classify advice items.
 * 
 * @author jturnbul
 */
public class YearMonth implements Comparable<YearMonth> {

	/**
	 * The year.
	 */
	private final int year;

	/**
	 * The month number (1-12).
	 */
	private final int month;

	/**
	 * A string representation of this object suitable for display.
	 */
	private String display;

	/**
	 * Creates a new <code>YearMonth</code> from the given date.
	 * 
	 * @param date The date to use.
	 */
	public YearMonth(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		// Extract the year and month from the date.
		year = cal.get(Calendar.YEAR);
		month = cal.get(Calendar.MONTH) + 1;

		// Set the display field.
		setDisplay(date);
	}

	/**
	 * Creates a new <code>YearMonth</code> from an explicit year and month.
	 * 
	 * @param year The year to use.
	 * @param month The month number to use.
	 */
	public YearMonth(final int year, final int month) {
		this.year = year;
		this.month = month;

		// Create a new Date object based on the given year and month.
		final DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); //$NON-NLS-1$
		Date date = null;
		try {
			final String input = "1/" + month + "/" + year; //$NON-NLS-1$ //$NON-NLS-2$
			date = df.parse(input);
		} catch (final ParseException e) {
			e.printStackTrace();
		}

		setDisplay(date);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(final YearMonth ym) {
		return year == ym.year ? month > ym.month ? -1 : month < ym.month ? 1 : 0 : year > ym.year ? -1
		    : year < ym.year ? 1 : 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final YearMonth other = (YearMonth)obj;
		if (month != other.month) {
			return false;
		}
		if (year != other.year) {
			return false;
		}

		return true;
	}

	/**
	 * Returns a string representation of this object suitable for display.
	 * 
	 * @return A string representation of this object suitable for display.
	 */
	public String getDisplay() {
		return display;
	}

	/**
	 * Returns the month.
	 * 
	 * @return The month number.
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * Returns the year.
	 * 
	 * @return The year.
	 */
	public int getYear() {
		return year;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 17;

		result = prime * result + month;
		result = prime * result + year;

		return result;
	}

	/**
	 * Sets the display string for this object based on a specified date.
	 * 
	 * @param date The date to derive the display string from.
	 */
	private void setDisplay(final Date date) {
		display = String.format("%1$tB - %1$tY", date); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getDisplay();
	}
}