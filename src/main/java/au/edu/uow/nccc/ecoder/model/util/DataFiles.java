
package au.edu.uow.nccc.ecoder.model.util;

/**
 * A <code>DataFiles</code> is .
 * 
 */
public class DataFiles {

	public static final String SEPARATOR_FIELD = "^"; //$NON-NLS-1$

	public static final String EXTENSION = "data"; //$NON-NLS-1$

	public static final int BUFFER_SIZE = 32_000_000;

	public static final String FILE_NAME_DISEASES_INDEX = "001"; //$NON-NLS-1$

	public static final String FILE_NAME_DISEASES_TABULAR = "002"; //$NON-NLS-1$

	public static final String FILE_NAME_INTERVENTIONS_INDEX = "003"; //$NON-NLS-1$

	public static final String FILE_NAME_INTERVENTIONS_TABULAR = "004"; //$NON-NLS-1$

	public static final String FILE_NAME_ACS = "005"; //$NON-NLS-1$

	public static final String FILE_NAME_ADVICE = "006"; //$NON-NLS-1$

	public static final String FILE_NAME_MORPHOLOGY = "007"; //$NON-NLS-1$

	public static final String FILE_NAME_DISEASES_EDITS = "008"; //$NON-NLS-1$

	public static final String FILE_NAME_INTERVENTIONS_EDITS = "009"; //$NON-NLS-1$

	public static final String FILE_NAME_LATIN_WORDS = "010"; //$NON-NLS-1$

	public static final String FILE_NAME_LATIN_PHRASES = "011"; //$NON-NLS-1$

	public static final String FILE_NAME_REFERENCES = "012"; //$NON-NLS-1$
}