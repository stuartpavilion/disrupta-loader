
package au.edu.uow.nccc.ecoder.model.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import au.edu.uow.nccc.ecoder.model.bean.DiseasesMasterCode;
import au.edu.uow.nccc.ecoder.model.bean.MasterCode;
import au.edu.uow.nccc.ecoder.special.SpecialCharacters;

/**
 * <code>DiseasesMasterCodeUtils</code> is a utility class that provides static methods for manipulating diseases master
 * codes.
 * 
 * @author jturnbul
 */
public class DiseasesMasterCodeUtils {

	/**
	 * A pre-compiled pattern for matching diseases master codes.
	 */
	private static final Pattern CODE_PATTERN = Pattern.compile(DiseasesMasterCode.REGEX_CODE);

	/**
	 * A pre-compiled pattern for matching diseases master code ranges.
	 */
	private static final Pattern CODE_RANGE_PATTERN = Pattern.compile(DiseasesMasterCode.REGEX_CODE_RANGE);

	/**
	 * A pre-compiled pattern for matching special diseases master code ranges.
	 */
	private static final Pattern SPECIAL_CODE_RANGE_PATTERN = Pattern
	    .compile(DiseasesMasterCode.REGEX_SPECIAL_CODE_RANGE);

	/**
	 * A pre-compiled pattern for matching exact diseases master codes.
	 */
	private static final Pattern EXACT_CODE_PATTERN = Pattern.compile(DiseasesMasterCode.REGEX_EXACT_CODE);

	/**
	 * A pre-compiled pattern for matching diseases chapter codes.
	 */
	private static final Pattern CHAPTER_CODE_PATTERN = Pattern.compile(DiseasesMasterCode.REGEX_CHAPTER_CODE);

	/**
	 * A pre-compiled pattern for matching special diseases master code expressions.
	 */
	private static final Pattern SPECIAL_EXPRESSION_PATTERN = Pattern.compile(".*with.*(fourth|fifth) character.*"); //$NON-NLS-1$

	/**
	 * Returns true if two diseases master codes are siblings.
	 * 
	 * @param a The first diseases master code to compare.
	 * @param b The second diseases master code to compare.
	 * @return True if the two codes are siblings.
	 */
	private static boolean areSiblings(final DiseasesMasterCode a, final DiseasesMasterCode b) {
		return a.getParent() == null ? b.getParent() == null : a.getParent() == b.getParent();
	}

	/**
	 * Collapses a list of codes to the most concise representation of the list as possible. This is done in several
	 * passes where each pass identifies sets of siblings that completely represent their parent code and replaces them
	 * with their parent code until there are no more complete sibling sets remaining.
	 * 
	 * @param codes The list of codes to collapse which must be in classification order.
	 */
	private static void collapseList(final List<DiseasesMasterCode> codes) {

		// Nothing to do with list of size 1.
		if (codes.size() == 1) {
			return;
		}

		boolean changed = false;
		do {
			int index = 0;
			changed = false;
			do {
				// Loop through the list identifying adjacent siblings.
				final List<DiseasesMasterCode> siblings = new ArrayList<>();
				final DiseasesMasterCode start = codes.get(index);
				for (; index < codes.size() && areSiblings(start, codes.get(index)); index++) {
					siblings.add(codes.get(index));
				}

				// If the siblings are the complete set of all children for their parent then
				// replace them with the parent.
				final MasterCode parent = start.getParent();
				if (parent != null && parent.getChildren().equals(siblings) && codes.size() > 1) {
					codes.removeAll(siblings);
					index -= siblings.size();
					codes.add(index, (DiseasesMasterCode)parent);
					changed = true;
					++index;
				}
			} while (index < codes.size());
		} while (changed);

		// Now remove any duplicates.
		for (int index = 0; index < codes.size() - 1;) {
			if (codes.get(index).equals(codes.get(index + 1))) {
				codes.remove(index + 1);
			} else {
				++index;
			}
		}
	}

	/**
	 * Fully expands a code so that the resulting collection includes the code itself and all its descendants.
	 * 
	 * @param code The code to expand.
	 * @return A list containing the code itself and all its descendants in classification order.
	 */
	public static List<DiseasesMasterCode> expandCode(final DiseasesMasterCode code) {
		final List<DiseasesMasterCode> codes = new ArrayList<>();
		codes.add(code);
		for (final MasterCode child : code.getChildren()) {
			codes.addAll(expandCode((DiseasesMasterCode)child));
		}

		return codes;
	}

	/**
	 * Finds the first code in the classification that matches the specified expression.
	 * 
	 * @param codeList A list of codes in classification order.
	 * @param expression The expression to match.
	 * @return The first matching code.
	 */
	private static DiseasesMasterCode
	    firstMatchingCode(final List<DiseasesMasterCode> codeList, final String expression) {

		// Convert the expression into a valid regular expression.
		String regex = expression.replace(".", "\\."); //$NON-NLS-1$ //$NON-NLS-2$
		regex = regex.replace("-", "."); //$NON-NLS-1$ //$NON-NLS-2$

		// Loop through the classification returning the first matching code.
		for (final DiseasesMasterCode code : codeList) {
			if (Pattern.matches(regex, code.getText())) {
				return code;
			}
		}

		// No code matched the expression so return null.
		return null;
	}

	/**
	 * Takes an expression and returns a list of codes that match that expression, optionally collapsing the list if
	 * required.
	 * 
	 * @param codeList The list of codes in the classification. This is passed in as an argument to permit the use of
	 *            this method in the CodeXpert and the DB Loader.
	 * @param codesByText A map of codes by code text. This is passed in as an argument to permit the use of this method
	 *            in the CodeXpert and the DB Loader.
	 * @param expression The expression to convert into a list of codes.
	 * @param collapse True if the list is to be collapsed.
	 * @return A list of matching codes which may be collapsed if specified.
	 */
	public static List<DiseasesMasterCode> getCodesForExpression(final List<DiseasesMasterCode> codeList,
	    final Map<String, DiseasesMasterCode> codesByText, final String expression, final boolean collapse) {

		// Handle null expressions.
		if (expression == null) {
			return Collections.emptyList();
		}

		// A list of sub-expressions, derived from the specified expression.
		final List<String> expressions = new ArrayList<>();

		// Determine if an additional regular expression is required. This additional expression is
		// used to filter the results according to a specific digit in the code
		String additionalRegex = null;
		if (expression.contains("/")) { //$NON-NLS-1$

			// Here the expression is in the form of a series of codes separated by commas followed
			// by a digit.
			final String[] parts = expression.split("/"); //$NON-NLS-1$
			additionalRegex = ".*\\." + parts[1].substring(1) + "\\d?"; //$NON-NLS-1$ //$NON-NLS-2$
			final String[] codes = parts[0].split(","); //$NON-NLS-1$
			for (final String codeText : codes) {
				expressions.add(codeText);
			}
		} else {
			final Matcher m = SPECIAL_EXPRESSION_PATTERN.matcher(expression);

			// Here the expression is in the form similar to "... with fourth character 7 or 9";
			if (m.matches()) {
				final int n = expression.contains("fourth") ? 4 : 5; //$NON-NLS-1$
				final StringBuilder sb = new StringBuilder();

				// Pad the regular expression with dots for each digit.
				for (int i = 0; i < n; i++) {
					sb.append("."); //$NON-NLS-1$
				}

				// Add a digit matcher (\d) for the digit or digits specified in the expression.
				final String end = expression.substring(expression.length() - 7);
				if (Pattern.matches(".*\\d or .*\\d", end)) { //$NON-NLS-1$
					sb.append("(" + expression.charAt(expression.lastIndexOf(" or ") - 1) + "|" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					    + expression.charAt(expression.length() - 1) + ")"); //$NON-NLS-1$
				} else {
					sb.append(expression.charAt(expression.length() - 1));
				}
				sb.append("\\d*"); //$NON-NLS-1$

				// Extract the code specified in the expression.
				final int spaceIndex = expression.indexOf(' ');
				final String codePart = spaceIndex > -1 ? expression.substring(0, spaceIndex) : expression;

				expressions.add(codePart);
				additionalRegex = sb.toString();
			} else {
				expressions.add(expression);
			}
		}

		final List<DiseasesMasterCode> result = new LinkedList<>();

		// Loop through all sub-expressions.
		for (final String ex : expressions) {
			int dashPos = ex.indexOf(SpecialCharacters.EN_DASH);

			// Sanitise the expression.
			final String sanitisedExpression = ex.replaceAll(SpecialCharacters.EN_DASH, "-").toUpperCase().trim(); //$NON-NLS-1$

			// See if the expression matches a code directly.
			final DiseasesMasterCode code = codesByText.get(sanitisedExpression);
			if (code != null && additionalRegex == null) {

				// If the code is an exact code then simply add it to the list.
				if (isExactCode(sanitisedExpression)) {
					result.add(code);
				} else {

					// The expression matches a code in the form of a code range so either add just
					// the child codes if collapsing is required or otherwise fully expand the code
					// range.
					if (collapse) {
						result.add(code);
						for (final MasterCode child : code.getChildren()) {
							result.add((DiseasesMasterCode)child);
						}
					} else {
						result.addAll(expandCode(code));
					}
				}

				// The expression does not correspond directly to a single entry in the
				// classification.
			} else {
				if (isCodeRange(sanitisedExpression) || additionalRegex != null) {

					// The expression is in the form of a range of codes so extract the first and
					// second codes in the range.
					String part1 = null;
					String part2 = null;
					if (sanitisedExpression.contains(" ")) { //$NON-NLS-1$
						final String[] parts = sanitisedExpression.split(" "); //$NON-NLS-1$
						part1 = parts[0];
						part2 = parts[parts.length - 1];
						if (part2.charAt(0) == '-') {
							part2 = part2.substring(1);
						}
					} else {

						// Count the number of dashes.
						int dashCount = 0;
						for (int i = 0; i < sanitisedExpression.length(); i++) {
							if (sanitisedExpression.charAt(i) == '-') {
								++dashCount;
							}
						}
						dashPos =
						    dashCount == 3
						        ? sanitisedExpression.indexOf("-", sanitisedExpression.indexOf("-") + 1) : sanitisedExpression.indexOf("-"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

						part1 = dashPos == -1 ? sanitisedExpression : sanitisedExpression.substring(0, dashPos);
						part2 = dashPos == -1 ? sanitisedExpression : sanitisedExpression.substring(dashPos + 1);
					}

					// Find the first and last matching codes and then build a list of sub-codes,
					// applying the additional regular expression if required.
					final DiseasesMasterCode first = firstMatchingCode(codeList, part1);
					final DiseasesMasterCode last = lastMatchingCode(codeList, part2);
					for (int index = codeList.indexOf(first); index <= codeList.indexOf(last); index++) {

						// Sometimes the code will not exist such as references to U00-U99 in Volume 5 - Introduction in
						// the references.
						if (index == -1) {
							break;
						}

						final DiseasesMasterCode dmc = codeList.get(index);
						if ((!collapse || dmc.getChildren().isEmpty() || dmc == last)
						    && (additionalRegex == null || Pattern.matches(additionalRegex, dmc.getText()))) {
							result.add(codeList.get(index));
						}
					}
				} else {

					// The expression is a non-specific representation of a set of codes using the
					// '-' character so loop through all codes in the classification that match the
					// specification, collapsing if required.
					String regex = sanitisedExpression.replace(".", "\\."); //$NON-NLS-1$ //$NON-NLS-2$
					regex = regex.replace("-", "."); //$NON-NLS-1$ //$NON-NLS-2$
					for (final DiseasesMasterCode mc : codeList) {
						if (Pattern.matches(regex, mc.getText())) {
							if (collapse) {
								result.add(mc);
							} else {
								result.addAll(expandCode(mc));
							}
						}
					}
				}
			}
		}

		// Sort the resulting list of codes in classification order.
		Collections.sort(result, new Comparator<DiseasesMasterCode>() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
			 */
			@Override
			public int compare(final DiseasesMasterCode lhs, final DiseasesMasterCode rhs) {
				return lhs.getID().compareTo(rhs.getID());
			}
		});

		// Collapse the list if required.
		if (!result.isEmpty() && collapse) {
			collapseList(result);

			// For lists of size 1 add in all child codes (if present).
			if (result.size() == 1) {
				for (final MasterCode child : result.get(0).getChildren()) {
					result.add((DiseasesMasterCode)child);
				}
			}
		}

		return result;
	}

	/**
	 * Returns true if the text matches a chapter code.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the text represents a chapter code.
	 */
	public static boolean isChapterCode(final String text) {
		return CHAPTER_CODE_PATTERN.matcher(text.toUpperCase()).matches();
	}

	/**
	 * Returns true if the text matches a diseases master code.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the text represents a diseases master code.
	 */
	public static boolean isCode(final String text) {
		return CODE_PATTERN.matcher(text).matches();
	}

	/**
	 * Returns true if the text matches a range of diseases master codes.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the text represents a range of diseases master codes.
	 */
	public static boolean isCodeRange(final String text) {
		return CODE_RANGE_PATTERN.matcher(text.replaceAll("%20", " ")).matches(); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Returns true if the text matches a range of diseases master codes or a single code.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the text represents a range of diseases master codes or a single code.
	 */
	public static boolean isCodeSpec(final String spec) {
		return isCode(spec) || isCodeRange(spec);
	}

	/**
	 * Returns true if the text matches an exact diseases master code.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the text represents an exact diseases master code.
	 */
	public static boolean isExactCode(final String text) {
		return EXACT_CODE_PATTERN.matcher(text).matches() && !text.equals("B12"); //$NON-NLS-1$
	}

	/**
	 * Returns true if the text matches a "special" range of diseases master codes.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the text represents a "special" range of diseases master codes.
	 */
	public static boolean isSpecialCodeRange(final String text) {
		return SPECIAL_CODE_RANGE_PATTERN.matcher(text).matches();
	}

	/**
	 * Finds the last code in the classification that matches the specified expression.
	 * 
	 * @param codeList A list of codes in classification order.
	 * @param expression The expression to match.
	 * @return The last matching code.
	 */
	private static DiseasesMasterCode
	    lastMatchingCode(final List<DiseasesMasterCode> codeList, final String expression) {
		DiseasesMasterCode last = null;

		// Convert the expression into a valid regular expression.
		final int regexLength = expression.length();
		String regex = expression.replace(".", "\\."); //$NON-NLS-1$ //$NON-NLS-2$
		regex = regex.replace("-", "."); //$NON-NLS-1$ //$NON-NLS-2$

		// Loop through the classification to find the last code that matches the regular
		// expression.
		for (final DiseasesMasterCode code : codeList) {
			if (Pattern.matches(regex, code.getText().substring(0, Math.min(regexLength, code.getText().length())))) {
				last = code;
			}
		}

		return last;
	}

	/**
	 * Extracts the "raw" code from an expression with asterisks and daggers removed.
	 * 
	 * @param expression The expression from which the code is to be extracted.
	 * @return The raw code.
	 */
	public static String rawCode(final String expression) {
		if (expression == null) {
			return null;
		}

		String rawCode = expression.trim();
		rawCode = rawCode.replace("*", ""); //$NON-NLS-1$ //$NON-NLS-2$
		rawCode = rawCode.replace("+", ""); //$NON-NLS-1$ //$NON-NLS-2$
		rawCode = rawCode.replace(SpecialCharacters.DAGGER, ""); //$NON-NLS-1$
		rawCode = rawCode.replace(SpecialCharacters.EN_DASH, "-"); //$NON-NLS-1$

		return rawCode;
	}
}