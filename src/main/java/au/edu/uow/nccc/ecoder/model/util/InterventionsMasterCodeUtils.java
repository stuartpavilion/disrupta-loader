
package au.edu.uow.nccc.ecoder.model.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import au.edu.uow.nccc.ecoder.model.bean.InterventionsMasterCode;
import au.edu.uow.nccc.ecoder.model.bean.MasterCode;

/**
 * <code>InterventionsMasterCodeUtils</code> is a utility class that provides static methods for manipulating
 * interventions master codes.
 * 
 * @author jturnbul
 */
public class InterventionsMasterCodeUtils {

	/**
	 * A pre-compiled pattern for matching interventions master codes.
	 */
	private static final Pattern PATTERN_CODE = Pattern.compile(InterventionsMasterCode.REGEX_CODE);

	/**
	 * A pre-compiled pattern for matching fully specified interventions master codes.
	 */
	private static final Pattern PATTERN_CODE_SPECIFIC = Pattern.compile(InterventionsMasterCode.REGEX_CODE_SPECIFIC);

	/**
	 * A pre-compiled pattern for matching block numbers.
	 */
	private static final Pattern PATTERN_BLOCK = Pattern.compile(InterventionsMasterCode.REGEX_BLOCK);

	/**
	 * A pre-compiled pattern for matching a range of block numbers.
	 */
	private static final Pattern PATTERN_BLOCK_RANGE = Pattern.compile(InterventionsMasterCode.REGEX_BLOCK_RANGE);

	/**
	 * A pre-compiled pattern for matching special block numbers where the extension is also specified.
	 */
	private static final Pattern PATTERN_BLOCK_SPECIAL = Pattern.compile(InterventionsMasterCode.REGEX_BLOCK_SPECIAL);

	/**
	 * Returns true if the two codes have the same parent.
	 * 
	 * @param a A potential interventions master code sibling.
	 * @param b A potential interventions master code sibling.
	 * @return True if the two codes are siblings.
	 */
	private static boolean areSiblings(final InterventionsMasterCode a, final InterventionsMasterCode b) {
		return a.getParent() == null ? b.getParent() == null : a.getParent() == b.getParent();
	}

	/**
	 * Collapses a list of codes to the most concise representation of the list as possible. This is done in several
	 * passes where each pass identifies sets of siblings that completely represent their parent code and replaces them
	 * with their parent code until there are no more complete sibling sets remaining.
	 * 
	 * @param codes The list of codes to collapse which must be in classification order.
	 */
	private static void collapseList(final List<InterventionsMasterCode> codes) {

		// Nothing to do with list of size 1.
		if (codes.size() == 1) {
			return;
		}

		boolean changed = false;
		do {
			int index = 0;
			changed = false;
			do {
				// Loop through the list identifying adjacent siblings.
				final List<InterventionsMasterCode> siblings = new ArrayList<>();
				final InterventionsMasterCode start = codes.get(index);
				for (; index < codes.size() && areSiblings(start, codes.get(index)); index++) {
					siblings.add(codes.get(index));
				}

				// If the siblings are the complete set of all children for their parent then
				// replace them with the parent.
				final MasterCode parent = start.getParent();
				if (parent != null && parent.getChildren().equals(siblings)) {
					codes.removeAll(siblings);
					index -= siblings.size();
					codes.add(index, (InterventionsMasterCode)parent);
					changed = true;
					++index;
				}
			} while (index < codes.size());
		} while (changed);

		// Now remove any duplicates.
		for (int index = 0; index < codes.size() - 1;) {
			if (codes.get(index).equals(codes.get(index + 1))) {
				codes.remove(index + 1);
			} else {
				++index;
			}
		}
	}

	/**
	 * Fully expands a code so that the resulting collection includes the code itself and all its descendants.
	 * 
	 * @param code The code to expand.
	 * @return A list containing the code itself and all its descendants in classification order.
	 */
	public static List<InterventionsMasterCode> expandCode(final InterventionsMasterCode code) {
		final List<InterventionsMasterCode> codes = new ArrayList<>();
		codes.add(code);
		for (final MasterCode child : code.getChildren()) {
			codes.addAll(expandCode((InterventionsMasterCode)child));
		}

		return codes;
	}

	/**
	 * Finds all code in the classification that match the specified expression.
	 * 
	 * @param codeList A list of codes in classification order.
	 * @param expression The expression to match.
	 * @return A list of matching codes.
	 */
	private static List<InterventionsMasterCode> findMatchingCodes(final List<InterventionsMasterCode> codeList,
	    final String expression) {
		final List<InterventionsMasterCode> result = new ArrayList<>();
		final String regex = expression + ".*"; //$NON-NLS-1$
		for (final InterventionsMasterCode code : codeList) {
			if (Pattern.matches(regex, code.getText())) {
				result.add(code);
			}
		}

		return result;
	}

	/**
	 * Takes an expression and returns a list of codes that match that expression, optionally collapsing the list if
	 * required.
	 * 
	 * @param codeList The list of codes in the classification. This is passed in as an argument to permit the use of
	 *            this method in the CodeXpert and the DB Loader.
	 * @param codesByText A map of codes by code text. This is passed in as an argument to permit the use of this method
	 *            in the CodeXpert and the DB Loader.
	 * @param expression The expression to convert into a list of codes.
	 * @param collapse True if the list is to be collapsed.
	 * @return A list of matching codes which may be collapsed if specified.
	 */
	public static List<InterventionsMasterCode> getCodesForExpression(final List<InterventionsMasterCode> codeList,
	    final Map<String, InterventionsMasterCode> codesByText, final String expression, final boolean collapse) {

		// Handle null expressions.
		if (expression == null) {
			return Collections.emptyList();
		}

		// Sanitise the expression by stripping out HTML spaces as the expression may have been
		// extracted from an HTML page.
		String sanitisedExpression = expression.replaceAll("%20", " "); //$NON-NLS-1$ //$NON-NLS-2$

		// Determine if an additional regular expression is required. This additional expression is
		// used to filter the results according to a specific digit in the code
		String additionalRegex = null;

		// Handle special expressions such as "[1920] with extension -00".
		final Matcher m = PATTERN_BLOCK_SPECIAL.matcher(sanitisedExpression);
		if (m.matches()) {

			// Build the additional regular expression so that it will match any code with the
			// specified final two digits.
			additionalRegex = "\\d{5}\\-" + sanitisedExpression.substring(sanitisedExpression.length() - 2); //$NON-NLS-1$

			// Extract the actual block number.
			sanitisedExpression = expression.substring(1, expression.indexOf(']'));
		}

		final List<InterventionsMasterCode> result = new LinkedList<>();

		// Handle block ranges.
		if (InterventionsMasterCodeUtils.isBlockRange(expression)) {

			// Extract the from and to block numbers.
			int closeBracketPos = sanitisedExpression.indexOf(']');
			final String from = sanitisedExpression.substring(1, closeBracketPos);
			final int openBracketPos = sanitisedExpression.indexOf('[', closeBracketPos);
			closeBracketPos = sanitisedExpression.indexOf(']', openBracketPos);
			final String to = sanitisedExpression.substring(openBracketPos + 1, closeBracketPos);
			final int fromBlock = Integer.parseInt(from);
			final int toBlock = Integer.parseInt(to);

			// Analyse the format of the block range. It's either in a form such as
			// "[1234] to [5678]" or "[1234] and [5678]" or "[1234] or [5678]" .
			if (Pattern.matches(".* to .*", sanitisedExpression)) { //$NON-NLS-1$

				// Add all blocks in the range.
				for (int blockNumber = fromBlock; blockNumber <= toBlock; blockNumber++) {
					final InterventionsMasterCode block = codesByText.get(String.valueOf(blockNumber));
					if (block != null) {
						result.add(block);
					}
				}
			} else {

				// Add only the from block and to block.
				result.add(codesByText.get(String.valueOf(fromBlock)));
				result.add(codesByText.get(String.valueOf(toBlock)));
			}
		} else {

			// Sanitise the expression.
			sanitisedExpression = InterventionsMasterCodeUtils.rawCode(sanitisedExpression);

			// See if the expression matches a code directly.
			final InterventionsMasterCode code = codesByText.get(sanitisedExpression);
			if (code != null) {

				// The expression is an exact match for a code so expand it unless we are
				// collapsing.
				if (collapse) {
					result.add(code);
				} else {
					result.addAll(expandCode(code));
				}
			} else {

				// The expression is not a direct match for a code.
				//
				// First, extract the block (if specified).
				final int openBracketPos = expression.indexOf('[');
				final int closeBracketPos = expression.indexOf(']');
				final String block =
				    openBracketPos > -1 ? expression.substring(openBracketPos + 1, closeBracketPos) : ""; //$NON-NLS-1$

				// Find all codes that match the sanitised expression.
				final List<InterventionsMasterCode> matches = findMatchingCodes(codeList, sanitisedExpression);

				// Loop through the matches and add codes to the result being careful to only
				// include codes within the block part of the expression.
				for (int index = 0; index < matches.size(); index++) {
					final InterventionsMasterCode match = matches.get(index);
					if (!collapse || match.getChildren().isEmpty() || index == matches.size() - 1) {
						if (match.getType() == InterventionsMasterCode.Type.CODE) {
							final InterventionsMasterCode parent = (InterventionsMasterCode)match.getParent();
							if (block.equals(parent.getText()) || block.isEmpty()) {
								result.add(match);
							}
						} else {
							result.add(match);
						}
					}
				}
			}
		}

		// Collapse the resulting list of codes if required.
		if (!result.isEmpty() && collapse) {
			collapseList(result);

			// For lists of size 1 add in all child codes (if present and they match the additional
			// regular expression).
			if (result.size() == 1) {
				for (final MasterCode child : result.get(0).getChildren()) {
					if (additionalRegex == null || Pattern.matches(additionalRegex, child.getText())) {
						result.add((InterventionsMasterCode)child);
					}
				}
			}
		}

		return result;
	}

	/**
	 * Returns true if the specified text matches a block.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the text matches a block.
	 */
	public static boolean isBlock(final String text) {
		return PATTERN_BLOCK.matcher(text).matches();
	}

	/**
	 * Returns true if the specified text matches a range of blocks.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the specified text matches a range of blocks.
	 */
	public static boolean isBlockRange(final String text) {
		return PATTERN_BLOCK_RANGE.matcher(text.replaceAll("%20", " ")).matches(); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Returns true if the specified text matches a special block range.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the specified text matches a special block range.
	 */
	public static boolean isBlockSpecial(final String text) {
		return PATTERN_BLOCK_SPECIAL.matcher(text).matches();
	}

	/**
	 * Returns true if the specified text matches an ACHI code.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the specified text matches an ACHI code.
	 */
	public static boolean isCode(final String text) {
		return PATTERN_CODE.matcher(text).matches();
	}

	/**
	 * Returns true if the specified text matches a fully-specified ACHI code.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the specified text matches a fully-specified ACHI code.
	 */
	public static boolean isSpecificCode(final String text) {
		return PATTERN_CODE_SPECIFIC.matcher(text).matches();
	}

	/**
	 * Extracts the "raw" code from an expression which may contain a block number in addition to a specific code.
	 * 
	 * @param expression The expression from which the code is to be extracted.
	 * @return The raw code.
	 */
	public static String rawCode(final String expression) {
		String rawCode = expression.trim();

		// Ignore the block if specified.
		rawCode = rawCode.startsWith("block ") ? rawCode.substring(rawCode.indexOf(' ') + 1) : rawCode; //$NON-NLS-1$
		if (Pattern.matches(".*\\[\\d+\\]", expression)) { //$NON-NLS-1$
			final int bracketPos = rawCode.indexOf('[');
			rawCode =
			    bracketPos == -1 ? rawCode : bracketPos == 0 ? rawCode.substring(1, rawCode.length() - 1) : rawCode
			        .substring(0, bracketPos);
		}

		// Ignore the generic trailer if specified.
		if (rawCode.endsWith("-XX")) { //$NON-NLS-1$
			rawCode = rawCode.substring(0, rawCode.indexOf('-'));
		}

		// Handle chapters.
		rawCode = rawCode.toUpperCase();

		return rawCode;
	}
}