package au.edu.uow.nccc.ecoder.model.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import au.edu.uow.nccc.ecoder.model.bean.MorphologyCode;

/**
 * <code>InterventionsMasterCodeUtils</code> is a utility class that provides static methods for
 * manipulating morphology codes.
 * 
 * @author jturnbul
 */
public class MorphologyCodeUtils {

	/**
	 * A pre-compiled pattern for matching morphology codes.
	 */
	private static final Pattern PATTERN_CODE = Pattern.compile(MorphologyCode.REGEX_CODE);

	/**
	 * A pre-compiled pattern for matching fully-specified morphology codes.
	 */
	private static final Pattern PATTERN_SPECIFIC_CODE = Pattern.compile(MorphologyCode.REGEX_SPECIFIC_CODE);

	/**
	 * Returns true if the specified text matches a morphology code.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the specified text matches a morphology code.
	 */
	public static boolean isCode(final String text) {
		final Matcher m = PATTERN_CODE.matcher(text);

		return m.matches();
	}

	/**
	 * Returns true if the specified text matches a specific morphology code.
	 * 
	 * @param text The text to evaluate.
	 * @return True if the specified text matches a specific morphology code.
	 */
	public static boolean isSpecificCode(final String text) {
		final Matcher m = PATTERN_SPECIFIC_CODE.matcher(text);

		return m.matches();
	}
}