
package au.edu.uow.nccc.ecoder.search;


/**
 * <code>ContentIndexing</code> is a place-holder for definitions related to indexing content with Lucene.
 * 
 * @author jturnbul
 */
public class ContentIndexing {

	/**
	 * <code>DocumentType</code> enumerates the various type of document to be indexed and associates an icon with each
	 * one.
	 * 
	 * @author jturnbul
	 */
	public enum DocumentType {

		DISEASES_INDEX_TERM("Index (Diseases)"), //$NON-NLS-1$
		DISEASES_MASTER_CODE("Tabular (Diseases)"), //$NON-NLS-1$
		INTERVENTIONS_INDEX_TERM("Index (interventions)"), //$NON-NLS-1$
		INTERVENTIONS_MASTER_CODE("Tabular (Interventions)"), //$NON-NLS-1$
		ACS_CODE("ACS"), //$NON-NLS-1$
		ADVICE_ITEM("Advice"), //$NON-NLS-1$
		REFERENCE("References"); //$NON-NLS-1$

		/**
		 * A string representation of the type suitable for display.
		 */
		private final String display;

		/**
		 * Creates a new <code>DocumentType</code>.
		 * 
		 * @param display The display string to be used.
		 * @param icon The icon to be used.
		 */
		private DocumentType(final String display) {
			this.display = display;
		}

		/**
		 * Gets the type's display string.
		 * 
		 * @return The type's display string.
		 */
		public String getDisplay() {
			return display;
		}

	}

	// Field names.

	/**
	 * The field containing the type of document.
	 */
	public static final String FIELD_TYPE = "type"; //$NON-NLS-1$

	/**
	 * The field containing the identifier of the document.
	 */
	public static final String FIELD_ID = "id"; //$NON-NLS-1$

	/**
	 * The field containing the title of the document.
	 */
	public static final String FIELD_TITLE = "title"; //$NON-NLS-1$

	/**
	 * The field containing the sub-title of the document.
	 */
	public static final String FIELD_SUBTITLE = "subtitle"; //$NON-NLS-1$

	/**
	 * The field containing the notes (i.e. content) of the document.
	 */
	public static final String FIELD_NOTES = "notes"; //$NON-NLS-1$
}