
package au.edu.uow.nccc.ecoder.search;

import au.edu.uow.nccc.ecoder.model.util.DiseasesMasterCodeUtils;
import au.edu.uow.nccc.ecoder.special.SpecialCharacters;

/**
 * <code>Highlighter</code> is the class responsible for highlighting occurrences of text fragments within a block of
 * HTML that caused a particular model object/document to be included in the results of a search query. The highlighting
 * is accomplished by changing the background colour of the matching text fragments.
 * 
 * @author jturnbul
 */
public class Highlighter {

	/**
	 * Marks-up the given block of HTML so that the text fragments responsible for a model object/document being
	 * included in the search results are highlighted.
	 * 
	 * @param html The block of HTML to mark-up.
	 * @return The block of HTML with text fragments highlighted.
	 */
	public static String highlight(final String html) {
		String result = html;

		// Loop through all search result highlights.
		for (String highlight : SearchResult.getHighlights()) {

			// If the highlight is a disases master code range then replace standard dashes with
			// En-dashes to ensure the displayed code range is highlighted. This is because the
			// original document contains En-dashes but these are converted into standard dashes by
			// a Lucene filter.
			if (DiseasesMasterCodeUtils.isCodeRange(highlight)) {
				highlight = highlight.replaceAll("\\-", SpecialCharacters.EN_DASH); //$NON-NLS-1$
			}

			int x = 0;
			final StringBuilder sb = new StringBuilder();
			int pos = 0;
			do {

				// Find the next occurrence of the highlight.
				pos = result.indexOf(highlight, pos);
				if (pos > -1) {

					// Set the background colour of the text fragment but only if it is not adjacent
					// to a letter or a digit and it is not within an HTML tag.
					if ((pos == 0 || !Character.isLetterOrDigit(result.charAt(pos - 1)))
					    && (pos == result.length() - 1 || !Character.isLetterOrDigit(result.charAt(pos
					        + highlight.length()))) && !inTag(result, pos)) {
						sb.append(result.substring(x, pos));
						sb.append("<span style='background:#88FF88; color:black'>"); //$NON-NLS-1$
						sb.append(result.substring(pos, pos + highlight.length()));
						sb.append("</span>"); //$NON-NLS-1$
					} else {
						sb.append(result.substring(x, pos + highlight.length()));
					}
					x = pos + highlight.length();
					pos = x;
				}
			} while (pos > -1);

			sb.append(result.substring(x, result.length()));
			result = sb.toString();
		}

		return result;
	}

	/**
	 * Returns true if the given position within the block of HTML lies within the opening and closing angled brackets
	 * of an HTML tag.
	 * 
	 * @param html The block of HTML.
	 * @param pos The position within the HTML to check.
	 * @return True if the position is within a tag.
	 */
	private static boolean inTag(final String html, final int pos) {

		// Step backwards through the text looking for angled brackets.
		for (int index = pos - 1; index > 0; index--) {

			// If a left angled bracket is found before a right angled bracket then we are within a
			// tag.
			if (html.charAt(index) == '<') {
				return true;
			}

			// Only loop as far as the next tag.
			if (html.charAt(index) == '>') {
				break;
			}
		}

		// Step forwards through the text looking for angled brackets.
		for (int index = pos + 1; index < html.length(); index++) {

			// If a right angled bracket is found before a left angled bracket then we are within a
			// tag.
			if (html.charAt(index) == '>') {
				return true;
			}

			// Only loop as far as the next tag.
			if (html.charAt(index) == '<') {
				break;
			}
		}

		// We got this far so no tag was detected.
		return false;
	}
}