
package au.edu.uow.nccc.ecoder.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import au.edu.uow.nccc.ecoder.model.bean.ACSCode;
import au.edu.uow.nccc.ecoder.model.bean.AdviceItem;
import au.edu.uow.nccc.ecoder.model.bean.CodeTerm;
import au.edu.uow.nccc.ecoder.model.bean.CodeTermParent;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesIndexTerm;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesMasterCode;
import au.edu.uow.nccc.ecoder.model.bean.InterventionsIndexTerm;
import au.edu.uow.nccc.ecoder.model.bean.InterventionsMasterCode;
import au.edu.uow.nccc.ecoder.model.bean.Reference;
import au.edu.uow.nccc.ecoder.search.ContentIndexing.DocumentType;

/**
 * A <code>SearchResult</code> represents a result returned from a query submitted to the Lucene search engine and
 * encapsulates the Lucene document and its type.
 * 
 * @author jturnbul
 */
public class SearchResult {

	/**
	 * A set of text fragments for highlighting as indicated by Lucene. This set contains all highlights for all current
	 * search results.
	 */
	private static Set<String> highlights = new HashSet<>();

	/**
	 * The type of document returned from the search query.
	 */
	private final ContentIndexing.DocumentType type;

	/**
	 * The actual Lucene document returned from the search query.
	 */
	private final Object document;

	/**
	 * The score assigned by Lucene for the search result.
	 */
	private final float score;

	/**
	 * Creates a new <code>SearchResult</code>.
	 * 
	 * @param type The document type.
	 * @param document The actual document.
	 * @param score The matching score.
	 */
	SearchResult(final DocumentType type, final Object document, final float score) {
		this.type = type;
		this.document = document;
		this.score = score;
	}

	/**
	 * Returns the set of text fragments for search result highlighting. This set contains all highlights for all
	 * current search results.
	 * 
	 * @return The set of search result hightlights.
	 */
	public static List<String> getHighlights() {
		final List<String> list = new ArrayList<>(highlights);

		// Sort the highlights by length to ensure that the longest highlights are processed first.
		// This is to ensure that any HTML DOM changes are made in such a way that shorter strings
		// are not highlighted before longer strings as this will prevent the longer string from
		// being highlighted.
		Collections.sort(list, new Comparator<String>() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
			 */
			@Override
			public int compare(final String s1, final String s2) {
				final int l1 = s1.length();
				final int l2 = s2.length();

				return l1 > l2 ? -1 : l1 < l2 ? 1 : 0;
			}

		});

		return list;
	}

	/**
	 * Sets the set of search result highlights.
	 * 
	 * @param highlights The set of search result highlights.
	 */
	public static void setHighlights(final Set<String> highlights) {
		SearchResult.highlights = highlights;
	}

	/**
	 * Returns the search result in a form suitable for display.
	 * 
	 * @return The display string for the search result.
	 */
	public String getDisplay() {
		String display = null;

		switch (getType()) {
		case ACS_CODE: {
			display = ((ACSCode)getDocument()).getFullDisplay();
			break;
		}

		case DISEASES_INDEX_TERM: {
			display = ((DiseasesIndexTerm)getDocument()).getFullText();
			break;
		}

		case DISEASES_MASTER_CODE: {
			display = ((DiseasesMasterCode)getDocument()).getFullDisplay();
			break;
		}

		case INTERVENTIONS_INDEX_TERM: {
			display = ((InterventionsIndexTerm)getDocument()).getFullText();
			break;
		}

		case INTERVENTIONS_MASTER_CODE: {
			display = ((InterventionsMasterCode)getDocument()).getFullDisplay();
			break;
		}

		case ADVICE_ITEM: {
			display = ((AdviceItem)getDocument()).getDisplay();
			break;
		}

		case REFERENCE: {
			display = ((Reference)getDocument()).getDisplay();
			break;
		}
		}

		return display == null ? "" : display; //$NON-NLS-1$
	}

	/**
	 * Return the actual search result document from Lucene.
	 * 
	 * @return The Lucene document.
	 */
	public Object getDocument() {
		return document;
	}
	
	public CodeTerm getDocumentAsCodeTerm(){
		return (CodeTerm) document;
	}
	
	public CodeTermParent getDocumentAsCodeTermParent(){
		return (CodeTermParent) document;
	}
	public String getPreferredText(){
		return ((CodeTerm) document).getPreferredText();
	}

	/**
	 * Returns the numeric ID of the model object represented by this search result.
	 * 
	 * @return The model object's ID.
	 */
	public Integer getID() {
		Integer id = null;

		switch (getType()) {
		case ACS_CODE: {
			id = ((ACSCode)getDocument()).getID();
			break;
		}

		case DISEASES_INDEX_TERM: {
			id = ((DiseasesIndexTerm)getDocument()).getID();
			break;
		}

		case DISEASES_MASTER_CODE: {
			id = ((DiseasesMasterCode)getDocument()).getID();
			break;
		}

		case INTERVENTIONS_INDEX_TERM: {
			id = ((InterventionsIndexTerm)getDocument()).getID();
			break;
		}

		case INTERVENTIONS_MASTER_CODE: {
			id = ((InterventionsMasterCode)getDocument()).getID();
			break;
		}

		case ADVICE_ITEM: {
			id = ((AdviceItem)getDocument()).getID();
			break;
		}

		case REFERENCE: {
			id = ((Reference)getDocument()).getID();
			break;
		}
		}

		return id;
	}

	/**
	 * Returns the Lucene-assigned search result score.
	 * 
	 * @return The Lucene score.
	 */
	public Float getScore() {
		return score;
	}

	/**
	 * Returns the document type.
	 * 
	 * @return The document type.
	 */
	public ContentIndexing.DocumentType getType() {
		return type;
	}
}