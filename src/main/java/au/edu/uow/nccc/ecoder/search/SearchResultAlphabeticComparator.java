package au.edu.uow.nccc.ecoder.search;

import java.util.Comparator;

/**
 * A <code>SearchResultAlphabeticComparator</code> is a comparator for sorting search results
 * alphabetically by display string.
 * 
 * @author jturnbul
 */
public class SearchResultAlphabeticComparator implements Comparator<SearchResult> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final SearchResult a, final SearchResult b) {
		return a.getDisplay().compareTo(b.getDisplay());
	}
}