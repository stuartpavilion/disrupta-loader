package au.edu.uow.nccc.ecoder.search;

import java.util.Comparator;

/**
 * A <code>SearchResultIDComparator</code> is a comparator for sorting search results in model
 * object ID order.
 * 
 * @author jturnbul
 */
public class SearchResultIDComparator implements Comparator<SearchResult> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final SearchResult a, final SearchResult b) {
		return a.getID().compareTo(b.getID());
	}
}