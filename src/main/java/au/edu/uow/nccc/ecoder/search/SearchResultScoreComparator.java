package au.edu.uow.nccc.ecoder.search;

import java.util.Comparator;

/**
 * A <code>SearchResultScoreComparator</code> is a comparator for sorting search results in score
 * order.
 * 
 * @author jturnbul
 */
public class SearchResultScoreComparator implements Comparator<SearchResult> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final SearchResult a, final SearchResult b) {
		return b.getScore().compareTo(a.getScore());
	}
}