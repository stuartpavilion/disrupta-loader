package au.edu.uow.nccc.ecoder.search;

/**
 * <code>SearchResultSelectionStrategy</code> is the interface implemented by each of the concrete
 * strategies for selecting a model object in the appropriate panel that corresponds to a search
 * result as per the GoF Strategy Pattern.
 * 
 * @author jturnbul
 */
public interface SearchResultSelectionStrategy {

	/**
	 * Selects the model object that corresponds to the given search result in the appropriate
	 * panel.
	 * 
	 * @param result The search result.
	 */
	public void select(final SearchResult result);
}