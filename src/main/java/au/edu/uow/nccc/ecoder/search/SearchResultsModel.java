
package au.edu.uow.nccc.ecoder.search;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.DefaultEncoder;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.NullFragmenter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.suggest.Lookup.LookupResult;
import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import au.edu.uow.nccc.ecoder.ECoder;
import au.edu.uow.nccc.ecoder.Resources;
import au.edu.uow.nccc.ecoder.model.Model;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesIndexTerm;
import au.edu.uow.nccc.ecoder.model.bean.InterventionsIndexTerm;

import com.i_mmerce.disruptalookup.util.Stopwatch;

/**
 * A <code>SearchResultsModel</code> is the model class responsible for submitting search queries to the Lucene search
 * engine and manipulating the results of those queries.
 * 
 * @author jturnbul
 */
public class SearchResultsModel {

	/**
	 * The logger used by this class.
	 */
	private static final Logger logger = Logger.getLogger(SearchResultsModel.class);

	/**
	 * The maximum number of results to return from a search query.
	 */
	private static final int SIZE_SEARCH = 10000;

	/**
	 * The number of text fragments for highlighting to return from a search query. This is currently set to 1 because
	 * we are using the <code>NullFragmenter</code> which treats all text as one fragment.
	 */
	private static final int SIZE_FRAGMENT_SAMPLE = 1;

	/**
	 * The base directory containing the Lucene index files.
	 */
	private static final String DIRECTORY_INDEX_BASE = ".." + Resources.SEPARATOR_FILE + "index" //$NON-NLS-1$ //$NON-NLS-2$
	    + Resources.SEPARATOR_FILE;

	/**
	 * The sub-directory containing the index built without stemming applied.
	 */
	private static final String DIRECTORY_INDEX_WITHOUT_STEMMING = DIRECTORY_INDEX_BASE + "1"; //$NON-NLS-1$

	/**
	 * The sub-directory containing the index built with stemming applied.
	 */
	private static final String DIRECTORY_INDEX_WITH_STEMMING = DIRECTORY_INDEX_BASE + "2"; //$NON-NLS-1$

	/**
	 * A list of search results.
	 */
	private List<SearchResult> resultsList = null;

	/**
	 * The scope of the search.
	 */
	private ContentIndexing.DocumentType scope;
	
	private IndexSearcher stemSearcher;
	private IndexSearcher stemlessSearcher;
	
	private Directory dirWithStem;
	private Directory dirWithOutStem;
	private AnalyzingSuggester suggester;
	

	/**
	 * Builds the results model by running a query and processing the results.
	 * 
	 * @param rawQuery The raw query string.
	 * @param scope The scope of the search.
	 * @param leadTermsOnly A flag which is true if the search is to be restricted to lead terms only.
	 * @return A list of results from submitting the search query to Lucene.
	 */
	private List<SearchResult> buildModel(final String rawQuery,
	    @SuppressWarnings("hiding") final ContentIndexing.DocumentType scope, final boolean leadTermsOnly) {
		this.scope = scope;

		// Create a list to store the search results.
		final List<SearchResult> results = new ArrayList<>();

		// Create a set to store the text fragment highlights.
		final Set<String> highlights = new HashSet<>();

		// If the raw query contains wild cards then we must use the index without stemming applied
		// as stemming and wild cards are not compatible and will yield erroneous results.
		final boolean applyStemming = !rawQuery.contains("*"); //$NON-NLS-1$

		try {
			Stopwatch swSearch = new Stopwatch("Initial Search setup Excution time");swSearch.start();
			//needed to add baseDir
			Stopwatch sw2 = new Stopwatch("Index Searcher Selection Excution time");sw2.start();
			final IndexSearcher searcher = (applyStemming ? stemSearcher : stemlessSearcher);
			sw2.stop();logger.debug(sw2);
			// Use our own custom analyser.
			sw2.reset("Customanalyzer creation Execution time");sw2.start();
			final CustomAnalyzer analyzer = new CustomAnalyzer(Version.LUCENE_32, applyStemming);
			sw2.stop();logger.debug(sw2);
			// Create a query parser using our custom analyser. Note: The same analyser must be used
			// when creating the index.
			sw2.reset("QueryParser creation Execution time");sw2.start();
			final QueryParser parser = new QueryParser(Version.LUCENE_32, ContentIndexing.FIELD_NOTES, analyzer);
			sw2.stop();logger.debug(sw2);
			// Sanitise the query.
			sw2.reset("Sanitise Query Execution time");sw2.start();
			final String sanitisedQuery = sanitiseQuery(rawQuery);
			sw2.stop();logger.debug(sw2);
			// Create a Lucene-friendly query which applies the sanitised query over all searchable
			// fields.
			final String actualQuery = "(" + ContentIndexing.FIELD_TITLE + ": " + sanitisedQuery + ") OR (" //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			    + ContentIndexing.FIELD_SUBTITLE + ": " + sanitisedQuery + ") OR (" + ContentIndexing.FIELD_NOTES //$NON-NLS-1$ //$NON-NLS-2$
			    + ": " + sanitisedQuery + ")"; //$NON-NLS-1$ //$NON-NLS-2$
			logger.info("Actual query: " + actualQuery); //$NON-NLS-1$
			swSearch.stop();logger.debug(swSearch);
			// Create a query by parsing the actual query string.
			swSearch.reset("Query Parsing Excution time");swSearch.start();
			final Query query = parser.parse(actualQuery);
			swSearch.stop();logger.debug(swSearch);
			// Perform the search itself.
			swSearch.reset("Excution time for search itself");swSearch.start();
			final TopDocs hits = searcher.search(query, SIZE_SEARCH);
			swSearch.stop();logger.debug(swSearch);
			// Create a highlighter for highlighting search results.
			swSearch.reset("Highlighter execution time.");swSearch.start();
			final Highlighter highlighter =
			    new Highlighter(new SimpleHTMLFormatter(), new DefaultEncoder(), new QueryScorer(query));
			swSearch.stop();logger.debug(swSearch);
			// Use the <code>NullFragmenter</code> so that all text is treated as a single fragment.
			highlighter.setTextFragmenter(new NullFragmenter());
			highlighter.setMaxDocCharsToAnalyze(Integer.MAX_VALUE);

			// Loop through the resulting documents.
			swSearch.reset("Bigloop execution time.");swSearch.start();
			for (final ScoreDoc sd : hits.scoreDocs) {

				// Extract the document and document type.
				final Document doc = searcher.doc(sd.doc);
				final ContentIndexing.DocumentType dt =
				    ContentIndexing.DocumentType.valueOf(doc.get(ContentIndexing.FIELD_TYPE));

				// Only include documents of the specified type or include all if scope is ALL.
				if (scope == null || dt == scope) {

					// Extract the model object's ID and create a new search result object.
					final Integer id = Integer.valueOf(doc.get(ContentIndexing.FIELD_ID));
					final SearchResult result = createSearchResult(dt, id, sd.score);
					/*
					// Add any text fragment highlights from the title field.
					String text = doc.get(ContentIndexing.FIELD_TITLE);
					TokenStream tokenStream =
					    TokenSources.getAnyTokenStream(searcher.getIndexReader(), sd.doc, ContentIndexing.FIELD_TITLE,
					        analyzer);
					TextFragment[] fragments =
					    highlighter.getBestTextFragments(tokenStream, text, true, SIZE_FRAGMENT_SAMPLE);
					for (final TextFragment fragment : fragments) {
						if (fragment != null && fragment.getScore() > 0) {
							highlights.addAll(extractHighlights(fragment.toString()));
						}
					}

					// Add any text fragment highlights from the subtitle field.
					text = doc.get(ContentIndexing.FIELD_SUBTITLE);
					tokenStream =
					    TokenSources.getAnyTokenStream(searcher.getIndexReader(), sd.doc,
					        ContentIndexing.FIELD_SUBTITLE, analyzer);
					fragments = highlighter.getBestTextFragments(tokenStream, text, true, SIZE_FRAGMENT_SAMPLE);
					boolean foundInSubtitle = false;
					for (final TextFragment fragment : fragments) {
						if (fragment != null && fragment.getScore() > 0) {
							foundInSubtitle = true;
							highlights.addAll(extractHighlights(fragment.toString()));
						}
					}

					// Add any text fragment highlights from the notes field.
					text = doc.get(ContentIndexing.FIELD_NOTES);
					tokenStream =
					    TokenSources.getAnyTokenStream(searcher.getIndexReader(), sd.doc, ContentIndexing.FIELD_NOTES,
					        analyzer);
					fragments = highlighter.getBestTextFragments(tokenStream, text, true, SIZE_FRAGMENT_SAMPLE);
					boolean foundInNotes = false;
					for (final TextFragment fragment : fragments) {
						if (fragment != null && fragment.getScore() > 0) {
							foundInNotes = true;
							highlights.addAll(extractHighlights(fragment.toString()));
						}
					}
					*/
					// Determine if the search result is relevant.
					final boolean relevant =
					    leadTermsOnly ? dt == ContentIndexing.DocumentType.DISEASES_INDEX_TERM
					        && ((DiseasesIndexTerm)result.getDocument()).isLeadTerm()
					        || dt == ContentIndexing.DocumentType.INTERVENTIONS_INDEX_TERM
					        && ((InterventionsIndexTerm)result.getDocument()).isLeadTerm() : true;
					/*
					// Only results that are a result of matching on the subtitle or the notes
					// fields will be included. This is because we want to exclude any results for
					// the diseases or interventions index terms which match only on the title field
					// (which contains the full text of the term) but not the subtitle field (which
					// contains the specific text of the term) to restrict the search results to
					// those that are most relevant.
					if ((foundInSubtitle || foundInNotes) && relevant) {
						results.add(result);
					}
					*/
					if (relevant){
						results.add(result);
					}
				}
			}
			swSearch.stop();logger.debug(swSearch);
		} catch (final Exception e) {
			logger.error("Error during search", e); //$NON-NLS-1$
		}

		SearchResult.setHighlights(highlights);

		return results;
	}

	/**
	 * Creates a <code>SearchResult</code> by looking up the object from the model with the specified ID.
	 * 
	 * @param type The document type.
	 * @param id The model object's ID.
	 * @param score The matching score.
	 * @return A search result object.
	 */
	private SearchResult
	    createSearchResult(final ContentIndexing.DocumentType type, final Integer id, final float score) {
		Object document = null;
		final Model model = Model.INSTANCE;

		switch (type) {
		case ACS_CODE: {
			document = model.getACSCodeForID(id);
			break;
		}

		case DISEASES_INDEX_TERM: {
			document = model.getDiseasesIndexTermForID(id);
			break;
		}

		case DISEASES_MASTER_CODE: {
			document = model.getDiseasesCodeForID(id);
			break;
		}

		case INTERVENTIONS_INDEX_TERM: {
			document = model.getInterventionsIndexTermForID(id);
			break;
		}

		case INTERVENTIONS_MASTER_CODE: {
			document = model.getInterventionsCodeForID(id);
			break;
		}

		case ADVICE_ITEM: {
			document = model.getAdviceItemForID(id);
			break;
		}

		case REFERENCE: {
			document = model.getReferenceForID(id);
			break;
		}
		}

		return new SearchResult(type, document, score);
	}

	/**
	 * Warning: This is extremely ugly! Lucene does not support search queries that begin with wild cards but given that
	 * the business requires this functionality I had to implement it by constructing a search query that would 'OR'
	 * together all possible combinations of letters and the remainder of the search string to simulate this behaviour.
	 * Of course there is a performance hit but it doesn't appear to be overly significant.
	 * 
	 * @param expression
	 * @return
	 */
	private String expandTermForWildcards(final String expression) {
		final StringBuilder sb = new StringBuilder();
		sb.append("("); //$NON-NLS-1$
		for (char c = 'a'; c <= 'z'; c++) {
			if (c > 'a') {
				sb.append(" OR "); //$NON-NLS-1$
			}
			sb.append(c + expression);
		}
		sb.append(")"); //$NON-NLS-1$

		return sb.toString();
	}

	/**
	 * Extracts a set of text fragments for highlighting from an input string that is the output from the HTML
	 * highlighter which wraps text fragments for highlighting in &lt;B&gt; and &lt;/B&gt; tags.
	 * 
	 * @param input The input string as outputted by the HTML highlighter.
	 * @return A set of text fragments to be highlighted.
	 */
	private Set<String> extractHighlights(final String input) {
		final Set<String> fragments = new HashSet<>();
		int start = 0;
		do {
			start = input.indexOf("<B>", start); //$NON-NLS-1$
			if (start > -1) {
				final int end = input.indexOf("</B>", start); //$NON-NLS-1$
				final String fragment = input.substring(start + 3, end);
				fragments.add(fragment);
				start = end + 4;
			}
		} while (start > -1);

		return fragments;
	}

	/**
	 * Returns the list of search results.
	 * 
	 * @return The list of search results.
	 */
	public List<SearchResult> getResults() {
		return resultsList;
	}

	/**
	 * Returns the scope of the most recently run search query.
	 * 
	 * @return The scope of the most recently run search query.
	 */
	public ContentIndexing.DocumentType getScope() {
		return scope;
	}

	/**
	 * Returns true if no matches were returned from the most recently run search query.
	 * 
	 * @return True if the search results list is empty.
	 */
	public boolean isEmpty() {
		return resultsList.isEmpty();
	}

	
	public SearchResultsModel() {
		Stopwatch swSearch = new Stopwatch("Initial Search setup Excution time");swSearch.start();
		//needed to add baseDir
		Stopwatch sw2 = new Stopwatch("Directory preparation Excution time");sw2.start();
		final String baseDir = ECoder.getUserDirectory() + Resources.SEPARATOR_FILE + ".." + Resources.SEPARATOR_FILE;
		// Open the index.
		try {
			dirWithStem = FSDirectory.open(new File(baseDir + DIRECTORY_INDEX_WITH_STEMMING));
			dirWithOutStem = FSDirectory.open(new File(baseDir + DIRECTORY_INDEX_WITHOUT_STEMMING));		
			sw2.stop();logger.debug(sw2);
			// Create an index searcher.
			sw2.reset("IndexSearch creation Execution time");sw2.start();
			stemSearcher = new IndexSearcher(DirectoryReader.open(dirWithStem));
			stemlessSearcher = new IndexSearcher(DirectoryReader.open(dirWithOutStem));
			sw2.stop();logger.debug(sw2);
			sw2.reset("AnalyzingSuggester initialisation Execution time");sw2.start();
			initAnalyzingSuggester();
			sw2.stop();logger.debug(sw2);
		} catch (CorruptIndexException e) {
			logger.error("Exception encountered attempting to create IndexSearchers",e);
		} catch (IOException e) {
			logger.error("Exception encountered attempting to create IndexSearchers",e);
		}
		
	}
	
	private void initAnalyzingSuggester(){
		Set<String> suggFields = new HashSet<>();
		suggFields.add(ContentIndexing.FIELD_TITLE);
		//suggFields.add(ContentIndexing.FIELD_SUBTITLE);
		//suggFields.add(ContentIndexing.FIELD_NOTES);
		try {
			IndexReader ireader = DirectoryReader.open(dirWithOutStem);
			TermFreqIteratorListWrapper tfit = new TermFreqIteratorListWrapper();
			List<AtomicReaderContext> readercs = ireader.leaves();
			for (AtomicReaderContext readerc : readercs) {
				AtomicReader reader = readerc.reader();
				Fields fields = reader.fields();
				for (String field : fields) {
					if (suggFields.size() > 0 && !suggFields.contains(field)) {
						continue;
					}
					Terms terms = fields.terms(field);
					TermsEnum termsEnum = terms.iterator(null);
					tfit.add(termsEnum);
				}
			}
			
			SuggestionAnalyzer analyzer = new SuggestionAnalyzer(Version.LUCENE_32, false);
			//StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_32);
			//StopAnalyzer analyzer = new StopAnalyzer(Version.LUCENE_32);
			//SimpleAnalyzer analyzer = new SimpleAnalyzer(Version.LUCENE_32);
			suggester = new AnalyzingSuggester(analyzer);
			suggester.build(tfit);
			ireader.close();			
		} catch (IOException e) {
			logger.error("Exception encountered attempting to initialise AnalyzingSuggester",e);
		}
	}

	/** Look up terms starting with 'query' in the index. */
	public List<LookupResult> suggest(String query, int numSuggestions) throws Exception {
		return suggester.lookup(query, false, numSuggestions);
	}
	/**
	 * Runs a search query and builds the search results model.
	 * 
	 * @param rawQuery The raw search query.
	 * @param scope The scope of the search.
	 * @param leadTermsOnly A flag which is true if only results which are lead terms are required.
	 */
	public void runQuery(final String rawQuery, @SuppressWarnings("hiding") final ContentIndexing.DocumentType scope,
	    final boolean leadTermsOnly) {
		Stopwatch swBuildModel = new Stopwatch("BuildModel Execution time");
		swBuildModel.start();
		resultsList = buildModel(rawQuery, scope, leadTermsOnly);
		swBuildModel.stop();
		logger.debug(swBuildModel);
	}

	/**
	 * Sanitises part of a raw search query.
	 * 
	 * @param part The part of the query.
	 * @return A sanitised version of the part.
	 */
	private String sanitisePart(final String part) {

		// Ensure dashes are processed correctly.
		if (part.indexOf('-') > -1) {
			return "(\"" + part + "\" OR " + part.replaceAll("\\-", "") + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
		}

		// Wrap parts which contain some symbols in double quotes.
		return part.indexOf('/') > -1 || part.indexOf('.') > -1 || part.indexOf('\'') > -1 ? "\"" + part + "\"" : part; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Sanitises a raw query string so that it is more suitable for the Lucene search engine and so that multiple parts
	 * of the query are 'AND'-ed together to override the default 'OR' behaviour.
	 * 
	 * @param rawQuery The raw query string.
	 * @return The sanitised query.
	 */
	private String sanitiseQuery(final String rawQuery) {
		String sanitisedQuery = rawQuery;

		// Ignore certain characters.
		sanitisedQuery = sanitisedQuery.replaceAll("\\[", ""); //$NON-NLS-1$ //$NON-NLS-2$
		sanitisedQuery = sanitisedQuery.replaceAll("\\]", ""); //$NON-NLS-1$ //$NON-NLS-2$
		sanitisedQuery = sanitisedQuery.replaceAll("\\(", ""); //$NON-NLS-1$ //$NON-NLS-2$
		sanitisedQuery = sanitisedQuery.replaceAll("\\)", ""); //$NON-NLS-1$ //$NON-NLS-2$

		// Convert to lower case.
		sanitisedQuery = sanitisedQuery.toLowerCase();

		// Break the raw query up into parts.
		final List<String> parts = new ArrayList<>();
		int lastPartStart = 0;
		int index = 0;
		do {
			final char c = sanitisedQuery.charAt(index);
			if (c == '\"') {
				int end = -1;
				if (index < sanitisedQuery.length() - 1) {
					end = sanitisedQuery.indexOf('\"', index + 1);
				}
				if (end > -1) {
					parts.add(sanitisedQuery.substring(index, end + 1));
					index = end + 1;
					lastPartStart = index;
				} else {
					++index;
				}
			} else {
				if (c == ' ') {
					if (index > lastPartStart) {
						parts.add(sanitisePart(sanitisedQuery.substring(lastPartStart, index)));
					}
					lastPartStart = index + 1;
				} else if (index == sanitisedQuery.length() - 1) {
					if (index >= lastPartStart) {
						parts.add(sanitisePart(sanitisedQuery.substring(lastPartStart, index + 1)));
					}
				}
				++index;
			}
		} while (index < sanitisedQuery.length());

		// 'AND' the parts together to override the default 'OR' behaviour of queries containing
		// multiple words in Lucene.
		final StringBuilder sb = new StringBuilder();
		sb.append("("); //$NON-NLS-1$
		final boolean containsConjunctions = sanitisedQuery.contains(" and ") || sanitisedQuery.contains(" or "); //$NON-NLS-1$ //$NON-NLS-2$
		for (final String part : parts) {
			final String term = part.trim();
			if (sb.length() > 1) {
				sb.append(containsConjunctions ? " " : " AND "); //$NON-NLS-1$ //$NON-NLS-2$
			}
			sb.append(sanitiseTerm(term));
		}
		sb.append(")"); //$NON-NLS-1$

		return sb.toString();
	}

	/**
	 * Sanitises a term in the query string by expanding terms that start with wild cards.
	 * 
	 * @param expression The search term.
	 * @return The term sanitised.
	 */
	private String sanitiseTerm(final String expression) {
		return expression.startsWith("*") ? expandTermForWildcards(expression) : expression; //$NON-NLS-1$
	}
}