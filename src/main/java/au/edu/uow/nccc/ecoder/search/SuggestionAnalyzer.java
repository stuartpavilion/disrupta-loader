package au.edu.uow.nccc.ecoder.search;

import java.io.Reader;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.util.StopwordAnalyzerBase;
import org.apache.lucene.util.Version;

/**
 * A <code>CustomAnalyzer</code> is a Lucene analyser that is customised for use with CodeXpert.
 * Tokens are delimited by white space, case is ignored, non-ASCII characters are converted to ASCII
 * equivalents where possible and stemming functionality is added.
 * 
 * @author jturnbul
 */
public final class SuggestionAnalyzer extends StopwordAnalyzerBase {

	/**
	 * A flag which is true if stemming is to be applied.
	 */
	private final boolean applyStemming;

	/**
	 * Creates a new <code>CustomAnalyzer</code>.
	 * 
	 * @param matchVersion
	 */
	public SuggestionAnalyzer(final Version matchVersion, final boolean applyStemming) {

		// Use an empty set for the stop words so that all words are indexed.
		//super(matchVersion, new HashSet<String>());
		super(matchVersion);

		this.applyStemming = applyStemming;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.lucene.analysis.ReusableAnalyzerBase#createComponents(java.lang.String,
	 * java.io.Reader)
	 */
	@Override
	protected TokenStreamComponents createComponents(final String fieldName, final Reader reader) {

		// Use white space delimiters for tokens.
		final WhitespaceTokenizer src = new WhitespaceTokenizer(matchVersion, reader);

		// Process words and sub-words.
		TokenStream tok = new WordDelimiterFilter(src, 1, 1, 1, 1, 1, 1, 1, 0, 0, null);
		//TokenStream tok = new WordDelimiterFilter(src, WordDelimiterFilter.GENERATE_WORD_PARTS, null);
		// Convert to lower case.
		tok = new LowerCaseFilter(matchVersion, tok);

		// Apply stop word processing (currently the empty set so that all words are indexed).
		tok = new StopFilter(matchVersion, tok, stopwords);

		// Convert non-ASCII characters to their ASCII equivalent where possible.
		tok = new ASCIIFoldingFilter(tok);

		// Apply stemming.
		if (applyStemming) {
			tok = new PorterStemFilter(tok);
		}

		return new TokenStreamComponents(src, tok);
	}
}