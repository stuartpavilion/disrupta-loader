
package au.edu.uow.nccc.ecoder.special;

/**
 * <code>SpecialCharacters</code> is a place-holder for definitions of special non-ASCII characters used throughout
 * CodeXpert.
 * 
 * @author jturnbul
 */
public class SpecialCharacters {

	/**
	 * The dagger character used in diseases tabular codes.
	 */
	public static final String DAGGER = "\u2020"; //$NON-NLS-1$

	/**
	 * The lozenge character used in the Neoplasms section of the diseases index.
	 */
	public static final String LOZENGE = "\u25ca"; //$NON-NLS-1$

	/**
	 * The Greek alpha character.
	 */
	public static final String ALPHA = "\u03b1"; //$NON-NLS-1$

	/**
	 * The Greek beta character.
	 */
	public static final String BETA = "\u03b2"; //$NON-NLS-1$

	/**
	 * The em-dash character used in diseases tabular content.
	 */
	public static final String EM_DASH = "\u2014"; //$NON-NLS-1$

	/**
	 * The en-dash character used in diseases tabular content.
	 */
	public static final String EN_DASH = "\u2013"; //$NON-NLS-1$

	/**
	 * A black triangle pointing downwards which is used in tabular content to indicate the presence of an ACS standard.
	 */
	public static final String BLACK_DOWN_TRIANGLE = "\u25bc"; //$NON-NLS-1$

	/**
	 * The bullet character used in HTML content.
	 */
	public static final String BULLET = "\u2022"; //$NON-NLS-1$

	/**
	 * The ellipsis character used in HTML content.
	 */
	public static final String ELLIPSIS = "\u2026"; //$NON-NLS-1$

	/**
	 * The raw character that is used to represent the star in circle symbol in the source database.
	 */
	public static final String STAR_IN_CIRCLE = "�"; //$NON-NLS-1$

	/**
	 * A dot character as read from the Access database within tabular content.
	 */
	public static final int DOT = 8226;

	/**
	 * Creates an <img> tag used to display a star in circle for Australian codes within the tabular content.
	 * 
	 * @param size The desired size of the image.
	 * @return An <img> tag used to display a star in circle.
	 */
	public static String createStarInCircle(final int size) {
		return String.format("<img src=\"images/star-in-circle.gif\" width=\"%d\" height=\"%d\">", size, size); //$NON-NLS-1$
	}
}