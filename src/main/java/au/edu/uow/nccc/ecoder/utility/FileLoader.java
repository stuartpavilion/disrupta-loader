
package au.edu.uow.nccc.ecoder.utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * <code>FileLoader</code> is a utility class for loading text files.
 * 
 * @author jturnbul
 */
public class FileLoader {

	/**
	 * Loads the text file from the given stream and returns the contents.
	 * 
	 * @param is The input stream from which to load the file.
	 * @return The contents of the file.
	 */
	public static String loadFile(final InputStream is) {
		return loadFile(new InputStreamReader(is));
	}

	/**
	 * Loads the text file using the given reader and returns the contents.
	 * 
	 * @param fileName The name of the file to load.
	 * @return The contents of the file.
	 */
	private static String loadFile(final Reader reader) {
		final StringBuilder sb = new StringBuilder();
		BufferedReader br;
		try {
			br = new BufferedReader(reader);
			String s;
			while ((s = br.readLine()) != null) {
				sb.append(s);
				sb.append("\n"); //$NON-NLS-1$
			}
			br.close();
		} catch (final IOException ioe) {
			return null;
		}

		return sb.toString();
	}

	/**
	 * Loads the text file with the given name and returns the contents.
	 * 
	 * @param fileName The name of the file to load.
	 * @return The contents of the file.
	 */
	public static String loadFile(final String fileName) {
		try {
			return loadFile(new FileReader(fileName));
		} catch (final FileNotFoundException fnfe) {
			return null;
		}
	}
}