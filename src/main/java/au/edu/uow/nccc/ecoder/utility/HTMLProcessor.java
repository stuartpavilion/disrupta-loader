
package au.edu.uow.nccc.ecoder.utility;

/**
 * <code>HTMLProcessor</code> is a utility class for processing HTML documents.
 * 
 * @author mzhang
 */
public class HTMLProcessor {

	/**
	 * Inserts a &lt;base&gt; tag into an HTML document.
	 * 
	 * @param html The HTML content.
	 * @param folderName The name of the folder to refer to in the base tag.
	 * @return The HTML content with the base tag inserted.
	 */
	public static String addBaseTag(final String html, final String folderName) {
		int pos = html.toLowerCase().indexOf("<head"); //$NON-NLS-1$
		if (pos > 0) {
			pos = html.indexOf('>', pos) + 1;

			return html.substring(0, pos) + "<base href=\"" + folderName + "\"/>" + html.substring(pos); //$NON-NLS-1$ //$NON-NLS-2$
		}

		return html;
	}
}