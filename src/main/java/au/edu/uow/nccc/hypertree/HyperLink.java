/**
 * 
 */
package au.edu.uow.nccc.hypertree;

/**
 * @author jturnbul
 * 
 */
public class HyperLink {

	public enum Type {
		ID, EXPRESSION
	}

	private final Type type;
	private final Object view;
	private final String category;
	private final Integer id;
	private final String expression;
	private String display;

	public HyperLink(final Object view, final String category, final Integer id, final String display) {
		this(Type.ID, view, category, id, null, display);
	}

	public HyperLink(final Object view, final String category, final String expression, final String display) {
		this(Type.EXPRESSION, view, category, -1, expression, display);
	}

	/**
	 * @param view
	 * @param id
	 */
	private HyperLink(final Type type, final Object view, final String category, final Integer id,
			final String expression, final String display) {
		this.type = type;
		this.view = view;
		this.category = category;
		this.id = id;
		this.expression = expression;
		this.display = display;
	}

	public String getCategory() {
		return category;
	}

	/**
	 * @return the display
	 */
	public String getDisplay() {
		return display;
	}

	public String getExpression() {
		return expression;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	public Type getType() {
		return type;
	}

	/**
	 * @return the view
	 */
	public Object getView() {
		return view;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay(final String display) {
		this.display = display;
	}
}