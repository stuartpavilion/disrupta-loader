package com.i_mmerce.disruptalookup.dao;

public interface BaseCrudDao<T> extends BaseDao {
	public void save(T t);
	public void delete(T t);
	public T findById(long id);
	void merge(T t);
}
