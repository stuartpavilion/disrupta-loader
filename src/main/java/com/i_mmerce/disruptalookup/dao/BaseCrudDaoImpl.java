package com.i_mmerce.disruptalookup.dao;

public abstract class BaseCrudDaoImpl<T> extends BaseDaoImpl implements BaseCrudDao<T> {

	private Class<T> theClass;
	
	
	public Class<T> getTheClass() {
		return theClass;
	}

	public BaseCrudDaoImpl(Class<T> theClass) {
		super();
		this.theClass = theClass;
	}

	@Override
	public void save(T t) {
		getCurrentSession().saveOrUpdate(t);
		getCurrentSession().flush();		
	}

	@Override
	public void delete(T t) {
		getCurrentSession().delete(t);
		getCurrentSession().flush();
	}

	@Override
	public T findById(long id) {
		return (T) getCurrentSession().load(theClass, id);
	}
	
	@Override
	public void merge(T t) {
		getCurrentSession().merge(t);
		getCurrentSession().flush();		
	}	

}
