package com.i_mmerce.disruptalookup.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public interface BaseDao {
	
	public Session getCurrentSession();

	public SessionFactory getSessionFactory();

	public void setSessionFactory(SessionFactory sessionFactory);
}
