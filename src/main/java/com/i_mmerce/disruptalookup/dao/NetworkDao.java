package com.i_mmerce.disruptalookup.dao;

import java.util.List;

import com.i_mmerce.disruptalookup.domain.Network;

public interface NetworkDao 
{
	void save(Network network);
	
	void delete(Network network);
	
	void delete(long id);
	
	public Network findById(long id);
	
	public List<Network> getAll();
	
	public List<Network> searchAllExcept(Network network, String includeStr,
			  String excludeStr); 

}
