package com.i_mmerce.disruptalookup.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.global.StringUtils;

@Repository("networkDao")
public class NetworkDaoImpl extends BaseDaoImpl implements NetworkDao {

	@Override
	public void save(Network network) {
		getCurrentSession().saveOrUpdate(network);
		getCurrentSession().flush();
	}

	@Override
	public void delete(Network network) {
		getCurrentSession().delete(network);
		getCurrentSession().flush();
	}

	@Override
	public void delete(long id) {
		Query query = getCurrentSession().createQuery("delete Network where id = :id");
		query.setParameter("id", id);
		int result = query.executeUpdate();
	}

	@Override
	public Network findById(long id) {
		return (Network) getCurrentSession().createCriteria(Network.class).add(Restrictions.eq("id", id)).uniqueResult();
	}

	@Override
	public List<Network> getAll() {
		return getCurrentSession().createCriteria(Network.class).
				setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public List<Network> searchAllExcept(Network network, String includeStr, String excludeStr) {
		Criteria criteria = getCurrentSession().createCriteria(Network.class);
		if (network != null) {
			criteria.add(Restrictions.not(Restrictions.eq("id", network.getId())));
		}
		if (StringUtils.isNotEmpty(includeStr)){
			//replace all * with % .
			String inc = includeStr.replace('*', '%');
			//Make sure search that there is atleast on % at the end
			if (!inc.contains("%")){
				inc = inc + "%";
			}
			Disjunction any = Restrictions.disjunction();
			any.add(Restrictions.like("name", inc).ignoreCase());
			any.add(Restrictions.like("description", inc).ignoreCase());
			criteria.add(any);
		}
		if (StringUtils.isNotEmpty(excludeStr)){
			//replace all * with % .
			String ex = excludeStr.replace('*', '%');
			//Make sure search that there is atleast on % at the end
			if (!ex.contains("%")){
				ex = ex + "%";
			}
			criteria.add(Restrictions.not(Restrictions.like("name", ex).ignoreCase()));
			criteria.add(Restrictions.not(Restrictions.like("description", ex).ignoreCase()));
		}		
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

}
