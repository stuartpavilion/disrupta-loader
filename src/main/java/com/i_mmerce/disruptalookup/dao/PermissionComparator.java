package com.i_mmerce.disruptalookup.dao;

import java.util.Comparator;

import com.i_mmerce.disruptalookup.domain.Permission;


public class PermissionComparator implements Comparator<Permission> {
	
	public enum Mode {
		Description,
		Name,
		AreaAndDescripton,
		AreaAndName
	}
	
	public enum Direction{
		Asc,
		Desc
	}
	
	private Mode comparisonMode = Mode.Description;
	private Direction sortDir = Direction.Asc;
	
	public PermissionComparator(Mode comparisonMode, Direction sortDir) {
		super();
		this.comparisonMode = comparisonMode;
		this.sortDir = sortDir;
	}

	public PermissionComparator() {
		super();
	}

	public PermissionComparator(Mode comparisonMode) {
		super();
		this.comparisonMode = comparisonMode;
	}
	
	public PermissionComparator(Direction sortDir) {
		super();
		this.sortDir = sortDir;
	}	

	@Override
	public int compare(Permission p0, Permission p1) {
		int multiple = 1;
		switch (sortDir){
			case Desc: multiple = -1; 
		}
		switch (comparisonMode){
			case Description:
				return multiple * p0.getDescription().compareToIgnoreCase(p1.getDescription());		
			case Name:
				return multiple * p0.getName().compareToIgnoreCase(p1.getName());
			case AreaAndDescripton:
				String s0 = p0.getArea()+p0.getDescription();
				String s1 = p1.getArea()+p1.getDescription();
				return multiple * s0.compareToIgnoreCase(s1);				
			case AreaAndName:
				s0 = p0.getArea()+p0.getName();
				s1 = p1.getArea()+p1.getName();
				return multiple * s0.compareToIgnoreCase(s1);
			default:
				return multiple * p0.getDescription().compareToIgnoreCase(p1.getDescription());
		}
	}

}
