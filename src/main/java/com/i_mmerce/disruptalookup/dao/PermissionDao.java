package com.i_mmerce.disruptalookup.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.i_mmerce.disruptalookup.domain.Permission;
import com.i_mmerce.disruptalookup.domain.User;

public interface PermissionDao{

	public List<Permission> getAll();

	public Permission findByName(String name);
	
	public Set<Permission> getAllCurrentAtDate(User user, Date date);

	public List<Permission> getAllExcept(String ... names);

}
