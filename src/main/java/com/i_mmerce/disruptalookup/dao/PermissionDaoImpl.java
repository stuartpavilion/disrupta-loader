package com.i_mmerce.disruptalookup.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.i_mmerce.disruptalookup.domain.Permission;
import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroup;



@Repository("permissionDao")
public class PermissionDaoImpl extends BaseDaoImpl implements
		PermissionDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Permission> getAll() {
		return (List<Permission>)getCurrentSession().createCriteria(Permission.class)
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public Permission findByName(String name) {
		return (Permission)getCurrentSession().createCriteria(Permission.class)
					.add(Restrictions.eq("name", name)).uniqueResult();
	}

	@Override
	public Set<Permission> getAllCurrentAtDate(User user, Date date) {

		Query query = getCurrentSession().createQuery("select uga.userGroup from UserGroupAccess as uga"+
													  "  where uga.user = :user and ( uga.timeLimitedAccess = false or" +
													  " (uga.effectiveFrom <= :date and uga.effectiveTo >= :date) )"
													);
		query.setParameter("user", user);
		query.setParameter("date", date);
		@SuppressWarnings("unchecked")
		List<UserGroup> userGroups = (List<UserGroup>) query.list();
		Set<Permission> permissions = new HashSet<Permission>();
		for (UserGroup userGroup: userGroups){
			permissions.addAll(userGroup.getPermissions());
		}
		return permissions;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Permission> getAllExcept(String ... names) {
		return (List<Permission>)getCurrentSession().createCriteria(Permission.class)
				.add(Restrictions.not(Restrictions.in("name", names)))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
		
	}	

}
