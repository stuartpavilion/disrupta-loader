package com.i_mmerce.disruptalookup.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.i_mmerce.disruptalookup.domain.Permission;

public class PermissionSorter {
	
	PermissionComparator comparator;

	public PermissionSorter() {
		super();
		comparator = new PermissionComparator();
	}
	
	public PermissionSorter(PermissionComparator.Mode comparisonMode, PermissionComparator.Direction sortDir) {
		super();
		comparator = new PermissionComparator(comparisonMode,sortDir);
	}	

	public PermissionSorter(PermissionComparator.Mode comparisonMode) {
		super();
		comparator = new PermissionComparator(comparisonMode);
	}
	
	public PermissionSorter(PermissionComparator.Direction sortDir) {
		super();
		comparator = new PermissionComparator(sortDir);
	}
	
	
	public List<Permission> sortCollection(Collection<Permission> perms){
		List<Permission> permList = new ArrayList<Permission>();
		permList.addAll(perms);
		Collections.sort(permList, comparator);
		return permList;
	}
}
