package com.i_mmerce.disruptalookup.dao;

import java.util.List;
import java.util.Set;

import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroup;
import com.i_mmerce.disruptalookup.domain.UserGroupAccess;

public interface UserDao extends BaseDao{

	User findByUserName(String username);
	
	List<User> findAll();
	
	List<UserGroup> findUserGroups(User user);
	
	Set<UserGroupAccess> findAccessProfilesByUserGroup(Long groupId);
	
	void save(User user);
	
	void delete(User user);
	
	void delete(long id);
	
	User findById(long id);

	List<User> findAllExcept(User user);

	List<User> searchAllExcept(User user, String includeStr, String excludeStr,Network network);

	void merge(User user);

}
