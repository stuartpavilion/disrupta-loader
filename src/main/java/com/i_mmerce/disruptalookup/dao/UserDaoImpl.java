package com.i_mmerce.disruptalookup.dao;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroup;
import com.i_mmerce.disruptalookup.domain.UserGroupAccess;
import com.i_mmerce.global.StringUtils;

@Repository("userDao")
//atTransactional
public class UserDaoImpl extends BaseDaoImpl implements UserDao{
    private static final Logger logger = Logger.getLogger(UserDaoImpl.class); 

	@Override
	public List<User> findAll() {
		return getCurrentSession().createCriteria(User.class).
				setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public User findById(long id) {
		//return (User) getCurrentSession().load(User.class,id);
		return (User) getCurrentSession().createCriteria(User.class).add(Restrictions.eq("id", id)).uniqueResult();
	}

	@Override
	public void delete(User user) {
		getCurrentSession().delete(user);
		getCurrentSession().flush();
		
	}

	@Override
	public void delete(long id) {
		Query query = getCurrentSession().createQuery("delete User where id = :id");
		query.setParameter("id", id);
		int result = query.executeUpdate();
	}

	@Override
	public List<User> findAllExcept(User user) {
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.not(Restrictions.eq("id", user.getId())));
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	@Override
	public List<User> searchAllExcept(User user, String includeStr,
			String excludeStr, Network network) {
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		if (user != null){
			criteria.add(Restrictions.not(Restrictions.eq("id", user.getId())));
		}
		if (network != null){
			criteria.add(Restrictions.eq("network.id", network.getId()));
		}
		if (StringUtils.isNotEmpty(includeStr)){
			//replace all * with % .
			String inc = includeStr.replace('*', '%');
			//Make sure search that there is atleast one % at the end
			if (!inc.contains("%")){
				inc = inc + "%";
			}
			Disjunction any = Restrictions.disjunction();
			any.add(Restrictions.like("username", inc).ignoreCase());
			any.add(Restrictions.like("firstName", inc).ignoreCase());
			any.add(Restrictions.like("lastName", inc).ignoreCase());
			criteria.add(any);
		}
		if (StringUtils.isNotEmpty(excludeStr)){
			//replace all * with % .
			String ex = excludeStr.replace('*', '%');
			//Make sure search that there is atleast one % at the end
			if (!ex.contains("%")){
				ex = ex + "%";
			}
			criteria.add(Restrictions.not(Restrictions.like("username", ex).ignoreCase()));
			criteria.add(Restrictions.not(Restrictions.like("firstName", ex).ignoreCase()));
			criteria.add(Restrictions.not(Restrictions.like("lastName", ex).ignoreCase()));
		}		
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	@Override
	public User findByUserName(String username) 
	{
		return (User)getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("username", username))
				.uniqueResult();	
		}

	@Override
	public List<UserGroup> findUserGroups(User user) {
		return null;
	}
	
	@Override
	public Set<UserGroupAccess> findAccessProfilesByUserGroup(Long groupId) {
		return null;
	}

	@Override
	public void save(User user)
	{
		getCurrentSession().saveOrUpdate(user);
		getCurrentSession().flush();
	}
	
	@Override
	public void merge(User user)
	{
		getCurrentSession().merge(user);
		getCurrentSession().flush();
	}
}
