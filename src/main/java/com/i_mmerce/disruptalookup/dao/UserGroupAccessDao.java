package com.i_mmerce.disruptalookup.dao;

import java.util.List;

import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroupAccess;

public interface UserGroupAccessDao {
	public List<UserGroupAccess> getUserGroupAccessList(User user);

	public void save(UserGroupAccess userGroupAccess);

	public void delete(long id);

	public void deleteByUserId(long id); 
}
