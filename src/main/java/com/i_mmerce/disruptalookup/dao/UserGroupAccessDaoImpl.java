package com.i_mmerce.disruptalookup.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroupAccess;

@Repository("userGroupAccessDao")
public class UserGroupAccessDaoImpl extends BaseDaoImpl implements
		UserGroupAccessDao {

	@Override
	public List<UserGroupAccess> getUserGroupAccessList(User user) {
		Criteria criteria = getCurrentSession().createCriteria(UserGroupAccess.class);
		criteria.add(Restrictions.eq("user", user));
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}


	@Override
	public void save(UserGroupAccess userGroupAccess)
	{
		getCurrentSession().saveOrUpdate(userGroupAccess);
	}
	
	@Override
	public void delete(long id) {
		Query query = getCurrentSession().createQuery("delete UserGroupAccess where id = :id");
		query.setParameter("id", id);
		int result = query.executeUpdate();
	}
	
	@Override
	public void deleteByUserId(long id) {
		Query query = getCurrentSession().createQuery("delete UserGroupAccess where user.id = :id");
		query.setParameter("id", id);
		int result = query.executeUpdate();
	}
}
