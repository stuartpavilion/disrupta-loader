package com.i_mmerce.disruptalookup.dao;

import java.util.List;

import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.domain.UserGroup;

public interface UserGroupDao {
	public List<UserGroup> getAllAvailUserGroups(Network network);
	public void save(UserGroup userGroup);
	void delete(long id);
	public UserGroup findById(long id);
	public List<UserGroup> searchAllExcept(UserGroup userGroup, String includeStr,
									  String excludeStr, Network network); 

}
