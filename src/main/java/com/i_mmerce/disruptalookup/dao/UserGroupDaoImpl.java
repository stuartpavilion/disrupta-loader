package com.i_mmerce.disruptalookup.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.domain.UserGroup;
import com.i_mmerce.global.StringUtils;

@Repository("userGroupDao")
public class UserGroupDaoImpl extends BaseDaoImpl implements UserGroupDao {

	@Override
	public List<UserGroup> getAllAvailUserGroups(Network network) {
		Criteria criteria = getCurrentSession().createCriteria(UserGroup.class);
		criteria.add(Restrictions.eq("network", network));
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}

	@Override
	public void save(UserGroup userGroup) {
		getCurrentSession().saveOrUpdate(userGroup);
		getCurrentSession().flush();
	}
	
	@Override
	public void delete(long id) {
		Session session = getCurrentSession();
		UserGroup userGroup = (UserGroup) session.load(UserGroup.class, id);
		userGroup.getPermissions().clear();
		session.delete(userGroup);
		session.flush();
	}

	@Override
	public UserGroup findById(long id) {
		return (UserGroup) getCurrentSession().createCriteria(UserGroup.class).add(Restrictions.eq("id", id)).uniqueResult();
	}
	
	@Override
	public List<UserGroup> searchAllExcept(UserGroup userGroup, String includeStr, String excludeStr,
			Network network) {
		Criteria criteria = getCurrentSession().createCriteria(UserGroup.class);
		if (userGroup != null) {
			criteria.add(Restrictions.not(Restrictions.eq("id", userGroup.getId())));
		}
		if (network != null) {
			criteria.add(Restrictions.eq("network", network));
		}
		if (StringUtils.isNotEmpty(includeStr)){
			//replace all * with % .
			String inc = includeStr.replace('*', '%');
			//Make sure search that there is atleast on % at the end
			if (!inc.contains("%")){
				inc = inc + "%";
			}
			Disjunction any = Restrictions.disjunction();
			any.add(Restrictions.like("name", inc).ignoreCase());
			any.add(Restrictions.like("description", inc).ignoreCase());
			criteria.add(any);
		}
		if (StringUtils.isNotEmpty(excludeStr)){
			//replace all * with % .
			String ex = excludeStr.replace('*', '%');
			//Make sure search that there is atleast on % at the end
			if (!ex.contains("%")){
				ex = ex + "%";
			}
			criteria.add(Restrictions.not(Restrictions.like("name", ex).ignoreCase()));
			criteria.add(Restrictions.not(Restrictions.like("description", ex).ignoreCase()));
		}		
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return criteria.list();
	}	

}
