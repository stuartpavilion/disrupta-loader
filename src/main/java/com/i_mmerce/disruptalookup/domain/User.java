/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.i_mmerce.global.StringUtils;

//import org.hibernate.annotations.Cascade;
//import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Stuart Sontag
 */
@Entity
@Table(name="USER_ACCOUNT")
@NamedQueries({
    @NamedQuery(name = "User.findByUsername",
    			query = "SELECT u FROM User u WHERE u.username = :username")     			
})
public class User implements Serializable {
    @Id
    @GeneratedValue
	private Long id;
    
    @Column(name="USERNAME", nullable=false, unique=true)
	private String username;
	private String email;
	@Column(name="firstname", nullable=false)
	private String firstName;
	@Column(name="lastname", nullable=false)
	private String lastName;
	private String password;
	@Column(name="created", nullable=false)
	private Timestamp createdTimestamp;
	
	//Spring security fields
    private boolean enabled;
    private boolean accountNotExpired;
    private boolean credentialsNonExpired;
    private boolean accountNonLocked;
    @Column(name="LDAP_USER")
    private boolean ldapUser;
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="network_id")
	private Network network;

	
	public User() {
		super();
	}
	
	
	public User(Long id, String username, String email, String firstName,
			String lastName, String password, Timestamp createdTimestamp,
			boolean enabled, boolean accountNotExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Network network) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.createdTimestamp = createdTimestamp;
		this.enabled = enabled;
		this.accountNotExpired = accountNotExpired;
		this.credentialsNonExpired = credentialsNonExpired;
		this.accountNonLocked = accountNonLocked;
		this.network = network;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String userName) {
		this.username = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFullName() {
		return (StringUtils.trim(firstName)+" "+StringUtils.trim(lastName)).trim();
	}
	
	public String getDisplayName() {
		return (StringUtils.trim(firstName)+" "+StringUtils.trim(lastName)).trim();
	}	
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Timestamp getCreatedTimestamp() {
		return createdTimestamp;
	}
	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	public Network getNetwork() {
		return network;
	}
	public void setNetwork(Network network) {
		this.network = network;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean isAccountNotExpired() {
		return accountNotExpired;
	}
	public void setAccountNotExpired(boolean accountNotExpired) {
		this.accountNotExpired = accountNotExpired;
	}
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}
	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}
	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}
	public boolean isLdapUser() {
		return ldapUser;
	}
	public void setLdapUser(boolean ldapUser) {
		this.ldapUser = ldapUser;
	}


	public String toSafeString() {
		return "User [id=" + id + ", username=" + username + ", email=" + email
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", createdTimestamp=" + createdTimestamp + ", enabled="
				+ enabled + ", accountNotExpired=" + accountNotExpired
				+ ", credentialsNonExpired=" + credentialsNonExpired
				+ ", accountNonLocked=" + accountNonLocked
				+ ", ldapUser=" + ldapUser
				+ ", network=" + network + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (accountNonLocked ? 1231 : 1237);
		result = prime * result + (accountNotExpired ? 1231 : 1237);
		result = prime
				* result
				+ ((createdTimestamp == null) ? 0 : createdTimestamp.hashCode());
		result = prime * result + (credentialsNonExpired ? 1231 : 1237);
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (enabled ? 1231 : 1237);
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + (ldapUser ? 1231 : 1237);
		result = prime * result + ((network == null) ? 0 : network.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (accountNonLocked != other.accountNonLocked)
			return false;
		if (accountNotExpired != other.accountNotExpired)
			return false;
		if (createdTimestamp == null) {
			if (other.createdTimestamp != null)
				return false;
		} else if (!createdTimestamp.equals(other.createdTimestamp))
			return false;
		if (credentialsNonExpired != other.credentialsNonExpired)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (enabled != other.enabled)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (ldapUser != other.ldapUser)
			return false;
		if (network == null) {
			if (other.network != null)
				return false;
		} else if (!network.equals(other.network))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName()+"("+id+") ["+username+"]";
	}
	
}