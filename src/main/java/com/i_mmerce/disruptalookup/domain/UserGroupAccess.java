package com.i_mmerce.disruptalookup.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="USER_GROUP_ACCESS")
public class UserGroupAccess implements Serializable
{
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="user_id")	
	private User user;
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="usergroup_id")	
	private UserGroup userGroup;
	
	private boolean timeLimitedAccess;
	
	/** Effective "From" date in yyyy-MM-dd. */
	private Date effectiveFrom;
	
	/** Effective "To" date in yyyy-MM-dd. */
	private Date effectiveTo;

	public UserGroupAccess() {
		super();
	}

	public UserGroupAccess(Long id, User user, UserGroup userGroup,
			boolean timeLimitedAccess, Date effectiveFrom, Date effectiveTo) {
		super();
		this.id = id;
		this.user = user;
		this.userGroup = userGroup;
		this.timeLimitedAccess = timeLimitedAccess;
		this.effectiveFrom = effectiveFrom;
		this.effectiveTo = effectiveTo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserGroup getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}
	
	public boolean isTimeLimitedAccess() {
		return timeLimitedAccess;
	}

	public void setTimeLimitedAccess(boolean timeLimitedAccess) {
		this.timeLimitedAccess = timeLimitedAccess;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((effectiveFrom == null) ? 0 : effectiveFrom.hashCode());
		result = prime * result
				+ ((effectiveTo == null) ? 0 : effectiveTo.hashCode());
		result = prime * result + (timeLimitedAccess ? 1231 : 1237);
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result
				+ ((userGroup == null) ? 0 : userGroup.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserGroupAccess other = (UserGroupAccess) obj;
		if (effectiveFrom == null) {
			if (other.effectiveFrom != null)
				return false;
		} else if (!effectiveFrom.equals(other.effectiveFrom))
			return false;
		if (effectiveTo == null) {
			if (other.effectiveTo != null)
				return false;
		} else if (!effectiveTo.equals(other.effectiveTo))
			return false;
		if (timeLimitedAccess != other.timeLimitedAccess)
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (userGroup == null) {
			if (other.userGroup != null)
				return false;
		} else if (!userGroup.equals(other.userGroup))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserGroupAccess [id=" + id + ", user=" + user + ", userGroup="
				+ userGroup + ", timeLimitedAccess=" + timeLimitedAccess
				+ ", effectiveFrom=" + effectiveFrom + ", effectiveTo="
				+ effectiveTo + "]";
	}

	



}
