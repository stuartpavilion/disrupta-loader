package com.i_mmerce.disruptalookup.mvc;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.search.suggest.Lookup.LookupResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.i_mmerce.disruptalookup.mvc.json.Autocomplete;
import com.i_mmerce.disruptalookup.service.LuceneSearchService;
import com.i_mmerce.disruptalookup.util.Stopwatch;

@Controller
@RequestMapping("/autocomplete")
public class AutomcompleteSearchController {
	
	private static final Logger logger = Logger.getLogger(AutomcompleteSearchController.class);
	private static final int MAX_NUM_SUGGESTIONS = 30;
	@Autowired
	private LuceneSearchService luceneSearchService;

	@RequestMapping(value="/orig/{ngram}", method=RequestMethod.GET)
	public @ResponseBody Autocomplete ngramSearch(@PathVariable("ngram") String ngram, Model model){
		logger.debug("ngram="+ngram);
		Stopwatch sw = new Stopwatch("Suggest Lookup execution time");sw.start();
		//int searchType = 2;
		//List<SearchResult> searchResults = luceneSearchService.runCodeExpertQuery(query, searchType);
		List<LookupResult> lookupResults = luceneSearchService.suggestLookup(ngram, MAX_NUM_SUGGESTIONS); 
		sw.stop();logger.debug(sw);
		Autocomplete autocomplete= new Autocomplete(ngram);
		
		//for (SearchResult searchResult: searchResults){
		//	autocomplete.getResults().add(searchResult.getDocumentAsCodeTerm().getPreferredText());
		//}
		for (LookupResult lookupResult: lookupResults){
			autocomplete.getResults().add((String) lookupResult.key);
		}
		return autocomplete;
	}
	
	@RequestMapping(value="/{ngram}", method=RequestMethod.GET)
	public @ResponseBody Autocomplete ngramPostProcessingSearch(@PathVariable("ngram") String ngram, Model model){
		logger.debug("ngram="+ngram);
		Stopwatch sw = new Stopwatch("Suggest Lookup execution time");sw.start();
		List<LookupResult> lookupResults = luceneSearchService.suggestLookup(ngram, MAX_NUM_SUGGESTIONS); 
		sw.stop();logger.debug(sw);
		Autocomplete autocomplete= new Autocomplete(ngram);
		
		for (LookupResult lookupResult: lookupResults){
			String tok = (String) lookupResult.key;
			//if (  !(tok.contains(",") || tok.contains("/")  ) ){
			// \w is the set of word characters is A..Z,a..z,_,0..9
			if (tok.matches("\\w*")){
				autocomplete.getResults().add(tok);
			}
		}
		return autocomplete;
	}	
}
