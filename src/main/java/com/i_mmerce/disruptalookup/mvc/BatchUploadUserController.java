/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.mvc;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.mvc.form.BatchUploadForm;
import com.i_mmerce.disruptalookup.service.BatchUserFactory;
import com.i_mmerce.disruptalookup.service.UserManager;

/** 
 *
 * @author Stuart Sontag
 */
@Controller
@RequestMapping("/admin/user")
@SessionAttributes({"allRoles","userForm","batchUploadedUsers"})
public class BatchUploadUserController{

    private static final Logger logger = Logger.getLogger(BatchUploadUserController.class);
    
    private static final String BASE_UPLOAD_DIRECTORY = "../statusview/upload/"; 
    private static final String CRM_ROLE_NAME = "ROLE_CRM";
    
    @Autowired
    private UserManager userManager;

    @RequestMapping(value="/batchupload.htm", method=RequestMethod.GET)
    public String getForm(Model model){
    	return "batchuploaduser";
    }
    
    @RequestMapping(value="/batchimport.htm", method=RequestMethod.POST)
    public String updateUser(@ModelAttribute("batchUploadForm") BatchUploadForm batchUploadForm,
    		Model model){
    	
    	BatchUserFactory batchUserFactory = new BatchUserFactory(/**userManager.getRole(CRM_ROLE_NAME),**/ userManager.getPasswordEncoder());
    	List<User> users = batchUserFactory.createUsersFromImportFile(BASE_UPLOAD_DIRECTORY, batchUploadForm);
    	for (User user: users){
    		logger.debug("About to persist: " + user.toSafeString());
    		userManager.persistUser(user, true);
    	}
    	model.addAttribute("batchUploadedUsers", users);
    	return "redirect:/admin/user/viewusers.htm";
    }  
    
    @RequestMapping(value="/batchdelete.htm", method=RequestMethod.GET)
    public String batchDelete(@ModelAttribute("batchUploadedUsers") List<User> batchUploadedUsers,
    		Model model){
    	for (User user: batchUploadedUsers){
    		logger.debug("About to delete: " + user.toSafeString());
    		userManager.deleteById(user.getId());
    	}
    	batchUploadedUsers.clear();
    	model.addAttribute("batchUploadedUsers", batchUploadedUsers);
    	return "redirect:/admin/user/viewusers.htm";
    }  
    
 
}