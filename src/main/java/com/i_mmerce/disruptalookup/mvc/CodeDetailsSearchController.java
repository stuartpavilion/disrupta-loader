package com.i_mmerce.disruptalookup.mvc;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.uow.nccc.ecoder.model.bean.MasterCode;
import au.edu.uow.nccc.ecoder.search.ContentIndexing;
import au.edu.uow.nccc.ecoder.search.SearchResult;
import au.edu.uow.nccc.ecoder.search.SearchResultIDComparator;

import com.i_mmerce.disruptalookup.mvc.json.CodeDetails;
import com.i_mmerce.disruptalookup.service.LuceneSearchService;
import com.i_mmerce.disruptalookup.util.Stopwatch;

@Controller
@RequestMapping("/code_details")
public class CodeDetailsSearchController {
	
	private static final Logger logger = Logger.getLogger(CodeDetailsSearchController.class);
	
	@Autowired
	private LuceneSearchService luceneSearchService;

	@RequestMapping(value="/{code}", method=RequestMethod.GET)
	public @ResponseBody CodeDetails indexTreeSearch(@PathVariable("code") String code, Model model){
		logger.info("Code Details Search: code="+code);
		CodeDetails codeDetails = new CodeDetails(code);
		//First try looking for an Intervention
		Stopwatch sw = new Stopwatch("Code Details INTERVENTIONS_MASTER_CODE Search execution time");sw.start();
		List<SearchResult> searchResults = luceneSearchService.runCodeExpertQuery(code, ContentIndexing.DocumentType.INTERVENTIONS_MASTER_CODE, false);
		Collections.sort(searchResults, new SearchResultIDComparator());		
		sw.stop();logger.info(sw);
		if (searchResults!= null && !searchResults.isEmpty()){
			MasterCode masterCode = (MasterCode) searchResults.get(0).getDocumentAsCodeTerm();
			codeDetails.setDescription(masterCode.getDescription());
			codeDetails.setBody(masterCode.getNotes());
			return codeDetails;
		}
		//Haven't found an intervention so look for a disease.
		sw.reset("Code Details DISEASES_MASTER_CODE Search execution time");sw.start();
		searchResults = luceneSearchService.runCodeExpertQuery(code, ContentIndexing.DocumentType.DISEASES_MASTER_CODE, false);
		Collections.sort(searchResults, new SearchResultIDComparator());		
		sw.stop();logger.info(sw);
		if (searchResults!= null && !searchResults.isEmpty()){
			MasterCode masterCode = (MasterCode) searchResults.get(0).getDocumentAsCodeTerm();
			codeDetails.setDescription(masterCode.getDescription());
			codeDetails.setBody(masterCode.getNotes());
			return codeDetails;
		}		
		return codeDetails;
	}
	
}
