package com.i_mmerce.disruptalookup.mvc;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.uow.nccc.ecoder.search.SearchResult;

import com.i_mmerce.disruptalookup.mvc.json.MessageComposite;
import com.i_mmerce.disruptalookup.service.LuceneSearchService;
import com.i_mmerce.disruptalookup.util.Stopwatch;

@Controller
@RequestMapping("/codexp")
public class CodeExpSearchController {
	
	private static final Logger logger = Logger.getLogger(CodeExpSearchController.class);
	
	@Autowired
	private LuceneSearchService luceneSearchService;

	@RequestMapping(value="/search", method=RequestMethod.GET)
	public String getSearch(Model model){
		model.addAttribute("query", "");
		model.addAttribute("searchType", 0);
		model.addAttribute("searchResults", "");
		model.addAttribute("maxDepth", -1);
		return "searchcodepage";
	}	
	
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public String search(@RequestParam String query, @RequestParam int searchType,
			@RequestParam(value="maxdepth", required=false) Integer maxChildDepth, Model model){
		logger.debug("Query="+query+", searchType="+searchType);
		Stopwatch sw = new Stopwatch("RunCodeExperQuery execution time");
		sw.start();
		List<SearchResult> searchResults = luceneSearchService.runCodeExpertQuery(query, searchType);
		sw.stop();
		logger.debug(sw);
		if (maxChildDepth!=null && maxChildDepth<0){
			maxChildDepth=null;
		}
		StringBuilder sbHierarchy = new StringBuilder();
		StringBuilder sbFlattened = new StringBuilder();
		boolean afterFirstLine = false;
		for (SearchResult searchResult: searchResults){
			if (afterFirstLine){
				sbFlattened.append("\n");
				sbHierarchy.append("\n");
			} else {
				afterFirstLine = true;
			}
			//sb.append(searchResult.getDisplay());
			sbHierarchy.append(searchResult.getDocumentAsCodeTerm().getHeirarchyMessage(maxChildDepth).toString());
			sbFlattened.append(searchResult.getDocumentAsCodeTerm().getPreferredText());
		}
		model.addAttribute("query", query);
		model.addAttribute("searchType", searchType);
		model.addAttribute("searchResults", sbHierarchy.toString());
		model.addAttribute("searchResultsFlat", sbFlattened.toString());
		model.addAttribute("maxDepth", maxChildDepth);
		return "searchcodepage";
	}
	
	@RequestMapping(value="/codesearch", method=RequestMethod.GET)
	public @ResponseBody List<MessageComposite> apiSearch(@RequestParam String query, @RequestParam int searchType,
			@RequestParam(value="maxdepth", required=false) Integer maxChildDepth, Model model){
		logger.debug("Query="+query+", searchType="+searchType+", maxChildDepth="+maxChildDepth);
		Stopwatch sw = new Stopwatch("RunCodeExperQuery execution time");
		sw.start();
		List<SearchResult> searchResults = luceneSearchService.runCodeExpertQuery(query, searchType);
		sw.stop();
		logger.debug(sw);
		if (maxChildDepth!=null && maxChildDepth<0){
			maxChildDepth=null;
		}
		
		List<MessageComposite> messages = new ArrayList<>();
		for (SearchResult searchResult: searchResults){
			messages.add(searchResult.getDocumentAsCodeTerm().getHeirarchyMessage(maxChildDepth));
		}
		return messages;
	}	
}
