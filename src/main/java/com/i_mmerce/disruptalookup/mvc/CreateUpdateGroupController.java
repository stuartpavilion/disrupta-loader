/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.mvc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.i_mmerce.disruptalookup.dao.PermissionComparator;
import com.i_mmerce.disruptalookup.dao.PermissionSorter;
import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.domain.Permission;
import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroup;
import com.i_mmerce.disruptalookup.mvc.form.UserGroupForm;
import com.i_mmerce.disruptalookup.service.UserManager;
import com.i_mmerce.disruptalookup.service.UserManagerException;
import com.i_mmerce.global.StringUtils;

/** 
 *
 * @author Stuart Sontag
 */
@Controller
@RequestMapping("/admin/group")
@SessionAttributes({"currentUser","userGroupForm", "userGroupIncludeStr", "userGroupExcludeStr"})
public class CreateUpdateGroupController{

    private static final Logger logger = Logger.getLogger(CreateUpdateGroupController.class);
    
    private static final PermissionComparator.Mode permCompMode = PermissionComparator.Mode.AreaAndDescripton;
    
    @Autowired
    private UserManager userManager;

    @RequestMapping(value="/createupdate.htm", method=RequestMethod.GET)
    public String getForm(Model model, HttpSession session){
    	UserGroupForm userGroupForm = new UserGroupForm();
    	UserGroup userGroup = userGroupForm.getUserGroup();
    	//set the network
    	User currentUser = userManager.getCurrentLoggedOnUser(session,false);
    	if (currentUser == null){
    		logger.error("No user logged on.");
    		return "redirect:/logout.htm";
    	}
    	Network network = currentUser.getNetwork();
    	userGroup.setNetwork(network);
    	//set the permissions
    	userGroupForm.setAllPermissions(userManager.getAssignablePermissionsByUser(currentUser));
    	userGroupForm.adjustUsedPermissions();
    	userGroupForm.setImportGroups(userManager.getAllAvailUserGroups(network));
    	model.addAttribute("userGroupForm", userGroupForm);
    	return "createupdateusergroup";
    }
    
    @RequestMapping(value="/edit.htm", method=RequestMethod.GET)
    public String updateUserGroup(@RequestParam ("id") long id, Model model, HttpSession session){
    	UserGroup userGroup = userManager.findUserGroupById(id);
    	UserGroupForm userGroupForm = new UserGroupForm();
    	userGroupForm.setUserGroup(userGroup);
    	userGroupForm.setEditMode(true);
    	User currentUser = userManager.getCurrentLoggedOnUser(session, false);
    	if (currentUser == null){
    		logger.error("No user logged on.");
    		return "redirect:/logout.htm";
    	}
    	userGroupForm.setAllPermissions(userManager.getAssignablePermissionsByUser(currentUser));
    	//Network network = userManager.getCurrentLoggedOnUser().getNetwork();
    	userGroupForm.adjustUsedPermissions();
    	userGroupForm.setImportGroups(userManager.getAllAvailUserGroups(currentUser.getNetwork()));
    	model.addAttribute("userGroupForm", userGroupForm);
    	return "createupdateusergroup";
    }
    
    
    @RequestMapping(value="/delete.htm", method=RequestMethod.GET)
    public String deleteUser(@RequestParam ("id") long id, Model model){
    	userManager.deleteUserGroupById(id);
    	return "viewusergroups";
    }       
    
    //atRequestMapping(value="/list.htm", method=RequestMethod.GET)
    @RequestMapping(value="/list.htm")
    public String getUserGroupsList(@ModelAttribute ("userGroupIncludeStr") String userGroupIncludeStr,
    						   @ModelAttribute ("userGroupExcludeStr") String userGroupExcludeStr, Model model, HttpSession session){
		List<UserGroup> userGroups = null;
    	//if currentUser has (PERM_NETWORK_ADMIN and PERM_GROUP_ADMIN) then show user groups belonging to all networks
    	//otherwise restrict user groups to those in currentUser's own Network.
		try {
			if (userManager.currentUserHasAllPermissions(UserManager.PERM_NETWORK_ADMIN, UserManager.PERM_GROUP_ADMIN)) {
				userGroups = userManager.searchAllUserGroupsExcept(null,userGroupIncludeStr, userGroupExcludeStr, null);
			} else {
				User currentUser = userManager.getCurrentLoggedOnUser(session,false);
		    	if (currentUser == null){
		    		logger.error("No user logged on.");
		    		return "redirect:/logout.htm";
		    	}
				Network network = currentUser.getNetwork();
				userGroups = userManager.searchAllUserGroupsExcept(null, userGroupIncludeStr, userGroupExcludeStr, network);
			}
			model.addAttribute("allUserGroups", userGroups);
			return "usergroupslist";
		} catch (UserManagerException e) {
			return "redirect:/logout.htm";
		}
    }
    
    @RequestMapping(value="/search.htm")
    public String searchUsersList(@RequestParam ("include") String userGroupIncludeStr, @RequestParam ("exclude") String userGroupExcludeStr, Model model){
    	model.addAttribute("userGroupIncludeStr", userGroupIncludeStr);
    	model.addAttribute("userGroupExcludeStr", userGroupExcludeStr);
    	return "viewusergroups";
    }    

    @RequestMapping(value="/viewusergroups.htm", method=RequestMethod.GET)
    public String viewUserGroups(Model model){
    	model.addAttribute("userGroupIncludeStr", "");
    	model.addAttribute("userGroupExcludeStr", "");    	
    	return "viewusergroups";
    }
    
    @RequestMapping(value="/create.htm", method=RequestMethod.POST)
    public String create(@ModelAttribute("userGroupForm") UserGroupForm userGroupForm, Model model){
        logger.info("UserGroupForm :" + userGroupForm.toString());
    	UserGroup userGroup = userGroupForm.getUserGroup();
    	//set the network
    	//Network network = userManager.getCurrentLoggedOnUser().getNetwork();
    	//userGroup.setNetwork(network);
    	model.addAttribute("userGroupForm", userGroupForm);        

    	if (StringUtils.isEmpty(userGroup.getName())){
    		model.addAttribute("errorMsg", "Name can not be empty.");
    		return "createupdateusergroup";
    	}
    	if (StringUtils.isEmpty(userGroup.getDescription())){
    		model.addAttribute("errorMsg", "Description can not be empty.");
    		return "createupdateusergroup";
    	}    	
        try {
        	if (userManager.currentUserHasAllPermissions(userManager.PERM_GROUP_LOCKED_ADMIN)){
        		userGroup.setLocked(userGroupForm.getLocked());
        	} else {
        		userGroup.setLocked(Boolean.FALSE);
        	}
    		userManager.persistUserGroup(userGroup);        	
        } catch (Exception e){
            logger.error("Error encountered attempting to create new user group:",e);
			model.addAttribute("errorMsg", "Exception a encountered attempting create new user group. Cause:" + (e == null || e.getCause() == null ? "null" : e.getCause().getMessage() ));
            return "createupdateusergroup";
        }

        return "pageIndex";
    }
    
    @RequestMapping(value="/update.htm", method=RequestMethod.POST)
    public String update(@ModelAttribute("userGroupForm") UserGroupForm userGroupForm, Model model){
        logger.info("UserGroupForm :" + userGroupForm.toString());
        boolean hasGpLockedAdminRights = false;

        UserGroup userGroup = userGroupForm.getUserGroup();
 
  	
        try {
    		hasGpLockedAdminRights = userManager.currentUserHasAllPermissions(userManager.PERM_GROUP_LOCKED_ADMIN);
	    	if (hasGpLockedAdminRights){
	    		userGroup.setLocked(userGroupForm.getLocked());
	    	}
	
	    	if (!userGroup.getLocked() || hasGpLockedAdminRights ){
		    	userGroupForm.adjustUsedPermissions();
		    	model.addAttribute("userGroupForm", userGroupForm);
		
		    	if (StringUtils.isEmpty(userGroup.getName())){
		    		model.addAttribute("errorMsg", "Name can not be empty.");
		    		return "createupdateusergroup";
		    	}
		    	if (StringUtils.isEmpty(userGroup.getDescription())){
		    		model.addAttribute("errorMsg", "Description can not be empty.");
		    		return "createupdateusergroup";
		    	}    	
	
	    		userManager.persistUserGroup(userGroup);
	    	}
        } catch (Exception e){
            logger.error("Error encountered attempting to create/update usergroup:",e);
            model.addAttribute("errorMsg", "Exception a encountered attempting create/update user group. Cause:" + (e == null || e.getCause() == null ? "null" : e.getCause().getMessage() ));
            return "createupdateusergroup";
        }
    	
    	return "pageIndex";
    }
    
    @RequestMapping(value="/deleteselectedpermissions.htm", method=RequestMethod.POST)
    public String deleteSelectedPermissions(@ModelAttribute("userGroupForm") UserGroupForm userGroupForm,
    		@RequestParam (value="scrolltop", required=false) Integer scrollTop, Model model){
        logger.debug("Removing select permissions from user group in UserGroupForm :" + userGroupForm.toString());
        UserGroup userGroup = userGroupForm.getUserGroup();
        try {
    		if (userGroup.getPermissions() == null){
    			userGroup.setPermissions(new HashSet<Permission>());
    		}
    		logger.debug("Selected Permissions: " + userManager.createPermLogString(userGroupForm.getSelectedPermissions()));
    		logger.debug("Initial userGroupForm.uSerGroup Permissions: " + userManager.createPermLogString(userGroup.getPermissions()));
    		logger.debug("Initial userGroupForm.availPermissions: " + userManager.createPermLogString(userGroupForm.getAvailPermissions()));
			userGroup.getPermissions().removeAll(userGroupForm.getSelectedPermissions());
			logger.debug("Final userGroupForm.uSerGroup Permissions: " + userManager.createPermLogString(userGroup.getPermissions()));
			userGroupForm.getAvailPermissions().addAll(userGroupForm.getSelectedPermissions());
			logger.debug("Final userGroupForm.availPermissions: " + userManager.createPermLogString(userGroupForm.getAvailPermissions()));
    		if (scrollTop!=null){
    			model.addAttribute("scrollTop", scrollTop);
    		}			
        } catch (Exception e){
            logger.error("Error encountered attempting to remove  permissions from user group:"  + userGroupForm.getUserGroup().toString(),e);
        }
    	return "createupdateusergroup";
    }
    
    @RequestMapping(value="/addselectedavailpermissions.htm", method=RequestMethod.POST)
    public String addSelectedAvailPermissions(@ModelAttribute("userGroupForm") UserGroupForm userGroupForm, 
    										@RequestParam (value="scrolltop", required=false) Integer scrollTop, Model model){
        logger.debug("Adding selected available permissions to user group in UserGroupForm :" + userGroupForm.toString());
        UserGroup userGroup = userGroupForm.getUserGroup();
        try {
    		if (userGroup.getPermissions() == null){
    			userGroup.setPermissions(new HashSet<Permission>());
    		}	
			userGroup.getPermissions().addAll(userGroupForm.getSelectedAvailPermissions());
			userGroupForm.getAvailPermissions().removeAll(userGroup.getPermissions());
			
    		if (scrollTop!=null){
    			model.addAttribute("scrollTop", scrollTop);
    		}
        } catch (Exception e){
            logger.error("Error encountered attempting to add selected avail permissions to user group:"  + userGroupForm.getUserGroup().toString(),e);
        }
    	return "createupdateusergroup";
    }
    
    @RequestMapping(value="/importpermissions", method=RequestMethod.POST)
    public String importPermissionsFromGroup(@ModelAttribute("userGroupForm") UserGroupForm userGroupForm,
    										@RequestParam (value="scrolltop", required=false) Integer scrollTop, Model model, HttpSession session){
    	long srcId = userGroupForm.getSelectedImportGroupId();
    	if (srcId<0){
    		logger.error("No source Group for permission import selected.");
    		return "createupdateusergroup";
    	}
    	UserGroup srcUserGroup = userManager.findUserGroupById(srcId);
    	if (srcUserGroup==null){
    		logger.error("Unable to find a srcUserGroup for srcId="+srcId);
    		return "createupdateusergroup";
    	}
        logger.debug("Importing permissions from source UserGroup: (id="+srcId+")to user group:"  + userGroupForm.getUserGroup().toString());
        UserGroup userGroup = userGroupForm.getUserGroup();
        try {
    		if (userGroup.getPermissions() == null){
    			userGroup.setPermissions(new HashSet<Permission>());
    		}
    		logger.debug("Initial uSerGroup Permissions: " + userManager.createPermLogString(userGroup.getPermissions()));
    		logger.debug("srcUserGroup Permissions: " + userManager.createPermLogString(srcUserGroup.getPermissions()));
    		logger.debug("All Avail Permissions: " + userManager.createPermLogString(userGroupForm.getAllPermissions()));
    		Set<Permission> importPerms = null;
    		//if the srcUserGroup permissions contains PERM_ALL_EXCEPT and the current User doesn't have this permission then import the
    		//intersection of the user's available permissions and the set of all permissions except for those specified in the srcUserGroup permission sets
    		//otherwise import the intersection of the user's available permissions and those specified in the srcUserGroup permission set.
    		if (userManager.permissionsContain(srcUserGroup.getPermissions(), UserManager.PERM_ALL_EXCEPT) && 
    				!userManager.permissionsContain(userGroupForm.getAllPermissions(), UserManager.PERM_ALL_EXCEPT)){
    			importPerms = userManager.getPermissionSetsIntersection(userGroupForm.getAllPermissions(),userManager.getAllExceptPermissions(srcUserGroup.getPermissions()));	
    			logger.debug("import Permissions (intersection between all permissions except for those specified in the srUserGroup and the user's Available permissions): " + userManager.createPermLogString(importPerms));
    		} else {
    			importPerms = userManager.getPermissionSetsIntersection(userGroupForm.getAllPermissions(), srcUserGroup.getPermissions());
    			logger.debug("import Permissions (intersection between srUserGroup and Avail permissions): " + userManager.createPermLogString(importPerms));
    		}
    		
			userGroup.getPermissions().addAll(importPerms);
			logger.debug("Final uSerGroup Permissions: " + userManager.createPermLogString(userGroup.getPermissions()));
			userGroupForm.getAvailPermissions().removeAll(userGroup.getPermissions());
			userGroupForm.resetSelectedImportGroupId();
			
    		if (scrollTop!=null){
    			model.addAttribute("scrollTop", scrollTop);
    		}
        } catch (Exception e){
            logger.error("Error encountered attempting to import permissions from srcUserGroup (id="+srcId+")to user group:"  + userGroupForm.getUserGroup().toString(),e);
        }
    	return "createupdateusergroup";
    	
    	
    }
    
    @RequestMapping(value="/getperms")
    public String getUserGroupPermissions(@RequestParam ("gpid") long gpId, Model model){
    	UserGroup userGroup = userManager.findUserGroupById(gpId);
    	List<Permission> permissions = null;
    	if (userGroup == null || userGroup.getPermissions()== null){
    		permissions = new ArrayList<Permission>();
    	} else {
    		permissions = (new PermissionSorter(permCompMode)).sortCollection(userGroup.getPermissions());
    	}
    	model.addAttribute("permissions", permissions);
    	return "usergroupperms";
    }
    

    
}