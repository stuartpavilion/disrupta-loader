/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.mvc;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.mvc.form.NetworkForm;
import com.i_mmerce.disruptalookup.service.UserManager;
import com.i_mmerce.global.StringUtils;

/** 
 *
 * @author Stuart Sontag
 */
@Controller
@RequestMapping("/admin/network")
@SessionAttributes({"networkForm", "networkIncludeStr", "networkExcludeStr"})
public class CreateUpdateNetworkController{

    private static final Logger logger = Logger.getLogger(CreateUpdateNetworkController.class);
    
    @Autowired
    private UserManager userManager;

    @RequestMapping(value="/createupdate.htm", method=RequestMethod.GET)
    public String getForm(Model model){
    	NetworkForm networkForm = new NetworkForm();
    	model.addAttribute("networkForm", networkForm);
    	return "createupdatenetwork";
    }
    
    @RequestMapping(value="/edit.htm", method=RequestMethod.GET)
    public String updateUser(@RequestParam ("id") long id, Model model){
    	Network network = userManager.findNetworkById(id);
    	NetworkForm networkForm = new NetworkForm();
    	networkForm.setNetwork(network);
    	networkForm.setEditMode(true);
    	model.addAttribute("networkForm", networkForm);
    	return "createupdatenetwork";
    }
    
    @RequestMapping(value="/delete.htm", method=RequestMethod.GET)
    public String deleteUser(@RequestParam ("id") long id, Model model){
    	userManager.deleteNetworkById(id);
    	return "viewnetworks";
    }       
    
    //atRequestMapping(value="/list.htm", method=RequestMethod.GET)
    @RequestMapping(value="/list.htm")
    public String getUsersList(@ModelAttribute ("networkIncludeStr") String networkIncludeStr,
    						   @ModelAttribute ("networkExcludeStr") String networkExcludeStr, Model model){
    	List<Network> networks = userManager.searchAllNetworksExcept(null, networkIncludeStr, networkExcludeStr);
    	model.addAttribute("allNetworks", networks);
    	return "networkslist";
    }
    
    @RequestMapping(value="/search.htm")
    public String searchUsersList(@RequestParam ("include") String networkIncludeStr, @RequestParam ("exclude") String networkExcludeStr, Model model){
    	model.addAttribute("networkIncludeStr", networkIncludeStr);
    	model.addAttribute("networkExcludeStr", networkExcludeStr);
    	return "viewnetworks";
    }    

    @RequestMapping(value="/viewnetworks.htm", method=RequestMethod.GET)
    public String viewUsers(Model model){
    	model.addAttribute("networkIncludeStr", "");
    	model.addAttribute("networkExcludeStr", "");    	
    	return "viewnetworks";
    }
    
    @RequestMapping(value="/create.htm", method=RequestMethod.POST)
    public String create(@ModelAttribute("networkForm") NetworkForm networkForm, Model model){
        logger.info("NetworkForm :" + networkForm.toString());
    	Network network = networkForm.getNetwork();
    	//set the network
    	//Network network = userManager.getCurrentLoggedOnUser().getNetwork();
    	//userGroup.setNetwork(network);
    	model.addAttribute("networkForm", networkForm);        

    	if (StringUtils.isEmpty(network.getName())){
    		model.addAttribute("errorMsg", "Name can not be empty.");
    		return "createupdatenetwork";
    	}
    	if (StringUtils.isEmpty(network.getDescription())){
    		model.addAttribute("errorMsg", "Description can not be empty.");
    		return "createupdatenetwork";
    	}    	
        try {        	
    		//userManager.persistNetwork(network);
        	userManager.createNetworkAndDefaultAdminUser(network);
        } catch (Exception e){
            logger.error("Error encountered attempting to create new Network:",e);
			model.addAttribute("errorMsg", "Exception a encountered attempting create new Network. Cause:" + (e == null || e.getCause() == null ? "null" : e.getCause().getMessage() ));
            return "createupdatenetwork";
        }

        return "pageIndex";
    }
    
    @RequestMapping(value="/update.htm", method=RequestMethod.POST)
    public String update(@ModelAttribute("networkForm") NetworkForm networkForm, Model model){
        logger.info("NetworkForm :" + networkForm.toString());
    	Network network = networkForm.getNetwork();
    	model.addAttribute("networkForm", networkForm);

    	if (StringUtils.isEmpty(network.getName())){
    		model.addAttribute("errorMsg", "Name can not be empty.");
    		return "createupdatenetwork";
    	}
    	if (StringUtils.isEmpty(network.getDescription())){
    		model.addAttribute("errorMsg", "Description can not be empty.");
    		return "createupdatenetwork";
    	}    	
        try {
        	userManager.persistNetwork(network);
        } catch (Exception e){
            logger.error("Error encountered attempting to update Network:",e);
            model.addAttribute("errorMsg", "Exception a encountered attempting to update Network. Cause:" + (e == null || e.getCause() == null ? "null" : e.getCause().getMessage() ));
            return "createupdatenetwork";
        }
    	return "pageIndex";
    }
    
    
}