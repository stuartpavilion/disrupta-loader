/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.mvc;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroup;
import com.i_mmerce.disruptalookup.domain.UserGroupAccess;
import com.i_mmerce.disruptalookup.mvc.form.UserForm;
import com.i_mmerce.disruptalookup.service.UserManager;
import com.i_mmerce.disruptalookup.service.UserManagerException;
import com.i_mmerce.global.DateFormats;
import com.i_mmerce.global.StringUtils;



/** 
 *
 * @author Stuart Sontag
 */
@Controller
@RequestMapping("/admin/user")
@SessionAttributes({"currentUser","userForm", "userIncludeStr", "userExcludeStr"})
public class CreateUpdateUserController{

    private static final Logger logger = Logger.getLogger(CreateUpdateUserController.class);
    
    @Autowired
    private UserManager userManager;
    
    private static final String LOCK_ACTION = "Lock";
    
	@Value("#{propBean['useraccount_enable_deletion']}")
	private Boolean enableUserAccountDeletion= Boolean.FALSE;

    @RequestMapping(value="/createupdate.htm", method=RequestMethod.GET)
    public String getForm(Model model, HttpSession session){
    	UserForm userForm = new UserForm();
    	User currentUser = userManager.getCurrentLoggedOnUser(session, false);
    	if (currentUser == null){
    		logger.error("No user logged on.");
    		return "redirect:/logout.htm";
    	}
    	Network network = currentUser.getNetwork();
    	userForm.setAvailUserGroups(userManager.getAllAvailUserGroups(network));
    	model.addAttribute("userForm", userForm);
    	return "createupdateuser";
    }
    
    @RequestMapping(value="/edit.htm", method=RequestMethod.GET)
    public String updateUser(@RequestParam ("id") long id, Model model, HttpSession session){
    	User user = userManager.findById(id);
    	UserForm userForm = new UserForm();
    	userForm.setUser(user);
    	userForm.setEditMode(true);
    	//Network network = userManager.getCurrentLoggedOnUser(session, false).getNetwork();
    	//userForm.setAvailUserGroups(userManager.getAllAvailUserGroups(network));
    	userForm.setAvailUserGroups(userManager.getAllAvailUserGroups(user.getNetwork()));
    	userForm.setCurrentUGAs(userManager.getUserGroupAccessListForUser(user));
    	userForm.cullUsedUserGroups();
    	model.addAttribute("userForm", userForm);
    	return "createupdateuser";
    }
    
    @RequestMapping(value="/delete.htm", method=RequestMethod.GET)
    public String deleteUser(@RequestParam ("id") long id, Model model){
    	logger.debug("useraccount_enable_deletion =" + enableUserAccountDeletion);
    	if (enableUserAccountDeletion){
    		userManager.deleteById(id);
    	}
    	//return new ModelAndView("redirect:/admin/user/viewusers.htm");
    	return "viewusers";
    }
    
    @RequestMapping(value="/lockUnLockUrl.htm", method=RequestMethod.GET)
    public String lockUnlockUser(@RequestParam ("id") long id, @RequestParam ("action") String lockAction, Model model){
    	userManager.lockUnlockUser(id, !LOCK_ACTION.equals(lockAction));
    	return "viewusers";
    }  
    
    //atRequestMapping(value="/list.htm", method=RequestMethod.GET)
    @RequestMapping(value="/list.htm")
    public String getUsersList(@ModelAttribute ("userIncludeStr") String userIncludeStr,
    						   @ModelAttribute ("userExcludeStr") String userExcludeStr, Model model, HttpSession session){
    	User currentUser = userManager.getCurrentLoggedOnUser(session, false);
    	if (currentUser == null){
    		logger.error("No user logged on.");
    		return "redirect:/logout.htm";
    	}
    	List<User> users = null;
    	//if currentUser has (PERM_NETWORK_ADMIN and PERM_USER_ADMIN) then show users belonging to all networks
    	//otherwise restrict users to those in currentUser's own Network.
    	//Also excludes the currentUser from the returned search
    	try {
			if (userManager.currentUserHasAllPermissions(UserManager.PERM_NETWORK_ADMIN, UserManager.PERM_USER_ADMIN)){
				users = userManager.searchAllUsersExcept(currentUser, userIncludeStr, userExcludeStr, null);
			} else {
				users = userManager.searchAllUsersExcept(currentUser, userIncludeStr, userExcludeStr, currentUser.getNetwork());
			}
			model.addAttribute("allUsers", users);
			return "userslist";
		} catch (UserManagerException e) {
			return "redirect:/logout.htm";
		}
    }
    
    @RequestMapping(value="/search.htm")
    public String searchUsersList(@RequestParam ("include") String userIncludeStr, @RequestParam ("exclude") String userExcludeStr, Model model){
    	model.addAttribute("userIncludeStr", userIncludeStr);
    	model.addAttribute("userExcludeStr", userExcludeStr);
    	return "viewusers";
    }    

    @RequestMapping(value="/viewusers.htm", method=RequestMethod.GET)
    public String viewUsers(Model model){
    	model.addAttribute("userIncludeStr", "");
    	model.addAttribute("userExcludeStr", "");    	
    	return "viewusers";
    }
    
    @RequestMapping(value="/create.htm", method=RequestMethod.POST)
    public String create(@ModelAttribute("userForm") UserForm userForm, Model model, HttpSession session){
        logger.info("UserForm :" + userForm.toString());
    	User user = userForm.getUser();
    	//set creation time stamp
    	user.setCreatedTimestamp(new Timestamp(new Date().getTime()));
    	User currentUser = userManager.getCurrentLoggedOnUser(session, false);
    	if (currentUser == null){
    		logger.error("No user logged on.");
    		return "redirect:/logout.htm";
    	}
    	Network network = currentUser.getNetwork();
    	user.setNetwork(network);
    	model.addAttribute("userForm", userForm);        

    	if (StringUtils.isEmpty(user.getUsername())){
    		model.addAttribute("errorMsg", "Username can not be empty.");
    		return "createupdateuser";
    	}
    	
		if (!userForm.getUser().isLdapUser()) {
			if (StringUtils.isEmpty(userForm.getNewPassword()) || (!userForm.getNewPassword().equals(userForm.getConfirmPassword()))) {
					model.addAttribute("errorMsg","Password and Password Confirmation fields must be non-empty and equal.");
					return "createupdateuser";
			}
		} else {
			if (userForm.getNewPassword()!=null && (!userForm.getNewPassword().equals(userForm.getConfirmPassword()))){
				model.addAttribute("errorMsg","Password and Password Confirmation fields must be equal.");
				return "createupdateuser";				
			}
		}
        try {        	
    		user.setPassword(userForm.getNewPassword());
    		userManager.setUserAccessFlags(user, true, true, true, true);
    		userManager.persistUser(user, true);        	
        } catch (Exception e){
            logger.error("Error encountered attempting to create new user:",e);
			model.addAttribute("errorMsg", "Exception a encountered attempting create new user. Cause:" + (e == null || e.getCause() == null ? "null" : e.getCause().getMessage() ));
            return "createupdateuser";
        }

        //Switch over into edit mode so we can add User Group Accesses
    	userForm.setEditMode(true);
    	//populate Avail Groups and current UGAs which should be empty
    	userForm.setAvailUserGroups(userManager.getAllAvailUserGroups(network));
    	userForm.setCurrentUGAs(userManager.getUserGroupAccessListForUser(user));
    	userForm.cullUsedUserGroups();
    	return "createupdateuser";
    }
    
    @RequestMapping(value="/update.htm", method=RequestMethod.POST)
    public String update(@ModelAttribute("userForm") UserForm userForm, Model model){
        logger.info("UserForm :" + userForm.toString());
    	User user = userForm.getUser();
    	model.addAttribute("userForm", userForm);

    	if (StringUtils.isEmpty(user.getUsername())){
    		model.addAttribute("errorMsg", "Username can not be empty.");
    		return "createupdateuser";
    	}    	
        try {
        	userManager.setUserAccessFlags(user, true, true, true, true);
        	if (userForm.getChangePassword()){
        		if (!userForm.getUser().isLdapUser()) {        		
	        		if  (StringUtils.isEmpty(userForm.getNewPassword()) ||
	            		 (!userForm.getNewPassword().equals(userForm.getConfirmPassword()))) {
	            		model.addAttribute("errorMsg", "Password and Password Confirmation fields must be non-empty and equal.");
	            		return "createupdateuser";
	        		}
        		}  else {
        			if (userForm.getNewPassword()!=null && (!userForm.getNewPassword().equals(userForm.getConfirmPassword()))){
        				model.addAttribute("errorMsg","Password and Password Confirmation fields must be equal.");
        				return "createupdateuser";				
        			}
        		}
        		user.setPassword(userForm.getNewPassword());
        		userManager.persistUser(user, true);        		
        	} else {
        		userManager.persistUser(user, false);
        	}
        } catch (Exception e){
            logger.error("Error encountered attempting to update new user:",e);
            model.addAttribute("errorMsg", "Exception a encountered attempting create new user. Cause:" + (e == null || e.getCause() == null ? "null" : e.getCause().getMessage() ));
            return "createupdateuser";
        }
        return "redirect:/index.htm";
    }
    
    @RequestMapping(value="/addUserGroupAccess.htm", method=RequestMethod.POST)
    public String addUserGroupAccess(@ModelAttribute("userForm") UserForm userForm, Model model, @RequestParam ("fromdate") String fromDateStr, 
    		@RequestParam ("todate") String toDateStr, @RequestParam (value="scrolltop", required=false) Integer scrollTop){
        logger.debug("Adding User Group Access UserForm :" + userForm.toString());
        try {
            User user = userForm.getUser();
            UserGroup ug = userForm.getSelectedUserGroup();
            if (ug == null){
            	throw new Exception("No user group selected.");
            }
            Date effectiveFrom = null;
            Date effectiveTo = null;
            if (userForm.getTimeLimitedAccess()){
	            SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormats.XFER_AUS_DATEFORMAT);
	            effectiveFrom = dateFormat.parse(fromDateStr);
	            effectiveTo = dateFormat.parse(toDateStr);
            } 
            UserGroupAccess uga = new UserGroupAccess(null, user, ug, userForm.getTimeLimitedAccess(), effectiveFrom, effectiveTo);
            logger.debug("About to persist Populated UserGroupAccess: "+ uga.toString());
            userManager.persistUserGroupAccess(uga);
            //Update AvailUserGroups and Current UserGroupAccess sets.
        	userForm.setAvailUserGroups(userManager.getAllAvailUserGroups(user.getNetwork()));
        	userForm.setCurrentUGAs(userManager.getUserGroupAccessListForUser(user));
        	userForm.cullUsedUserGroups();
        	
    		if (scrollTop!=null){
    			model.addAttribute("scrollTop", scrollTop);
    		}
        } catch (ParseException pe){
        	logger.error("Error attempting to parse fromDate or toDate. fromDateStr=" + fromDateStr + ", toDateStr=" + toDateStr, pe);
        	model.addAttribute("errorMsg", "Error attempting to parse fromDate or toDate. fromDateStr=" + fromDateStr + ", toDateStr=" + toDateStr);
        	return "createupdateuser";
        } catch (Exception e){
            logger.error("Error encountered attempting to add User Group Access:" + userForm.toString() + " to user:",e);
            model.addAttribute("errorMsg", "Error encountered attempting to add User Group Access to user");
            return "createupdateuser";
        }
        return "createupdateuser";
    }
    
    @RequestMapping(value="/deleteUserGroupAccess.htm", method=RequestMethod.POST)
    public String deleteCustId(@ModelAttribute("userForm") UserForm userForm, Model model
    		, @RequestParam (value="scrolltop", required=false) Integer scrollTop){
    	if (userForm.getSelectedUGA() == null){
    		logger.error("A UserGroupAccess for deletion has been not successfully selected.");
    		model.addAttribute("errorMsg","A UserGroupAccess for deletion has been not successfully selected.");
    		return "createupdateuser";
    	}
        logger.debug("Removing UserGroupAccess:" + userForm.getSelectedUGA().toString()
        		+ ":from user in UserForm :" + userForm.toString());
        
        try {
        	userManager.deleteUserGroupAccessById(Long.valueOf(userForm.getSelectedUserGroupAccessId()));
        	//Update AvailUserGroups and Current UserGroupAccess sets.
        	userForm.setAvailUserGroups(userManager.getAllAvailUserGroups(userForm.getUser().getNetwork()));
        	userForm.setCurrentUGAs(userManager.getUserGroupAccessListForUser(userForm.getUser()));
        	userForm.cullUsedUserGroups();
        	userForm.setSelectedUGA(null);
        	userForm.setTimeLimitedAccess(false);
    		if (scrollTop!=null){
    			model.addAttribute("scrollTop", scrollTop);
    		}
        } catch (Exception e){
            logger.error("Error encountered attempting to remove UserGroupAccess:" +
            				userForm.getSelectedUGA().toString() + " from user:" + userForm.getUser().toString(),e);
            model.addAttribute("errorMsg", "Error encountered attempting to remove UserGroupAccess from User");
            return "createupdateuser";
        }
    	return "createupdateuser";
    }    
    
 /*
    @RequestMapping(value="/addcustid.htm", method=RequestMethod.POST)
    public String addCustId(@ModelAttribute("userForm") UserForm userForm, Model model){
        logger.debug("Adding custid to user in UserForm :" + userForm.toString());
        try {
            String newCustId = userForm.getNewCustId();
        	User user = userForm.getUser();
        	user.getCustIds().add(newCustId);
        } catch (Exception e){
            logger.error("Error encountered attempting to add custId:" + userForm.getNewCustId() + " to user:",e);
            return "createupdateuser";
        }
    	return "createupdateuser";
    }     

    
    @RequestMapping(value="/deletecustid.htm", method=RequestMethod.POST)
    public String deleteCustId(@ModelAttribute("userForm") UserForm userForm, Model model){
        logger.debug("Removing custid from user in UserForm :" + userForm.toString());
        String selectedCustId="";
        try {
            selectedCustId = userForm.getSelectedCustId();
        	User user = userForm.getUser();
        	user.getCustIds().remove(selectedCustId);
        } catch (Exception e){
            logger.error("Error encountered attempting to remove custId:" + selectedCustId + " from user:" +
            			userForm.getUser().toString(),e);
            return "createupdateuser";
        }
    	return "createupdateuser";
    }
    */    
}