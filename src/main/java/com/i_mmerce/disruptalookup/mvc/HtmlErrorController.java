package com.i_mmerce.disruptalookup.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.i_mmerce.global.HttpRequestUtil;


@Controller
@RequestMapping("/errors")
public class HtmlErrorController {
	
	@RequestMapping("/404.html")
	public String handle404error(Model model, HttpServletRequest request)
	{
		String clientIp = HttpRequestUtil.getClientIpAddress(request);
		String uri = HttpRequestUtil.getErrorRequestURI(request);
		
		System.err.println("HTTP 404 detected!");
		System.err.println("uri=["+uri+"]");
		System.err.println("clientIp=["+clientIp+"]");
		
		model.addAttribute("errorMsg", "You attempted to not access a non-existent page or one for which you do not have access rights!");
		model.addAttribute("returnUrl",  request.getContextPath()+"/index.htm");
		return "errorDisplayView";
	};
	
	@RequestMapping("/500.html")
	public String handle500error(Model model, HttpServletRequest request){
		model.addAttribute("errorMsg", "HTTP Error 500 Unexpected Internal server error!");
		model.addAttribute("returnUrl",  request.getContextPath()+"/index.htm");
		return "errorDisplayView";
	};	

}
