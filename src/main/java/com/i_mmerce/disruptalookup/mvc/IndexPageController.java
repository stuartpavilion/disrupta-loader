/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.mvc;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.service.UserManager;
/**
 *
 * @author Stuart Sontag
 */
@Controller
@RequestMapping("/index.htm")
//SessionAttributes({"currentUser"})
public class IndexPageController {
    private static Logger logger = Logger.getLogger(IndexPageController.class);

	@Autowired
	private UserManager userManager;
	
    @RequestMapping(value = "/index.htm", method = RequestMethod.GET)
    public String getIndexPage(ModelMap model, HttpSession session){
    	logger.debug("forwarding from /index.htm to 'pageIndex'");
    	//User currentUser = userManager.getCurrentLoggedOnUser();
    	User currentUser = userManager.getCurrentLoggedOnUser(session,true);
    	model.addAttribute("currentUser", currentUser);
    	logger.debug("Added to Session, currentUser: " + (currentUser == null ? "null" : currentUser.toSafeString()));
    	return "pageIndex";
    }

}