package com.i_mmerce.disruptalookup.mvc;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.uow.nccc.ecoder.search.ContentIndexing;
import au.edu.uow.nccc.ecoder.search.SearchResult;
import au.edu.uow.nccc.ecoder.search.SearchResultScoreComparator;

import com.i_mmerce.disruptalookup.mvc.json.IndexTree;
import com.i_mmerce.disruptalookup.service.LuceneSearchService;
import com.i_mmerce.disruptalookup.util.IndexTreeMapper;
import com.i_mmerce.disruptalookup.util.Stopwatch;

@Controller
@RequestMapping("/index_tree")
public class IndexTreeSearchController {
	
	private static final Logger logger = Logger.getLogger(IndexTreeSearchController.class);
	
	@Autowired
	private LuceneSearchService luceneSearchService;

	@RequestMapping(value="/{query}/{depth}", method=RequestMethod.GET)
	public @ResponseBody IndexTree indexTreeSearch(@PathVariable("query") String query, @PathVariable("depth") Integer depth, Model model){
		logger.info("Index Tree Search Query="+query+", maxChildDepth="+depth);
		Stopwatch sw = new Stopwatch("Diseases Index Term Tree Search & Sort execution time");sw.start();
		List<SearchResult> searchResults = luceneSearchService.runCodeExpertQuery(query, ContentIndexing.DocumentType.DISEASES_INDEX_TERM, false);
		sw.stop();logger.info(sw);
		
		sw.reset("Interventions Index Term Tree Search & Sort execution time");sw.start();
		List<SearchResult> intervSearchResults = luceneSearchService.runCodeExpertQuery(query, ContentIndexing.DocumentType.INTERVENTIONS_INDEX_TERM, false);
		sw.stop();logger.info(sw);	
		//Concatenate Interventions and Diseases search results
		searchResults.addAll(intervSearchResults);
		//Sort all the results
		Collections.sort(searchResults, new SearchResultScoreComparator());
		if (depth!=null && depth<0){
			depth=null;
		}

        //
        // MP: Created a mapper (see below)
        //

        /*
		IndexTree indexTree = new IndexTree(query, depth);
//		boolean firstOne = true;
		for (SearchResult searchResult: searchResults){
			IndexTerm indexTerm = searchResult.getDocumentAsCodeTerm().getIndexTerm(depth);
//			if (firstOne){
//				indexTree.setResults(indexTerm);
//			} else {
			    indexTree.getResults().add(indexTerm);
//			    firstOne = false;
//			}
		}
		*/

        IndexTree indexTree = IndexTreeMapper.map(query, depth, searchResults);

        return indexTree;
	}
	
}
