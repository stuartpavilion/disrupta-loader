/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.mvc;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
/**
 *
 * @author Stuart Sontag
 */
@Controller
public class LogonController {
    private static Logger logger = Logger.getLogger(LogonController.class);

  
    @RequestMapping(value = "/logon.htm", method = RequestMethod.GET)
    public String getIndexPage(ModelMap model){
    	logger.info("forwarding from /index.htm to 'pageIndex'");
    	return "logon";
    }

    @RequestMapping(value = "do/logon.htm", method = RequestMethod.GET)
    public String getIndexPageDo(ModelMap model){
    	logger.info("forwarding from do/logon.htm /index.htm to 'pageIndex'");
    	return "logon";
    }
	@RequestMapping(value="/logout.htm", method=RequestMethod.GET)
	public String getLogoutForm(final ModelMap model, HttpSession session) {
		return "logon";
	}
	
	@RequestMapping(value="/logout-success.htm", method=RequestMethod.GET)
	public String getLogoutSuccessForm(final ModelMap model, HttpSession session) {
		model.remove("currentUser");
		return "logon";
	}
	
	@RequestMapping(value="/logon-failure.htm", method=RequestMethod.GET)
	public String getLogonFailureForm(final ModelMap model, HttpSession session) {
		model.addAttribute("errorMsg", "Username/Password Authentication failure");
		return "logon";
	}
}

