package com.i_mmerce.disruptalookup.mvc;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.mvc.form.UserForm;
import com.i_mmerce.disruptalookup.service.UserManager;
import com.i_mmerce.global.StringUtils;


@Controller
@RequestMapping("/user")
@SessionAttributes({"userForm"})
public class MyAccountController {
	
	private static final Logger logger = Logger.getLogger(MyAccountController.class);

    @Autowired
    private UserManager userManager;

    @RequestMapping(value="/current.htm", method=RequestMethod.GET)
    public String updateUser(Model model, HttpSession session){
    	User user = userManager.getCurrentLoggedOnUser(session,true);
    	if (user == null){
    		logger.error("No user logged on.");
    		return "redirect:/logout.htm";
    	}
    	UserForm userForm = new UserForm();
    	userForm.setUser(user);
    	userForm.setEditMode(true);
    	Network network = user.getNetwork();
    	userForm.setAvailUserGroups(userManager.getAllAvailUserGroups(network));
    	userForm.setCurrentUGAs(userManager.getUserGroupAccessListForUser(user));
    	userForm.cullUsedUserGroups();
    	model.addAttribute("myaccountMode",Boolean.TRUE);
    	model.addAttribute("userForm", userForm);
    	return "createupdateuser";
    }
    
    
    
    @RequestMapping(value="/update.htm", method=RequestMethod.POST)
    public String update(@ModelAttribute("userForm") UserForm userForm, Model model, HttpSession session){
        logger.info("UserForm :" + userForm.toString());
        try {
            //User user = userManager.findByUserName(userForm.getUser().getUsername());
        	User currentUser = userManager.getCurrentLoggedOnUser(session,true);
        	if (currentUser == null){
        		logger.error("No user logged on.");
        		return "redirect:/logout.htm";
        	}
        	User user = userForm.getUser();
        	if (currentUser.getId() != user.getId()){
        		logger.error("Attempt to update user: "  + user.toSafeString() +  " forbidden. Only permitted to update current user: " + currentUser.toSafeString());
        		return "createupdateuser";
        	}
        	userManager.setUserAccessFlags(user, true, true, true, true);
        	if (userForm.getChangePassword()){
        		if  (StringUtils.isNotEmpty(userForm.getNewPassword()) &&
            		 userForm.getNewPassword().equals(userForm.getConfirmPassword()))
        		{
            		user.setPassword(userForm.getNewPassword());
            		//userManager.persistUser(user, true);
            		userManager.mergeUser(user, true);
        		}
        	} else {
        		//userManager.persistUser(user, false);
        		userManager.mergeUser(user, false);
        	}
        } catch (Exception e){
            logger.error("Error encountered attempting to create new user:",e);
            //TODO add error to errors object
            return "createupdateuser";
        }
    	return "redirect:/index.htm";
    } 

}
