package com.i_mmerce.disruptalookup.mvc.form;

import java.io.Serializable;

import com.i_mmerce.disruptalookup.service.FilterMode;

public class BatchUploadForm implements Serializable {
	 private String filename; 
	 private String columnSeparator;
	 private int typeCol;
	 private String typeRegex;
	 private FilterMode borisFilterMode;
	 private int borisCol;
	 private FilterMode usernameFilterMode;
	 private int usernameCol;
	 private String usernamePrefix;
	 private String usernameSuffix;
	 private FilterMode passwordFilterMode;
	 private int passwordCol;
	 private String passwordPrefix;
	 private String passwordSuffix;
	 
	 
	public BatchUploadForm() {
		super();
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getColumnSeparator() {
		return columnSeparator;
	}
	public void setColumnSeparator(String columnSeparator) {
		this.columnSeparator = columnSeparator;
	}
	public int getTypeCol() {
		return typeCol;
	}
	public void setTypeCol(int typeCol) {
		this.typeCol = typeCol;
	}
	public String getTypeRegex() {
		return typeRegex;
	}
	public void setTypeRegex(String typeRegex) {
		this.typeRegex = typeRegex;
	}
	public int getBorisCol() {
		return borisCol;
	}
	public void setBorisCol(int borisCol) {
		this.borisCol = borisCol;
	}
	public int getUsernameCol() {
		return usernameCol;
	}
	public void setUsernameCol(int usernameCol) {
		this.usernameCol = usernameCol;
	}
	public String getUsernamePrefix() {
		return usernamePrefix;
	}
	public void setUsernamePrefix(String usernamePrefix) {
		this.usernamePrefix = usernamePrefix;
	}
	public String getUsernameSuffix() {
		return usernameSuffix;
	}
	public void setUsernameSuffix(String usernameSuffix) {
		this.usernameSuffix = usernameSuffix;
	}
	public int getPasswordCol() {
		return passwordCol;
	}
	public void setPasswordCol(int passwordCol) {
		this.passwordCol = passwordCol;
	}
	public String getPasswordPrefix() {
		return passwordPrefix;
	}
	public void setPasswordPrefix(String passwordPrefix) {
		this.passwordPrefix = passwordPrefix;
	}
	public String getPasswordSuffix() {
		return passwordSuffix;
	}
	public void setPasswordSuffix(String passwordSuffix) {
		this.passwordSuffix = passwordSuffix;
	}

	public FilterMode getBorisFilterMode() {
		return borisFilterMode;
	}

	public void setBorisFilterMode(FilterMode borisFilterMode) {
		this.borisFilterMode = borisFilterMode;
	}

	public FilterMode getUsernameFilterMode() {
		return usernameFilterMode;
	}

	public void setUsernameFilterMode(FilterMode usernameFilterMode) {
		this.usernameFilterMode = usernameFilterMode;
	}

	public FilterMode getPasswordFilterMode() {
		return passwordFilterMode;
	}

	public void setPasswordFilterMode(FilterMode passwordFilterMode) {
		this.passwordFilterMode = passwordFilterMode;
	}
	 
	public void setBorisFilterModeStr(String fms){
		setBorisFilterMode(FilterMode.valueOf(fms));
	}
	
	public String getBorisFilterModeStr(){
		return borisFilterMode.name();
	}
	
	public void setUsernameFilterModeStr(String fms){
		setUsernameFilterMode(FilterMode.valueOf(fms));
	}
	
	public String getUsernameFilterModeStr(){
		return usernameFilterMode.name();
	}
	
	public void setPasswordFilterModeStr(String fms){
		setPasswordFilterMode(FilterMode.valueOf(fms));
	}
	
	public String getPasswordFilterModeStr(){
		return passwordFilterMode.name();
	}	
}
