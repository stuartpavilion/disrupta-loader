package com.i_mmerce.disruptalookup.mvc.form;

import com.i_mmerce.disruptalookup.domain.Network;

public class NetworkForm {
	private Network network = new Network();
	private boolean editMode = false;
	
	
	public Network getNetwork() {
		return network;
	}
	public void setNetwork(Network network) {
		this.network = network;
	}
	public boolean isEditMode() {
		return editMode;
	}
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}
	
	
	
}
