/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.mvc.form;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroup;
import com.i_mmerce.disruptalookup.domain.UserGroupAccess;

/**
 *
 * @author Stuart Sontag
 */
public class UserForm implements Serializable {
    private User user = new User();
    private boolean editMode = false;
    private Boolean changePassword = false;
    private String newPassword;
    private String confirmPassword;
    private UserGroupAccess selectedUGA;
    private Set<UserGroupAccess> currentUGAs = new HashSet<UserGroupAccess>();
    private Set<UserGroup> availUserGroups = new HashSet<UserGroup>();
    private UserGroup selectedUserGroup = new UserGroup();
    private Boolean timeLimitedAccess = false;

    public UserForm() {
        super();
    }
    
    public UserForm(Set<UserGroupAccess> currentUGAs,
			Set<UserGroup> availUserGroups) {
		super();
		this.currentUGAs = currentUGAs;
		this.availUserGroups = availUserGroups;
	}
	public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    private UserGroupAccess getUGAidFromCurrentUGAs(Long id){
    	for (UserGroupAccess uag: currentUGAs){
    		if (uag.getId().longValue() == id.longValue()){
    			return uag;
    		}
    	}
    	return null;
    }    
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
	public void setSelectedUserGroupAccessId(String idStr){
		long id = Long.parseLong(idStr);
		setSelectedUserGroupAccessId(id);
	}
    
	public void setSelectedUserGroupAccessId(long id){
		selectedUGA = getUGAidFromCurrentUGAs(id);
	}
	
	public long getSelectedUserGroupAccessId(){
		return (selectedUGA == null ? null : selectedUGA.getId());
	}
	public UserGroupAccess getSelectedUGA() {
		return selectedUGA;
	}
	public void setSelectedUGA(UserGroupAccess selectedUGA) {
		this.selectedUGA = selectedUGA;
	}
	public Set<UserGroup> getAvailUserGroups() {
		return availUserGroups;
	}
	public void setAvailUserGroups(Collection<UserGroup> availUserGroups) {
		this.availUserGroups = new HashSet<UserGroup>();
		this.availUserGroups.addAll(availUserGroups);
	}
	
	public void cullUsedUserGroups(){
		for (UserGroupAccess uga: currentUGAs){
			availUserGroups.remove(uga.getUserGroup());
		}
		
	}
	public UserGroup getSelectedUserGroup() {
		return selectedUserGroup;
	}

	public void setSelectedUserGroup(UserGroup selectedUserGroup) {
		this.selectedUserGroup = selectedUserGroup;
	}

	public void setSelectedUserGroupId(Long id) {
		for (UserGroup ug: availUserGroups){
			if (ug.getId().equals(id)){
				this.selectedUserGroup = ug;
				return;
			}
		}
		this.selectedUserGroup = null;
	}
	
	public Long getSelectedUserGroupId() {
		return (selectedUserGroup == null ? null : selectedUserGroup.getId());
	}	
	public UserGroup getSelectedUserGroup(Long id) {
		setSelectedUserGroupId(id);
		return selectedUserGroup;
	}
	public Set<UserGroupAccess> getCurrentUGAs() {
		return currentUGAs;
	}
	public void setCurrentUGAs(Collection<UserGroupAccess> currentUGAs) {
		this.currentUGAs = new HashSet<UserGroupAccess>();
		this.currentUGAs.addAll(currentUGAs);
	}
	
	public boolean isEditMode() {
		return editMode;
	}
	
	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}
	
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public Boolean getChangePassword() {
		return changePassword;
	}
	public void setChangePassword(Boolean changePassword) {
		this.changePassword = changePassword;
	}

	public Boolean getTimeLimitedAccess() {
		return timeLimitedAccess;
	}

	public void setTimeLimitedAccess(Boolean timeLimitedAccess) {
		this.timeLimitedAccess = timeLimitedAccess;
	}


}
