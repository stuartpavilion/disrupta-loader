/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.mvc.form;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.i_mmerce.disruptalookup.dao.PermissionComparator;
import com.i_mmerce.disruptalookup.dao.PermissionSorter;
import com.i_mmerce.disruptalookup.domain.Permission;
import com.i_mmerce.disruptalookup.domain.UserGroup;

/**
 *
 * @author Stuart Sontag
 */
public class UserGroupForm implements Serializable{
	public static long IMPORT_GP_ID_NOTSELECTED = -1;
	private Boolean locked;
    private UserGroup userGroup = new UserGroup();
    private boolean editMode = false;
    private Set<Permission> allPermissions = new HashSet<Permission>();;
    private Set<Permission> availPermissions = new HashSet<Permission>();
    private Set<Permission> selectedAvailPermissions = new HashSet<Permission>();;
    private Set<Permission> selectedPermissions = new HashSet<Permission>();;
    private Collection<UserGroup> importGroups = new HashSet<UserGroup>();
    private long selectedImportGroupId=IMPORT_GP_ID_NOTSELECTED;
    
    private static final PermissionComparator.Mode permCompMode = PermissionComparator.Mode.AreaAndDescripton;

    public UserGroupForm() {
        super();
    }

    private Permission getPermissionById(Long id){
    	for (Permission permission: allPermissions){
    		if (permission.getId().equals(id)){
    			return permission;
    		}
    	}
    	return null;
    }
    
    public void setSelectedAvailPermissionIds(Long[] ids){
    	selectedAvailPermissions = new HashSet<Permission>();
    	for (Long id :ids){
    		Permission permission = getPermissionById(id);
    		if (permission != null){
    			selectedAvailPermissions.add(permission);
    		}
    	}
    }

	public Long[] getSelectedAvailPermissionIds(){
		if (selectedAvailPermissions == null){
			return new Long[0];
		}
		Long[] ids = new Long[selectedAvailPermissions.size()];
		int i=0;
		for (Permission permission: selectedAvailPermissions){
			ids[i++]=permission.getId();
		}
		return ids;
    }    
    
    public void setSelectedPermissionIds(Long[] ids){
    	selectedPermissions = new HashSet<Permission>();
    	for (Long id :ids){
    		Permission permission = getPermissionById(id);
    		if (permission != null){
    			selectedPermissions.add(permission);
    		}
    	}
    }

	public Long[] getSelectedPermissionIds(){
		if (selectedPermissions == null){
			return new Long[0];
		}
		Long[] ids = new Long[selectedPermissions.size()];
		int i=0;
		for (Permission permission: selectedPermissions){
			ids[i++]=permission.getId();
		}
		return ids;
    }
	    
    
    public Set<Permission> getAllPermissions() {
		return allPermissions;
	}

	public void setAllPermissions(Set<Permission> allPermissions) {
		this.allPermissions = allPermissions;
	}


	public Set<Permission> getAvailPermissions() {
		return availPermissions;
	}

	public void setAvailPermissions(Set<Permission> availPermissions) {
		this.availPermissions = availPermissions;
	}
	
	public Collection<Permission> getSortedAvailPermissions() {
		return (new PermissionSorter(permCompMode)).sortCollection(availPermissions);
	}

	public void setSortedAvailPermissions(Collection<Permission> availPermissionsIn) {
		//this.availPermissions.clear();
		this.availPermissions = new HashSet<Permission>();
		this.availPermissions.addAll(availPermissionsIn);
	}	
	
	public void adjustUsedPermissions(){
		availPermissions = new HashSet<Permission>();
		availPermissions.addAll(allPermissions);
		if (userGroup.getPermissions() != null){
			availPermissions.removeAll(userGroup.getPermissions());
		}
	}
	
	public Set<Permission> getSelectedAvailPermissions() {
		return selectedAvailPermissions;
	}

	public void setSelectedAvailPermissions(Set<Permission> selectedAvailPermissions) {
		this.selectedAvailPermissions = selectedAvailPermissions;
	}

	public Set<Permission> getSelectedPermissions() {
		return selectedPermissions;
	}
	
	public Collection<Permission> getSortedCurrentPermissions(){
		return (new PermissionSorter(permCompMode)).sortCollection(userGroup.getPermissions());
	}

	public void setSelectedPermissions(Set<Permission> selectedPermissions) {
		this.selectedPermissions = selectedPermissions;
	}

	public UserGroup getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
		setLocked(userGroup.getLocked());
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Collection<UserGroup> getImportGroups() {
		return importGroups;
	}

	public void setImportGroups(Collection<UserGroup> importGroups) {
		this.importGroups = importGroups;
	}

	public long getSelectedImportGroupId() {
		return selectedImportGroupId;
	}

	public void setSelectedImportGroupId(long selectedImportGroupId) {
		this.selectedImportGroupId = selectedImportGroupId;
	}
	
	public void resetSelectedImportGroupId(){
		this.selectedImportGroupId = IMPORT_GP_ID_NOTSELECTED;
	}

    

}
