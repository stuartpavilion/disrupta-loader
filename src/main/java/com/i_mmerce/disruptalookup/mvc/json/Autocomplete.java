package com.i_mmerce.disruptalookup.mvc.json;

import java.util.ArrayList;
import java.util.List;

public class Autocomplete {
	private String ngram;
	private List<String> results = new ArrayList<>();
	public String getNgram() {
		return ngram;
	}
	public void setNgram(String ngram) {
		this.ngram = ngram;
	}
	public List<String> getResults() {
		return results;
	}
	public void setResults(List<String> results) {
		this.results = results;
	}
	public Autocomplete(String ngram) {
		super();
		this.ngram = ngram;
	}
	
	
}
