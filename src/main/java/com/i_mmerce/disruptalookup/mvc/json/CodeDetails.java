package com.i_mmerce.disruptalookup.mvc.json;

public class CodeDetails {
	private String code;
	private String description;
	private String body;
	
	public CodeDetails(String code, String description, String body) {
		super();
		this.code = code;
		this.description = description;
		this.body = body;
	}

	public CodeDetails(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	
}
