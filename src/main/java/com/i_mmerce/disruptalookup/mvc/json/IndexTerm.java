package com.i_mmerce.disruptalookup.mvc.json;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IndexTerm {
	private String label;
	private IndexTermData data;
	private List<IndexTerm> children = new ArrayList<>();
	
	public IndexTerm(String label) {
		super();
		this.label = label;
        this.data = new IndexTermData(new ArrayList<IndexTermDataNote>(), "", "", -1);
	}

    public String getLabel() {
		return label;
	}
	public void setLabel(String index_term) {
		this.label = index_term;
	}
	public List<IndexTerm> getChildren() {
		return children;
	}
	public void setChildren(List<IndexTerm> children) {
		this.children = children;
	}
	
	public void add(IndexTerm child){
		children.add(child);
	}

	public IndexTermData getData() {
		return data;
	}

	public void setData(IndexTermData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "IndexTerm [label=" + label + ", data=" + data + ", children="
				+ children + "]";
	}

}
