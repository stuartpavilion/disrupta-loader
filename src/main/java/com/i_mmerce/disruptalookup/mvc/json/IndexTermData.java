package com.i_mmerce.disruptalookup.mvc.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IndexTermData {
	private List<IndexTermDataNote> notes;
	private String code;
    private String scheme;
    private int id;

	public IndexTermData(List<IndexTermDataNote> notes, String code, String scheme, int id) {
		super();
		this.notes = new ArrayList<IndexTermDataNote>(notes);
        this.code = code;
        this.scheme = scheme;
        this.id = id;
	}

    public List<IndexTermDataNote> getNotes() {
		return notes;
	}
	public void setNotes(List<IndexTermDataNote> notes) {
		this.notes = new ArrayList<IndexTermDataNote>(notes);
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
    public String getScheme() { return scheme; }
    public void setScheme(String scheme) { this.scheme = scheme; }
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

	@Override
	public String toString() {
		return "IndexTermData [notes=" + notes.toString() + ", code=" + code + ", scheme="
				+ scheme + ", id=" + id + "]";
	}

}
