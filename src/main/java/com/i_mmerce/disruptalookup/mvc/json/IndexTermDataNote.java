package com.i_mmerce.disruptalookup.mvc.json;

/**
 * Created by mikepollitt on 23/06/2014.
 */
public class IndexTermDataNote {

    private String _type;
    private String _description;

    public IndexTermDataNote(String type, String description) {
        _type = type;
        _description = description;
    }

    public String getType() {
        return _type;
    }

    public String getDescription() {
        return _description;
    }

}
