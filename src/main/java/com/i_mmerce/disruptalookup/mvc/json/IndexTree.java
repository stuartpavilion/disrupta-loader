package com.i_mmerce.disruptalookup.mvc.json;

import java.util.ArrayList;
import java.util.List;

public class IndexTree {
		private String query;
		private Integer depth;
		private List<IndexTerm> results;
		
		public IndexTree(String query, Integer depth) {
			super();
			this.query = query;
			this.depth = depth;
			this.results = new ArrayList<IndexTerm>();
		}
		
		public String getQuery() {
			return query;
		}
		public void setQuery(String query) {
			this.query = query;
		}
		public Integer getDepth() {
			return depth;
		}
		public void setDepth(Integer depth) {
			this.depth = depth;
		}

		public List<IndexTerm> getResults() {
			return results;
		}
		public void setResults(List<IndexTerm> results) {
			this.results = results;
		}

		@Override
		public String toString() {
			return "IndexTree [query=" + query + ", depth=" + depth
					+ ", results=" + results + "]";
		}
		
}
