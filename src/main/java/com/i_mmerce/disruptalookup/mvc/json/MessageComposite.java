package com.i_mmerce.disruptalookup.mvc.json;

import java.util.ArrayList;
import java.util.List;

public class MessageComposite {
		private String text;
		private List<MessageComposite> children = new ArrayList<>();
		
		public MessageComposite() {
			super();
		}
		public MessageComposite(String text) {
			super();
			this.text = text;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public List<MessageComposite> getChildren() {
			return children;
		}
		
		public void add(MessageComposite child){
			children.add(child);
		}
		
		@Override
		public String toString(){
			StringBuilder sb = new StringBuilder();
			sb.append('[');
			sb.append(text);
			if (!children.isEmpty()){
				sb.append(",<");
				boolean notFirstBorn = false;
				for (MessageComposite msgComp: children){
					if (notFirstBorn){
						sb.append(',');
					} else {
						notFirstBorn = true;
					}
					sb.append(msgComp.toString());
				}
				sb.append(">");
			}
			sb.append(']');
			return sb.toString();
		}
		
}
