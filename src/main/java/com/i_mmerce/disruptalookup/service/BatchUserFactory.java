package com.i_mmerce.disruptalookup.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.management.relation.Role;

import org.springframework.security.authentication.encoding.PasswordEncoder;

import com.i_mmerce.disruptalookup.domain.Permission;
import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.mvc.form.BatchUploadForm;
import com.i_mmerce.global.StringUtils;

public class BatchUserFactory {
	
	//private Permission roleCRM;
	private PasswordEncoder passwordEncoder;
	
	public BatchUserFactory(/**Permission roleCRM,**/ PasswordEncoder passwordEncoder) {
		super();
		//this.roleCRM = roleCRM;
		this.passwordEncoder = passwordEncoder;
	}

	public User createVpnReportCRMUser(String borisCode, String password, boolean setPasswordToUsername){
		String username = deriveUsernameFromBorisCode(borisCode);
		if (setPasswordToUsername){
			return createVpnReportCRMUser(borisCode, username, username);
		} else {
			return createVpnReportCRMUser(borisCode, username, password);
		}
	}
	
	public String deriveUsernameFromBorisCode(String borisCode){
		return borisCode.trim().toLowerCase() + "VPNreport";
	}
	public User createVpnReportCRMUser(String borisCode, String username, String password){
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		//user.getCustIds().add(borisCode.toUpperCase());
		
		//Set<Permission> roles = new HashSet<Permission>();
		//roles.add(roleCRM);
		//user.setPass)(roles);
		user.setEnabled(true);
		user.setAccountNotExpired(true);
		user.setCredentialsNonExpired(true);
		user.setAccountNonLocked(true);		
		return user;
	}	
	
    public List<User> createUsersFromImportFile(String dir, BatchUploadForm batchUploadForm){
    	List<User> users = new ArrayList<User>();
    	FileHelper fileHelper = new FileHelper();
    	File file = new File(dir+batchUploadForm.getFilename());
    	List<String[]> rows = fileHelper.extractRowsFromTextFile(file, ";", new int[]{batchUploadForm.getTypeCol(),batchUploadForm.getBorisCol(),batchUploadForm.getUsernameCol(),batchUploadForm.getPasswordCol()});
		for (String[] row: rows){
//			String entryType=row[batchUploadForm.getTypeCol()-1];
//			String borisCode=row[batchUploadForm.getBorisCol()-1];
//			String usernameVar = row[batchUploadForm.getUsernameCol()-1];
//			
//			//String usernameVar = (StringUtils.isEmpty(borisCode) ? "" : borisCode.toLowerCase());
//			String passwordVar = row[batchUploadForm.getPasswordCol() - 1];
			String entryType=row[0];
			String borisCode=row[1];
			String usernameVar = row[2];
			
			//String usernameVar = (StringUtils.isEmpty(borisCode) ? "" : borisCode.toLowerCase());
			String passwordVar = row[3];			
			if (entryType.matches(batchUploadForm.getTypeRegex()) && StringUtils.isNotEmpty(usernameVar) &&
				(passwordVar!=null) && StringUtils.isNotEmpty(borisCode)){
				switch (batchUploadForm.getBorisFilterMode()){
					case ToLowercase:
						borisCode = borisCode.toLowerCase();
						break;	
					case ToUppercase:
						borisCode = borisCode.toUpperCase();
						break;
				}	
				switch (batchUploadForm.getUsernameFilterMode()){
					case ToLowercase:
						usernameVar = usernameVar.toLowerCase();
						break;	
					case ToUppercase:
						usernameVar = usernameVar.toUpperCase();
						break;
				}
				switch (batchUploadForm.getPasswordFilterMode()){
					case ToLowercase:
						passwordVar = passwordVar.toLowerCase();
						break;	
					case ToUppercase:
						passwordVar = passwordVar.toUpperCase();
						break;
				}
				
				User user = createVpnReportCRMUser(borisCode, batchUploadForm.getUsernamePrefix() + usernameVar + batchUploadForm.getUsernameSuffix(),
						batchUploadForm.getPasswordPrefix() + passwordVar + batchUploadForm.getPasswordSuffix());
				users.add(user);
			}
		} 	
    	return users;
    }
}
