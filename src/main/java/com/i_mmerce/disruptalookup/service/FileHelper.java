package com.i_mmerce.disruptalookup.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

public class FileHelper {
    private static final Logger log = Logger.getLogger(FileHelper.class);
    public FileHelper() {
    }
    public void writeFile(String file, String content, Date date) throws IOException {
        writeFile(new File(file), content, date);
    }

    public void writeFile(String file, String content) throws IOException {
        writeFile(new File(file), content, new Date());
    }

    public void writeFile(File file, String content, Date date) throws IOException {

        FileWriter fr = new FileWriter(file);
        fr.write(content);
        fr.close();

        if (date != null) {
            file.setLastModified(date.getTime());
        }
    }
    
    public String readFileAsString(File file) {
    	StringBuilder contents = new StringBuilder();
    	BufferedReader reader = null;
    	
    	try {
    		reader = new BufferedReader(new FileReader(file));
    		String line = reader.readLine();
    		while ( line != null){
    			contents.append(line);
    			contents.append('\n');
    			line = reader.readLine();
    		}
    		return contents.toString();
    	} catch (FileNotFoundException nfe){
    		log.error("File: " + file.getAbsolutePath() + " not found.");
    	} catch (IOException ioe) {
    		log.error("IOException encountered attempting to read File: " + file.getAbsolutePath() + " .", ioe);
    	} finally {
    		try {
    			if (reader !=null){
    				reader.close();
    			}
    		} catch (IOException ioe){
    			log.error("IOException encountered attempting to close File: " + file.getAbsolutePath() + " .", ioe);	
    		}
    	}
    	return null;
    }
    
    public List<String> extractColumnFromTextFile(File file, String delim, int columnNumber) {
    	List<String> column = new ArrayList<String>();
    	BufferedReader reader = null;
    	
    	try {
    		reader = new BufferedReader(new FileReader(file));
    		String line = reader.readLine();
    		while ( line != null){
    			String[] cols= line.split(delim);
    			if (columnNumber <= cols.length){
    				column.add(cols[columnNumber-1]);
    			}
    			line = reader.readLine();
    		}
    		return column;
    	} catch (FileNotFoundException nfe){
    		log.error("File: " + file.getAbsolutePath() + " not found.");
    	} catch (IOException ioe) {
    		log.error("IOException encountered attempting to read File: " + file.getAbsolutePath() + " .", ioe);
    	} finally {
    		try {
    			if (reader !=null){
    				reader.close();
    			}
    		} catch (IOException ioe){
    			log.error("IOException encountered attempting to close File: " + file.getAbsolutePath() + " .", ioe);	
    		}
    	}
    	return null;
    }
    
    public List<String[]> extractRowsFromTextFile(File file, String delim, int[] columnNumbers) {
    	List<String[]> rows = new ArrayList<String[]>();
    	BufferedReader reader = null;
    	
    	try {
    		reader = new BufferedReader(new FileReader(file));
    		String line = reader.readLine();
    		while ( line != null){
    			String[] allCols= line.split(delim);
    			String[] cols = new String[columnNumbers.length];
    			boolean foundColumn = false;
    			for (int i=0; i<columnNumbers.length; i++){
    				if (columnNumbers[i] > 0 && columnNumbers[i] <= allCols.length){
    					cols[i]= (allCols[columnNumbers[i]-1]).trim();
    					foundColumn = true;
    				} else if (columnNumbers[i] == 0){
    					//special case if columnNumber[i] set to empty
    					cols[i] = "";
    					foundColumn = true;
    				}
    			}
    			if (foundColumn){
    				rows.add(cols);
    			}
    			line = reader.readLine();
    		}
    		return rows;
    	} catch (FileNotFoundException nfe){
    		log.error("File: " + file.getAbsolutePath() + " not found.");
    	} catch (IOException ioe) {
    		log.error("IOException encountered attempting to read File: " + file.getAbsolutePath() + " .", ioe);
    	} finally {
    		try {
    			if (reader !=null){
    				reader.close();
    			}
    		} catch (IOException ioe){
    			log.error("IOException encountered attempting to close File: " + file.getAbsolutePath() + " .", ioe);	
    		}
    	}
    	return null;
    }    
}
