package com.i_mmerce.disruptalookup.service;

public enum FilterMode {
	ToLowercase,
	ToUppercase,
	Unchanged
}
