package com.i_mmerce.disruptalookup.service;

import java.util.List;

import org.apache.lucene.search.suggest.Lookup.LookupResult;

import au.edu.uow.nccc.ecoder.search.ContentIndexing.DocumentType;
import au.edu.uow.nccc.ecoder.search.SearchResult;

public interface LuceneSearchService {

	List<SearchResult> runCodeExpertQuery(String query, int documentTypeIndex);

	void initialLoad();

	public List<LookupResult> suggestLookup(String startQuery, int numSuggestions);

	public List<SearchResult> runCodeExpertQuery(String query, DocumentType scope,	boolean leadTermsOnly);

}
