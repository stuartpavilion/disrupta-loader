package com.i_mmerce.disruptalookup.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.search.suggest.Lookup.LookupResult;
import org.springframework.stereotype.Service;

import au.edu.uow.nccc.ecoder.model.Model;
import au.edu.uow.nccc.ecoder.search.ContentIndexing;
import au.edu.uow.nccc.ecoder.search.ContentIndexing.DocumentType;
import au.edu.uow.nccc.ecoder.search.SearchResult;
import au.edu.uow.nccc.ecoder.search.SearchResultsModel;

@Service("luceneSearchService")
public class LuceneSearchServiceImpl implements LuceneSearchService{
	private static final Logger logger = Logger.getLogger(LuceneSearchServiceImpl.class);
	
	/**
	 * A list of valid document types which is used to populate the scope selection control.
	 */
	private final List<ContentIndexing.DocumentType> docTypes = new ArrayList<>();
	private final SearchResultsModel model = new SearchResultsModel();
	
	public LuceneSearchServiceImpl() {
		initialise();
	}
	
	private void initialise(){
		final List<String> scopes = new ArrayList<>();
		scopes.add("All"); //$NON-NLS-1$
		scopes.add("Lead Terms"); //$NON-NLS-1$
		for (final ContentIndexing.DocumentType type : ContentIndexing.DocumentType.values()) {
			docTypes.add(type);
			scopes.add(type.getDisplay());
		}
		// Load the application model.
		//Model.INSTANCE.load();	
	}
	
	@Override
	public List<SearchResult> runCodeExpertQuery(String query, int documentTypeIndex){
		// Determine the current parameters.
		final DocumentType scope = documentTypeIndex <= 1 ? null : docTypes.get(documentTypeIndex - 2);
		// Run the query.
		model.runQuery(query, scope, documentTypeIndex == 1);
		
		logger.info("Results found: " + model.getResults().size()); //$NON-NLS-1$
		
		return model.getResults();
	}
	@Override
	public List<SearchResult> runCodeExpertQuery(String query,DocumentType scope, boolean leadTermsOnly){
		model.runQuery(query, scope, leadTermsOnly);
		logger.info("Results found: " + model.getResults().size()); //$NON-NLS-1$
		return model.getResults();
	}
	@Override
	public List<LookupResult> suggestLookup(String startQuery, int numSuggestions){
		List<LookupResult> results;
		try {
			results = model.suggest(startQuery, numSuggestions);
			logger.info("Suggest Results found: " + results.size() + " for start Query="+ startQuery);
			return results;
		} catch (Exception e) {
			logger.error("Exception encountered looking up suggestions.",e);;
		}
		return null;
	}
	
	@Override
	public void initialLoad(){
		// Load the application model.
		Model.INSTANCE.load();		
	}
}
