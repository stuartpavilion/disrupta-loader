package com.i_mmerce.disruptalookup.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.i_mmerce.disruptalookup.domain.Permission;

public class UserDetailsServiceImpl implements UserDetailsService{

	//atAutowired
	private UserManager userManager;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		com.i_mmerce.disruptalookup.domain.User usr = userManager.findByUserName(username);
		if (usr==null){
			throw new UsernameNotFoundException("Unknown username" + username);
		}
		//Only copy in the following line for development purposes. It will resave User's password in
		//encrypted form in database. DO NOT COMMENT THIS IN IN ANY PRODUCTION SYSTEM.
		//userManager.persistUser(usr, true);
		Collection<Permission> permissions = userManager.getCurrentPermissions(usr);
		User user = new User(usr.getUsername(), usr.getPassword(),
				usr.isEnabled(), usr.isAccountNotExpired(),
				usr.isCredentialsNonExpired(), usr.isAccountNonLocked(),
				getAuthorities(permissions));
		return user;
	}

	private Collection<GrantedAuthority> getAuthorities(Collection<Permission> permissions){
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (Permission permission: permissions) {
			authorities.add(new GrantedAuthorityImpl(permission.getName()));
			//authorities.add(new SimpleGrantedAuthority(permission.getName()));
		}
		return authorities;
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	
	
}
