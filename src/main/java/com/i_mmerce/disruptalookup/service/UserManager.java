/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.ui.Model;

import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.domain.Permission;
import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroup;
import com.i_mmerce.disruptalookup.domain.UserGroupAccess;


/**
 *
 * @author Stuart Sontag
 */
public interface UserManager {
	public static final String PERM_ALL = "PERM_ALL";
	public static final String PERM_ALL_EXCEPT = "PERM_ALL_EXCEPT";
	public static final String PERM_ALL_EXCEPT_NETWORK_ADMIN = "PERM_ALL_EXCEPT_NETWORK_ADMIN";
	public static final String PERM_NETWORK_ADMIN = "PERM_NETWORK_ADMIN";
	public static final String PERM_GROUP_ADMIN = "PERM_GROUP_ADMIN";
	public static final String PERM_USER_ADMIN = "PERM_USER_ADMIN";
	public static final String PERM_ACCESS_ALL_VPNS = "PERM_ACCESS_ALL_VPNS";
	public static final String PERM_EDIT_ALL_CRS = "PERM_EDIT_ALL_CRS";
	public static final String PERM_ACCESS_ALL_CRS = "PERM_ACCESS_ALL_CRS";
	public static final String PERM_EDIT_CRM = "PERM_EDIT_CRM";
	public static final String PERM_VIEW_CRM = "PERM_VIEW_CRM";
	public static final String PERM_APPROVE_CRM = "PERM_APPROVE_CRM";
	public static final String PERM_GROUP_LOCKED_ADMIN = "PERM_GROUP_LOCKED_ADMIN";
	public static final String PERM_DEPLOYMENT_CR_VIEW = "PERM_DEPLOYMENT_CR_VIEW";
	public static final String PERM_DEPLOYMENT_CR_EDIT = "PERM_DEPLOYMENT_CR_EDIT";
	public static final String PERM_DEPLOYMENT_CR_VIEW_ALL = "PERM_DEPLOYMENT_CR_VIEW_ALL";
	public static final String PERM_DEPLOYMENT_CR_EDIT_ALL = "PERM_DEPLOYMENT_CR_EDIT_ALL";
	public static final String PERM_DEPLOYMENT_CR_ACTION_ALL = "PERM_DEPLOYMENT_CR_ACTION_ALL";
	public static final String PERM_DEPLOYMENT_CR_APPROVE = "PERM_DEPLOYMENT_CR_APPROVE";
	public static final String PERM_DEPLOYMENT_CR_SCHEDULE = "PERM_DEPLOYMENT_CR_SCHEDULE";
	public static final String PERM_DEPLOYMENT_CR_DEPLOY = "PERM_DEPLOYMENT_CR_DEPLOY";
	public static final String PERM_DEPLOYMENT_CR_ABANDON = "PERM_DEPLOYMENT_CR_ABANDON";
	
	public static final String EMAIL_DOMAIN = "@railcorp.nsw.gov.au";
	public static final String DEFAULT_ADMIN_USERNAME_BASE = "admin";
	public static final String DEFAULT_ADMIN_PASSWORD = "password";
	public static final String DEFAULT_AMIN_GROUP_NAME_PREFIX = "Administrator_";
	public static final String DEFAULT_AMIN_GROUP_DESCRIPTION_SUFFIX = " Group";
	public static final String CURRENT_USER_KEY = "currentUser";
	
	public User getCurrentLoggedOnUser();
    public User findByUserName(String userName) /* throws NotFoundException*/;
    public void persistUser(User user, boolean passwordChanged);
    public void mergeUser(User user, boolean passwordChanged);
    public void setUserAccessFlags(User user, boolean enabled, boolean accountNotExpired, boolean credentialsNonExpired, boolean accountNonLocked);
    public List<Permission> getAllPermissions();
    public List<User> getAllUsers();
	public List<User> getAllUsersExcept(User user);
	public List<UserGroup> getAllAvailUserGroups(Network network);
	public List<UserGroupAccess> getUserGroupAccessListForUser(User user);
	public User findById(long id);
	public Permission getPermission(String name);
	public Set<Permission> getCurrentPermissions(User user);
	void deleteById(long id);
	public PasswordEncoder getPasswordEncoder();
	public List<User> searchAllUsersExcept(User user, String includeStr, String excludeStr, Network network);
	public boolean permissionsContain(Collection<Permission> permissions, String ... names);
	public void persistUserGroupAccess(UserGroupAccess userGroupAccess);
	public void deleteUserGroupAccessById(long id);
	public void persistUserGroup(UserGroup userGroup);
	public void deleteUserGroupById(long id);
	public UserGroup findUserGroupById(long id);
	public List<UserGroup> searchAllUserGroupsExcept(UserGroup userGroup, String includeStr, String excludeStr, Network network);
	public Network findNetworkById(long id);
	public void persistNetwork(Network network);
	public List<Network> searchAllNetworksExcept(Network network, String includeStr, String excludeStr);
	public void deleteNetworkById(long id);
	public Set<Permission> getAssignablePermissionsByUser(User user);
	public void createNetworkAndDefaultAdminUser(Network network);
	public Set<String> getCurrentUserPermissionNames() throws UserManagerException;
	public User getCurrentLoggedOnUser(Model model, boolean forcePersistenceRetrieval);
	public User getCurrentLoggedOnUser(HttpSession session,	boolean forcePersistenceRetrieval);
	public boolean currentUserHasAtleastOnePermission(String ... permissionNames) throws UserManagerException;
	public boolean currentUserHasAllPermissions(String ... permissionNames) throws UserManagerException;
	public String getCurrentLoggedOnUsername();
	public String createPermLogString(Collection<Permission> perms);
	public Set<Permission> getPermissionSetsIntersection(Set<Permission> s1, Set<Permission> s2);
	public Set<Permission> getAllExceptPermissions(Set<Permission> currentPerms);
	public void lockUnlockUser(long userId, boolean unlock);
}
