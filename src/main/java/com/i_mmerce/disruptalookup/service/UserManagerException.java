package com.i_mmerce.disruptalookup.service;

public class UserManagerException extends Exception {

	public UserManagerException() {
	}

	public UserManagerException(String msg) {
		super(msg);
	}

	public UserManagerException(Throwable t) {
		super(t);
	}

	public UserManagerException(String msg, Throwable t) {
		super(msg, t);
	}

}
