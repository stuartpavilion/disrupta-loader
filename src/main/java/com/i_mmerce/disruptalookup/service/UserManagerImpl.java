/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.i_mmerce.disruptalookup.service;

import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.i_mmerce.disruptalookup.dao.NetworkDao;
import com.i_mmerce.disruptalookup.dao.PermissionDao;
import com.i_mmerce.disruptalookup.dao.UserDao;
import com.i_mmerce.disruptalookup.dao.UserGroupAccessDao;
import com.i_mmerce.disruptalookup.dao.UserGroupDao;
import com.i_mmerce.disruptalookup.domain.Network;
import com.i_mmerce.disruptalookup.domain.Permission;
import com.i_mmerce.disruptalookup.domain.User;
import com.i_mmerce.disruptalookup.domain.UserGroup;
import com.i_mmerce.disruptalookup.domain.UserGroupAccess;

/**
 *
 * @author Stuart Sontag
 */
@Service("userManager")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
//@Transactional
public class UserManagerImpl implements UserManager {
	private static final Logger logger = Logger.getLogger(UserManagerImpl.class);
	
    @Autowired
    private UserDao userDao;
    
    @Autowired
    private PermissionDao permissionDao;
    
    @Autowired(required = true)
    @Qualifier("shaPasswordEncoder")   
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private UserGroupDao userGroupDao;

    @Autowired
    private UserGroupAccessDao userGroupAccessDao;
    
    @Autowired
    private NetworkDao networkDao;
    
    public UserManagerImpl() {
        super();
    }

	@Override
	public User getCurrentLoggedOnUser() {
    		org.springframework.security.core.userdetails.UserDetails usrDetails;
			try {
				usrDetails = (org.springframework.security.core.userdetails.UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			} catch (ClassCastException e) {
				return null;
			}
    		if (usrDetails == null){
    			return null;
    		}
    		String userName = usrDetails.getUsername();
    		return userDao.findByUserName(userName);
	}
	
	@Override
	public String getCurrentLoggedOnUsername(){
		org.springframework.security.core.userdetails.UserDetails usrDetails;
		try {
			usrDetails = (org.springframework.security.core.userdetails.UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (ClassCastException e) {
			return null;
		}
		if (usrDetails == null){
			return null;
		}
		return usrDetails.getUsername();
	}
	
	@Override
	public User getCurrentLoggedOnUser(Model model, boolean forcePersistenceRetrieval) {
		User user = null;
		if (!forcePersistenceRetrieval){
		    user = (User) model.asMap().get(CURRENT_USER_KEY);
		}
		if (forcePersistenceRetrieval || user == null) {
			org.springframework.security.core.userdetails.UserDetails usrDetails;
			try {
				usrDetails = (org.springframework.security.core.userdetails.UserDetails) SecurityContextHolder
						.getContext().getAuthentication().getPrincipal();
			} catch (ClassCastException e) {
				return null;
			}
			if (usrDetails == null) {
				return null;
			}
			String userName = usrDetails.getUsername();
			user = userDao.findByUserName(userName);
			if (user != null){
				model.addAttribute(CURRENT_USER_KEY, user);
			}
		}
		return user;
	}
	@Override
	public User getCurrentLoggedOnUser(HttpSession session, boolean forcePersistenceRetrieval) {
		User user = null;
		if (!forcePersistenceRetrieval){
		    user = (User)session.getAttribute(CURRENT_USER_KEY);
		}
		if (forcePersistenceRetrieval || user == null) {
			org.springframework.security.core.userdetails.UserDetails usrDetails = null;
			try {
				usrDetails = (org.springframework.security.core.userdetails.UserDetails) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();
			} catch (ClassCastException e) {
				return null;
			}
			if (usrDetails == null) {
				return null;
			}
			String userName = usrDetails.getUsername();
			user = userDao.findByUserName(userName);
			if (user != null){
				session.setAttribute(CURRENT_USER_KEY, user);
			}
		}
		return user;
	}
	
	@Override
	public Set<String> getCurrentUserPermissionNames() throws UserManagerException{
		Set<String> permissionNames = new HashSet<String>();
		org.springframework.security.core.userdetails.UserDetails usrDetails = null;
		try {
			usrDetails = (org.springframework.security.core.userdetails.UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (usrDetails == null){
				throw  new UserManagerException("Exception encountered attempting to retrieve user permissions:");
			}
			Collection<? extends GrantedAuthority> authorities =  usrDetails.getAuthorities();
			for (GrantedAuthority authority: authorities){
				permissionNames.add(authority.getAuthority());
			}
		} catch (ClassCastException e) {
			logger.error("Exception encountered attempting to retrieve user permissions:", e);
			throw  new UserManagerException("Exception encountered attempting to retrieve user permissions:",e);
		}
		return permissionNames;
	}
	
	@Override
	public boolean currentUserHasAtleastOnePermission(String ... permissionNames) throws UserManagerException{
		Set<String> currentPermNames = getCurrentUserPermissionNames();
		for (String testPermName: permissionNames){
			if (currentPermNames.contains(testPermName)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean currentUserHasAllPermissions(String ... permissionNames) throws UserManagerException{
		Set<String> currentPermNames = getCurrentUserPermissionNames();
		for (String testPermName: permissionNames){
			if (!currentPermNames.contains(testPermName)){
				return false;
			}
		}
		return true;
	}
    @Override
	public List<Permission> getAllPermissions() {
		return permissionDao.getAll();
	}


	@Override
    public User findByUserName(String username) /* throws NotFoundException */{
    	return userDao.findByUserName(username);
    }
	
	@Override
    public User findById(long id) /* throws NotFoundException */{
    	return userDao.findById(id);
    }
	
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
    public void deleteById(long id) /* throws NotFoundException */{
		userGroupAccessDao.deleteByUserId(id);
    	userDao.delete(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void persistUser(User user, boolean passwordChanged) {
    	if (passwordChanged){
    		encodePassword(user);
    	}
        userDao.save(user);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void lockUnlockUser(long userId, boolean unlock) {
    	User user = userDao.findById(userId);
    	user.setAccountNonLocked(unlock);
        userDao.save(user);
    }    

    private void encodePassword(User user) {
        String salt = user.getUsername();
        user.setPassword(passwordEncoder.encodePassword(user.getPassword(), salt));
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void persistUserGroupAccess(UserGroupAccess userGroupAccess) {
        userGroupAccessDao.save(userGroupAccess);
    }
    
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
    public void deleteUserGroupAccessById(long id) /* throws NotFoundException */{
		userGroupAccessDao.delete(id);
    }
	
	@Override
	public void setUserAccessFlags(User user, boolean enabled,
			boolean accountNotExpired, boolean credentialsNonExpired,
			boolean accountNonLocked) {
		user.setEnabled(enabled);
		user.setAccountNotExpired(accountNotExpired);
		user.setCredentialsNonExpired(credentialsNonExpired);
		user.setAccountNonLocked(accountNonLocked);
	}


	@Override
	public List<User> getAllUsers() {
		return userDao.findAll();
	}
	
	@Override
	public List<User> getAllUsersExcept(User user){
		return userDao.findAllExcept(user);
	}

	@Override
	public List<User> searchAllUsersExcept(User user, String includeStr, String excludeStr, Network network){
		return userDao.searchAllExcept(user, includeStr, excludeStr, network);
	}
	
	@Override
	public Permission getPermission(String name) {
		return permissionDao.findByName(name);
	}
	
	@Override
	public PasswordEncoder getPasswordEncoder(){
		return passwordEncoder;
	}


	@Override
	public Set<Permission> getCurrentPermissions(User user) {
		Date now = new Date();
		Set<Permission> currentPerms = permissionDao.getAllCurrentAtDate(user, now);
		//if PERM_ALL permission is present then add in all other permissions.
		if (permissionsContain(currentPerms, PERM_ALL)){
			currentPerms.addAll(permissionDao.getAll());
		} else if (permissionsContain(currentPerms, PERM_ALL_EXCEPT_NETWORK_ADMIN)){
			currentPerms.addAll(permissionDao.getAllExcept(PERM_ALL, PERM_NETWORK_ADMIN, PERM_ALL_EXCEPT_NETWORK_ADMIN));
		} else if (permissionsContain(currentPerms, PERM_ALL_EXCEPT)){
			return getAllExceptPermissions(currentPerms);
		}
		return currentPerms;
	}
	
	
	@Override
	public Set<Permission> getAssignablePermissionsByUser(User user) {
		Date now = new Date();
		Set<Permission> usersPerms = permissionDao.getAllCurrentAtDate(user, now);
		logger.debug("UserPerms=" + createPermLogString(usersPerms));
		Set<Permission> assignablePerms = new HashSet<Permission>();
		//if the current Users permissions include ALL or NETWORK_ADMIN then may assign all Permissions
		if (permissionsContain(usersPerms, PERM_ALL, PERM_NETWORK_ADMIN) && !permissionsContain(usersPerms, PERM_ALL_EXCEPT)){
			assignablePerms.addAll(permissionDao.getAll());
			
		//CHECK THIS LOGIC! Otherwise if current Users permissions include PERM_ALL_EXCEPT
		} else if (permissionsContain(usersPerms, PERM_ALL_EXCEPT)){
			assignablePerms.addAll(getAllExceptPermissions(usersPerms));
						
		//Otherwise, if current Users permissions include ALL_EXCEPT_NETWORK_ADMIN or PERM_GROUP_ADMIN
		//then may assign All permissions except ALL or NETWORK_ADMIN
		} else if (permissionsContain(usersPerms, PERM_ALL_EXCEPT_NETWORK_ADMIN, PERM_GROUP_ADMIN)){
			assignablePerms.addAll(permissionDao.getAllExcept(PERM_ALL, PERM_NETWORK_ADMIN, PERM_ALL_EXCEPT));
		//Otherwise, if current Users permissions include PERM_USER_ADMIN 
		//then may assign All permissions except ALL or NETWORK_ADMIN or GROUP_ADMIN			
		} else if (permissionsContain(usersPerms, PERM_USER_ADMIN)){
			assignablePerms.addAll(permissionDao.getAllExcept(PERM_ALL, PERM_NETWORK_ADMIN,  PERM_GROUP_ADMIN, PERM_ALL_EXCEPT));
		}
		logger.debug("Assignable Perms=" + createPermLogString(assignablePerms));
		return assignablePerms;
	}	
	
	@Override
	public String createPermLogString(Collection<Permission> perms){
		StringWriter sw = new StringWriter();
		sw.append("[");
		boolean atleastOne = false;
		for (Permission perm: perms){
			if (atleastOne){
				sw.append(",");
			}
			sw.append(perm.getName());
			atleastOne = true;
		}
		sw.append("]");
		return sw.toString();
	}
	
	@Override
	public Set<Permission> getAllExceptPermissions(Set<Permission> currentPerms){
		if (!permissionsContain(currentPerms, PERM_ALL_EXCEPT)){
			return currentPerms;
		}
		List<String> disallowedPerms = new ArrayList<String>();
		disallowedPerms.add(PERM_ALL);
		disallowedPerms.add(PERM_ALL_EXCEPT);
		for (Permission perm: currentPerms){
			disallowedPerms.add(perm.getName());
		}
		Set<Permission> permissions = new HashSet<Permission>();
		permissions.addAll(permissionDao.getAllExcept(disallowedPerms.toArray(new String[0])));
		return permissions;
	}	
	@Override
	public boolean permissionsContain(Collection<Permission> permissions, String ... names){
		for (Permission permission: permissions){
			for (String name : names){
				if (name.equals(permission.getName())){
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
    public Set<Permission> getPermissionSetsIntersection(Set<Permission> s1, Set<Permission> s2){
    	Set<Permission> intersection = new HashSet<Permission>();
    	for (Permission p1: s1){
    		for (Permission p2: s2){
    			if (p1.getId() == p2.getId()){
    				intersection.add(p1);
    			}
    		}
    	}
    	return intersection;
    }


	@Override
	public List<UserGroup> getAllAvailUserGroups(Network network) {
		return userGroupDao.getAllAvailUserGroups(network);
	}


	@Override
	public List<UserGroupAccess> getUserGroupAccessListForUser(User user) {
		return userGroupAccessDao.getUserGroupAccessList(user);
	}

	@Override
	public void persistUserGroup(UserGroup userGroup) {
		userGroupDao.save(userGroup);
	}

	@Override
	public void deleteUserGroupById(long id) {
		userGroupDao.delete(id);	
		
	}
	
	@Override
    public UserGroup findUserGroupById(long id) /* throws NotFoundException */{
    	return userGroupDao.findById(id);
    }

	@Override
	public List<UserGroup> searchAllUserGroupsExcept(UserGroup userGroup,
			String includeStr, String excludeStr, Network network) {
		return userGroupDao.searchAllExcept(userGroup, includeStr, excludeStr, network);
	}

	@Override
	public Network findNetworkById(long id) {
		return networkDao.findById(id);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void persistNetwork(Network network) {
		networkDao.save(network);
	}
	
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
	public void createNetworkAndDefaultAdminUser(Network network) {
		networkDao.save(network);
		
		//Create Admin User
		String adminName = DEFAULT_ADMIN_USERNAME_BASE+network.getName();
		User adminUser = new User(null, adminName, adminName + EMAIL_DOMAIN, adminName, adminName,
				DEFAULT_ADMIN_PASSWORD, new Timestamp((new Date()).getTime()),
				true, true, true, true, network);
		encodePassword(adminUser);
		userDao.save(adminUser);
		
		//Create Admin Group
		Set<Permission> groupPerms = new HashSet<Permission>();
		groupPerms.add(permissionDao.findByName(PERM_ALL_EXCEPT_NETWORK_ADMIN));
		UserGroup adminGroup = new UserGroup(null, DEFAULT_AMIN_GROUP_NAME_PREFIX + network.getName() , network, groupPerms,
				DEFAULT_AMIN_GROUP_NAME_PREFIX + network.getName() + DEFAULT_AMIN_GROUP_DESCRIPTION_SUFFIX);
		userGroupDao.save(adminGroup);
		
		//create a non time limited User Group Access
		 UserGroupAccess uga = new UserGroupAccess(null, adminUser, adminGroup, false, null, null);
		 userGroupAccessDao.save(uga);
	}

	@Override
	public List<Network> searchAllNetworksExcept(Network network,
			String includeStr, String excludeStr) {
		return networkDao.searchAllExcept(network, includeStr, excludeStr);
	}

	@Override
	public void deleteNetworkById(long id) {
		networkDao.delete(id);
		
	}

	@Override
	public void mergeUser(User user, boolean passwordChanged) {
    	if (passwordChanged){
    		encodePassword(user);
    	}
        userDao.merge(user);
	}
	
	
}
