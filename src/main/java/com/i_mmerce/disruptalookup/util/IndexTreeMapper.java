package com.i_mmerce.disruptalookup.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import au.edu.uow.nccc.ecoder.model.bean.CodeTermParent;
import au.edu.uow.nccc.ecoder.model.bean.CodeTermParentBase;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesIndexTerm;
import au.edu.uow.nccc.ecoder.model.bean.DiseasesIndexTermNote;
import au.edu.uow.nccc.ecoder.search.SearchResult;

import com.i_mmerce.disruptalookup.mvc.json.IndexTerm;
import com.i_mmerce.disruptalookup.mvc.json.IndexTermData;
import com.i_mmerce.disruptalookup.mvc.json.IndexTree;

/**
 * Created by mikepollitt on 21/06/2014.
 */
public class IndexTreeMapper {

    private IndexTreeMapper() {}

    private static class SearchResultScoreComparator implements Comparator<SearchResult> {

        @Override
        public int compare(SearchResult o1, SearchResult o2) {
            if (o2.getScore() > o1.getScore()) {
                return 1;
            } else if (o2.getScore() < o1.getScore()) {
                return -1;
            }
            return 0;
        }
    }

    public static IndexTree map(String query, Integer depth, List<SearchResult> searchResults) {

        IndexTree tree = new IndexTree(query, depth);

        //Collections.sort(searchResults, new SearchResultScoreComparator());

        for (SearchResult r : searchResults) {
        //SearchResult r = searchResults.get(0);
                CodeTermParent term =r.getDocumentAsCodeTermParent();
                IndexTerm indexTerm = attachToParent(tree, term);
                if (indexTerm != null) {
                    attachChildren(indexTerm, term.getChildren());
                }
        }

        return tree;

    }

    private static void attachChildren(IndexTerm indexTerm, List<CodeTermParent> children) {
        for (CodeTermParent term : children) {
            IndexTerm childIndexTerm = getIndexTreeTermFromCodeTermParentTerm(term);
            indexTerm.add(childIndexTerm);
            attachChildren(childIndexTerm, term.getChildren());
        }
    }

    private static IndexTerm attachToParent(IndexTree tree, CodeTermParent term) {
        if (term.getLevel() < 1) {
            // Discard index nodes like "A", "B", "C", etc. we only want the first level below these
            return null;
        }

        if (term.getLevel() == 1) {
            IndexTerm indexTerm = getIndexTreeTermFromCodeTermParentTerm(term);
            tree.getResults().add(indexTerm);
            return indexTerm;
        }

        IndexTerm parent = findIndexTermById(tree.getResults(), ((CodeTermParent) term.getParent()).getID());
        if (parent == null) {
            parent = attachToParent(tree, (CodeTermParent)term.getParent());
            if (parent == null) {
                return null;
            }
        }
        if (findIndexTermById(parent.getChildren(), term.getID()) != null) {
            // this child already exists here
            return null;
        }

        IndexTerm indexTerm = getIndexTreeTermFromCodeTermParentTerm(term);
        parent.add(indexTerm);
        return indexTerm;
    }

    public static IndexTerm findIndexTermById(List<IndexTerm> children, int id) {
        for (IndexTerm child : children) {
            if (child.getData().getId() == id) {
                return child;
            }
            IndexTerm term = findIndexTermById(child.getChildren(), id);
            if (term != null) return term;
        }
        return null;
    }


//    private static IndexTerm getIndexTreeTermFromDiseaseIndexTerm(DiseasesIndexTerm term) {
//        IndexTerm t = new IndexTerm(term.getText() + (term.getSecondPart() == null ? "" : term.getSecondPart()));
//        String code = "", note = "";
//        for (DiseasesIndexTermNote tn : term.getNotes()) {
//            if (tn.getType() == DiseasesIndexTermNote.Type.CODE) {
//                code = tn.getText();
//            } else {
//                note += "\n" + tn.getText(); // FIXME: Different types of notes should be handled differently
//            }
//        }
//        t.setData(new IndexTermData(note, code, "icd-10-am", term.getID()));
//        return t;
//    }
    
    private static IndexTerm getIndexTreeTermFromCodeTermParentTerm(CodeTermParent term) {
    	IndexTerm t = null;
    	if (term.getText() == null){
    		t = new IndexTerm(term.getPreferredText());
    	} else {
    		t = new IndexTerm(term.getText() + (term.getSecondPart() == null ? "" : term.getSecondPart()));
    	}
        t.setData(term.getIndexTermData());
        return t;
    }    
}
