package com.i_mmerce.disruptalookup.util;

import java.math.BigDecimal;

public class Stopwatch {
	private String description;
	private long startTime;
	private long stopTime;
	
	private static final BigDecimal BIG_DECIMAL_MILLION = new BigDecimal("1000000");
	private static final BigDecimal BIG_DECIMAL_BILLION = new BigDecimal("1000000000");

	public Stopwatch(String description) {
		this.description = description;
		reset();
	}

	public Stopwatch() {
		description = "";
		reset();
	}
	
	public void reset(){
		this.startTime=0;
		this.stopTime=0;
	}
	
	public void reset(String description){
		this.startTime=0;
		this.stopTime=0;
		this.description=description;
	}
	
	public void start(){
		this.startTime = System.nanoTime();
		this.stopTime = 0;
	}
	
	public void stop(){
		this.stopTime = System.nanoTime();
	}
	
	public long getIntervalNanoSecs(){
		return (stopTime-startTime);
	}
	
	public BigDecimal getIntervalMilliSecs(){
		BigDecimal bdInterval = new BigDecimal(getIntervalNanoSecs());
		return bdInterval.divide(BIG_DECIMAL_MILLION,3,BigDecimal.ROUND_HALF_UP);
	}
	
	public BigDecimal getIntervalSecs(){
		BigDecimal bdInterval = new BigDecimal(getIntervalNanoSecs());
		return bdInterval.divide(BIG_DECIMAL_BILLION,3,BigDecimal.ROUND_HALF_UP);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(description);
		sb.append(": ").append(getIntervalNanoSecs()+" ns");
		sb.append(": ").append(getIntervalMilliSecs()+" ms");
		sb.append(": ").append(getIntervalSecs()+" s");
		return sb.toString();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
