package com.i_mmerce.disruptalookup.util;

import java.io.Serializable;

public class Tuple<L, R> implements Serializable
{
	private L mLeft;
	private R mRight;
	
	public Tuple()
	{
	}
	
	public Tuple(L left, R right)
	{
		mLeft = left;
		mRight = right;
	}
	
	public L getLeft() {
		return mLeft;
	}
	
	public void setLeft(L left) {
		mLeft = left;
	}
	
	public R getRight() {
		return mRight;
	}
	
	public void setRight(R right) {
		mRight = right;
	}
	
	@Override
	public String toString() {
		return "Tuple: left="+mLeft+", right="+mRight;
	}
}
