package com.i_mmerce.global;

public class Country {
	private String name;
	private String alpha3;
	private int countryCode;
	public Country(String name, String alpha3, int countryCode) {
		super();
		this.name = name;
		this.alpha3 = alpha3;
		this.countryCode = countryCode;
	}
	public Country() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlpha3() {
		return alpha3;
	}
	public void setAlpha3(String alpha3) {
		this.alpha3 = alpha3;
	}
	public int getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	
	
}
