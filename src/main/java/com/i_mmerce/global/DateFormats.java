package com.i_mmerce.global;

public interface DateFormats {
	
	String DTO_DATE_FORMAT = "yyyyMMdd";
	String SHORT_DISPLAY_DATE_FORMAT = "yyyy/MM/dd";
	String DTO_DATETIME_FORMAT = "yyyyMMddHHmmss";
	String SHORT_DISPLAY_DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss";	
	String SHORT_DISPLAY_TIME_FORMAT = "HH:mm";
	String HOUR_ONLY_DISPLAY_TIME_FORMAT = "HH";
	
    String SHORT_AUS_DATE_FORMAT  = "dd/mm/yy";
    String XFER_AUS_DATEFORMAT = "yyyyMMdd";
    String EDITION_DATE_FORMAT = "yyyy-MM-dd";
    //2013-08-01T15:00:37.041+10:00
    String SITE_CODES_UPDATE_FORMAT = "yyyy-MM-dd'T'k:mm:ss.SZ";
}
