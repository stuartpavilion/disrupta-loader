package com.i_mmerce.global;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class HttpRequestUtil 
{
    private static Logger LOGGER = Logger.getLogger(HttpRequestUtil.class);

    private HttpRequestUtil() {}
	
	public static final String UTF_8 = "UTF-8";
	
	public static final String DEFAULT_URL_ENCODER = UTF_8;
	
	/**
	 * Return the request client's IP address.
	 * If client is behind a proxy and the proxy stores the real ip address in x-fowarded-for header then
	 * it is returned.
	 * Otherwise it will return <code>HttpServletRequest.getRemoteAddr()</code>.
	 *   
	 * @param request
	 * @return
	 */
	public static String getClientIpAddress(HttpServletRequest request)
	{
		if (request==null) return null;
		
		// Get client's IP address
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress!=null) return ipAddress;
        
        ipAddress = request.getHeader("X_FORWARDED_FOR");
        if (ipAddress!=null) return ipAddress;

        return request.getRemoteAddr();
	}
 	
	public static String getErrorRequestURI(HttpServletRequest request)
	{
		if (request==null) return null;
		return (String)request.getAttribute("javax.servlet.error.request_uri");
	}
	
}
