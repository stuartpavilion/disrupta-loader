package com.i_mmerce.global;

import java.net.URLDecoder;
import java.net.URLEncoder;

public class StringUtils {
	public static final String UTF_8 = "UTF-8";
	
	public static boolean isEmpty(String s){
		return (s == null || s.length() == 0);
	}
	
	public static boolean isNotEmpty(String s){
		return (s != null && s.length() > 0);
	}
	
    /**
     * Return the given object's string value.
     * All leading and trailing white spaces will be trimmed.
     * If given object is null, an empty string (length 0) is returned.
     * 
     * @param o
     * @return
     */
    public static String trim(Object o)
    {
    	return o==null ? "" : o.toString().trim();
    }	

	/**
     * Encode a string into <code>application/x-www-form-urlencoded</code>
     * format using UTF-8 character encoding scheme.
     * Return empty string if input is null.
	 */
	public static String urlEncode(String s)
		throws Exception
	{
		if (s==null) return "";
		return URLEncoder.encode(s, UTF_8);
	}

	/**
	 * Decode a <code>application/x-www-form-urlencoded</code> string using UTF-8 character encoding scheme.
	 * Return empty string if input is null.
	 */
	public static String urlDecode(String s)
			throws Exception
	{
		if (s==null) return "";
		return URLDecoder.decode(s, UTF_8);
	}    
}
