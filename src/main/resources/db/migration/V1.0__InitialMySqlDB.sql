CREATE TABLE `NETWORK` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  `description` varchar(255) DEFAULT NULL,
  `NON_DEPLOYABLE` boolean
);

CREATE TABLE `PERMISSION` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `area` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
);

-- User Accounts.
CREATE TABLE `USER_ACCOUNT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `LDAP_USER` BOOLEAN DEFAULT FALSE,
  `accountNonLocked` bit(1) NOT NULL,
  `accountNotExpired` bit(1) NOT NULL,
  `created` datetime NOT NULL,
  `credentialsNonExpired` bit(1) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `network_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  FOREIGN KEY (`network_id`) REFERENCES `NETWORK` (`id`)
);


CREATE TABLE `USER_GROUP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `network_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  `locked` BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (`network_id`) REFERENCES `NETWORK` (`id`)
);

CREATE TABLE `USER_GROUP_ACCESS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `effectiveFrom` datetime DEFAULT NULL,
  `effectiveTo` datetime DEFAULT NULL,
  `timeLimitedAccess` bit(1) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `usergroup_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`usergroup_id`) REFERENCES `USER_GROUP` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `USER_ACCOUNT` (`id`)
);

CREATE TABLE `USER_GROUP_PERMISSION` (
  `usergroup_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  PRIMARY KEY (`usergroup_id`,`permission_id`),
  FOREIGN KEY (`usergroup_id`) REFERENCES `USER_GROUP` (`id`),
  FOREIGN KEY (`permission_id`) REFERENCES `PERMISSION` (`id`)
);






