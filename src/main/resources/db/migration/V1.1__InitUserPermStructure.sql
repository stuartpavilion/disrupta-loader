-- Insert system defined groups.
INSERT INTO USER_GROUP (name) VALUES ('Administrator');
INSERT INTO USER_GROUP (name) VALUES ('Consumer');
INSERT INTO USER_GROUP (name) VALUES ('Network Manager');

--V1.1
-- Define default dev network
INSERT INTO NETWORK (id, name, description) values (1,'dev','Development Network');

-- Define admin user : username = admin, password= password
INSERT INTO USER_ACCOUNT
(id,accountNonLocked,accountNotExpired,created,credentialsNonExpired,email,enabled,firstname,lastname,password,username,network_id)
VALUES
(1,true,true,'2012-01-01 00:00:00',true,'dev-admin@somewhere.com',true, 'dev-admin', 'dev-admin','102382b79c4eeb26efb917fd38e4ddac72e9b2a8','admin',1);

-- update network_id for User Group
UPDATE USER_GROUP SET network_id =1 where id <= 7;
-- update network description to be name + ' Group' for each relevant row
UPDATE USER_GROUP SET description = concat(name,' Group') where id <= 7;

-- Define basic hosts permission
INSERT INTO PERMISSION (id,name,area,description) values (1, 'PERM_ALL', 'Administration', 'Super User permissions');

-- Add this permission to Administrator  user_group_id#1
INSERT INTO USER_GROUP_PERMISSION (usergroup_id, permission_id) values (1,1);

--Add Admin user (user_id=1) permanantly to Administrator Group (usergroup_id=1);
INSERT INTO USER_GROUP_ACCESS (id, timeLimitedAccess, user_id, usergroup_id) values (1,false,1,1);

--V1.2
-- Define Design Engineer user : username = deseng, password= password
--INSERT INTO USER_ACCOUNT
--(id,accountNonLocked,accountNotExpired,created,credentialsNonExpired,email,enabled,firstname,lastname,password,username,network_id)
--VALUES
--(2,true,true,'2012-01-01 00:00:00',true,'designeng-dev-admin@railcorp.nsw.gov.au',true, 'design-engineer-dev', 'design-engineer-dev','4c19029f6356e852dbbbeb609629c69e4c391d3c','deseng',1);

-- Define basic hosts permission
INSERT INTO PERMISSION (id,name,area,description) values (2, 'PERM_ADVERT_ADMIN', 'Adverts', 'Advert Administration permission');

-- Add this permission to Design Engineer Group  user_group_id#3
INSERT INTO USER_GROUP_PERMISSION (usergroup_id, permission_id) values (3,2);

--Add Design Engineer user (user_id=2) permanantly to Design Engineer Group (usergroup_id=3);
--INSERT INTO USER_GROUP_ACCESS (id, timeLimitedAccess, user_id, usergroup_id) values (2,false,2,3);

--V1.3
-- Define user admin permission
INSERT INTO PERMISSION (id,name,area,description) values (3, 'PERM_USER_ADMIN', 'Administration', 'User Administration permission');
INSERT INTO PERMISSION (id,name,area,description) values (4, 'PERM_GROUP_ADMIN', 'Administration', 'Group Administration permission');

-- Add these permissions to Administartor Group  user_group_id#1
INSERT INTO USER_GROUP_PERMISSION (usergroup_id, permission_id) values (1,3);
INSERT INTO USER_GROUP_PERMISSION (usergroup_id, permission_id) values (1,4);

--V1.5
-- Define Network admin permission
INSERT INTO PERMISSION (id,name,area,description) values (5, 'PERM_NETWORK_ADMIN', 'Administration', 'Network Administration permission');

-- Add this permission to Administartor Group  user_group_id#1
INSERT INTO USER_GROUP_PERMISSION (usergroup_id, permission_id) values (1,5);

--V1.6
-- Define Network admin permission
INSERT INTO PERMISSION (id,name,area,description) values (6, 'PERM_ALL_EXCEPT_NETWORK_ADMIN', 'Administration', 'All Except Network Administration permission');

--V11.0
-- Add permission to admin locked groups
INSERT INTO PERMISSION (id,name,area,description) values (7, 'PERM_GROUP_LOCKED_ADMIN', 'Administration', 'Locked Group Admin');

--V11.1
-- Define ALL EXCEPT permissions. This will add all Permissions except those specified.
INSERT INTO PERMISSION (id,name,area,description) values (8, 'PERM_ALL_EXCEPT', 'Administration', 'All Except Specified Permission');


