<%-- 
    Document   : baseLayout.jsp
    Created on : 18/01/2010, 6:15:55 PM
    Author     : Stuart Sontag
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@ page language="java"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><tiles:getAsString name="title" /></title>
<s:url var="url_css" value="/css"/>
<s:url var="url_scripts" value="/scripts"/>
<link rel='stylesheet' type='text/css' href='${url_css}/style.css' />
<link rel='stylesheet' type='text/css' href='${url_css}/css-button.css' />
<link rel="stylesheet" type="text/css" href="${url_css}/start/jquery-ui-1.8.18.custom.css">
<link rel="stylesheet" type="text/css" href="${url_css}/superfish.css" media="screen">
<link rel="stylesheet" media="screen" href="${url_css}/superfish-vertical.css" />
<link rel="stylesheet" media="screen" href="${url_css}/defaultTheme.css" />
<script type="text/javascript" src="${url_scripts}/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="${url_scripts}/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript" src="${url_scripts}/hoverIntent.js"></script>
<script type="text/javascript" src="${url_scripts}/superfish.js"></script>
<script type="text/javascript" src="${url_scripts}/jquery.jstree.js" ></script>
<link href="${url_css}/jquery.treeview.css" rel="stylesheet">
<!--script type="text/javascript" src="/statusview/scripts/jquery.treeview.js"-->
<script type="text/javascript">
	$(function(){
		jQuery('ul.sf-menu').superfish();
	});
</script>

</head>
<body>

<div id="header-wrap">
	<div id="header-area"><tiles:insertAttribute name="header" /></div>
</div>
<div id="content-area">
	<!--div id="leftnav"><tiles:insertAttribute name="navigation" /></div-->
	<div id="main-area"><tiles:insertAttribute name="body" /></div>
	<div id="footer-area"><tiles:insertAttribute name="footer" /></div>
</div> <%-- content-area --%>

</body>

</html>
