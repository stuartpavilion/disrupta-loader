<%-- 
    Document   : logon
    Created on : 25/01/2010, 6:22:33 PM
    Author     : Stuart Sontag
--%>
<%@ page contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script type="text/javascript" src="/statusview/scripts/jquery.busy.min.js"></script>
	<sec:authorize access="hasAnyRole('ROLE_ADMINISTRATOR')">
		<spring:url var="actionUrl" value="/admin/user/batchimport.htm" />
		<spring:url var="batchDeleteUrl" value="/admin/user/batchdelete.htm" />
	</sec:authorize>
	<spring:url var="busyImgUrl" value="/images/busy.gif" />
	<sec:authorize access="hasAnyRole('ROLE_ADMINISTRATOR')">
    <form:form method="POST" action="${actionUrl}" modelAttribute="batchUploadForm">
               <label for="filename">Upload file name located in $TOMCAT_HOME/statusview/upload directory</label>
               <input id="filename" type="text" name="filename" />
               <p>
          	   <label for="columnSeparator">Column Seperator</label>
          	   <input id="columnSeparator" name="columnSeparator" type="text" value=";"/>
          	   </p>          
          	 <fieldset><legend>Type of Row Data</legend>
          	   <label for="typeCol">Type Column Number (Starting from 1)</label>
          	   <input id="typeCol" name="typeCol" type="number" value="1"/>
          	   <label for="typeRegex">Type Column Selected Value Regex</label>
          	   <input id="typeRegex" name="typeRegex" type="text" value="CUSTOMER" />
            </fieldset>
            <fieldset><legend>Boris Code</legend>
			   <label for="borisCol">boris code Column Number (Starting from 1)</label>
          	   <input id="borisCol" name="borisCol" type="number" value="2"/>            
            	<label for="borisFilterModeStr">Boris filtering</label>
            	<select id="borisFilterModeStr" name="borisFilterModeStr" >
            		<option value="Unchanged" >unchanged</option>
            		<option value="ToLowercase">To Lowercase</option>
            		<option value="ToUppercase" selected="selected">To Uppercase</option>
            	</select>
            </fieldset>
          	<fieldset><legend>Username</legend>
 				<label for="usernameCol">Username Column Number (Starting from 1)</label>
          	   <input id="usernameCol" name="usernameCol" type="number" value="2"/>
               <p>
          	   <label for="usernamePrefix">Username Prefix</label>
          	   <input id="usernamePrefix" name="usernamePrefix" type="text" value=""/>          	   
         	   <label for="usernameFilterModeStr">Username filtering</label>
               <select id="usernameFilterModeStr" name="usernameFilterModeStr" >
            		<option value="Unchanged" >unchanged</option>
            		<option value="ToLowercase" selected="selected">To Lowercase</option>
            		<option value="ToUppercase">To Uppercase</option>
               </select>
          	   <label for="usernameSuffix">Username Suffix</label>
          	   <input id="usernameSuffix" name="usernameSuffix" type="text" value="VPNreport"/>
          	   </p>          
            </fieldset>
            <fieldset><legend>Password</legend>
          	   <label for="passwordCol">Password Column Number (Starting from 1. Setting to 0 assigns all passwords to constant = prefix + suffix)</label>
          	   <input id="passwordCol" name="passwordCol" type="number" value="2"/>
        	   
           	   <p>
          	   <label for="passwordPrefix">password prefix</label>
          	   <input id="passwordPrefix" name="passwordPrefix" type="text" value=""/>
          	   <label for="passwordFilterModeStr">Password filtering</label>
               <select id="passwordFilterModeStr" name="passwordFilterModeStr" >
            		<option value="Unchanged" >unchanged</option>
            		<option value="ToLowercase" selected="selected">To Lowercase</option>
            		<option value="ToUppercase">To Uppercase</option>
               </select>          	   
          	   <label for="passwordSuffix">password suffix</label>
          	   <input id="passwordSuffix" name="passwordSuffix" type="text" value="VPNreport"/>
          	   </p>             	   
          	</fieldset>
          
          	<c:if test="${errorMsg != null}">
				<div class="errormsg"> ${errorMsg}</div>
			</c:if>
            <p><input id="uploadBtn" type="submit" value="Upload" />
             <c:if test="${not empty batchUploadedUsers}" >
             	<input id="deleteAllUploadedBtn" type="button" value="Delete Batch Uploaded Users" />
             </c:if>
            </p>
    </form:form>
    
    <script type="text/javascript">
   // $(function(){
   // 	jQuery().busy("preload", {img : "${busyImgUrl}" });
   // })
    $("#uploadBtn").click(function(){
    	$(this).busy({img : "${busyImgUrl}" });
    	return true;
    })
    <c:if test="${not empty batchUploadedUsers}" >
	     $("#deleteAllUploadedBtn").click(function(){
	      	//var ans = confirm("Are you sure that you want delete User: " + uname);
	      	//return (ans);
	      	$('<div>').dialog({
	      		open: function(){$(this).text("Are you sure that you want delete the entire batch of recently uploaded Users?");},
	      		modal: true,
	      		title: 'Confirm delete All Batch Uploaded Users',
	      		buttons: {
	      			Yes: function(){ 
	      				$("#deleteAllUploadedBtn").busy({img : "${busyImgUrl}" });
	      				   open("${batchDeleteUrl}");
	      					$(this).dialog('close');
	      				 },
	      			No:  function(){
	      					$(this).dialog('close');	        				
	      				}
	      		}
	      	})
	      	return false;
	       });
     </c:if>
    </script>
    </sec:authorize>
    

