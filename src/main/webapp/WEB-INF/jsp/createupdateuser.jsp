<%-- 
    Document   : logon
    Created on : 25/01/2010, 6:22:33 PM
    Author     : Stuart Sontag
--%>
<%@ page contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<sec:authorize access="not(hasRole('PERM_USER_ADMIN'))">
		<spring:url var="updateUserUrl" value="/user/update.htm" />
	</sec:authorize>
	<sec:authorize access="hasAnyRole('PERM_USER_ADMIN')">
		<spring:url var="createUserUrl" value="/admin/user/create.htm" />
		<spring:url var="updateUserUrl" value="/admin/user/update.htm" />
		<spring:url var="addUserGroupAccessUrl" value="/admin/user/addUserGroupAccess.htm" />
		<spring:url var="deleteUserAccessGroupUrl" value="/admin/user/deleteUserGroupAccess.htm" />
	</sec:authorize>
	<c:choose>
		<c:when test="${myaccountMode == true}">
			<spring:url var="backToViewUsersUrl" value="/index.htm"/>
		</c:when>
		<c:otherwise>
			<spring:url var="backToViewUsersUrl" value="/admin/user/viewusers.htm"/>
		</c:otherwise>	
	</c:choose>
    <form:form method="POST" modelAttribute="userForm">
      <table class="forminputtbl">
          <tr>
              <td>Username:</td><td><form:input path="user.username" /></td>
          </tr>
          <tr>
              <td>First Name:</td><td><form:input path="user.firstName" /></td>
          </tr>
          <tr>
              <td>Last Name:</td><td><form:input path="user.lastName" /></td>
          </tr>
                              <tr>
              <td>Email:</td><td><form:input path="user.email" size="80"/></td>
          </tr> 
          
          <sec:authorize access="hasAnyRole('PERM_USER_ADMIN')">
	       	  <tr>
	       	  		<td><label for="ldapUserElem">Ldap User</label><form:checkbox id="ldapUserElem" path="user.ldapUser" value="Ldap User"/></td>
	       	  </tr>
          </sec:authorize>
          
		  <c:if test="${userForm.editMode == false}">          	
          	  <input id="changePasswordElem" type="hidden" name="changePassword" value="true" />
		  </c:if> 
		  <!--          	  
	          <tr>
	              <td>Password:</td><td><form:input type="password" path="newPassword" /></td>
	          </tr>
	          <tr>
	              <td>Re Enter Password:</td><td><form:input type="password" path="confirmPassword" /></td>
	          </tr>
           -->
           <c:if test="${userForm.editMode == true}">
           	   <input id="changePasswordElem" type="hidden" name="changePassword" value="false" />
			</c:if>	           	   
           	   <tr id="newPasswordRow"></tr>
           	   <tr id="confirmPasswordRow"></tr>
           
           <sec:authorize access="hasAnyRole('PERM_USER_ADMIN')">
           <tr><td  colspan="2">
           <c:if test="${userForm.user.id != null}">
		  <fieldset style="padding-bottom: 30px;">
		   <legend>User Group Access Rights</legend>
         		<input id="fromDate" type="hidden" name="fromdate" />
		  		<input id="toDate" type="hidden" name="todate" />
		   <table class="forminnerinputtbl" >	
		   <tr>
          	   <td style="width: 300px;"><label for="availGps">Available Groups: </label><form:select id="availGps" size="15" path="selectedUserGroupId" multiple="false" items="${userForm.availUserGroups}" itemLabel="name" itemValue="id" style="width: 250px;" />
          	   </td>
          	   <td style="width: 140px;"><div style="height: 40px"><div id="timeLimitedAccessDiv"><label for="timeLimitedAccess">Time Limited</label><form:checkbox id="timeLimitedAccess" path="timeLimitedAccess" value="Time Limited"/></div></div>
          	   <div style="height: 100px"><div id="dateSelectDiv">
	          	   <label for ="viewFromDate" >From Date: </label><br /><input id ='viewFromDate' type="text" size='9' />	<br /><br />
	          	   <label for="viewToDate" >To Date: </label><br /><input id ='viewToDate' type="text" size='9' /></div></div><br />
	          	   <input id="addUserGroupAccessButton" type="button" value="Add -->" class="innerFormButton button blue" /></br></br>
	    		   <input id="deleteUserGroupAccessButton" type="button" value="<-- Remove" class="innerFormButton button blue" />
    			</td>
    			
          	   <td style="width: 600px; vertical-align: top; horizontal-align: left;" >
          	   <label for="userGroupAccessSelect">Current User Group Access Rights</label>
          	   <select id="userGroupAccessSelect" size="15" style="width: 100%;" name="selectedUserGroupAccessId"/>
		         	 	<c:forEach items="${userForm.currentUGAs}" var="uga">
								<option value="${uga.id}">${uga.userGroup.name}  : 
								 <c:if test="${uga.timeLimitedAccess == false}">NOT TIME LIMITED</c:if>
								 <c:if test="${uga.timeLimitedAccess == true}"> TIME LIMITED  
								 from <fmt:formatDate pattern="dd/MM/yy" value="${uga.effectiveFrom}"/>
								  to <fmt:formatDate pattern="dd/MM/yy" value="${uga.effectiveTo}"/>
								 </c:if>
								</option>
					 	</c:forEach>
			 		</select>
			   </td>
		 </tr>
		 </table>
		 </fieldset>
		 </c:if>
		 </td></tr>
		 </sec:authorize>         
          <tr>
              <td colspan="4">
              	<c:if test="${userForm.editMode == false}">
                  <input id="createUpdateUserButton" type="button" value="Create User" class="button blue"/>
                 </c:if>
              	<c:if test="${userForm.editMode == true}">
                  <input id="createUpdateUserButton" type="button" value="Save User" class="button blue" />
                 </c:if>
                 <c:if test="${userForm.editMode == true}">
                   <input id="changePasswordButton" type="button" value="Toggle Change Password Fields" style="position: relative; left: 100px;" class="button blue" />
                 </c:if>
                   <input id="backButton" type="button" value="Back" class="button blue" style="position: relative; left: 390px;" />                 
              </td>
          </tr>
          <tr><td colspan="2">
			<c:if test="${!empty errorMsg}">
				<div class="error-msg-box">
			<c:out value="${errorMsg}"/>
		</div>
	</c:if>
          </td></tr>
      </table>
    </form:form>
    
    
    <script type="text/javascript">	
    	var setDateDivsVisibility = function (){
    		if ($(this).is(':checked')){
    			$('#dateSelectDiv').show();
    		} else {
    			$('#dateSelectDiv').hide();
    		}
    	}
    	$('#timeLimitedAccess').click(setDateDivsVisibility);
		$('#addUserGroupAccessButton').click(function() {
			$('#userForm').attr("action","${addUserGroupAccessUrl}?scrolltop="+$(window).scrollTop());
			$('#userForm').submit();
		});
		
		$('#deleteUserGroupAccessButton').click(function() {
			$('#userForm').attr("action","${deleteUserAccessGroupUrl}?scrolltop="+$(window).scrollTop());
			$('#userForm').submit();
		});
		
		$('#createUpdateUserButton').click(function() {
          	<c:if test="${userForm.editMode == false}">
				$('#userForm').attr("action","${createUserUrl}");
			</c:if>
			<c:if test="${userForm.editMode == true}">
			$('#userForm').attr("action","${updateUserUrl}");
			</c:if>
			$('#userForm').submit();
		});
		
		function showHidePasswordElements(show){
			if (show){
				$('#newPasswordRow').html("<td>Password:</td><td><input type='password' name='newPassword' value='' /></td>");
				$('#confirmPasswordRow').html("<td>Re Enter Password:</td><td><input type='password' name='confirmPassword' value='' /></td>");
				$("#changePasswordElem").val("true");
			} else {
				$('#newPasswordRow').html("");
				$('#confirmPasswordRow').html("");
				$("#changePasswordElem").val("false");
			}
		}
		
		function togglePasswordElements(test){
			showHidePasswordElements($("#changePasswordElem").val() == test);
		};
		
		$('#changePasswordButton').click(function(){togglePasswordElements('false')});
		
		$('#ldapUserElem').change(function(){
			showHidePasswordElements(!this.checked);
			<c:if test="${userForm.editMode == true}">
			if (this.checked){
				$('#changePasswordButton').hide();
			} else {
				$('#changePasswordButton').show();
			}
			</c:if>
		});
		
		function showAndInitialiseTimeLimitedAccessDiv(){
			$('#timeLimitedAccessDiv').show();
			$('#timeLimitedAccess').attr('checked', false);
			$('#fromDate').val("");
			$('#toDate').val("");
			$('#viewFromDate').val("");
			$('#viewToDate').val("");			
			//$('#timeLimitedAccess').setDateDivsVisibility;
			$('#dateSelectDiv').hide();
		}
		
		$('#availGps').change(function(){
			//$('#timeLimitedAccessDiv').show();
			showAndInitialiseTimeLimitedAccessDiv();
		})
    		
		
		$('#backButton').click(function() {
			open("${backToViewUsersUrl}");
		});	
		
		
		//Initialisations
		$(function(){
			<c:if test="${!empty scrollTop}">
				$(window).scrollTop(${scrollTop})
			</c:if>
				
			setDateDivsVisibility();
			$('#timeLimitedAccessDiv').hide();
			
			var availGpsDropdown = document.getElementById('availGps');
			//alert(availGpsDropdown.selectedIndex);
			if (availGpsDropdown!=null && availGpsDropdown.selectedIndex > -1){
				showAndInitialiseTimeLimitedAccessDiv();
			}			
			
			$('#viewFromDate').datepicker({
				dateFormat: "dd/mm/yy",
				altField: "#fromDate",
				altFormat: "yymmdd"
			});		
			$('#viewToDate').datepicker({
				dateFormat: "dd/mm/yy",
				altField: "#toDate",
				altFormat: "yymmdd"
			});	
			
			//togglePasswordElements('true');
			<c:if test="${userForm.editMode == true}">
				togglePasswordElements('true');
				<c:if test="${userForm.user.ldapUser == true}">
			    	$('#changePasswordButton').hide();
				</c:if>
				<c:if test="${userForm.user.ldapUser == false}">
			    	$('#changePasswordButton').show();
				</c:if>				
			</c:if>	
			
			<c:if test="${userForm.editMode == false}">
				<c:if test="${userForm.user.ldapUser == true}">
					showHidePasswordElements(false);
				</c:if>
				<c:if test="${userForm.user.ldapUser == false}">
					showHidePasswordElements(true);
				</c:if>				
			</c:if>	
		});	
    </script>

