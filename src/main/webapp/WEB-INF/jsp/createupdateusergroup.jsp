<%-- 
    Document   : logon
    Created on : 25/01/2010, 6:22:33 PM
    Author     : Stuart Sontag
--%>
<%@ page contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

	<sec:authorize access="hasAnyRole('ROLE_CRM')">
		<spring:url var="updateUserGroupUrl" value="/group/update.htm" />
	</sec:authorize>
	<sec:authorize access="hasAnyRole('PERM_GROUP_ADMIN')">
		<spring:url var="createUserGroupUrl" value="/admin/group/create.htm" />
		<spring:url var="updateUserGroupUrl" value="/admin/group/update.htm" />
		<spring:url var="importPermissionsUrl" value="/admin/group/importpermissions.htm" />
		<spring:url var="getUserGpPermissionsUrl" value="/admin/group/getperms" />
		<spring:url var="addSelectedAvailPermissionUrl" value="/admin/group/addselectedavailpermissions.htm" />
		<spring:url var="deleteSelectedPermissionsUrl" value="/admin/group/deleteselectedpermissions.htm" />
		<spring:url var="backToUrl" value="/admin/group/viewusergroups.htm"/>
	</sec:authorize>
    <form:form method="POST" modelAttribute="userGroupForm">
      <table class="forminputtbl" >
       	  <tr>
       	  		<td>Locked:</td><td><form:checkbox id="lockedElem" path="locked" value="locked"/></td>
       	  </tr>      
          <tr>
              <td>Group Name:</td><td><form:input path="userGroup.name" size="80" /></td>
          </tr>
          <tr>
              <td>Description:</td><td><form:input path="userGroup.description" size="80" /></td>
          </tr>
          <tr>
              <td>Network:</td><td><form:input path="userGroup.network.name" size="80" disabled='true' /></td>
          </tr>
          <sec:authorize access="hasAnyRole('PERM_GROUP_ADMIN')">
          <tr><td  colspan="2">
          <legend>User Group Permissions</legend>
          <table class="forminnerinputtbl" >
		   <tr>
          	   <td style="width: 300px;"><label for="importGroupsSelect">Groups to import: </label>
          	   <select id="importGroupsSelect" size="15" style="width: 100%;" name="selectedImportGroupId" value="${userGroupForm.selectedImportGroupId}"/>
          	   			<option value="-1">-- Select Group to Import its Permissions --</option>
		         	 	<c:forEach items="${userGroupForm.importGroups}" var="group">
								<option value="${group.id}">${group.name} 
								 <c:if test="${group.locked == true}">: LOCKED</c:if>
								</option>
					 	</c:forEach>
			 	</select>          	   
          	   </td>
          	   <td></td>
          	    <td style="width: 300px;"><div id="userGroupPermsDiv" style="width: 280px;"/></td>
          </tr>
          <tr style="height: 30px;"><td colspan="2" style="height: 30px;"></td>
          	<td style="height: 30px;"><input id="importPermissionsButton" type="button" value="Import &darr;" class="innerFormButton button blue" /></td>
          </tr>
          </table> 
          </td></tr>         
           <tr><td  colspan="2">
           <!-- c:if test="${userGroupForm.userGroup.id != null}"-->
		  <fieldset>
		   <table class="forminnerinputtbl" >	
		   <tr>
          	   <td style="width: 300px;"><label for="availPermissions">Available Permissions: </label>
          	   <form:select id="availPermissions" size="15" style="width: 100%;" path="selectedAvailPermissionIds" multiple="true" items="${userGroupForm.sortedAvailPermissions}" itemLabel="description" itemValue="id" /></td>
          	   
          	   <td><input id="addPermissionsButton" type="button" value="Add &rarr;" class="innerFormButton button blue" /></br></br>
				<input id="deletePermissionsButton" type="button" value="&larr; Remove" class="innerFormButton button blue" /></td>
          	   
          	   <td style="width: 300px;"><label for="currentPermissions">Current Permissions: </label>
          	   <form:select id="currentPermissions" size="15"  style="width: 100%;" path="selectedPermissionIds" multiple="true" items="${userGroupForm.sortedCurrentPermissions}" itemLabel="description" itemValue="id" /></td>
          	   
          </tr>          
		 </table>
		 </fieldset>
		 <!--/c:if-->
		 </td></tr>
		 </sec:authorize>         
          <tr>
              <td colspan="2">
              	<c:if test="${userGroupForm.editMode == false}">
                  <input id="createUpdateUserGroupButton" type="button" value="Create User Group" class="button blue" />
                 </c:if>
              	<c:if test="${userGroupForm.editMode == true}">
                  <input id="createUpdateUserGroupButton" type="button" value="Save User Group" class="button blue" />
                 </c:if>
                 <input id="backButton" type="button" value="Back" class="button blue" style="position: relative; left: 500px;" />  
              </td>
          </tr>
          <tr><td colspan="2">
          	<c:if test="${errorMsg != null}">
				<div class="errormsg"> ${errorMsg}</div>
			</c:if>
          </td></tr>
      </table>
    </form:form>
    
    
    <script type="text/javascript">	
		$('#addPermissionsButton').click(function() {
			$('#userGroupForm').attr("action","${addSelectedAvailPermissionUrl}?scrolltop="+$(window).scrollTop());
			$('#userGroupForm').submit();
		});
		
		
		$('#importPermissionsButton').click(function() {
			if($('#importGroupsSelect').val()>-1){
				$('#userGroupForm').attr("action","${importPermissionsUrl}?scrolltop="+$(window).scrollTop());
				$('#userGroupForm').submit();
			}
		});
		
		function updateUserGroupsPerms(){
			var queryS = "${getUserGpPermissionsUrl}?gpid=" + $('#importGroupsSelect').val();
			$('#userGroupPermsDiv').load(queryS);
		}
		
		$('#importGroupsSelect').change(function(){
			updateUserGroupsPerms();
		});		
    
		$('#deletePermissionsButton').click(function() {
			$('#userGroupForm').attr("action","${deleteSelectedPermissionsUrl}?scrolltop="+$(window).scrollTop());
			$('#userGroupForm').submit();
		});
		
		$('#createUpdateUserGroupButton').click(function() {
          	<c:if test="${userGroupForm.editMode == false}">
				$('#userGroupForm').attr("action","${createUserGroupUrl}");
			</c:if>
			<c:if test="${userGroupForm.editMode == true}">
				$('#userGroupForm').attr("action","${updateUserGroupUrl}");
			</c:if>
			//enable form before submission
			enableDisableFormElements(true);
			$('#userGroupForm').submit();
		});
		
		$('#backButton').click(function() {
			open("${backToUrl}");
		});	
		
		function enableDisableFormElements(enable){
			if (enable){
				$("input").removeAttr("disabled");
				$("select").removeAttr("disabled");			
			} else {
				$("input").attr("disabled","disabled");
				$("select").attr("disabled","disabled");				
			}
			//Set locked Checkbox depending on permission no enable parameter
			<sec:authorize access="hasAnyRole('PERM_GROUP_LOCKED_ADMIN')">
				$("#lockedElem").removeAttr("disabled");
				$("#createUpdateUserGroupButton").removeAttr("disabled");
			</sec:authorize>
			<sec:authorize access="!hasAnyRole('PERM_GROUP_LOCKED_ADMIN')">
				$("#lockedElem").attr("disabled","disabled");
			</sec:authorize>
			$("#backButton").removeAttr("disabled");
		}
		
		$("#lockedElem").change(function(){
			enableDisableFormElements(!this.checked);
		})
		$(function(){
			<c:if test="${userGroupForm.locked == true}" >
				enableDisableFormElements(false);
			</c:if>
			<c:if test="${userGroupForm.locked == false}" >
				enableDisableFormElements(true);
			</c:if>	
			
			$('#importGroupsSelect').val("${userGroupForm.selectedImportGroupId}");
			updateUserGroupsPerms();
			
			<c:if test="${!empty scrollTop}">
				$(window).scrollTop(${scrollTop})
			</c:if>
		});
    </script>

