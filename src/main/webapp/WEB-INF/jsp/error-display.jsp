<%@page import="com.i_mmerce.global.StringUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
	final String CONTEXT_PATH = request.getContextPath();

	String returnUrl = StringUtils.trim(request.getAttribute("returnUrl"));
%>

<br/><br/>

<div class="error-msg-box">
	<c:out value="${errorMsg}"/>
</div>

<br/>

<% if (returnUrl.length()>0) { %>
	<a href="<%=returnUrl%>" class="button blue medium">Ok</a>
<% } // if %>