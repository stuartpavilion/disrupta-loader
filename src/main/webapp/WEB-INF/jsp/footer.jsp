<%-- 
    Document   : footer
    Created on : 22/01/2010, 6:06:42 PM
    Author     : Stuart Sontag
--%>

<%@page contentType="text/html" %>

<div style="padding-top: 50px; clear: both;">
<hr/>
<span>Copyright &copy; <%= new java.util.Date().getYear() + 1900 %> I-mmerce Pty Ltd</span>
</div>
<div id='console' />