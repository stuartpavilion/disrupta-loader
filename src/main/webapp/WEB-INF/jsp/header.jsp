<%-- 
    Document   : header
    Created on : 22/01/2010, 6:37:57 PM
    Author     : Stuart Sontag
--%>

<%@page contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<s:url var="imgUrl" value="/images/i-mmercelogotrans.gif" />
<div style='text-align: left'>
<a href="index.htm"> <img id="logo" src="${imgUrl}" width="150" border="0"></a>
</div>
<div style='text-align: right'>
<ul id="nav-one" class="sf-menu">
	<sec:authorize access="hasAnyRole('PERM_USER_ADMIN', 'PERM_GROUP_ADMIN', 'PERM_NETWORK_ADMIN')">
	<li><a href="#">Admin</a>
		<ul>
			<sec:authorize access="hasAnyRole('PERM_USER_ADMIN')">
			<li><a>Users</a>
				<ul>
					<li><a href='<s:url value="/admin/user/createupdate.htm"/>'>Create</a></li>
					<li><a href='<s:url value="/admin/user/viewusers.htm"/>'>View/Edit</a></li>
				</ul>
			</li>
			</sec:authorize>
			<sec:authorize access="hasAnyRole('PERM_GROUP_ADMIN')">
			<li><a>Groups</a>
				<ul>
					<li><a href='<s:url value="/admin/group/createupdate.htm"/>'>Create</a></li>
					<li><a href='<s:url value="/admin/group/viewusergroups.htm"/>'>View/Edit</a></li>
				</ul>
			</li>
			</sec:authorize>	
			<sec:authorize access="hasAnyRole('PERM_NETWORK_ADMIN')">
			<li><a>Networks</a>
				<ul>
					<li><a href='<s:url value="/admin/network/createupdate.htm"/>'>Create</a></li>
					<li><a href='<s:url value="/admin/network/viewnetworks.htm"/>'>View/Edit</a></li>
				</ul>
			</li>
			</sec:authorize>
		</ul>
	</li>
	</sec:authorize>
	
	<li><a href="">CodeExpert</a>
		<ul>
			<li><a href='<s:url value="/codexp/search"/>'>Search</a></p></li>
		</ul>
	</li>
	
	<sec:authorize access="hasAnyRole('ROLE_CRM','ROLE_ADMINISTRATOR','ROLE_CONSUMER','ROLE_REPUTATION_HOLDER')">
	<li><a href="">My Account</a>
		<ul>
			<li><a href="/disruptalookup/user/current.htm">Edit</a></p></li>
		</ul>
	</li>
	</sec:authorize>
	<sec:authorize access="not (isAnonymous())">	
	<li><a href="/disruptalookup/logout.htm">Log Out</a>
	</li>
	</sec:authorize>
</ul>
</div>
<div id="userInfo" >
	<c:if test="${currentUser != null}">
		User: ${currentUser.username} <%--| Name: ${currentUser.firstName}  ${currentUser.lastName} | Network: ${currentUser.network.name} --%> 
	</c:if>
</div>

