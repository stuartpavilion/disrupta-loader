<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <s:url var="backImgUrl" value="/images/disrupgemstones.jpg" />
<%--
  <h2 id="logonHeading">Disrupta Lookup Logon</h2>
  <div id="logonBlock">
    <form class="loginform" method="post" action="/disruptalookup/j_spring_security_check">
          <a>Username:</a>
          <input type="text" name="j_username" value="" size="20" maxlength="20" /> 
          <br/><a>Password:&nbsp;</a>
          <input type="password" name="j_password" value="" size="20" maxlength="20" /><input type="submit" value="Log On" />
    </form>
	<c:if test="${errorMsg != null}">
		<div class="errormsg"> ${errorMsg}</div>
	</c:if>
 </div>
 
  --%>
 <br>
	<c:if test="${!empty errorMsg}">
		<div class="error-msg-box">
			<c:out value="${errorMsg}"/>
		</div>
	</c:if>
<br/>

<%
	final String CONTEXT_PATH = request.getContextPath();
%>
<form id="signin-form" method="POST" name="signin-form" action="<%=CONTEXT_PATH%>/j_spring_security_check" >
	<div id="login-panel">
		<div class="greeting"><i>Disrupta Lookup</i></div>
		<br/>
		<div class="label"><label for="user">User:</label></div>
		<div class="input"><input id="userId" tabindex="1" name="j_username" /></div>&nbsp;<form:errors path="userId" cssStyle="color: red;"/>
		<br/><br/>
		<div class="label"><label for="password">Password:</label></div>
		<div class="input"><input type="password" id="userPassword" tabindex="2" name="j_password"/></div>&nbsp;<form:errors path="userPassword" cssStyle="color: red;"/>
		<br/>
		<div class="greeting"></div>
		<br/>
		<div style="float: left; padding-left: 135px;">
			<a id="submit_link" href="#" class="button blue medium" onclick="this.blur(); document.forms['signin-form'].submit(); return false;">Sign In</a>
		</div>
		<br/><br/>
	</div>
	<img src="${backImgUrl}" class="centrebanner"/>
</form>	


<script type="text/javascript">
$(function(){
	//submit form if <return> key pressed
	$("#signin-form").bind('keypress', function(e){
		if (e.keyCode==13){
			$("#signin-form").submit();
		};
	});
	
	//$("#content-area").css({'background-image':"url('${backImgUrl}')",
	//	'background-repeat': 'no-repeat'});
});

</script>