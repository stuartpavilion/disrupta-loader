<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<spring:url var="searchQueryUrl" value="/codexp/search" />

<form action="${searchQueryUrl}" method="POST" >
	<div id="host-form">

	<div class="section">Search</div>
	<div class="input-box" style="width: 250px;">
		<div class="label"><label for="query-input" >Query</label></div>
		<div class="input">
		<input id="query-input" name="query" tabindex="2" cssStyle="width: 220px;" value="${query}" />
		</div>&nbsp;<form:errors path="query-input" cssStyle="color: red;"/>
	</div>

	<div class="input-box" style="width: 250px;">
		<div class="label"><label for="search-type-input" >Search Type</label></div>
		<div class="input">
		<select id="search-type-input" name="searchType" tabindex="2" cssStyle="width: 220px;">
			<option value="0">All</option>
			<option value="1">Lead Terms</option>
			<option value="2">Index (Diseases)</option>
			<option value="3">Tabular (Diseases)</option>
			<option value="4">Index (Interventions)</option>
			<option value="5">Tabular (Interventions)</option>
			<option value="6">ACS</option>
			<option value="7">Advice</option>
			<option value="8">References</option>		
		</select>
		</div>&nbsp;<form:errors path="topic.regionCode" cssStyle="color: red;"/>
	</div>
	<div class="input-box" style="width: 250px;">
		<div class="label"><label for="max-depth-input" >Max Child Depth</label></div>
		<div class="input">
		<select id="max-depth-input" name="maxdepth" tabindex="2" cssStyle="width: 220px;">
			<option value="-1">All</option>
			<% for (int i=0; i<25; i++){ %>
			<option value="<%= i%>"><%= i%></option>
			<% } %> %>		
		</select>
		</div>&nbsp;<form:errors path="topic.regionCode" cssStyle="color: red;"/>
	</div>	
	<div class="clear-div"/>
	<input id="searchButton" type="submit" value="Search" class="button blue" />
	
	<div class="section">Search Results</div>
	
	<div class="input-box" style="width: 500px;">
		<div class="label"><label for="search-results" >Search Results</label></div>
		<div class="input"><textarea id="search-results" rows=30 cols=100>${searchResults}</textarea>
	</div>	
	<div class="clear-div"/>
	<!-- div  style="position :relative; top: 50px;"-->
	<div class="input-box" style="width: 500px;">
		<div class="label"><label for="search-results-flat" >Search Results Flattened</label></div>
		<div class="input"><textarea id="search-results-flat" rows=30 cols=100>${searchResultsFlat}</textarea>
	</div>	
	<div class="clear-div"/>	
	<br/>
    <a id="cancelButton" type="button" value="Cancel" class="button blue" >Cancel</a>
    	

    
</form>

<script type="text/javascript">
$(function(){
	<c:if test="${!empty scrollTop}">
		$(window).scrollTop(${scrollTop})
	</c:if>	
	$("#addStreamButton").click(function(){
		$("#console").append("<p>Selected Stream Proposition Support Level: " + $("#streamPropSupportLvlPercentId").val() + "</p>");
		$('#topicForm').attr("action","${addStreamUrl}?scrolltop="+$(window).scrollTop());
		$('#topicForm').submit();
	});
	$("#search-type-input").val(${searchType});
	$("#max-depth-input").val(${maxDepth});
});
</script>