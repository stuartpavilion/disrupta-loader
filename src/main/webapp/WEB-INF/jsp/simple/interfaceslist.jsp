<%-- 
    Document   : navigation
    Created on : 29/01/2010, 5:38:13 PM
    Author     : Stuart Sontag
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" %>

	<c:if test="${ifDetailsList != null}">
		<ul id="uliflist">
			<c:forEach items="${ifDetailsList}" var="interfaceDetails">
				<li>
					<a href="routerid=${interfaceDetails.routerId}&interfaceid=${interfaceDetails.interfaceId}&interfaceip=${interfaceDetails.interfaceIp}">${interfaceDetails.routerId} ${interfaceDetails.interfaceId} ${interfaceDetails.interfaceIp}</a>
				</li>
			</c:forEach>
		</ul>
	</c:if>
	
	<c:if test="${errorMsg != null}">
		<div class="errormsg"> ${errorMsg}</div>
	</c:if>