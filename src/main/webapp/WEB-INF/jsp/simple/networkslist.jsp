<%-- 
    Document   : navigation
    Created on : 29/01/2010, 5:38:13 PM
    Author     : Stuart Sontag
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<%@page contentType="text/html" %>
	<c:if test="${allNetworks != null}">
		<spring:url var="displayTableUrl" value="/admin/network/list.htm" />
		<spring:url var="editUrl" value="/admin/network/edit.htm" />
		<spring:url var="deleteUrl" value="/admin/network/delete.htm" />
		<display:table id="networkTable" name="${allNetworks}" class="userlisttbl" requestURI="${displayTableUrl}" pagesize="10" sort="list">
		  <display:column property="id" title="ID" sortable="true"  paramId="id" href="${editUrl}" class="hidden" headerClass="hidden"  />
		  <display:column property="name" title="Name" sortable="true"  />
  		  <display:column property="description" title="Description" sortable="true" />
		</display:table>
	</c:if>
	
	<c:if test="${errorMsg != null}">
		<div class="errormsg"> ${errorMsg}</div>
	</c:if>