<%-- 
    Document   : navigation
    Created on : 29/01/2010, 5:38:13 PM
    Author     : Stuart Sontag
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" %>

	<c:if test="${xacctDys != null}">
		<table id="usagetbl">
		   <thead>
				<tr>
					<th>Instance</th>
					<th>Product Class</th>
					<th>SX Protection Level(%)</th>
					<th>In Usage (Daily)</th> 
					<th>Out Usage (Daily)</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${xacctDys}" var="xacctDy">
					<tr>
						<td>${xacctDy.routerId} ${xacctDy.interfaceId} ${xacctDy.interfaceIp}</td>
						<td>${xacctDy.prodClass}</td>
						<td>${xacctDy.protecLevel}</td>
						<td><fmt:formatNumber value="${xacctDy.inUsage}" /></td>
						<td><fmt:formatNumber value="${xacctDy.outUsage}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if>
	
	<c:if test="${xacctHrlys != null}">
		<table id="usagetbl">
			<thead>
				<tr>
					<th>Time</th>
					<th>Instance</th>
					<th>Product Class</th>
					<th>SX Protection Level(%)</th>
					<th>In Usage (Hourly)</th> 
					<th>Out Usage (Hourly)</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${xacctHrlys}" var="xacctHrly">
					<tr>
						<td><fmt:formatDate pattern="HH:mm" value="${xacctHrly.periodStartTime}"/></td>
						<td>${xacctHrly.routerId} ${xacctHrly.interfaceId} ${xacctHrly.interfaceIp}</td>
						<td>${xacctHrly.prodClass}</td>
						<td>${xacctHrly.protecLevel}</td>
						<td><fmt:formatNumber value="${xacctHrly.inUsage}" /></td>
						<td><fmt:formatNumber value="${xacctHrly.outUsage}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if>
	
	<c:if test="${errorMsg != null}">
		<div class="errormsg"> ${errorMsg}</div>
	</c:if>
