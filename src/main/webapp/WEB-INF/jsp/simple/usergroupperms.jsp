<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	final String CONTEXT_PATH = request.getContextPath();

%>		
			<label for="userGroupPermsSelect">Permissions to import: </label>
		    <select id="userGroupPermsSelect" size="15" style="width: 100%;" name="userGroupPermsSelect" disabled="disabled">
		         	 	<c:forEach items="${permissions}" var="p">
								<option value="${p.id}">${p.description}</option>
					 	</c:forEach>
			</select>