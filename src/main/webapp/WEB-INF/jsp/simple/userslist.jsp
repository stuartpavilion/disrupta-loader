<%-- 
    Document   : navigation
    Created on : 29/01/2010, 5:38:13 PM
    Author     : Stuart Sontag
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net"  %>
<%@page contentType="text/html" %>
	<c:if test="${allUsers != null}">
		<spring:url var="displayTableUrl" value="/admin/user/list.htm" />
		<spring:url var="editUrl" value="/admin/user/edit.htm" />
		<spring:url var="deleteUrl" value="/admin/user/delete.htm" />
		<display:table id="userTable" name="${allUsers}" class="userlisttbl" requestURI="${displayTableUrl}" pagesize="10" sort="list">
		  <display:column property="id" title="ID" sortable="true"  paramId="id" href="${editUrl}" class="hidden" headerClass="hidden"  />
		  <display:column property="username" title="UserName" sortable="true"  />
  		  <display:column property="firstName" title="First Name" sortable="true" />
		  <display:column property="lastName" title="Last Name" sortable="true" />
		  <display:column property="email" title="Email" sortable="true"   />
		  <display:column property="accountNonLocked" class="accountNonLockedClass" title="Non Locked" sortable="true"   />
		  <display:column property="network.name" title="Network" sortable="true"   />
		</display:table>
	</c:if>
	
	<c:if test="${errorMsg != null}">
		<div class="errormsg"> ${errorMsg}</div>
	</c:if>