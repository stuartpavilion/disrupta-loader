<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

 <spring:url var="retrieveUsageUrl" value="/user/usage/retrieveusage.htm" />
 <spring:url var="retrieveInterfaceUsageUrl" value="/user/usage/retrieveinterfaceusage.htm" />
 <spring:url var="retrieveFlotUsageUrl" value="/user/usage/flotretrieveusage.htm" />
 <spring:url var="retrieveFlotInterfaceUsageUrl" value="/user/usage/flotretrieveinterfaceusage.htm" /> 
 <spring:url var="getGMToffsetHoursUrl" value="/user/usage/getGMToffsetHours.htm" />
 
 <script type="text/javascript" src="/statusview/scripts/jquery.form.js"></script>
 <script type="text/javascript" src="/statusview/scripts/jquery-ui-timepicker-addon.js"></script>
 <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="/statusview/scripts/excanvas.min.js"></script><![endif]-->
  <script type="text/javascript" src="/statusview/scripts/jquery.flot.min.js"></script>
  <div id="usageTableBlock">
  	<h3></h3>
  	<div id="usagetable"></div>
  </div>
	  <div id="usageSearchFormBlock">
		  <form id="usageSearchForm">
		  		<input id="custid" type="hidden" name="custid" value="${custId}" />
		  		Date <input id ='viewstarttime' type="text" size='2'/>
		  		:<input id ='viewstartdate' type="text" size='9' />
		  		<select id='timezone' name='timezone'>
			  		<c:forEach var="i" begin="0" end="13" step="1" varStatus ="status">
						<option	value="${14-i}" >GMT+${14-i}</option>
					</c:forEach>
					<option	value="0" >GMT 0</option>
			  		<c:forEach var="i" begin="1" end="12" step="1" varStatus ="status">
						<option	value="${-i}" >GMT${-i}</option>
					</c:forEach>
		  		</select>
		  		Sample:<select id ='granularity' name='granularity'>
		  				<option value="Hourly">Hourly</option>
		  				<option value="Daily">Daily</option>
						<!--option value="Weekly">Weekly</option>
		  				<option value="Monthly">Monthly</option>
		  				<option value="Quarterly">Quarterly</option>
		  				<option value="Yearly">Yearly</option-->				  				
		  		</select>
		  		for:<input id = 'duration' name='duration' type='text' size='3'/>
		  		:<select id = 'durationscale' name='durationscale'>
		  				<option value="Hour">Hour(s)</option>
		  				<option value="Day">Day(s)</option>
						<option value="Week">Week(s)</option>
		  				<option value="Month">Month(s)</option>
		  				<option value="Quarter">Quarter(s)</option>
		  				<option value="Year">Year(s)</option>  		
		  		</select>
		  		<input id="startdate" type="hidden" name="startdate" />
		  		<input id="starttime" type="hidden" name="starttime" />
		  		<input id="region" type="hidden" name="region" value='GENERIC' />
		  		<!--input id="GraphUserButton" type="button" value='Graph Customer' />
		  		<input id='GraphInterfaceButton' type='button' value='Graph Interface' /-->
		  		<input id="GraphUserButtonFlot" type="button" value='Graph Customer' />
		  		<input id='GraphInterfaceButtonFlot' type='button' value='Graph Interface' />
		  </form>
	 
		  <div id="graphDiv2">
		  		<h3></h3>
		  		<div id="graphDiv2flot" style="width: 90%; height: 380px; padding: 0px; position: relative;"></div>
		  </div>
	   </div>
  	   <div id='console'></div>
  	<!-- 

    -->

<script type="text/javascript">	

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function drawGraph(url) {
		var startDateVal = $('#startdate').attr('value');
		var starttimeVal = $('#viewstarttime').attr('value');
		$('#starttime').attr('value', startDateVal + starttimeVal + '0000' );
		var queryString= $('#usageSearchForm').serialize() + '&' + serviceNavParms.interfacequeryparms;	
		//$('#console').html('<p>' + queryString + '</p>');
			
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			data: queryString,
			success: function(usageDto){
					var api2 = new jGCharts.Api();
					var opt2 = {
						type: 'lc',
						legend: ['Upload','Download'],
						size: '600x400',
						axis_step: 1
					}
					opt2.data = usageDto.inOutData;
					opt2.axis_labels = usageDto.strTimes;
					//alert("Returned from remote call :" + databack);
					
					if ( $('#graphimg2')[0] == null){
						jQuery('<img>').attr('src', api2.make(opt2)).attr('id','graphimg2').appendTo("#graphDiv2");
					} else {
						$('#graphimg2').attr('src', api2.make(opt2));
					}
				
				}
		});			
		
	};
	
	function setTimezone(startDateVal){
		var queryString = "&startdate=" + startDateVal;
		//$('#console').html('<p>' + queryString + '</p>');
		$.ajax({
			url: '${getGMToffsetHoursUrl}',
			type: "GET",
			dataType: "JSON",
			data: queryString,
			success: function(offsetGMThrs){
					$('#timezone').val(offsetGMThrs);
					$('#timezone').data("serverOffsetGMTHrs", offsetGMThrs);
				}
		});	
	}
	
	function drawFlotGraph(url) {
		var startDateVal = $('#startdate').attr('value');
		var starttimeVal = $('#viewstarttime').attr('value');
		$('#starttime').attr('value', startDateVal + starttimeVal + '0000' );
		var queryString= $('#usageSearchForm').serialize() + '&' + serviceNavParms.interfacequeryparms;	
		//$('#console').html('<p>' + queryString + '</p>');
			
		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			data: queryString,
			success: function(usageDto){
				$.plot($("#graphDiv2flot"), [{data: usageDto.inData, label: "In"}, {data: usageDto.outData, label: "Out"}],
						{ xaxis: { mode: "time"},
						  series: {
								lines: { show: true },
								points: { show: true }
						  },
						  grid: { hoverable: true, clickable: true }
						}
				);
				
				function showTooltip(x, y, contents) {
					$('<div id="tooltip">' + contents + '</div>').css( {
					position: 'absolute',
					display: 'none',
					top: y + 5,
					left: x + 5,
					border: '1px solid #fdd',
					padding: '2px',
					'background-color': '#fee',
					opacity: 0.80
					}).appendTo("body").fadeIn(200);
				}; 

					

					var previousPoint = null;
					var MILLISECONDS_IN_HR = 3600000;
					$("#graphDiv2flot").bind("plothover",
									function(event, pos, item) {
										$("#x").text(pos.x.toFixed(2));
										$("#y").text(pos.y.toFixed(2));
											if (item) {
												if (previousPoint != item.dataIndex) {
													previousPoint = item.dataIndex;
													$("#tooltip").remove();
													var x = item.datapoint[0];
													var y = item.datapoint[1].toFixed(2);
													var y = addCommas(item.datapoint[1]);
													var offset = MILLISECONDS_IN_HR * $('#timezone').data("serverOffsetGMTHrs");
													var dt = new Date(x-offset);
													showTooltip(item.pageX,	item.pageY,
															item.series.label + "  = "	+ y+ " : " + dt.toDateString() + ", " + pad(dt.getHours(),2) + ":" + pad(dt.getMinutes(),2) );
												}
											} else {
												$("#tooltip").remove();
												previousPoint = null;
											}
									});

					}
				
				});

	};
	
	function pad(number, length) {
	    var str = '' + number;
	    while (str.length < length) {
	        str = '0' + str;
	    }
	    return str;
	}
	
	$(function(){
		$('#viewstartdate').datepicker({
			dateFormat: "dd/mm/yy",
			altField: "#startdate",
			altFormat: "yymmdd",
			onSelect: function(dateText, inst){
				var dateParts = dateText.split("/");
				var dt = new Date(dateParts[2], dateParts[1]-1,dateParts[0]);
				var dayDt = pad(dt.getDate(),2);
				var monthDt = pad(dt.getMonth() + 1, 2);
				var yearDt = dt.getFullYear();
				//alert("DateText: " + dateText + " dt=" + yearDt + monthDt + dayDt );
				setTimezone(yearDt + monthDt + dayDt);
			}
		});		
		$('#viewstarttime').val("00");
		$('#viewstarttime').timepicker({showHour: true,
										showMinute: false,
										timeFormat: "hh"});
		
	});	
	
	$('#GraphUserButton').click(function() {
		drawGraph('${retrieveUsageUrl}');
		$('#graphDiv2 > h3').text("Usage for Customer id: ${custId}");
	});

	$('#GraphInterfaceButton').click(
			function() {
				drawGraph('${retrieveInterfaceUsageUrl}');
				$('#graphDiv2 > h3').text("Usage for Interface: "
										+ serviceNavParms.interfacetext);
			});

	$('#GraphUserButtonFlot').click(function() {
		drawFlotGraph('${retrieveFlotUsageUrl}');
		$('#graphDiv2 > h3').text("Usage for Customer id: ${custId}");
	});

	$('#GraphInterfaceButtonFlot').click(
			function() {
				drawFlotGraph('${retrieveFlotInterfaceUsageUrl}');
				$('#graphDiv2 > h3')
						.text(
								"Usage for Interface: "
										+ serviceNavParms.interfacetext);
			});
</script>