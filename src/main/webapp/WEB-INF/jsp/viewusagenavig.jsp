<%-- 
    Document   : navigation
    Created on : 29/01/2010, 5:38:13 PM
    Author     : Stuart Sontag
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" %>
<s:url var="viewUrl" value="/user/usage/viewcustomer.htm" />
<s:url var="retrieveUsageTblUrl" value="/user/usage/retrieveusagetbl.htm" />
<s:url var="retrieveInterfaceDetailsUrl" value="/user/usage/retrieveinterfaceslst.htm" />
<script type="text/javascript" src="/statusview/scripts/jquery.fixedheadertable.min.js"></script>
<div>
	<h2>Customer</h2>
	<form id="submitCustomerForm" action="${viewUrl}" >
	   <sec:authorize access="hasAnyRole('ROLE_ADMINISTRATOR')">
		  Customer Id: <input type="text" name="custid" value="${custId}" size="4"/>
		  <input type="submit" value="GO ->" />
		</sec:authorize>
		<sec:authorize access="hasAnyRole('ROLE_CRM')">
		   <c:if test="${currentUser != null && !empty(currentUser.custIds)}" >
   			<select id="custIdSelect" style="width: 60px;" name="custid"/>
         	 	<c:forEach items="${currentUser.custIds}" var="custId">
						<option value="${custId}">${custId}</option>
			 	</c:forEach>
		 	</select>
		 	<input type="submit" value="GO ->" />
			</c:if>
		</sec:authorize>
	</form>
</div>
<div id="timeNavBlock">
	<h2>Time Navigator</h2>
	<c:if test="${custId != null}" >
	  <form id="timeNavSearchForm" >
	  		<input id="custid" type="hidden" name="custid" value="${custId}" />
	  		Start date<input id ='tn_viewstartdate' type="text" size='9' />
	  		<select id ='tn_granularity' name='granularity'>
	  				<option value="Hourly">Hourly</option>
	  				<option value="Daily">Daily</option>
	  				<!-- 
					<option value="Weekly">Weekly</option>
	  				<option value="Monthly">Monthly</option>
	  				<option value="Quarterly">Quarterly</option>
	  				<option value="Yearly">Yearly</option>
	  				 -->				  				
	  		</select>
	  		<input id="tn_startdate" type="hidden" name="startdate" />
	  		<input id="tn_region" type="hidden" name="region" value='GENERIC' />
	  		<input id="tn_go" type='button' value='Go' />
	  	</form>
	  </c:if>	
</div>
<div>
	<h2>Service Navigator</h2>
	<c:if test="${custAddrListsByVpnMap != null}">
	  <div id="servicetree">
		<ul>
			<c:forEach items="${custAddrListsByVpnMap}" var="entry">
				<li><a href="">${entry.key}</a>
					<ul><c:forEach items="${entry.value}" var="custAddr">
						<li><a href="">${custAddr}</a></li>
						</c:forEach>
					</ul>
				</li>
			</c:forEach>
		</ul>
	 </div>
	</c:if>
</div>
<div>
	<h2>Interfaces</h2>
	<div id="interfaces">
	
	
	</div>
</div>
<c:if test="${custId != null}" >
	<script type="text/javascript">	
	$(function(){
		$('#custIdSelect').val("${custId}");
	})
	$(function(){
		$('#tn_viewstartdate').datepicker({ dateFormat: "dd/mm/yy"});
		$('#tn_viewstartdate').datepicker("option", "altField", "#tn_startdate");
		$('#tn_viewstartdate').datepicker("option", "altFormat", "yymmdd");
		
		$('#servicetree').jstree({
			"themes" : {
				"theme": "classic"
			},
			"plugins" : [ "themes", "html_data" ]
		});
		
	});
	
	var serviceNavParms = {
		custIdSelected: "",	
		granularitySelected: "",
		startDateSelected: "",
		startDateDisplayed: "",
		custAddrSelected: "",
		custVpnSelected: "",
		interfacequeryparms: "",
		interfacetext: ""
	};
	
	function showServiceNamParamsTbl(snp){
		var queryS = "custid=" + snp.custIdSelected + "&custvpn=" + snp.custVpnSelected + "&custaddr=" + snp.custAddrSelected + "&startdate=" + snp.startDateSelected + "&granularity=" + snp.granularitySelected;
		//alert(queryS);
		$("#usageTableBlock > h3").text("Report - " + snp.custAddrSelected + " : " + snp.startDateDisplayed );
		$('#usagetable').load("${retrieveUsageTblUrl}?"+queryS,
				function(){
							$('#usagetbl').fixedHeaderTable({ 
								footer: true,
								//cloneHeadToFoot: true,
								altClass: 'odd',
								autoShow: true,
								themeClass: 'usagetbldisp' 
				})
		});
	}
	
	var gatherParms = function (){
		serviceNavParms.custIdSelected = $('#custid').val();
		serviceNavParms.startDateSelected = $('#tn_startdate').val();
		serviceNavParms.startDateDisplayed = $('#tn_viewstartdate').val();
		showServiceNamParamsTbl(serviceNavParms);
		return false;		
	}
	
	function populateIntefaceDetailsList(snp){
		var queryStr = "custid=" + snp.custIdSelected + "&custaddr=" + snp.custAddrSelected;
		//alert("About to load interfaceDetails: " + queryStr)
		$('#interfaces').load("${retrieveInterfaceDetailsUrl}?"+queryStr,
				function(){
					
					$("ul > li > a",'div#interfaces').click(function(){
						serviceNavParms.interfacequeryparms = $.trim($(this).attr('href'));
						serviceNavParms.interfacetext = $.trim($(this).text());
						//unhighlight all entries
						$("ul > li > a",'div#interfaces').removeClass('highlight');
						//highlight selected entry
						$(this).addClass('highlight');
						return false;
					});
					
					$('#interfaces').jstree({
						"themes" : {
							"theme": "classic"
						},
						"plugins" : [ "themes", "html_data" ]
					});
					
					//set the first interface to be selected by default
					
					$("ul > li > a",'div#interfaces:first').trigger('click');
			
				}
		);
		
	}
	
	
	$('#tn_granularity').change(function(){
		serviceNavParms.granularitySelected = $('#timeNavSearchForm > select option:selected').text();
	}).trigger('change');
	
	$('#tn_go').click(gatherParms);
	
	$("ul > li ul > li > a",'div#servicetree').click(function(){
		serviceNavParms.custAddrSelected = $.trim($(this).text());
		serviceNavParms.custVpnSelected = $.trim($(this).closest('ul').siblings('a').text());
		//un highlight all <a> entries
		$("ul > li ul > li > a",'div#servicetree').removeClass('highlight');
		//highlight selected entry
		$(this).addClass('highlight');
		gatherParms();
		populateIntefaceDetailsList(serviceNavParms);
		return false;
	});
	

	
	</script>
</c:if>