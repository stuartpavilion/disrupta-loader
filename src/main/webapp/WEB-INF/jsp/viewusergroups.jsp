<%-- 
    Document   : logon
    Created on : 25/01/2010, 6:22:33 PM
    Author     : Stuart Sontag
--%>
<%@ page contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


	
    
    <spring:url var="displayTableUrl" value="/admin/group/list.htm" />
    <spring:url var="searchUrl" value="/admin/group/search.htm" />
	<spring:url var="editUrl" value="/admin/group/edit.htm" />
	<spring:url var="deleteUrl" value="/admin/group/delete.htm" />    

	<div id="searchbox">
		<form id="searchform" action="${searchUrl}">
			<label for="include">Include</label><input id="include" type="text" name="include" value="${userGroupIncludeStr}" />
			<label for="exclude">Exclude</label><input id="exclude" type="text" name="exclude" value="${userGroupExcludeStr}" />
			<label for="searchsubmit">(* is the wildcard)</label>
			<input id="searchsubmit" type="submit" name="searchsubmit" value="Search" />
		</form> 
	</div>
	<div id="userGroupTableContainer"></div>


    <script type="text/javascript">	
	 function parentOpen(url){
	   	window.location.href=url;
	 }
     function onUserGroupTableLoad() {
    	// Gets called when the data loads
    	      $("table#userGroupTable th.sortable").each(function() {
    	        // Iterate over each column header containing the sortable class, so
    	        // we can setup overriding click handlers to load via ajax, rather than
    	        // allowing the browser to follow a normal link
    	        $(this).click(function() {
    	          // "this" is scoped as the sortable th element
    	          var link = $(this).find("a").attr("href");
    	          $("div#userGroupTableContainer").load(link, {}, onUserGroupTableLoad);
    	          // Stop event propagation, i.e. tell browser not to follow the clicked link
    	          return false;
    	        });
    	      });
    		 $("div#userGroupTableContainer .pagelinks a").each(function() {
    	        // Iterate over the pagination-generated links to override also
    	        $(this).click(function() {
    	          var link = $(this).attr("href");
    	          $("div#userGroupTableContainer").load(link, {}, onUserGroupTableLoad);
    	          return false;
    	        });
    	     });
    		 
    		 $("td.hidden > a","#userGroupTable").each(function() {
     	        // Add edit and delete links to each row
    	         var txt = $(this).text();
     	         var ugname = ($(this).closest('tr')).find("td:nth-child(2)").text();
     	         var tdEditElem = $("<td>").append($("<a>Edit</a>").attr("href","${editUrl}?id=" + txt));
     	         var tdDeleteElem = $("<td>").append($("<a>Delete</a>").attr("href","${deleteUrl}?id=" + txt));
     	         var parentWindow = window;
     	         tdDeleteElem.click(function(){
     	        	//var ans = confirm("Are you sure that you want delete User Group: " + txt);
     	        	//return (ans);
     	        	$('<div>').dialog({
     	        		open: function(){$(this).text("Are you sure that you want delete User Group: " + ugname +" ?");},
     	        		modal: true,
     	        		title: 'Confirm delete User',
     	        		buttons: {
     	        			Yes: function(){ 
     	        				   //parentWindow.open("${deleteUrl}?id=" + txt);
     	        				   parentOpen("${deleteUrl}?id=" + txt);
     	        					$(this).dialog('close');
     	        				 },
     	        			No:  function(){
     	        					$(this).dialog('close');	        				
     	        				}
     	        		}
     	        	})
     	        	return false;
     	         });
     	         //this points to an existing anchor element
     	        $(this).closest('tr').append(tdEditElem).append(tdDeleteElem);
     	     });    		 
 	    }
    	  
    	  
    	  
    	$(function() {
    	    // Load the initial rendering when the dom is ready.  Injecting into div
    	    // with id "userGroupTableContainer".
    	    $("div#userGroupTableContainer").load("${displayTableUrl}", {}, onUserGroupTableLoad);
    	  });
    

    </script>

