<%-- 
    Document   : logon
    Created on : 25/01/2010, 6:22:33 PM
    Author     : Stuart Sontag
--%>
<%@ page contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


	
    
    <spring:url var="displayTableUrl" value="/admin/user/list.htm" />
    <c:choose>
	 	 <c:when test="${userSelectMode=='addStreamModerator'}">
 		 	<spring:url var="searchUrl" value="/admin/user/searchstreammod" />
	 	 </c:when>
 		 <c:otherwise>
 		 	<spring:url var="searchUrl" value="/admin/user/search.htm" />
 		 </c:otherwise>
    </c:choose>    
	<spring:url var="editUrl" value="/admin/user/edit.htm" />
	<spring:url var="deleteUrl" value="/admin/user/delete.htm" />    
	<spring:url var="lockUnLockUrl" value="/admin/user/lockUnLockUrl.htm" />
	
	<spring:eval var="enableUserDeletion" expression="@propBean['useraccount_enable_deletion']" /> 
	
	<div id="searchbox">
		<form id="searchform" action="${searchUrl}">
			<label for="include">Include</label><input id="include" type="text" name="include" value="${userIncludeStr}" />
			<label for="exclude">Exclude</label><input id="exclude" type="text" name="exclude" value="${userExcludeStr}" />
			<label for="searchsubmit">(* is the wildcard)</label>
			<input id="searchsubmit" type="submit" name="searchsubmit" value="Search" />
		</form> 
	</div>
	<div id="userTableContainer"></div>

    <script type="text/javascript">
     function parentOpen(url){
    	window.location.href=url;
     }
     function onUserTableLoad() {
    	// Gets called when the data loads
    	      $("table#userTable th.sortable").each(function() {
    	        // Iterate over each column header containing the sortable class, so
    	        // we can setup overriding click handlers to load via ajax, rather than
    	        // allowing the browser to follow a normal link
    	        $(this).click(function() {
    	          // "this" is scoped as the sortable th element
    	          var link = $(this).find("a").attr("href");
    	          $("div#userTableContainer").load(link, {}, onUserTableLoad);
    	          // Stop event propagation, i.e. tell browser not to follow the clicked link
    	          return false;
    	        });
    	      });
    		 $("div#userTableContainer .pagelinks a").each(function() {
    	        // Iterate over the pagination-generated links to override also
    	        $(this).click(function() {
    	          var link = $(this).attr("href");
    	          $("div#userTableContainer").load(link, {}, onUserTableLoad);
    	          return false;
    	        });
    	     });
    		 <c:choose>
    		 	 <c:when test="${userSelectMode=='addStreamModerator'}">
	    		 	$("td.hidden > a","#userTable").each(function() {
	    		 		var txt = $(this).text();
	    		 		var tdModeratorAssignElem = $("<td>").append($("<a>Assign As Moderator</a>").attr("href","${moderatorAssignUrl}&userid=" + txt));
	    		 		$(this).closest('tr').append(tdModeratorAssignElem);
	    		 	});
    		 	 </c:when>
	    		 <c:otherwise>
	    		 $("td.hidden > a","#userTable").each(function() {
	     	        // Add edit and delete links to each row
	    	         var txt = $(this).text();
	     	         var uname = ($(this).closest('tr')).find("td:nth-child(2)").text();
	     	         var nonLocked = ($(this).closest('tr')).find("td:nth-child(6)").text();
	     	         var lockUnlockMsg;
	     	         if (nonLocked == "true"){
	     	        	lockUnlockMsg = "Lock";
	     	         } else {
	     	        	lockUnlockMsg = "Unlock"; 
	     	         }
	     	         var parentWindow = window;
	     	        var tdEditElem = $("<td>").append($("<a>Edit</a>").attr("href","${editUrl}?id=" + txt));
	     	        
	     	         var tdLockUnLockElem = $("<td>").append($("<a>"+lockUnlockMsg+"</a>").attr("href","${lockUnLockUrl}?id=" + txt + "&action="+ lockUnlockMsg));
	      	         tdLockUnLockElem.click(function(){
	      	        	//var ans = confirm("Are you sure that you want delete User: " + uname);
	      	        	//return (ans);
	      	        	$('<div>').dialog({
	      	        		open: function(){$(this).text("Are you sure that you want " + lockUnlockMsg + " User: " + uname +" ?");},
	      	        		modal: true,
	      	        		title: 'Confirm ' +lockUnlockMsg +' User',
	      	        		buttons: {
	      	        			Yes: function(){ 
	      	        				   //parentWindow.open("${deleteUrl}?id=" + txt);
	      	        				   parentOpen("${lockUnLockUrl}?id=" + txt  + "&action="+ lockUnlockMsg);
	      	        					$(this).dialog('close');
	      	        				 },
	      	        			No:  function(){
	      	        					$(this).dialog('close');	        				
	      	        				}
	      	        		}
	      	        	})
	      	        	return false;
	      	          });
	     	        <c:choose> 
		     	        <c:when test="${enableUserDeletion}">
			     	         var tdDeleteElem = $("<td>").append($("<a>Delete</a>").attr("href","${deleteUrl}?id=" + txt));
			     	         
			     	         tdDeleteElem.click(function(){
			     	        	//var ans = confirm("Are you sure that you want delete User: " + uname);
			     	        	//return (ans);
			     	        	$('<div>').dialog({
			     	        		open: function(){$(this).text("Are you sure that you want delete User: " + uname +" ?");},
			     	        		modal: true,
			     	        		title: 'Confirm delete User',
			     	        		buttons: {
			     	        			Yes: function(){ 
			     	        				   //parentWindow.open("${deleteUrl}?id=" + txt);
			     	        				   parentOpen("${deleteUrl}?id=" + txt);
			     	        					$(this).dialog('close');
			     	        				 },
			     	        			No:  function(){
			     	        					$(this).dialog('close');	        				
			     	        				}
			     	        		}
			     	        	})
			     	        	return false;
			     	         });
			     	         
			     	         //this points to an existing anchor element
			     	        $(this).closest('tr').append(tdEditElem).append(tdDeleteElem).append(tdLockUnLockElem);
		     	         </c:when>
		     	         <c:otherwise>
		     	         	//this points to an existing anchor element
			     	        $(this).closest('tr').append(tdEditElem).append(tdLockUnLockElem);    	         
		     	         </c:otherwise>
	     	         </c:choose>
	     	     });
	    		 </c:otherwise>
    		 </c:choose>
    		 
 	    }
    	  
    	  
    	  
    	$(function() {
    	    // Load the initial rendering when the dom is ready.  Injecting into div
    	    // with id "userTableContainer".
    	    $("div#userTableContainer").load("${displayTableUrl}", {}, onUserTableLoad);
    	  });
    

    </script>

