		    function Spinner3D ( el, panelCount ) {
		        this.element = el;
	
		        this.rotation = 0;
		        this.panelCount = panelCount;
		        this.totalPanelCount = this.element.children.length;
		        this.theta = 0;
	
		        this.isHorizontal = true;
				this.modify();
		      }
		    Spinner3D.prototype.advance = function() {
		    	this.rotation -= this.theta;
		    	$("#console").append("<p>Advancing: rotation="+this.rotation+"</p>")
		    	this.modify();
		    }
		    Spinner3D.prototype.retreat = function() {
		    	this.rotation += this.theta;
		    	this.modify();
		    }		    
		    Spinner3D.prototype.modify = function() {

		        var panel, angle, i;

		        this.panelSize = this.element[ this.isHorizontal ? 'offsetWidth' : 'offsetHeight' ];
		        this.rotateFn = this.isHorizontal ? 'rotateY' : 'rotateX';
		        this.theta = 360 / this.panelCount;

		        // do some trig to figure out how big the spinner
		        // is in 3D space
		        this.radius = Math.round( ( this.panelSize / 2) / Math.tan( Math.PI / this.panelCount ) );

		        $("#console").append("<p>modify: panelcount="+this.panelCount+", totalPanelCount="+this.totalPanelCount+", radius="+this.radius+"</p>")

		        for ( i = 0; i < this.panelCount; i++ ) {
		          panel = this.element.children[i];
		          angle = this.theta * i;
		          panel.style.opacity = 100;
		          //panel.style.backgroundColor = 'hsla(' + angle + ', 100%, 50%, 0.8)';
		          
		          // rotate panel, then push it out in 3D space
		          //panel.style[ '-moz-transform' ] = this.rotateFn + '(' + angle + 'deg) translateZ(' + this.radius + 'px)';
		          //panel.style[ '-ms-transform' ] = this.rotateFn + '(' + angle + 'deg) translateZ(' + this.radius + 'px)';
		          //panel.style[ '-o-transform' ] = this.rotateFn + '(' + angle + 'deg) translateZ(' + this.radius + 'px)';
		          
		          //var xform = this.rotateFn + '(' + angle + 'deg) translateZ(' + this.radius + 'px)';
		          //var xform = 'translateZ(' + this.radius + 'px)' + this.rotateFn + '(' + angle + 'deg)'
		          var xform = this.rotateFn + '(' + angle + 'deg) translate3d(0px,0px,' + this.radius + 'px)';
		          $("#console").append("<p>modify: i="+i+", panel="+panel.tagName+", angle="+angle+", xform="+xform+"</p>")
		           
		           // For Mozilla browser: e.g. Firefox
		           $("#"+panel.id).css({ '-moz-transform': xform});
		           // IE 9 
		           $("#"+panel.id).css({ '-ms-transform': xform});	
		           // Opera 10.50-12.00 
		           $("#"+panel.id).css({ '-o-transform': xform});
		           
		           $("#"+panel.id).css({ '-webkit-transform': xform});
		           
		           $("#"+panel.id).css({ 'transform': xform});
		           /**
		           panel.style[ '-moz-transform' ] = xform;
		           panel.style[ '-ms-transform' ] = xform;
		           panel.style[ '-o-transform' ] = xform;
		           **/
		        }

		        // hide other panels
		        for (  ; i < this.totalPanelCount; i++ ) {
		          panel = this.element.children[i];
		          panel.style.opacity = 0;
		          panel.style[ transformProp ] = 'none';
		        }

		        // adjust rotation so panels are always flat
		        this.rotation = Math.round( this.rotation / this.theta ) * this.theta;

		        this.transform();

		      };

		      Spinner3D.prototype.transform = function() {
		        // push the spinner back in 3D space,
		        // and rotate it
		        $("#console").append("<p>tranform: rotation="+this.rotation+", radius="+this.radius+"</p>")
		         $("#console").append("<p>element: ="+this.element.id+"</p>");
		        var xform = 'translateZ(-' + this.radius + 'px) ' + this.rotateFn + '(' + this.rotation + 'deg)';
		        //this.element.style[ '-moz-transform' ] = 'translateZ(-' + this.radius + 'px) ' + this.rotateFn + '(' + this.rotation + 'deg)';
		        //this.element.style[ '-ms-transform' ] = 'translateZ(-' + this.radius + 'px) ' + this.rotateFn + '(' + this.rotation + 'deg)';
		        //this.element.style[ '-o-transform' ] = 'translateZ(-' + this.radius + 'px) ' + this.rotateFn + '(' + this.rotation + 'deg)';
		        
		           // For Mozilla browser: e.g. Firefox
		           $("#"+this.element.id).css({ '-moz-transform': xform});
		           // IE 9 
		           $("#"+this.element.id).css({ '-ms-transform': xform});	
		           // Opera 10.50-12.00 
		           $("#"+this.element.id).css({ '-o-transform': xform});
		           
		           $("#"+this.element.id).css({ '-webkit-transform': xform});
		           $("#"+this.element.id).css({ 'transform': xform + ' perspectiv(500px)'});
		           
		           $("#"+this.element.id).css({
			           '-webkit-transition': '-webkit-transform 1s',
			           '-moz-transition': '-moz-transform 1s',
			           '-o-transition': '-o-transform 1s',
			           'transition': 'transform 1s',
			           });
		        
		      };
		      
		      Spinner3D.prototype.rotate = function(alpha) {
			        // push the spinner back in 3D space,
			        // and rotate it
			        $("#console").append("<p>tranform: rotation alpha="+alpha+", radius="+this.radius+", rotateFn="+this.rotateFn +"</p>");
			         $("#console").append("<p>element: ="+this.element.id+"</p>");
			        var xform = 'translateZ(-' + this.radius + 'px) ' + this.rotateFn + '(' + alpha + 'deg)';
			         //var xform = this.rotateFn + '(' + alpha + 'deg)';
			         $("#console").append("<p>xform: ="+xform+"</p>");
			         /**
			        this.element.style[ '-moz-transform' ] = xform;
			        this.element.style[ '-ms-transform' ] = xform;
			        this.element.style[ '-o-transform' ] = xform;
			        **/
			        
			           
			           // For Mozilla browser: e.g. Firefox
			           $("#"+this.element.id).css({ '-moz-transform': xform});
			           // IE 9 
			           $("#"+this.element.id).css({ '-ms-transform': xform});	
			           // Opera 10.50-12.00 
			           $("#"+this.element.id).css({ '-o-transform': xform});
			        
			      };	